/**
 * Created by Nesto on 10/11/2014.
 */
var tool = require('./toolController');


exports.configRoutes = function (app) {
    /*API REST*/
    app.get('/load/getDataInfo',
        function (req, res, next) {
        res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        res.setHeader("Pragma", "no-cache");
        res.setHeader("Expires", 0);
        next();

        }, tool.getDataInfo);
    app.post('/load/loadData', tool.loadData);
    app.post('/load/loadDataByCollection', tool.loadCollection);
    app.post('/load/removeDataByCollection', tool.removeCollection);
    app.post('/load/removeSession', tool.removeSession);

    /*TEMPLATES*/
    app.get('/loadData', function (req, res) {
        res.render(('load/index'));
    });
};


