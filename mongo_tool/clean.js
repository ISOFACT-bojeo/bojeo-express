/**
 * Created by Home on 05/06/2015.
 */
var async = require('async');

require('./../config/bojeoConf');
require('./../config/mongoose');

db.Media.find(function (err, medias) {
    if (err || !medias) {
        console.log('ERROR 1');
    }
    else {
        var cont = medias.length;
        console.log('CANTIDAD: ' + medias.length);
        async.map(medias, function (media, callback) {
                async.series([
                    function (cb) {
                        db.Enterprise.findOne({'photo.name': media._doc.fieldName}, function (err, success) {
                            cb(err, success);
                        });
                    },
                    function (cb) {
                        db.Port.findOne({'photos.name': media._doc.fieldName}, function (err, success) {
                            cb(err, success);
                        });
                    },
                    function (cb) {
                        db.Ship.findOne({'photos.name': media._doc.fieldName}, function (err, success) {
                            cb(err, success);
                        });
                    },
                    function (cb) {
                        db.Service.findOne({'photos.name': media._doc.fieldName}, function (err, success) {
                            cb(err, success);
                        });
                    },
                    function (cb) {
                        db.Group.findOne({'photos.name': media._doc.fieldName}, function (err, success) {
                            cb(err, success);
                        });
                    },
                    function (cb) {
                        db.Tour.findOne({'photos.name': media._doc.fieldName}, function (err, success) {
                            cb(err, success);
                        });
                    },
                    function (cb) {
                        db.News.findOne({'photo.name': media._doc.fieldName}, function (err, success) {
                            cb(err, success);
                        });
                    }
                ], function (err, results) {
                    var flag = false;
                    for (var i = 0; i < results.length; i++) {
                        if (results[i]) {
                            flag = true;
                            break;
                        }
                    }
                    if (!flag) {
                        db.Media.remove({_id: media._doc._id}).exec(function (err, sucess) {
                            cont--;
                            callback(err, true);
                        });
                    }
                    else {
                        callback(null, false);
                    }
                })
            }, function (err, result) {
                console.log('CANTIDAD: ' + cont);
            }
        )
    }
});