/**
 * Created by ernestomr87@gmail.com on 06/06/2014.
 */

var fs = require('fs');
var nodemailer = require("nodemailer");
var paypal_api = require('paypal-rest-sdk');
var util = require('util');
var urlUtil = require('url');

exports.mongoDbConection = function (req, res, next) {
    if (conection != true) {
        return res.json(conection);
    } else {
        return next;
    }
};
exports.paypalRefund = function (monto, saleId, cb) {
    paypal_api.configure(paypalConfig.api);
    var tempPrice = Number(monto).toFixed(2);
    var data = {
        "amount": {
            "currency": "EUR",
            "total": tempPrice
        }
    };


    paypal_api.sale.refund(saleId, data, function (error, refund) {
        if (error == null && refund != null) {
            if (cb) cb(true);
            //return res.json({res: true})
        } else {
            if (cb) cb(false);
            //return res.json({res: false})
        }

    });
};
exports.payWithPaypal = function (req, res, payment) {
    paypal_api.configure(paypalConfig.api);
    paypal_api.payment.create(payment, function (error, payment) {
        if (error) {
            console.log(error);
            console.log(process.env.boj_paypal_client_id);
            console.log(process.env.boj_client_secret);
            var url = urlUtil.resolve(global.bojeoServer, req.user.language+"/error-conection");
            res.redirect(url);
        } else {
            if (payment.payer.payment_method === 'paypal') {

                if(!req.session.sale) req.session.sale = {};
                req.session.sale.paymentId = payment.id;

                var redirectUrl;
                for (var i = 0; i < payment.links.length; i++) {
                    var link = payment.links[i];
                    if (link.method === 'REDIRECT') {
                        redirectUrl = link.href;
                    }
                }
                res.redirect(redirectUrl);
            }
        }
    });

};
exports.executepayWithPaypal = function (req, cb) {
    paypal_api.payment.execute(req.session.sale.paymentId, {"payer_id": req.query.PayerID}, function (error, payment) {
        var res = null;
        if (error) {
            throw error;
            res = false;
        } else {
            res = payment.transactions[0].related_resources[0].sale.id;
        }

        if (cb) cb(res);
    });
};
exports.prepareMail = function (res, type, mailclients) {

    var clients = "";
    var tour = null;
    var token = null;

    if (type == 0 || type == 1) {
        for (var i = 0; i < mailclients.length; i++) {
            clients = clients + mailclients[i] + ',';
        }
    }
    else if (type == 2) {
        clients = mailclients.id;
        tour = mailclients.tour;
    }
    else if (type == 4) {
        clients = mailclients.email;
        token = mailclients.token;
    }
    else if (type == 5) {
        clients = mailclients.email;
        tour = mailclients.tour;
    }
    else {
        clients = mailclients;
    }


    db.Email.findOne(function (err, success) {
        if (err == null && success != null) {
            var mailServer = success._doc.mailServer,
                mailSMTPPort = success._doc.mailSMTPPort,
                mailUser = success._doc.mailUser,
                mailPass = success._doc.mailPass;

            var smtpTransport = nodemailer.createTransport("SMTP", {
                host: mailServer, // hostname
                secureConnection: true, // use SSL
                port: mailSMTPPort, // port for secure SMTP
                connectionTimeout : 300000,
                auth: {
                    user: mailUser,
                    pass: mailPass
                }
            })

            if (type == 0) {
                if (success._doc.promo.active) {
                    var mailOptions = {
                        from: "Bojeo <" + mailUser + ">", // sender address
                        to: clients, // list of receivers
                        subject: success._doc.promo.subject.es, // Subject line
                        html: success._doc.promo.body.es
                    }
                    sendMail(res, smtpTransport, mailOptions);
                }
                else {
                    return res.json({res: false})
                }
            }
            else if (type == 1) {
                if (success._doc.bulletin.active) {
                    var tmplte=util.format("<p>Email promocional %s</p>",'asd');
                    var mailOptions = {
                        from: "Bojeo <" + mailUser + ">", // sender address
                        to: clients, // list of receivers
                        subject: success._doc.bulletin.subject.es, // Subject line
                        html: tmplte
                    }
                    sendMail(res, smtpTransport, mailOptions);
                }
                else {
                    return res.json({res: false})
                }
            }
            else if (type == 2) {
                if (success._doc.notificationReserved.active) {
                    var mailOptions = {
                        from: "Bojeo <" + mailUser + ">", // sender address
                        to: clients, // list of receivers
                        subject: success._doc.notificationReserved.subject.es, // Subject line
                        html: templateMailRecive(clients, tour.name, "", tour.date, tour.ship, tour.departure + 'h', tour.cantPerson, 1, tour.total, tour.token)
                    }
                    sendMail(res, smtpTransport, mailOptions);
                }
                else {
                    return res.json({res: false})
                }
            }
            else if (type == 3) {
                if (success._doc.notificationRegister.active) {
                    var mailOptions = {
                        from: "Bojeo <" + mailUser + ">", // sender address
                        to: clients, // list of receivers
                        subject: success._doc.notificationRegister.subject.es, // Subject line
                        html: success._doc.notificationRegister.body.es
                    }
                    sendMail(res, smtpTransport, mailOptions);
                }
                else {
                    return res.json({res: true})
                }
            }
            else if (type == 4) {
                if (success._doc.notificationChangePass.active) {
                    var mailOptions = {
                        from: "Bojeo <" + mailUser + ">", // sender address
                        to: clients, // list of receivers
                        subject: success._doc.notificationChangePass.subject.es, // Subject line
                        html: templateForgivePass(clients, token)
                    }
                    sendMail(res, smtpTransport, mailOptions);
                }
                else {
                    return res.json({res: true})
                }
            }
            else if (type == 5) {
                if (success._doc.devolution.active) {
                    var mailOptions = {
                        from: "Bojeo <" + mailUser + ">", // sender address
                        to: clients, // list of receivers
                        subject: "Devolución de Reserva", // Subject line
                        html: templateMailRefund(clients, tour.name, '', tour.date, tour.ship, tour.departure + 'h', tour.cantPerson, tour.total, tour.reason)
                    }
                    sendMail(res, smtpTransport, mailOptions);
                }
                else {
                    return res.json({res: true})
                }
            }
            else {
                return res.json({res: false})
            }
        }
        else {
            return res.json({res: false})
        }
    })
};

function templateMailRecive(email, tourName, portAddress, date, shipName, departure, cantPerson, days, total, token) {
    var aux = new Date(date);
    var newDate = aux.getDate() + "/" + aux.getMonth() + "/" + aux.getFullYear();
    newDate = String(newDate);

    return "<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n" +
        "xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns:m=\"http://schemas.microsoft.com/office/2004/12/omml\"\n" +
        "xmlns=\"http://www.w3.org/TR/REC-html40\">\n" +
        "<head>\n" +
        "<meta http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">\n" +
        "<meta name=Generator content=\"Microsoft Word 15 (filtered medium)\" \n>" +
        "<!--[if !mso]><style>v\: * {behavior: url(#default#VML);}o\: * {behavior: url(#default#VML);}w\: * {behavior: url(#default#VML);}.shape {behavior: url(#default#VML);}</style><![endif]-->\n" +
        "<style>@font-face {font-family: \"Cambria Math\";panose-1: 0 0 0 0 0 0 0 0 0 0;}\n" +
        "@font-face {font-family: Calibri;panose-1: 2 15 5 2 2 2 4 3 2 4;}@font-face {font-family: \"Segoe UI Semilight\";panose-1: 2 11 4 2 4 2 4 2 2 3;}\n" +
        "p.MsoNormal, li.MsoNormal, div.MsoNormal {margin: 0cm;margin-bottom: .0001pt;font-size: 11.0pt;font-family: \"Calibri\", \"sans-serif\";mso-fareast-language: EN-US;}\n" +
        "a:link, span.MsoHyperlink {mso-style-priority: 99;color: #0563C1;text-decoration: underline;}a:visited, span.MsoHyperlinkFollowed {mso-style-priority: 99;color: #954F72;text-decoration: underline;}\n" +
        "span.EstiloCorreo17 {mso-style-type: personal-compose;font-family: \"Calibri\", \"sans-serif\";color: windowtext;}\n" +
        ".MsoChpDefault {mso-style-type: export-only;mso-fareast-language: EN-US;}@page WordSection1 {size: 612.0pt 792.0pt;margin: 70.85pt 3.0cm 70.85pt 3.0cm;}div.WordSection1 {page: WordSection1;}\n" +
        "--></style>\n" +

        "<!--[if gte mso 9]><xml><o:shapedefaults v:ext=\"edit\" spidmax=\"1026\"/></xml><![endif]-->\n" +
        "<!--[if gte mso 9]><xml><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></xml><![endif]-->\n" +
        "</head>\n" +

        "<body lang=ES link=\"#0563C1\" vlink=\"#954F72\">\n" +
        "<div class=WordSection1>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Hola," + email + "<o:p></o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Su reservación se realizó satisfactoriamente. Disfrute su Bojeo.<o:p></o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Descripción<o:p></o:p></span></p>\n" +

        "<div align=center><table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width=688 style='width:516.15pt;border-collapse:collapse;border:none'>\n" +
        "<tr style='height:24.4pt'><td width=688 colspan=6 valign=top style='width:516.15pt;border:none;border-bottom:solid #A6A6A6 1.0pt;padding:0cm 3.5pt 0cm 3.5pt;height:24.4pt'>\n" +
        "<p class=MsoNormal style='margin-bottom:8.0pt;text-align:justify;line-height:107%'>\n" +
        "<span style='font-size:18.0pt;line-height:107%;font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + tourName + "</span>\n" +
        "<span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + portAddress + "</span>\n" +
        "<span style='font-size:18.0pt;line-height:107%;font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p></o:p></span></p></td></tr>\n" +
        "<tr style='height:25.6pt;border-top: 1 solid #A6A6A6;'>\n" +
        "<td width=109 valign=top style='width:81.65pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.6pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Fecha<o:p></o:p></span></p></td>\n" +
        "<td width=133 valign=top style='width:99.5pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.6pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Embarcación<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.7pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.6pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Salida<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.7pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.6pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Plazas<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.7pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.6pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Días<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.8pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.6pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Precio<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.8pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.6pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Token<o:p></o:p></span></p></td></tr>\n" +
        "<tr style='height:21.9pt'><td width=109 valign=top style='width:81.65pt;border:none;border-bottom:solid #A6A6A6 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:21.9pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + newDate + "<o:p></o:p></span></p></td>\n" +
        "<td width=133 valign=top style='width:99.5pt;border:none;border-bottom:solid #A6A6A6 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:21.9pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + shipName + "<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.7pt;border:none;border-bottom:solid #A6A6A6 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:21.9pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + departure + "<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.7pt;border:none;border-bottom:solid #A6A6A6 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:21.9pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + cantPerson + "<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.7pt;border:none;border-bottom:solid #A6A6A6 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:21.9pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + days + "<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.8pt;border:none;border-bottom:solid #A6A6A6 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:21.9pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + total + "<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.8pt;border:none;border-bottom:solid #A6A6A6 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:21.9pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + token + "<o:p></o:p></span></p></td></tr></table>\n" +
        "</div>\n" +

        "<p class=MsoNormal style='margin-bottom:8.0pt;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>¡Ven a &#8220;Bojear&#8221; y a disfrutar de la ciudad desde el mar!<o:p></o:p></span></p>\n" +
        "<p class=MsoNormal style='margin-bottom:8.0pt;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal style='margin-bottom:8.0pt;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='background-image:url(\"../public/img/front/LogoBojeo.com.PNG\"),font-family:\"Segoe UI Semilight\",\"sans-serif\";mso-fareast-language:ES'></span><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p></o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Bojeo Tours L.S. Bercelona, España<o:p></o:p></span></p><p class=MsoNormal><o:p>&nbsp;</o:p></p>\n" +
        "</div></body></html>\n";
};
function templateMailRefund(email, tourName, portAddress, date, shipName, departure, cantPerson, total, reason) {
    var aux = new Date(date);
    var newDate = aux.getDate() + "/" + aux.getMonth() + "/" + aux.getFullYear();
    newDate = String(newDate);

    return "<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n" +
        "xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns:m=\"http://schemas.microsoft.com/office/2004/12/omml\"\n" +
        "xmlns=\"http://www.w3.org/TR/REC-html40\">\n" +
        "<head>\n" +
        "<meta http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">\n" +
        "<meta name=Generator content=\"Microsoft Word 15 (filtered medium)\" \n>" +
        "<!--[if !mso]><style>v\: * {behavior: url(#default#VML);}o\: * {behavior: url(#default#VML);}w\: * {behavior: url(#default#VML);}.shape {behavior: url(#default#VML);}</style><![endif]-->\n" +
        "<style>@font-face {font-family: \"Cambria Math\";panose-1: 0 0 0 0 0 0 0 0 0 0;}\n" +
        "@font-face {font-family: Calibri;panose-1: 2 15 5 2 2 2 4 3 2 4;}@font-face {font-family: \"Segoe UI Semilight\";panose-1: 2 11 4 2 4 2 4 2 2 3;}\n" +
        "p.MsoNormal, li.MsoNormal, div.MsoNormal {margin: 0cm;margin-bottom: .0001pt;font-size: 11.0pt;font-family: \"Calibri\", \"sans-serif\";mso-fareast-language: EN-US;}\n" +
        "a:link, span.MsoHyperlink {mso-style-priority: 99;color: #0563C1;text-decoration: underline;}a:visited, span.MsoHyperlinkFollowed {mso-style-priority: 99;color: #954F72;text-decoration: underline;}\n" +
        "span.EstiloCorreo17 {mso-style-type: personal-compose;font-family: \"Calibri\", \"sans-serif\";color: windowtext;}\n" +
        ".MsoChpDefault {mso-style-type: export-only;mso-fareast-language: EN-US;}@page WordSection1 {size: 612.0pt 792.0pt;margin: 70.85pt 3.0cm 70.85pt 3.0cm;}div.WordSection1 {page: WordSection1;}\n" +
        "--></style>\n" +

        "<!--[if gte mso 9]><xml><o:shapedefaults v:ext=\"edit\" spidmax=\"1026\"/></xml><![endif]-->\n" +
        "<!--[if gte mso 9]><xml><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></xml><![endif]-->\n" +
        "</head>\n" +

        "<body lang=ES link=\"#0563C1\" vlink=\"#954F72\">\n" +
        "<div class=WordSection1>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Hola," + email + "<o:p></o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Se ha realizado la devolución de la siguiente reservación<o:p></o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + reason + "<o:p></o:p></span></p>\n" +

        "<div align=center><table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width=688 style='width:516.15pt;border-collapse:collapse;border:none'>\n" +
        "<tr style='height:24.4pt'><td width=688 colspan=6 valign=top style='width:516.15pt;border:none;border-bottom:solid #A6A6A6 1.0pt;padding:0cm 3.5pt 0cm 3.5pt;height:24.4pt'>\n" +
        "<p class=MsoNormal style='margin-bottom:8.0pt;text-align:justify;line-height:107%'>\n" +
        "<span style='font-size:18.0pt;line-height:107%;font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + tourName + "</span>\n" +
        "<span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + portAddress + "</span>\n" +
        "<span style='font-size:18.0pt;line-height:107%;font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p></o:p></span></p></td></tr>\n" +
        "<tr style='height:25.6pt;border-top: 1 solid #A6A6A6;'>\n" +
        "<td width=109 valign=top style='width:81.65pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.6pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Fecha<o:p></o:p></span></p></td>\n" +
        "<td width=133 valign=top style='width:99.5pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.6pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Embarcación<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.7pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.6pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Salida<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.7pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.6pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Plazas<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.8pt;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:25.6pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Importe<o:p></o:p></span></p></td></tr>\n" +
        "<tr style='height:21.9pt'><td width=109 valign=top style='width:81.65pt;border:none;border-bottom:solid #A6A6A6 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:21.9pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + newDate + "<o:p></o:p></span></p></td>\n" +
        "<td width=133 valign=top style='width:99.5pt;border:none;border-bottom:solid #A6A6A6 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:21.9pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + shipName + "<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.7pt;border:none;border-bottom:solid #A6A6A6 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:21.9pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + departure + "<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.7pt;border:none;border-bottom:solid #A6A6A6 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:21.9pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + cantPerson + "<o:p></o:p></span></p></td>\n" +
        "<td width=112 valign=top style='width:83.8pt;border:none;border-bottom:solid #A6A6A6 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:21.9pt'><p class=MsoNormal align=center style='text-align:center'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>" + total + "<o:p></o:p></span></p></td></tr></table>\n" +
        "</div>\n" +

        "<p class=MsoNormal style='margin-bottom:8.0pt;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>¡Ven a &#8220;Bojear&#8221; y a disfrutar de la ciudad desde el mar!<o:p></o:p></span></p>\n" +
        "<p class=MsoNormal style='margin-bottom:8.0pt;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal style='margin-bottom:8.0pt;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='background-image:url(\"../public/img/front/LogoBojeo.com.PNG\"),font-family:\"Segoe UI Semilight\",\"sans-serif\";mso-fareast-language:ES'></span><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p></o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Bojeo Tours L.S. Bercelona, España<o:p></o:p></span></p><p class=MsoNormal><o:p>&nbsp;</o:p></p>\n" +
        "</div></body></html>\n";
};
function templateForgivePass(email, token) {
    var url = global.bojeoServer + "/change-password/" + token;
    return "\"<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n" +
        "xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns:m=\"http://schemas.microsoft.com/office/2004/12/omml\"\n" +
        "xmlns=\"http://www.w3.org/TR/REC-html40\">\n" +
        "<head>\n" +
        "<meta http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">\n" +
        "<meta name=Generator content=\"Microsoft Word 15 (filtered medium)\" \n>" +
        "<!--[if !mso]><style>v\: * {behavior: url(#default#VML);}o\: * {behavior: url(#default#VML);}w\: * {behavior: url(#default#VML);}.shape {behavior: url(#default#VML);}</style><![endif]-->\n" +
        "<style>@font-face {font-family: \"Cambria Math\";panose-1: 0 0 0 0 0 0 0 0 0 0;}\n" +
        "@font-face {font-family: Calibri;panose-1: 2 15 5 2 2 2 4 3 2 4;}@font-face {font-family: \"Segoe UI Semilight\";panose-1: 2 11 4 2 4 2 4 2 2 3;}\n" +
        "p.MsoNormal, li.MsoNormal, div.MsoNormal {margin: 0cm;margin-bottom: .0001pt;font-size: 11.0pt;font-family: \"Calibri\", \"sans-serif\";mso-fareast-language: EN-US;}\n" +
        "a:link, span.MsoHyperlink {mso-style-priority: 99;color: #0563C1;text-decoration: underline;}a:visited, span.MsoHyperlinkFollowed {mso-style-priority: 99;color: #954F72;text-decoration: underline;}\n" +
        "span.EstiloCorreo17 {mso-style-type: personal-compose;font-family: \"Calibri\", \"sans-serif\";color: windowtext;}\n" +
        ".MsoChpDefault {mso-style-type: export-only;mso-fareast-language: EN-US;}@page WordSection1 {size: 612.0pt 792.0pt;margin: 70.85pt 3.0cm 70.85pt 3.0cm;}div.WordSection1 {page: WordSection1;}\n" +
        "--></style>\n" +

        "<!--[if gte mso 9]><xml><o:shapedefaults v:ext=\"edit\" spidmax=\"1026\"/></xml><![endif]-->\n" +
        "<!--[if gte mso 9]><xml><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></xml><![endif]-->\n" +
        "</head>\n" +

        "<body lang=ES link=\"#0563C1\" vlink=\"#954F72\">\n" +
        "<div class=WordSection1>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Hola," + email + "<o:p></o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Acceda al siguiente enlance para cambiar su contraseña.<br><a href=\"" + url + "\">url</a><o:p></o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Descripción<o:p></o:p></span></p>\n" +


        "<p class=MsoNormal style='margin-bottom:8.0pt;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>¡Ven a &#8220;Bojear&#8221; y a disfrutar de la ciudad desde el mar!<o:p></o:p></span></p>\n" +
        "<p class=MsoNormal style='margin-bottom:8.0pt;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal style='margin-bottom:8.0pt;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Bojeo Tours L.S. Bercelona, España<o:p></o:p></span></p><p class=MsoNormal><o:p>&nbsp;</o:p></p>\n" +
        "</div></body></html>\n";
};
function sendMail(res, smtpTransport, mailOptions) {
    try {
        smtpTransport.sendMail(mailOptions, function (err) {
            if (err) return res.json({res: false});
            smtpTransport.close()
            return res.json({res: true});
        });
    }
    catch (err) {
        return res.json({res: true});
    }

};
exports.sendMail = function (res, smtpTransport, mailOptions) {
    try {
        smtpTransport.sendMail(mailOptions, function (err) {
            if (err) return res.json({res: true});
            smtpTransport.close()
            return res.json({res: true});
        });
    }
    catch (err) {
        return res.json({res: true});
    }

}
exports.logPaypal = function (req, res) {
    var params = req.body;
    ipn.verify(params, function callback(err, msg) {
        if (err) {
            console.error(msg);
            req.session.ipn.push(msg);
        } else {
            msg
            if (params.payment_status == 'Completed') {
                req.session.ipn.push(params);
            }
        }
        res.json(req.session.ipn);
    });
};
/**
 * @method Validar un archivo por su extension
 * @param type
 * @param typeArray
 * @returns {boolean}
 */
exports.validateFile = function (type, typeArray) {
    var flag = false;
    if (type != "" || type != null) {
        for (var j = 0; j < typeArray.length; j++) {
            if (typeArray[j] == type) {
                flag = true;
            }
        }
    }
    return flag;
};
exports.deleteFile = function (name){
    db.Media.remove({fieldName: name}).exec(function (err, success) {
        if (success != null) {
            return true;
        }else{
            return false;
        }
    })
};

exports.getSlug = function (string) {
    var clean = string.replace(/[^a-zA-Z0-9\/_| -]/g, '').replace(/^[\-]+/, '').replace(/[\-]+$/, '').toLowerCase().replace(/[\/_| -]+/g, '-');
    return clean;
};


