/**
 * Created by ernestomr87@gmail.com on 23/05/14.
 */

var async = require('async');
var os = require('os');
var _ = require('lodash');

exports.statistics = function (req, res) {
    var statistics = {
        users: 0,
        ships: 0,
        tours: 0,
        enterprises: 0,
        services: 0,
        packages: 0,
        groups: 0,
        ports: 0,
        bonds: 0,
        news: 0,
        contacts: 0,
        contactsUnread: 0,
        reservations: 0,
        refunds: 0,
        countries: 0,
        newUsers: 0, //today users registers
        reservationsToday: 0,
        refundsToday: 0,
        tags: 0
    };
    async.parallel([
        function (cb) {
            db.User.find({remove: false}).count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            db.Ship.find({remove: false}).count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            db.Tour.find({remove: false}).count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            db.Enterprise.find({remove: false}).count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            db.Service.find({remove: false}).count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            db.Pack.find({remove: false}).count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            db.Group.find().count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            db.Port.find({remove: false}).count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            db.Bonds.find({remove: false}).count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            db.News.find().count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            db.Contact.find().count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            db.ReservationClient.find({cancel: false}).count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            db.ReservationClient.find({cancel: true}).count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            db.Country.find().count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            db.Contact.find({reading: false}).count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            var aux = new Date();
            var today = new Date(aux.getFullYear(), aux.getMonth(), aux.getDate());
            var tomorrow = new Date(today.getTime() + 86400000);
            db.User.find({
                available: true,
                remove: false,
                createDate: {$gte: today, $lt: tomorrow}
            }).count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            var aux = new Date();
            var today = new Date(aux.getFullYear(), aux.getMonth(), aux.getDate());
            var tomorrow = new Date(today.getTime() + 86400000);
            db.Timeline.find({
                type: 1,
                date: {$gte: today, $lt: tomorrow}
            }).count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            var aux = new Date();
            var today = new Date(aux.getFullYear(), aux.getMonth(), aux.getDate());
            var tomorrow = new Date(today.getTime() + 86400000);
            db.Timeline.find({
                type: 2,
                date: {$gte: today, $lt: tomorrow}
            }).count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
        function (cb) {
            db.Tags.find({remove: false}).count().exec(function (err, num) {
                if (cb) cb(err, num)
            });
        },
    ], function (err, results) {
        if (err || !results) {
            return res.json({res: statistics});
        }
        else {
            var statistics = {
                users: results[0],
                ships: results[1],
                tours: results[2],
                enterprises: results[3],
                services: results[4],
                packages: results[5],
                groups: results[6],
                ports: results[7],
                bonds: results[8],
                news: results[9],
                contacts: results[10],
                reservations: results[11],
                refunds: results[12],
                countries: results[13],
                contactsUnread: results[14],
                newUsers: results[15],
                reservationsToday: results[16],
                refundsToday: results[17],
                tags: results[18]
            };
            var cpu = {
                model: os.cpus()[0].model,
                number: os.cpus().length
            };
            return res.json({res: statistics, cpu: cpu, memory: os.totalmem()});
        }
    });


};
exports.searchText = function (req, res) {

    var text = req.body.text;
    var lang = req.body.lang;

    text = text.replace(' ', '\n');

    var newText = new RegExp(text, 'i');
    var cont = 0;
    var max = 4;
    if (lang == 'es') {
        async.parallel([
            function (cb) {
                db.Tour.find({
                    remove: false,
                    available: true
                }).where('searchText.es').regex(newText).exec(function (err, tours) {
                    if (err || !tours) {
                        cb(err, tours);
                    }
                    else {
                        var tmp = new Array();
                        for (var i = 0; i < tours.length; i++) {
                            var aux = {
                                link: '/tour/' + tours[i]._doc._id,
                                id: tours[i]._doc._id,
                                title: tours[i]._doc.name,
                                text: tours[i]._doc.description,
                                img: tours[i]._doc.photos[0]
                            };
                            tmp.push(aux);
                        }
                        cb(null, tmp);

                    }
                });
            },
            function (cb) {
                db.News.find().where('searchText.es').regex(newText).exec(function (err, news) {
                    if (err || !news) {
                        cb(err, news);
                    }
                    else {
                        var tmp = new Array();
                        for (var i = 0; i < news.length; i++) {
                            var aux = {
                                link: '/news/' + news[i]._doc._id,
                                id: news[i]._doc._id,
                                title: news[i]._doc.title,
                                text: news[i]._doc.body,
                                img: news[i]._doc.photo
                            };
                            tmp.push(aux);
                        }
                        cb(null, tmp);

                    }
                });
            },
            function (cb) {
                db.GeneralInfo.findOne().where('searchUseText.es').regex(newText).exec(function (err, general) {
                    if (err || !general) {
                        cb(err, general);
                    }
                    else {
                        var aux = {
                            link: '/terms-of-use',
                            title: {
                                en: "Use Term",
                                es: "Términos de uso"
                            },
                            text: general._doc.useTerms,
                            img: general._doc.photos[0]
                        };
                        cb(null, aux);

                    }
                });
            },
            function (cb) {
                db.GeneralInfo.findOne().where('searchAboutText.es').regex(newText).exec(function (err, general) {
                    if (err || !general) {
                        cb(err, general);
                    }
                    else {
                        var aux = {
                            link: '/about',
                            title: {
                                en: "About Bojeo",
                                es: "Sobre Bojeo"
                            },
                            text: general._doc.aboutBojeo,
                            img: general._doc.photos[0]

                        };
                        cb(null, aux);
                    }
                });
            },
            function (cb) {
                db.Group.find().where('searchText.es').regex(newText).exec(function (err, groups) {
                    if (err || !groups) {
                        cb(err, groups);
                    }
                    else {
                        var tmp = new Array();
                        for (var i = 0; i < groups.length; i++) {
                            var aux = {
                                link: '/tour/' + groups[i]._doc.front,
                                id: groups[i]._doc._id,
                                title: groups[i]._doc.name,
                                text: groups[i]._doc.description,
                                img: null
                            };
                            tmp.push(aux);
                        }
                        cb(null, tmp);

                    }
                });
            },
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: new Array()});
            }
            else {
                var searchResult = new Array();
                if (results[0].length) {
                    for (var i = 0; i < results[0].length; i++) {
                        searchResult.push(results[0][i]);
                    }
                }
                if (results[1].length) {
                    for (var i = 0; i < results[1].length; i++) {
                        searchResult.push(results[1][i]);
                    }
                }
                if (results[4].length) {
                    for (var i = 0; i < results[4].length; i++) {
                        searchResult.push(results[4][i]);
                    }
                }
                if (results[2]) {
                    searchResult.push(results[2]);
                }
                if (results[3]) {
                    searchResult.push(results[3]);
                }
                return res.json({res: searchResult});
            }
        });
    }
    else {
        async.parallel([
            function (cb) {
                db.Tour.find({
                    remove: false,
                    available: true
                }).where('searchText.en').regex(newText).exec(function (err, tours) {
                    if (err || !tours) {
                        cb(err, tours);
                    }
                    else {
                        var tmp = new Array();
                        for (var i = 0; i < tours.length; i++) {
                            var aux = {
                                link: '/tour/' + tours[i]._doc._id,
                                id: tours[i]._doc._id,
                                title: tours[i]._doc.name,
                                text: tours[i]._doc.description,
                                img: tours[i]._doc.photos[0]
                            };
                            tmp.push(aux);
                        }
                        cb(null, tmp);

                    }
                });
            },
            function (cb) {
                db.News.find().where('searchText.en').regex(newText).exec(function (err, news) {
                    if (err || !news) {
                        cb(err, news);
                    }
                    else {
                        var tmp = new Array();
                        for (var i = 0; i < news.length; i++) {
                            var aux = {
                                link: '/news/' + news[i]._doc._id,
                                id: news[i]._doc._id,
                                title: news[i]._doc.title,
                                text: news[i]._doc.body,
                                img: news[i]._doc.photo
                            };
                            tmp.push(aux);
                        }
                        cb(null, tmp);

                    }
                });
            },
            function (cb) {
                db.GeneralInfo.findOne().where('searchUseText.en').regex(newText).exec(function (err, general) {
                    if (err || !general) {
                        cb(err, general);
                    }
                    else {
                        var aux = {
                            link: '/terms-of-use',
                            title: {
                                en: "Use Term",
                                es: "Términos de uso"
                            },
                            text: general._doc.useTerms,
                            img: general._doc.photos[0]
                        };
                        cb(null, aux);

                    }
                });
            },
            function (cb) {
                db.GeneralInfo.findOne().where('searchAboutText.en').regex(newText).exec(function (err, general) {
                    if (err || !general) {
                        cb(err, general);
                    }
                    else {
                        var aux = {
                            link: '/about',
                            title: {
                                en: "About Bojeo",
                                es: "Sobre Bojeo"
                            },
                            text: general._doc.aboutBojeo,
                            img: general._doc.photos[0]

                        };
                        cb(null, aux);
                    }
                });
            },
            function (cb) {
                db.Group.find().where('searchText.en').regex(newText).exec(function (err, groups) {
                    if (err || !groups) {
                        cb(err, groups);
                    }
                    else {
                        var tmp = new Array();
                        for (var i = 0; i < groups.length; i++) {
                            var aux = {
                                link: '/tour/' + groups[i]._doc.front,
                                id: groups[i]._doc._id,
                                title: groups[i]._doc.name,
                                text: groups[i]._doc.description,
                                img: null
                            };
                            tmp.push(aux);
                        }
                        cb(null, tmp);

                    }
                });
            },
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: new Array()});
            }
            else {
                var searchResult = new Array();
                if (results[0].length) {
                    for (var i = 0; i < results[0].length; i++) {
                        searchResult.push(results[0][i]);
                    }
                }
                if (results[1].length) {
                    for (var i = 0; i < results[1].length; i++) {
                        searchResult.push(results[1][i]);
                    }
                }
                if (results[4].length) {
                    for (var i = 0; i < results[4].length; i++) {
                        searchResult.push(results[4][i]);
                    }
                }
                if (results[2]) {
                    searchResult.push(results[2]);
                }
                if (results[3]) {
                    searchResult.push(results[3]);
                }
                return res.json({res: searchResult});
            }
        });
    }


};
exports.showGalery = function (req, res) {
    var images = {
        tours: [],
        ships: [],
        ports: []
    };
    var cont = 0;
    var max = 2;

    db.Port.find({remove: false, available: true}).exec(function (err, ports) {
        cont++;
        if (err || !ports) {
            images.ports = new Array();
        }
        else {
            for (var i = 0; i < ports.length; i++) {
                for (var j = 0; j < ports[i]._doc.photos.length; j++) {
                    var aux = {
                        link: '/index',
                        name: ports[i]._doc.name,
                        img: ports[i]._doc.photos[j]
                    };
                    images.ports.push(aux);
                }
            }
        }
        if (cont == max) {
            return res.json({res: images});
        }
    });

    db.Tour.find({remove: false, available: true}).exec(function (err, tours) {
        cont++;
        if (err || !tours) {
            images.ports = new Array();
        }
        else {
            for (var i = 0; i < tours.length; i++) {
                for (var j = 0; j < tours[i]._doc.photos.length; j++) {
                    var aux = {
                        link: '/tour/' + tours[i]._doc._id,
                        name: tours[i]._doc.name,
                        img: tours[i]._doc.photos[j]
                    };
                    images.tours.push(aux);
                }
            }
        }
        if (cont == max) {
            return res.json({res: images});
        }
    });
};
exports.reservationsAndRefundsByYear = function (req, res) {
    var year = req.body.year,
        start = new Date(year, 0, 1),
        end = new Date(year + 1, 0, 1),
        reservation = new Array();

    db.ReservationTour.find({date: {$gte: start, $lt: end}}).exec(function (err, resT) {
        if (err || !resT || resT.length == 0) {
            return res.json({res: reservation});
        }
        else {
            var cont = 0;
            for (var i = 0; i < resT.length; i++) {
                getResClientByResTour(resT[i]._id, i, function (data, pos) {
                    if (data) {
                        for (var j = 0; j < data.length; j++) {
                            var aux = {
                                res: resT[pos],
                                client: data[j]
                            };
                            reservation.push(aux);
                        }
                    }
                    cont++;
                    if (cont == resT.length) {
                        return res.json({res: reservation});
                    }
                })
            }
        }
    })

};
exports.getReservationTour = function (req, res) {
    db.ReservationTour.find({remove: false}).exec(function (err, resT) {
        if (err || !resT) {
            return res.json({res: false});
        }
        else {
            if (resT.length > 0) {
                var cont = 0;
                for (var i = 0; i < resT.length; i++) {
                    getTour(resT[i]._doc.tour, i, function (tour, posT) {
                        if (!tour) {
                            return res.json({res: false});
                        }
                        else {
                            getGroup(tour._doc.group.id, posT, function (group, postG) {
                                if (!group) {
                                    return res.json({res: false});
                                }
                                else {
                                    var aux = {
                                        id: resT[postG]._doc.tour,
                                        name: tour._doc.name,
                                        hours: (tour._doc.hours.type == 'hours' ? tour._doc.hours.duration : 24),
                                        color: group._doc.color
                                    };
                                    resT[postG]._doc.tour = aux;

                                    cont++;
                                    if (cont == resT.length) {
                                        return res.json({res: resT});
                                    }
                                }
                            });
                        }
                    })
                }
            }
            else {
                return res.json({res: new Array()});
            }

        }
    });
};
exports.getInvoicesServicesList = function (req, res) {
    var limit = req.body.limit,
        skip = req.body.skip,
        countServices = 0;

    async.waterfall([
        function (cb) {
            db.Service.count({
                remove: false
            }).exec(function (err, count) {
                countServices = count;
                cb(err, count);
            })
        },
        function (count, cb) {
            db.Service.find({
                remove: false
            }, 'name owner').limit(limit).skip(skip).exec(function (err, services) {
                cb(err, services);
            })
        },
        function (services, cb) {
            async.map(services, function (service, callback) {
                db.User.findOne({_id: service._doc.owner}, function (err, user) {
                    if (err || !user) {
                        callback(err, user);
                    }
                    else {
                        user.nameComplete(function (name) {
                            var aux = {
                                id: service._doc._id,
                                name: service._doc.name,
                                owner: {
                                    name: name,
                                    email: user._doc.email
                                }
                            }
                            callback(null, aux);
                        })


                    }
                })
            }, function (err, result) {
                cb(err, result);
            });
        }
    ], function (err, result) {
        if (err || !result) {
            return res.json({res: false});
        }
        else {
            return res.json({res: result, count: countServices});
        }
    });


};
exports.getInvoicesShipsList = function (req, res) {
    var limit = req.body.limit,
        skip = req.body.skip,
        countServices = 0;

    async.waterfall([
        function (cb) {
            db.Ship.count({
                remove: false
            }).exec(function (err, count) {
                countServices = count;
                cb(err, count);
            })
        },
        function (count, cb) {
            db.Ship.find({
                remove: false
            }, 'name owner').limit(limit).skip(skip).exec(function (err, ships) {
                cb(err, ships);
            })
        },
        function (ships, cb) {
            async.map(ships, function (ship, callback) {
                db.User.findOne({_id: ship._doc.owner.id}, function (err, user) {
                    if (err || !user) {
                        callback(err, user);
                    }
                    else {
                        user.nameComplete(function (name) {
                            var aux = {
                                id: ship._doc._id,
                                name: ship._doc.name,
                                owner: {
                                    name: name,
                                    email: user._doc.email
                                }
                            }
                            callback(null, aux);
                        })


                    }
                })
            }, function (err, result) {
                cb(err, result);
            });
        }
    ], function (err, result) {
        if (err || !result) {
            return res.json({res: false});
        }
        else {
            return res.json({res: result, count: countServices});
        }
    });
};
exports.getInvoice = function (req, res) {

    if (req.body.year != null && req.body.month != null) {
        var start = new Date(req.body.year, req.body.month, 1),
            end = req.body.month == 11 ? new Date(req.body.year + 1, 0, 1) : new Date(req.body.year, req.body.month + 1, 1);
    }
    else {
        var auxDate = new Date();
        var start = new Date(auxDate.getFullYear(), auxDate.getMonth(), 1),
            end = auxDate.getMonth() == 11 ? new Date(auxDate.getFullYear() + 1, 0, 1) : new Date(auxDate.getFullYear(), auxDate.getMonth() + 1, 1);
    }


    if (req.body.type == 'service') {
        updateStateTourReserved(function () {
            async.waterfall([
                function (cb) {
                    db.ReservationClient.find({
                        state: 1,
                        date: {$gte: start, $lt: end},
                        'services.id': req.body.id
                    }, function (err, resC) {
                        cb(err, resC);
                    })
                },
                function (resC, cb) {
                    var listArray = contResourcesServices(req.body.id, resC);
                    async.map(listArray, function (obj, callback) {
                        db.Pack.findOne({_id: obj.pack}, function (err, pack) {
                            if (err || !pack) {
                                callback(err, pack);
                            }
                            else {
                                pack.getName(function (name) {
                                    obj.pack = name;
                                    callback(null, obj);
                                })
                            }
                        })
                    }, function (err, result) {
                        cb(err, result);
                    });
                }
            ], function (err, result) {
                if (err || !result) {
                    return res.json({res: false});
                }
                else {
                    var total = 0;
                    for (var i = 0; i < result.length; i++) {
                        total = total + result[i].subTotal;
                    }
                    return res.json({res: {total: total, packs: result}});
                }
            });
        })
    }
    else if (req.body.type == 'ship') {
        updateStateTourReserved(function () {
            async.waterfall([
                function (cb) {
                    db.ReservationClient.find({
                        state: 1,
                        date: {$gte: start, $lt: end}
                    }, function (err, resC) {
                        cb(err, resC);
                    })
                },
                function (resC, cb) {
                    async.map(resC, function (obj, callback) {
                        db.ReservationTour.findOne({
                            _id: obj._doc.reservation,
                            ship: req.body.id
                        }, function (err, resT) {
                            if (err || !resT) {
                                callback(err, resT);
                            }
                            else {
                                obj._doc.reservation = resT._doc;
                                callback(null, obj);
                            }
                        })
                    }, function (err, result) {
                        if (err || !result) {
                            cb(err, result);
                        }
                        else {
                            var array = new Array();
                            for (var i = 0; i < result.length; i++) {
                                if (result[i]) {
                                    if (array.length) {
                                        var sem = true;
                                        for (var o = 0; o < array.length; o++) {
                                            if (array[o]._doc.reservation._id.toString() == result[i]._doc.reservation._id.toString()) {
                                                sem = false;
                                                break;
                                            }
                                        }
                                        if (sem) {
                                            array.push(result[i]);
                                        }
                                    }
                                    else {
                                        array.push(result[i]);
                                    }

                                }
                            }
                            var listArray = contResourcesShips(array);
                            async.map(listArray, function (obj, callback) {
                                db.Tour.findOne({_id: obj.tour}, function (err, tour) {
                                    if (err || !tour) {
                                        callback(err, tour);
                                    }
                                    else {
                                        tour.getName(function (name) {
                                            obj.tour = name;
                                            callback(null, obj);
                                        })
                                    }
                                })
                            }, function (err, result) {
                                cb(err, result);
                            });

                        }
                    });
                }
            ], function (err, result) {
                if (err || !result) {
                    return res.json({res: false});
                }
                else {
                    var total = 0, sp = 0;
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].payform != 'bonds') {
                            total = total + result[i].subTotal;
                        }
                    }
                    return res.json({res: {total: total, tours: result}});
                }
            });
        })
    }
}
function getTour(id, pos, cb) {
    db.Tour.findOne({_id: id}).exec(function (err, tour) {
        if (err || !tour) {
            if (cb) cb(false, pos);
        }
        else {
            if (cb) cb(tour, pos);
        }
    });
};
function getResClientByResTour(id, pos, cb) {
    db.ReservationClient.find({reservation: id}).exec(function (err, resC) {
        if (err || !resC || resC.length == 0) {
            if (cb) cb(false, pos);
        }
        else {
            if (cb) cb(resC, pos);
        }
    })
};
function getGroup(id, pos, cb) {
    db.Group.findOne({_id: id}).exec(function (err, group) {
        if (err || !group) {
            if (cb) cb(false, pos);
        }
        else {
            if (cb) cb(group, pos);
        }
    });
};
function updateStateTourReserved(cb) {
    var date = new Date();
    date = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() + 86400000;

    date = new Date(date);

    db.ReservationTour.find({date: {$lt: date}}).exec(function (err, resTours) {
        if (err || !resTours) {
            if (cb) cb(true);
        }
        else {
            var cont = 0;
            if (resTours.length > 0) {
                for (var i = 0; i < resTours.length; i++) {
                    db.ReservationClient.update({
                        reservation: resTours[i]._doc._id,
                        state: 0
                    }, {$set: {state: 1}}).exec(function () {
                        cont++;
                        if (cont == resTours.length) {
                            if (cb) cb(true);
                        }
                    })
                }
            }
            else {
                if (cb) cb(true);
            }


        }
    })

};
function contResourcesServices(id, reservations) {
    var array = new Array();
    for (var i = 0; i < reservations.length; i++) {
        var service = [];
        for (var a = 0; a < reservations[i].services.length; a++) {
            if (reservations[i].services[a].id == id) {
                service = reservations[i].services[a];
                break;
            }
        }
        if (array.length) {
            var sem = true;
            for (var j = 0; j < array.length; j++) {
                if (array[j].pack.toString() == reservations[i].pack.toString()) {
                    sem = false;
                    array[j].cant++;
                    array[j].subTotal = array[j].subTotal + ((reservations[i]._doc.days * reservations[i]._doc.cantPerson * service.price * service.comition) / 100);
                    array[j].cantPerson = array[j].cantPerson + reservations[i]._doc.cantPerson;
                }
            }
            if (sem) {
                var aux = {
                    cant: 1,
                    pack: reservations[i].pack,
                    subTotal: (reservations[i]._doc.days * reservations[i]._doc.cantPerson * service.price * service.comition) / 100,
                    comition: service.comition,
                    cantPerson: reservations[i]._doc.cantPerson,
                    price: service.price

                }
                array.push(aux);
            }
        }
        else {
            var aux = {
                cant: 1,
                pack: reservations[i].pack,
                subTotal: (reservations[i]._doc.days * reservations[i]._doc.cantPerson * service.price * service.comition) / 100,
                comition: service.comition,
                cantPerson: reservations[i]._doc.cantPerson,
                price: service.price

            }
            array.push(aux);
        }
    }
    return array;
};
function contResourcesShips(reservations) {
    var array = new Array();
    for (var i = 0; i < reservations.length; i++) {
        if (array.length) {
            var sem = true;
            for (var j = 0; j < array.length; j++) {
                if (array[j].tour.toString() == reservations[i].reservation.tour.toString()) {
                    sem = false;
                    var subTotal;
                    if (reservations[i].payform == 'bonds') {
                        subTotal = parseInt(reservations[i].monto);
                    }
                    else {
                        if (reservations[i].reservation.priceBy == 'person') {
                            subTotal = (reservations[i].reservation.priceTour * reservations[i].reservation.cantPerson * reservations[i].reservation.comitions) / 100;
                        }
                        else {
                            subTotal = (reservations[i].reservation.priceTour * reservations[i].reservation.comitions) / 100;
                        }
                    }
                    array[j].cant++;
                    array[j].subTotal = array[j].subTotal + subTotal;
                    if (reservations[i].payform == 'bonds') {
                        array[j].cantPerson = array[j].cantPerson + reservations[i].reservation.cantPerson;
                    }
                }
            }
            if (sem) {
                var subTotal, priceTour = null, cantPerson = null;
                if (reservations[i].payform == 'bonds') {
                    subTotal = parseInt(reservations[i].monto);
                    cantPerson = reservations[i].reservation.cantPerson;
                }
                else {
                    if (reservations[i].reservation.priceBy == 'person') {
                        priceTour = reservations[i].reservation.priceTour * reservations[i].reservation.cantPerson;
                        subTotal = (priceTour * reservations[i].reservation.comitions) / 100;
                    }
                    else {
                        priceTour = reservations[i].reservation.priceTour;
                        subTotal = (reservations[i].reservation.priceTour * reservations[i].reservation.comitions) / 100;
                    }

                }

                var aux = {
                    cant: 1,
                    tour: reservations[i].reservation.tour,
                    price: priceTour,
                    comition: reservations[i].reservation.comitions,
                    subTotal: subTotal,
                    payform: reservations[i].payform,
                    cantPerson: cantPerson
                }

                array.push(aux);
            }
        }
        else {
            var subTotal, priceTour = null, cantPerson = null;
            if (reservations[i].payform == 'bonds') {
                subTotal = parseInt(reservations[i].monto);
                cantPerson = reservations[i].reservation.cantPerson;
            }
            else {
                if (reservations[i].reservation.priceBy == 'person') {
                    priceTour = reservations[i].reservation.priceTour * reservations[i].reservation.cantPerson;
                    subTotal = (priceTour * reservations[i].reservation.comitions) / 100;
                }
                else {
                    priceTour = reservations[i].reservation.priceTour;
                    subTotal = (reservations[i].reservation.priceTour * reservations[i].reservation.comitions) / 100;
                }

            }

            var aux = {
                cant: 1,
                tour: reservations[i].reservation.tour,
                price: priceTour,
                comition: reservations[i].reservation.comitions,
                subTotal: subTotal,
                payform: reservations[i].payform,
                cantPerson: cantPerson
            }

            array.push(aux);
        }
    }
    return array;
};