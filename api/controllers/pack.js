/**
 * Created by Juan Javier on 27/11/2014.
 */
var async = require('async');
var toursController = require('./tours');
var serviceController = require('./service');
var utils = require('./../utils');


exports.create = function (req, res) {
    var tour = req.body.tour;
    var services = eval(req.body.services);
    var color = req.body.color;
    // verificar si existe un paquete esta registrado
    db.Pack.find({remove: false, tour: tour}).exec(function (err, packs) {
        if (err || !packs) {
            return res.json({res: false, code: 0});
        }
        else {

            db.Tour.findOne({_id: tour, available: true, remove: false}).exec(function (err, tourIns) {
                if (err || !tourIns) {
                    return res.json({res: false, code: 0});
                }
                else {
                    async.map(services, function (service, cb) {
                        db.Service.findOne({_id: service.id, status: true, remove: false}).exec(function (err, serv) {
                            cb(err, serv);
                        })

                    }, function (err, result) {
                        if (err || !result) {
                            return res.json({res: false, code: 0});
                        }
                        else {
                            var aux = [];
                            var string = tourIns.name.es;
                            for (var i = 0; i < result.length; i++) {
                                aux.push(result[i]._doc._id);
                                string = string + " " + result[i]._doc.name.es
                            }


                            if (packs.length == 0) {
                                var pack = new db.Pack({
                                    tour: tour,
                                    services: aux,
                                    color: color,
                                    slug: utils.getSlug(string)
                                });

                                pack.save(function (err, pack) {
                                    if (err || !pack) {
                                        return res.json({res: false, code: 0});
                                    }
                                    else {
                                        return res.json({res: true});
                                    }
                                });
                            } else {
                                var exist = false;
                                for (var i = 0; i < packs.length; i++) {
                                    var servicesPack = packs[i]._doc.services;
                                    if (servicesPack.length == services.length) {
                                        if (services.length == 1) {
                                            if (servicesPack[0].id == services[0].id) {
                                                exist = true;
                                                break;
                                            }
                                        } else {
                                            var cont = 0;
                                            for (var j = 0; j < servicesPack.length; j++) {
                                                for (var k = 0; k < services.length; k++) {
                                                    if (servicesPack[j].id == services[k].id) {
                                                        cont++;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (cont == servicesPack.length) {
                                                exist = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (exist) {
                                    return res.json({res: false, code: 1});
                                } else {
                                    var pack = new db.Pack({
                                        tour: tour,
                                        services: aux,
                                        color: color,
                                        slug: utils.getSlug(string)
                                    });
                                    pack.save(function (err, pack) {
                                        if (err || !pack) {
                                            return res.json({res: false, code: 0});
                                        }
                                        else {
                                            return res.json({res: true});
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            });


        }
    })
};

exports.update = function (req, res) {
    var idPackage = req.body.id;
    var tour = req.body.tour.id || req.body.tour;
    var color = req.body.color;
    var services = [], auxS = eval(req.body.services);
    for (var i = 0; i < auxS.length; i++) {
        services.push(auxS[i].id) ;
    }


    db.Pack.find({remove: false, tour: tour, services: services}).exec(function (err, packs) {
        if (err || !packs) {
            return res.json({res: false, code: 0});
        } else {
            if (packs.length == 0) {
                db.Pack.find({remove: false, tour: tour}).exec(function (err, packs) {
                    if (err || !packs) {
                        return res.json({res: false, code: 0});
                    } else {
                        if (packs.length == 0) {


                            db.Tour.findOne({_id: tour, available: true, remove: false}).exec(function (err, tourIns) {
                                if (err || !tourIns) {
                                    return res.json({res: false, code: 0});
                                }
                                else {
                                    async.map(services, function (service, cb) {
                                        db.Service.findOne({
                                            _id: service.id,
                                            status: true,
                                            remove: false
                                        }).exec(function (err, serv) {
                                            cb(err, serv);
                                        })

                                    }, function (err, result) {
                                        if (err || !result) {
                                            return res.json({res: false, code: 0});
                                        }
                                        else {
                                            var aux = [];
                                            var string = tourIns.name.es;
                                            for (var i = 0; i < result.length; i++) {
                                                aux.push(result[i]._doc._id);
                                                string = string + " " + result[i]._doc.name.es
                                            }

                                            db.Pack.update({_id: idPackage}, {
                                                $set: {
                                                    tour: tour,
                                                    services: aux,
                                                    color: color,
                                                    slug: utils.getSlug(string)
                                                }
                                            }).exec(function (err, pack) {
                                                if (err || !pack) {
                                                    return res.json({res: false, code: 0});
                                                }
                                                else {
                                                    return res.json({res: true});
                                                }
                                            });

                                        }
                                    });
                                }
                            });

                        } else {
                            var exist = false;
                            for (var i = 0; i < packs.length; i++) {
                                var servicesPack = packs[i]._doc.services;
                                if (servicesPack.length == services.length) {
                                    if (services.length == 1) {
                                        if (servicesPack[0].id == services[0].id) {
                                            exist = true;
                                            break;
                                        }
                                    } else {
                                        var cont = 0;
                                        for (var j = 0; j < servicesPack.length; j++) {
                                            for (var k = 0; k < services.length; k++) {
                                                if (servicesPack[j].id == services[k].id) {
                                                    cont++;
                                                    break;
                                                }
                                            }
                                        }
                                        if (cont == servicesPack.length) {
                                            exist = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (exist) {
                                return res.json({res: false, code: 1});
                            } else {

                                db.Tour.findOne({
                                    _id: tour,
                                    available: true,
                                    remove: false
                                }).exec(function (err, tourIns) {
                                    if (err || !tourIns) {
                                        return res.json({res: false, code: 0});
                                    }
                                    else {
                                        async.map(services, function (service, cb) {
                                            db.Service.findOne({
                                                _id: service.id,
                                                status: true,
                                                remove: false
                                            }).exec(function (err, serv) {
                                                cb(err, serv);
                                            })

                                        }, function (err, result) {
                                            if (err || !result) {
                                                return res.json({res: false, code: 0});
                                            }
                                            else {
                                                var aux = [];
                                                var string = tourIns.name.es;
                                                for (var i = 0; i < result.length; i++) {
                                                    aux.push(result[i]._doc._id);
                                                    string = string + " " + result[i]._doc.name.es
                                                }

                                                db.Pack.update({_id: idPackage}, {
                                                    $set: {
                                                        tour: tour,
                                                        services: aux,
                                                        color: color,
                                                        slug: utils.getSlug(string)
                                                    }
                                                }).exec(function (err, pack) {
                                                    if (err || !pack) {
                                                        return res.json({res: false, code: 0});
                                                    }
                                                    else {
                                                        return res.json({res: true});
                                                    }
                                                });

                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                })
            } else {
                if (packs[0]._doc._id == idPackage) {
                    db.Pack.update({_id: idPackage}, {
                        $set: {
                            color: color
                        }
                    }).exec(function (err, pack) {
                        if (err || !pack) {
                            return res.json({res: false, code: 0});
                        }
                        else {
                            return res.json({res: true});
                        }
                    });
                } else {
                    return res.json({res: false, code: 1});
                }
            }
        }
    })
};

exports.delete = function (req, res) {
    db.Pack.update({_id: req.params.id}, {$set: {remove: true}}).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false});
        }
        else {
            return res.json({res: true});
        }
    })
};

exports.list = function (req, res) {
    var limit = req.query.limit,
        skip = req.query.skip,
        text = req.query.text;
    if (text.length > 0) {
        async.parallel([
            function (cb) {
                db.Pack.count({remove: false}).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                text = new RegExp(text, 'i');
                db.Pack.find({remove: false}).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, packages) {
                    if (cb) cb(err, packages);
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                var cantidad = results[0];
                var packages = results[1];
                var list = new Array();
                if (packages.length > 0) {
                    async.parallel([
                        function (cb) {
                            var cont = 0;
                            for (var i = 0; i < packages.length; i++) {
                                toursController.getTourGroups(packages[i]._doc.tour, i, function (data, pos) {
                                    if (data) {
                                        var aux = {
                                            id: packages[pos]._doc.tour,
                                            idGroup: data.group.id,
                                            name: data.name,
                                            nameGroup: (data.group.name == 'vacio' ? 'vacio' : data.group.name)
                                        }
                                        packages[pos]._doc.tour = aux;
                                    }
                                    cont++;
                                    if (cont == packages.length) {
                                        if (cb) cb(err, packages);
                                    }
                                })
                            }
                        },
                        function (cb) {
                            var cont = 0;
                            for (var i = 0; i < packages.length; i++) {
                                serviceController.getServices(packages[i]._doc.services, i, function (data, pos) {
                                    if (data) {
                                        var services = [];
                                        for (var j = 0; j < data.length; j++) {
                                            services.push(data[j]);
                                        }
                                        packages[pos]._doc.services = services;
                                    }
                                    cont++;
                                    if (cont == packages.length) {
                                        if (cb) cb(err, packages);
                                    }
                                })
                            }
                        }
                    ], function (err, results) {
                        if (err || !results) {
                            return res.json({res: false, cont: 0});
                        }
                        else {
                            return res.json({res: results[1], cont: cantidad});
                        }
                    });
                } else {
                    return res.json({res: list, cont: cantidad});
                }
            }
        });
    }
    else {
        async.parallel([
            function (cb) {
                db.Pack.count({remove: false}).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {

                db.Pack.find({remove: false}).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, packages) {
                    if (cb) cb(err, packages);
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                var cantidad = results[0];
                var packages = results[1];
                var list = new Array();
                if (packages.length > 0) {
                    async.parallel([
                        function (cb) {
                            var cont = 0;
                            for (var i = 0; i < packages.length; i++) {
                                toursController.getTourGroups(packages[i]._doc.tour, i, function (data, pos) {
                                    if (data) {
                                        var aux = {
                                            id: packages[pos]._doc.tour,
                                            idGroup: data.group.id,
                                            name: data.name,
                                            nameGroup: (data.group.name == 'vacio' ? 'vacio' : data.group.name)
                                        }
                                        packages[pos]._doc.tour = aux;
                                    }
                                    cont++;
                                    if (cont == packages.length) {
                                        if (cb) cb(err, packages);
                                    }
                                })
                            }
                        },
                        function (cb) {
                            var cont = 0;
                            for (var i = 0; i < packages.length; i++) {
                                serviceController.getServices(packages[i]._doc.services, i, function (data, pos) {
                                    if (data) {
                                        var services = [];
                                        for (var j = 0; j < data.length; j++) {
                                            services.push(data[j]);
                                        }
                                        packages[pos]._doc.services = services;
                                    }
                                    cont++;
                                    if (cont == packages.length) {
                                        if (cb) cb(err, packages);
                                    }
                                })
                            }
                        }
                    ], function (err, results) {
                        if (err || !results) {
                            return res.json({res: false, cont: 0});
                        }
                        else {
                            return res.json({res: results[1], cont: cantidad});
                        }
                    });
                } else {
                    return res.json({res: list, cont: cantidad});
                }
            }
        });
    }
};

exports.listBack = function (req, res) {
    var limit = req.body.limit,
        skip = req.body.skip,
        id = req.body.id,
        text = req.body.text;

    if (text.length > 0) {
        async.parallel([
            function (cb) {
                db.Pack.count({remove: false}).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                text = new RegExp(text, 'i');
                db.Pack.find({remove: false}).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, packages) {
                    if (cb) cb(err, packages);
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                var cantidad = results[0];
                var packages = results[1];
                var list = new Array();
                if (packages.length > 0) {
                    async.parallel([
                        function (cb) {
                            var cont = 0;
                            for (var i = 0; i < packages.length; i++) {
                                toursController.getTourGroups(packages[i]._doc.tour, i, function (data, pos) {
                                    if (data) {
                                        var aux = {
                                            id: packages[pos]._doc.tour,
                                            idGroup: data.group.id,
                                            name: data.name,
                                            nameGroup: (data.group.name == 'vacio' ? 'vacio' : data.group.name)
                                        }
                                        packages[pos]._doc.tour = aux;
                                    }
                                    cont++;
                                    if (cont == packages.length) {
                                        if (cb) cb(err, packages);
                                    }
                                })
                            }
                        },
                        function (cb) {
                            var cont = 0;
                            for (var i = 0; i < packages.length; i++) {
                                serviceController.getServices(packages[i]._doc.services, i, function (data, pos) {
                                    if (data) {
                                        var services = [];
                                        for (var j = 0; j < data.length; j++) {
                                            services.push(data[j]);
                                        }
                                        packages[pos]._doc.services = services;
                                    }
                                    cont++;
                                    if (cont == packages.length) {
                                        if (cb) cb(err, packages);
                                    }
                                })
                            }
                        },
                        function (cb) {
                            var cont = 0;
                            for (var i = 0; i < packages.length; i++) {
                                exports.getReservationByPack(packages[i]._doc._id, i, function (data, pos) {
                                    if (data) {
                                        packages[pos]._doc.reservation = data;
                                    }
                                    cont++;
                                    if (cont == packages.length) {
                                        if (cb) cb(err, packages);
                                    }
                                })
                            }
                        }
                    ], function (err, results) {
                        if (err || !results) {
                            return res.json({res: false, cont: 0});
                        }
                        else {
                            return res.json({res: results[2], cont: cantidad});
                        }
                    });
                } else {
                    return res.json({res: list, cont: cantidad});
                }
            }
        });
    }
    else {
        async.parallel([
            function (cb) {
                db.Pack.count({remove: false}).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                if (id.length > 0) {
                    async.parallel([
                        function (cb1) {
                            db.Pack.find({
                                $or: [
                                    {_id: id},
                                    {tour: id}
                                ], remove: false
                            }).exec(function (err, packages) {
                                if (cb1) cb1(err, packages);
                            })
                        },
                        function (cb1) {
                            db.Pack.find({remove: false}).exec(function (err, packages) {
                                var arrayPack = [];
                                for (var i = 0; i < packages.length; i++) {
                                    if (packages[i].services.length > 1) {
                                        for (var j = 0; j < packages[i].services.length; j++) {
                                            if (packages[i].services[j].id == id) {
                                                arrayPack.push(packages[i]);
                                                break;
                                            }
                                        }
                                    } else {
                                        if (packages[i].services[0].id == id) {
                                            arrayPack.push(packages[i]);
                                        }
                                    }
                                }
                                if (cb1)cb1(err, arrayPack);
                            })
                        }
                    ], function (err, results) {
                        if (err || !results) {
                            cb(null, false);
                        } else {
                            if (results[0].length) {
                                cb(null, results[0]);
                            } else {
                                cb(null, results[1]);
                            }
                        }
                    })
                } else {
                    db.Pack.find({remove: false}).populate('services').sort({_id: 1}).limit(limit).skip(skip).exec(function (err, packages) {
                        if (cb) cb(err, packages);
                    })
                }
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                var cantidad = results[0];
                var packages = results[1];
                var list = new Array();
                if (packages.length > 0) {
                    async.series([
                        function (cb) {
                            async.map(packages, function (pack, callbackm1) {
                                    toursController.getTourGroups(pack._doc.tour, 0, function (data, pos) {
                                        if (data) {
                                            var aux = {
                                                id: packages[pos]._doc.tour,
                                                idGroup: data.group.id,
                                                name: data._doc.name,
                                                nameGroup: (data.group.name == 'vacio' ? 'vacio' : data.group.name)
                                            };
                                            pack._doc.tour = aux;
                                            callbackm1(null, pack);
                                        }
                                        else {
                                            callbackm1(null, pack);
                                        }
                                    })
                                }, function (err, result) {
                                    cb(err, result);
                                }
                            )
                        },
                        function (cb) {

                            async.map(packages, function (pack, callbackm2) {
                                    serviceController.getServices(pack._doc.services, 0, function (data, pos) {
                                        if (data) {
                                            var services = [];
                                            for (var j = 0; j < data.length; j++) {
                                                services.push(data[j]);
                                            }
                                            pack._doc.services = services;
                                            callbackm2(null, pack)
                                        }
                                        else {
                                            callbackm2(null, pack)
                                        }
                                    })
                                }, function (err, result) {
                                    cb(err, result);
                                }
                            )
                        },
                        function (cb) {
                            async.map(packages, function (pack, callbackm3) {
                                    exports.getReservationByPack(pack._doc._id, 0, function (data, pos) {
                                        if (data) {
                                            packages[pos]._doc.reservation = data;
                                            callbackm3(null, pack)
                                        }
                                        else {
                                            callbackm3(null, pack)
                                        }

                                    })
                                }, function (err, result) {
                                    cb(err, result);
                                }
                            )
                        }
                    ], function (err, results) {
                        if (err || !results) {
                            return res.json({res: false, cont: 0});
                        }
                        else {
                            return res.json({res: results[2], cont: cantidad});
                        }
                    });
                } else {
                    return res.json({res: list, cont: cantidad});
                }
            }
        });
    }
};

exports.get = function (req, res) {
    db.Pack.find({_id: req.params.id, remove: false}).exec(function (err, pack) {
        if (err || !pack) {
            return res.json({res: false});
        }
        else {
            return res.json({res: pack});
        }
    })
};

exports.status = function (req, res) {
    var id = req.body.id,
        action = req.body.action;

    if (action == 0) {
        async.waterfall([
            function (callback) {
                db.Pack.findOne({_id: id, remove: false}, function (err, pack) {
                    callback(err, pack);
                })
            },
            function (pack, callback) {
                if (!pack._doc.status) {
                    async.parallel([
                        function (cb) {
                            async.map(pack._doc.services, function (service, cbMap) {
                                db.Service.findOne({
                                    _id: service,
                                    remove: false,
                                    status: false
                                }, function (err, service) {
                                    cbMap(err, service);
                                })
                            }, function (err, result) {
                                if (err || !result) {
                                    cb(err, new Array());
                                }
                                else {
                                    var datos = [];
                                    for (var i = 0; i < result.length; i++) {
                                        if (result[i] != null) {
                                            var temp = {
                                                id: result[i]._doc._id,
                                                name: result[i]._doc.name
                                            };
                                            datos.push(temp)
                                        }
                                    }
                                    cb(null, datos);
                                }
                            });
                        },
                        function (cb) {
                            db.Tour.findOne({
                                _id: pack._doc.tour,
                                remove: false,
                                available: false
                            }, function (err, tour) {
                                if (err || !tour) {
                                    cb(err, new Array());
                                }
                                else {
                                    var temp = {
                                        id: tour._doc._id,
                                        name: tour._doc.name
                                    };
                                    cb(err, temp);
                                }

                            })
                        },
                        function (cb) {
                            db.Tour.findOne({
                                _id: pack._doc.tour,
                                remove: false,
                                available: false
                            }, function (err, tour) {
                                if (err || !tour) {
                                    cb(err, new Array());
                                }
                                else {
                                    db.Port.findOne({_id: tour._doc.port, available: false}, function (err, port) {
                                        if (err || !port) {
                                            cb(err, port);
                                        }
                                        else {
                                            var temp = {
                                                id: port._doc._id,
                                                name: port._doc.name
                                            };
                                            cb(err, temp);
                                        }
                                    })
                                }
                            })
                        },
                        function (cb) {
                            async.map(pack._doc.services, function (service, cbMap) {
                                db.Service.findOne({
                                    _id: service,
                                    remove: false,
                                    status: false
                                }, function (err, service) {
                                    if (err || !service) {
                                        cbMap(err, service);
                                    }
                                    else {
                                        async.parallel([
                                            function (cbS) {
                                                db.User.findOne({
                                                    _id: service._doc.owner,
                                                    available: false
                                                }, 'email', function (err, user) {
                                                    if (err || !user) {
                                                        cbS(err, user);
                                                    }
                                                    else {
                                                        cbS(null, {id: user._doc._id, name: user._doc.email});
                                                    }
                                                })
                                            },
                                            function (cbS) {
                                                db.Enterprise.findOne({
                                                    _id: service._doc.enterprise,
                                                    available: false
                                                }, 'name', function (err, enterprise) {
                                                    if (err || !enterprise) {
                                                        cbS(err, enterprise);
                                                    }
                                                    else {
                                                        cbS(null, {
                                                            id: enterprise._doc._id,
                                                            name: enterprise._doc.name
                                                        });
                                                    }

                                                })
                                            }
                                        ], function (err, result) {
                                            if (err || !result) {
                                                cbMap(err, result);
                                            }
                                            else {
                                                cbMap(null, {user: result[0], enterprise: result[1]});
                                            }
                                        });
                                    }
                                })
                            }, function (err, result) {
                                if (err || !result) {
                                    cb(err, new Array());
                                }
                                else {
                                    var auxUsers = new Array();
                                    var auxEnterprise = new Array();
                                    for (var i = 0; i < result.length; i++) {
                                        if (result[i] != null) {
                                            if (result[i].user && existElement(auxUsers, result[i].user)) {
                                                auxUsers.push(result[i].user);
                                            }
                                            if (result[i].enterprise && existElement(auxEnterprise, result[i].enterprise)) {
                                                auxEnterprise.push(result[i].enterprise);
                                            }
                                        }

                                    }
                                    cb(null, {users: auxUsers, enterprises: auxEnterprise});

                                }
                            });
                        }
                    ], function (err, result) {
                        if (err) {
                            callback(err);
                        }
                        else {
                            if (!result[0].length && result[1].length == 0 && result[2].length == 0 && result[3].users.length == 0 && result[3].enterprises.length == 0) {
                                db.Pack.update({_id: id}, {$set: {status: !pack._doc.status}},
                                    function (err, success) {
                                        if (err || !success) {
                                            callback(err);
                                        }
                                        else {
                                            callback(null, {res: true, complete: true});
                                        }
                                    });
                            } else {

                                var result = {
                                    services: result[0],
                                    tour: result[1],
                                    port: result[2],
                                    users: result[3].users,
                                    enterprises: result[3].enterprises

                                };
                                callback(null, {res: result, complete: false});
                            }
                        }
                    })
                }
                else {
                    db.Pack.update({_id: pack._doc._id}, {$set: {status: !pack._doc.status}},
                        function (err, success) {
                            if (err || !success) {
                                callback(err);
                            }
                            else {
                                callback(null, {res: true, complete: true});
                            }
                        });
                }

            }
        ], function (err, result) {
            if (err || !result) {
                return res.json({res: false});
            }
            else {
                return res.json({res: result.res, complete: result.complete});
            }
        });
    }
    else {
        async.waterfall([
            function (callback) {
                db.Pack.findOne({_id: id, remove: false}, function (err, pack) {
                    callback(err, pack);
                })
            },
            function (pack, callback) {
                async.parallel([
                    function (cb) {
                        async.map(pack._doc.services, function (service, cbMap) {
                            db.Service.update({
                                    _id: service,
                                    remove: false,
                                    status: false
                                }, {$set: {status: true}}
                                , function (err, service) {
                                    cbMap(err, service);
                                })
                        }, function (err, result) {
                            cb(err, result);
                        });
                    },
                    function (cb) {
                        db.Tour.update({
                                _id: pack._doc.tour,
                                remove: false,
                                available: false
                            }, {$set: {available: true}},
                            function (err, tour) {
                                cb(err, tour);
                            })
                    },
                    function (cb) {
                        db.Tour.findOne({
                            _id: pack._doc.tour,
                            remove: false
                        }, function (err, tour) {
                            if (err || !tour) {
                                cb(err, tour);
                            }
                            else {
                                db.Port.update({
                                    _id: tour._doc.port,
                                    available: false
                                }, {$set: {available: true}}, function (err, success) {
                                    cb(err, success);
                                })
                            }
                        })
                    },
                    function (cb) {
                        async.map(pack._doc.services, function (service, cbMap) {
                            db.Service.findOne({
                                _id: service,
                                remove: false
                            }, function (err, service) {
                                if (err || !service) {
                                    cbMap(err, service);
                                }
                                else {
                                    async.parallel([
                                        function (cbS) {
                                            db.User.update({
                                                _id: service._doc.owner
                                            }, {$set: {available: true}}, function (err, user) {
                                                cbS(err, user);
                                            })
                                        },
                                        function (cbS) {
                                            db.Enterprise.update({
                                                _id: service._doc.enterprise
                                            }, {$set: {available: true}}, function (err, enterprise) {
                                                cbS(err, enterprise);
                                            })
                                        }
                                    ], function (err, result) {
                                        cbMap(err, result);
                                    });
                                }
                            })
                        }, function (err, result) {
                            cb(err, result);
                        });
                    }
                ], function (err, result) {
                    if (err || !result) {
                        callback(err, result);
                    }
                    else {
                        db.Pack.update({_id: pack._doc._id}, {$set: {status: !pack._doc.status}},
                            function (err, success) {
                                if (err || !success) {
                                    callback(err);
                                }
                                else {
                                    callback(null, {res: true, complete: true});
                                }
                            });
                    }


                })
            }
        ], function (err, result) {
            if (err || !result) {
                return res.json({res: false});
            }
            else {
                return res.json({res: result.res, complete: result.complete});
            }
        });
    }


    //var status = req.body.status;
    //var check = req.body.check;
    //if (check) {
    //    var services = req.body.services;
    //    var idTour = req.body.idTour;
    //    async.parallel([
    //        function (cb) {
    //            async.map(services, function (service, callback) {
    //                db.Service.findOne({_id: service, remove: false, status: false}, function (err, service) {
    //                    callback(err, service);
    //                })
    //            }, function (err, result) {
    //                if (err || !result) {
    //                    cb(err, result);
    //                }
    //                else {
    //                    var datos = [];
    //                    for (var i = 0; i < result.length; i++) {
    //                        if (result[i] != null) {
    //                            var temp = {
    //                                id: result[i]._doc._id,
    //                                name: result[i]._doc.name
    //                            };
    //                            datos.push(temp)
    //                        }
    //
    //                    }
    //                    cb(null, datos);
    //                }
    //            })
    //        },
    //        function (cb) {
    //            db.Tour.count({_id: idTour, remove: false, available: false}, function (err, count) {
    //                cb(err, count);
    //            })
    //        }
    //    ], function (err, result) {
    //        if (err) {
    //            return res.json({res: false});
    //        } else {
    //            if (result[0].length == 0 && result[1] == 0) {
    //                db.Pack.update({_id: id}, {$set: {status: status}},
    //                    function (err, success) {
    //                        if (err || !success) {
    //                            return res.json({res: false})
    //                        }
    //                        else {
    //                            return res.json({res: true, complete: true})
    //                        }
    //                    });
    //            } else {
    //                var result = {
    //                    services: result[0],
    //                    tour: result[1]
    //                };
    //                return res.json({res: result, complete: false});
    //            }
    //        }
    //    })
    //} else {
    //    db.Pack.update({_id: id}, {$set: {status: status}},
    //        function (err, success) {
    //            if (err || !success) {
    //                return res.json({res: false})
    //            }
    //            else {
    //                return res.json({res: true, complete: true})
    //            }
    //        });
    //}
};

exports.getReservationByPack = function (pack, pos, cb) {
    db.ReservationClient.find({pack: pack, state: 0, cancel: true}).exec(function (err, client) {
        if (client.length > 0) {
            cb(true, pos);
        } else {
            cb(false, pos);
        }
    });
};

function existElement(array, obj) {
    if (array.length) {
        for (var i = 0; i < array.length; i++) {
            if (array[i].id.toString() == obj.id.toString()) {
                return false;
            }
        }
        return true;
    }
    else {
        return true;
    }
};