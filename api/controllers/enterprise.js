/**
 * Created by Juan Javier on 19/11/2014.
 */

var utils = require('../utils');
var async = require('async');
var serviceController = require('./service');
var toursController = require('./tours');

exports.create = function (req, res) {
    var name = {
        es: req.body.name_es,
        en: req.body.name_en
    };
    var description = {
        es: req.body.description_es,
        en: req.body.description_en
    };
    var url = req.body.url;
    var files = req.files;

    var photo = {
        path: "",
        name: "",
        originalName: "",
        extension: ""
    };

    var typeImgs = ["image/jpeg", "image/png"];
    var correct_photo = false;

    if (files.file0.mimetype != "" || files.file0.mimetype != null) {
        if (utils.validateFile(files.file0.mimetype, typeImgs)) {
            photo.name = files.file0.name;
            photo.path = "/api/media/" + files.file0.name;
            photo.originalName = files.file0.originalname;
            photo.extension = files.file0.extension;
            correct_photo = true;
        }
    }

    if (!correct_photo) {
        utils.deleteFile(files.file0.name)
        return res.json({error: 'incorrect image!!!'});
    }

    var enterprise = new db.Enterprise({
        name: name,
        description: description,
        photo: photo,
        url: url,
        slug: {
            es: utils.getSlug(name.es),
            en: utils.getSlug(name.en)
        }
    });

    enterprise.save(function (err, data) {
        if (err || !data) {
            return res.json({res: false});
        }
        else {
            return res.json({res: true});
        }
    });

};

exports.list = function (req, res) {
    var limit = req.query.limit;
    var skip = req.query.skip;
    var word = req.query.word;
    var contEnterprise = 0;
    var cont = 0;
    var enterprises = new Array();

    db.Enterprise.count({remove: false, available: true}).exec(function (err, num) {
        cont++;
        if (err || !num) {
            if (cont == 2) {
                if (num == 0) {
                    return res.json({res: true, enterprises: enterprises, cont: contEnterprise});
                }
                else {
                    return res.json({res: false, enterprises: enterprises, cont: contEnterprise});
                }
            }
        }
        else {
            contEnterprise = num;
            if (cont == 2) {
                return res.json({res: true, enterprises: enterprises, cont: contEnterprise});
            }

        }
    });

    var newText = new RegExp(word, 'i');

    db.Enterprise.find({
        remove: false,
        available: true
    }).where('name.es').regex(newText).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, data) {
        cont++;
        if (err || !data) {
            if (cont == 2) {
                return res.json({res: false, enterprises: enterprises, cont: contEnterprise});
            }
        }
        else {
            enterprises = data;
            if (cont == 2) {
                return res.json({res: true, enterprises: enterprises, cont: contEnterprise});
            }
        }
    })
};

exports.listBack = function (req, res) {
    var limit = req.body.limit;
    var skip = req.body.skip;
    var word = req.body.word;

    if (word.length > 0) {
        async.parallel([
            function (cb) {
                db.Enterprise.count({remove: false}).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                word = new RegExp(word, 'i');
                db.Enterprise.find({remove: false}).where('name.es').regex(word).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, enterprise) {
                    if (enterprise.length > 0) {
                        var cont = 0;
                        for (var i = 0; i < enterprise.length; i++) {
                            exports.getServicesByEnterprise(enterprise[i]._id, i, function (services, pos) {
                                var arrayServices = new Array();
                                for (var j = 0; j < services.length; j++) {
                                    arrayServices.push(services[j]._doc._id);
                                }
                                enterprise[pos]._doc.service = arrayServices;
                                cont++;
                                if (cont == enterprise.length) {
                                    if (cb) cb(err, enterprise);
                                }
                            })
                        }
                    } else {
                        if (cb) cb(err, enterprise);
                    }
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                async.map(results[1], function (enterprise, callback0) {
                    enterprise._doc.hasReservation = false;
                    if (enterprise._doc.service.length) {
                        var cont = 0;
                        for (var i = 0; i < enterprise._doc.service.length; i++) {
                            serviceController.getReservationClientByIdService(enterprise._doc.service[i], i, function (reservsClient, pos) {
                                if (reservsClient.length) {
                                    enterprise._doc.hasReservation = true;
                                }
                                cont++;
                                if (cont == enterprise._doc.service.length) {
                                    callback0(null, enterprise);
                                }
                            })
                        }
                    } else {
                        callback0(null, enterprise);
                    }
                }, function (err, result) {
                    return res.json({res: result, cont: results[0]});
                })
            }
        })
    } else {
        async.parallel([
            function (cb) {
                db.Enterprise.count({remove: false}).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                db.Enterprise.find({remove: false}).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, enterprise) {
                    if (enterprise.length > 0) {
                        var cont = 0;
                        for (var i = 0; i < enterprise.length; i++) {
                            exports.getServicesByEnterprise(enterprise[i]._id, i, function (services, pos) {
                                var arrayServices = new Array();
                                for (var j = 0; j < services.length; j++) {
                                    arrayServices.push(services[j]._doc._id);
                                }
                                enterprise[pos]._doc.service = arrayServices;
                                cont++;
                                if (cont == enterprise.length) {
                                    if (cb) cb(err, enterprise);
                                }
                            })
                        }
                    } else {
                        if (cb) cb(err, enterprise);
                    }
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                async.map(results[1], function (enterprise, callback0) {
                    enterprise._doc.hasReservation = false;
                    if (enterprise._doc.service.length) {
                        var cont = 0;
                        for (var i = 0; i < enterprise._doc.service.length; i++) {
                            serviceController.getReservationClientByIdService(enterprise._doc.service[i], i, function (reservsClient, pos) {
                                if (reservsClient.length) {
                                    enterprise._doc.hasReservation = true;
                                }
                                cont++;
                                if (cont == enterprise._doc.service.length) {
                                    callback0(null, enterprise);
                                }
                            })
                        }
                    } else {
                        callback0(null, enterprise);
                    }
                }, function (err, result) {
                    return res.json({res: result, cont: results[0]});
                })
            }
        })
    }
};

exports.get = function (req, res) {
    db.Enterprise.find({_id: req.params.id, remove: false, available: true}).exec(function (err, data) {
        if (err || !data) {
            return res.json({res: false});
        }
        else {
            return res.json({res: data});
        }
    })
};

exports.changeStatus = function (req, res) {
    var id = req.body.id;
    if (req.body.action == 2) {
        db.Enterprise.update({_id: id, remove: false}, {$set: {available: true}}).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true, complete: true})
            }
        })
    } else {
        exports.getEnterpriseForId(id, 0, function (enterprise, pos) {
            if (enterprise) {
                exports.getServicesAvailableByEnterprise(enterprise._doc._id, 0, function (services, pos) {
                    if (services.length) {
                        async.map(services, function (service, callback) {
                            var aux = {
                                id: service._doc._id,
                                name: service._doc.name,
                                packs: []
                            };
                            serviceController.getPackagesAvailablesByService(service._doc._id.toString(), 0, function (packages, pos) {
                                if (packages.length) {
                                    async.map(packages, function (pack, callback) {
                                        async.parallel([
                                            function (cb) {
                                                toursController.getTour(pack._doc.tour, function (tour) {
                                                    if (tour) {
                                                        var aux = {
                                                            tour: {
                                                                name: tour._doc.name,
                                                                group: tour._doc.group
                                                            }
                                                        };
                                                        cb(null, aux);
                                                    } else {
                                                        cb(null, false);
                                                    }
                                                })
                                            },
                                            function (cb) {
                                                async.map(pack._doc.services, function (service, callback1) {
                                                    db.Service.findOne({_id: service}).select('name').exec(function (err, servs) {
                                                        var aux = {
                                                            id: servs._doc._id,
                                                            name: servs._doc.name
                                                        };
                                                        callback1(err, aux);
                                                    })
                                                }, function (err, result) {
                                                    if (err || !result) {
                                                        cb(err, result);
                                                    }
                                                    else {
                                                        var aux = {
                                                            services: result
                                                        };
                                                        cb(null, aux);
                                                    }
                                                })
                                            }
                                        ], function (err, result) {
                                            if (err || !result) {
                                                callback(err, result);
                                            } else {
                                                var aux = {
                                                    id: pack._doc._id,
                                                    datTour: result[0],
                                                    datService: result[1]
                                                };
                                                callback(null, aux);
                                            }
                                        })
                                    }, function (err, result) {
                                        if (err || !result) {
                                            return res.json({res: false});
                                        }
                                        else {
                                            aux.packs = result;
                                            callback(null, aux);
                                        }
                                    })
                                } else {
                                    callback(null, aux);
                                }
                            })
                        }, function (err, result) {
                            if (err || !result) {
                                return res.json({res: false});
                            } else {
                                if (req.body.action == 0) {
                                    var packs = [];
                                    for (var i = 0; i < result.length; i++) {
                                        for (var j = 0; j < result[i].packs.length; j++) {
                                            var flag = false;
                                            for (var k = 0; k < packs.length; k++) {
                                                if (packs[k].id.toString() == result[i].packs[j].id.toString()) {
                                                    flag = true;
                                                    break;
                                                }
                                            }
                                            if (!flag) {
                                                packs.push(result[i].packs[j]);
                                            }
                                        }
                                    }
                                    var aux = {
                                        services: result,
                                        packs: packs
                                    };
                                    return res.json({res: aux, complete: false});
                                } else {
                                    async.map(result, function (service, callback) {
                                        if (service.packs.length) {
                                            async.map(service.packs, function (pack, callback1) {
                                                db.Pack.update({_id: pack.id}, {$set: {status: false}}, function (err, success) {
                                                    callback1(err, success)
                                                })
                                            }, function (err, result) {
                                                if (err || !result) {
                                                    return res.json({res: false, complete: false});
                                                }
                                                else {
                                                    db.Service.update({
                                                        _id: service
                                                    }, {$set: {status: false}}).exec(function (err, success) {
                                                        if (err || !success) {
                                                            return res.json({res: false})
                                                        }
                                                        else {
                                                            callback(err, success);
                                                        }
                                                    })
                                                }
                                            })
                                        } else {
                                            db.Service.update({_id: service}, {$set: {status: false}}, function (err, success) {
                                                callback(err, success);
                                            })
                                        }
                                    }, function (err, result) {
                                        if (err || !result) {
                                            return res.json({res: false, complete: false});
                                        } else {
                                            db.Enterprise.update({
                                                _id: id,
                                                remove: false
                                            }, {$set: {available: false}}).exec(function (err, success) {
                                                if (err || !success) {
                                                    return res.json({res: false});
                                                }
                                                else {
                                                    return res.json({res: true, complete: true})
                                                }
                                            })
                                        }
                                    })
                                }
                            }
                        })
                    } else {
                        db.Enterprise.update({
                            _id: id,
                            remove: false
                        }, {$set: {available: false}}).exec(function (err, success) {
                            if (err || !success) {
                                return res.json({res: false});
                            }
                            else {
                                return res.json({res: true, complete: true})
                            }
                        })
                    }
                })
            } else {
                return res.json({res: false});
            }
        })
    }
};

exports.getEnterpriseForId = function (id, pos, cb) {
    db.Enterprise.findOne({_id: id}).exec(function (err, data) {
        if (err || !data) {
            if (cb) cb(false, pos);
        }
        else {
            if (cb) cb(data, pos);
        }
    })
};

exports.update = function (req, res) {
    var id = req.body.id,
        name = req.body.name,
        description = req.body.description,
        url = req.body.url,
        slug = {
            es: utils.getSlug(name.es),
            en: utils.getSlug(name.en)
        }

    db.Enterprise.update({_id: id}, {
        $set: {
            name: name,
            description: description,
            url: url,
            slug: slug
        }
    }).exec(function (err, data) {
        if (err || !data) {
            return res.json({res: false});
        }
        else {
            return res.json({res: true});
        }
    });
};

exports.updateImg = function (req, res) {
    var idEnterprise = req.body.id;
    var nameImgDel = req.body.nameImgDel;
    var files = req.files;

    var photo = {
        path: "",
        name: "",
        originalName: "",
        extension: ""
    };

    var typeImgs = ["image/jpeg", "image/png"];
    var correct_photo = false;

    if (files.file0.mimetype != "" || files.file0.mimetype != null) {
        if (utils.validateFile(files.file0.mimetype, typeImgs)) {
            photo.name = files.file0.name;
            photo.path = "/api/media/" + files.file0.name;
            photo.originalName = files.file0.originalname;
            photo.extension = files.file0.extension;
            correct_photo = true;
        }
    }

    if (!correct_photo) {
        utils.deleteFile(files.file0.name);
        return res.json({error: 'incorrect image!!!'});
    } else {
        db.Enterprise.findOne({_id: idEnterprise, remove: false}).exec(function (err, success) {
            if (success != null) {
                db.Media.remove({fieldName: nameImgDel}).exec(function (err, success) {
                    if (success != null) {
                        db.Enterprise.update({_id: idEnterprise, remove: false}, {$set: {photo: photo}}).exec(
                            function (err, success) {
                                if (success) {
                                    return res.json({result: true});
                                } else {
                                    return res.json({result: false});
                                }
                            });
                    }
                    else {
                        return res.json({result: false})
                    }
                })
            }
            else {
                return res.json({result: false})
            }
        })
    }
};

exports.listServicesByEnterprise = function (req, res) {
    var id = req.body.id;
    exports.getEnterpriseForId(id, 0, function (enterprise, pos) {
        if (enterprise) {
            exports.getServicesByEnterprise(enterprise._doc._id, 0, function (services, pos) {
                if (services.length) {
                    async.map(services, function (service, callback) {
                        var aux = {
                            id: service._doc._id,
                            name: service._doc.name,
                            packs: []
                        };
                        serviceController.getPackagesByService(service._doc._id.toString(), 0, function (packages, pos) {
                            if (packages.length) {
                                async.map(packages, function (pack, callback) {
                                    async.parallel([
                                        function (cb) {
                                            toursController.getTour(pack._doc.tour, function (tour) {
                                                if (tour) {
                                                    var aux = {
                                                        tour: {
                                                            name: tour._doc.name,
                                                            group: tour._doc.group
                                                        }
                                                    };
                                                    cb(null, aux);
                                                } else {
                                                    cb(null, false);
                                                }
                                            })
                                        },
                                        function (cb) {
                                            async.map(pack._doc.services, function (service, callback1) {
                                                db.Service.findOne({_id: service}).select('name').exec(function (err, servs) {
                                                    var aux = {
                                                        id: servs._doc._id,
                                                        name: servs._doc.name
                                                    };
                                                    callback1(err, aux);
                                                })
                                            }, function (err, result) {
                                                if (err || !result) {
                                                    cb(err, result);
                                                }
                                                else {
                                                    var aux = {
                                                        services: result
                                                    };
                                                    cb(null, aux);
                                                }
                                            })
                                        }
                                    ], function (err, result) {
                                        if (err || !result) {
                                            callback(err, result);
                                        } else {
                                            var aux = {
                                                id: pack._doc._id,
                                                datTour: result[0],
                                                datService: result[1]
                                            };
                                            callback(null, aux);
                                        }
                                    })
                                }, function (err, result) {
                                    if (err || !result) {
                                        return res.json({res: false});
                                    }
                                    else {
                                        aux.packs = result;
                                        callback(null, aux);
                                    }
                                })
                            } else {
                                callback(null, aux);
                            }
                        })
                    }, function (err, result) {
                        if (err || !result) {
                            return res.json({res: false});
                        } else {
                            var packs = [];
                            for (var i = 0; i < result.length; i++) {
                                for (var j = 0; j < result[i].packs.length; j++) {
                                    var flag = false;
                                    for (var k = 0; k < packs.length; k++) {
                                        if (packs[k].id.toString() == result[i].packs[j].id.toString()) {
                                            flag = true;
                                            break;
                                        }
                                    }
                                    if (!flag) {
                                        packs.push(result[i].packs[j]);
                                    }
                                }
                            }
                            var aux = {
                                services: result,
                                packs: packs
                            };
                            return res.json({res: aux});
                        }
                    })
                } else {
                    return res.json({res: services});
                }
            })
        } else {
            return res.json({res: false});
        }
    })
};

exports.delete = function (req, res) {
    var id = req.params.id;
    exports.getEnterpriseForId(id, 0, function (enterprise, pos) {
        if (enterprise) {
            exports.getServicesByEnterprise(enterprise._doc._id, 0, function (services, pos) {
                if (services.length) {
                    async.map(services, function (service, callback) {
                        serviceController.getPackagesByService(service._doc._id.toString(), 0, function (packages, pos) {
                            if (packages.length) {
                                async.map(packages, function (pack, callback1) {
                                    db.Pack.update({_id: pack._doc._id}, {$set: {remove: true}}).exec(function (err, success) {
                                        callback1(err, success)
                                    })
                                }, function (err, result) {
                                    if (err || !result) {
                                        callback(err, result);
                                    } else {
                                        var photos = service._doc.photos;
                                        if (photos.length > 0) {
                                            var cont = 0;
                                            for (var j = 0; j < photos.length; j++) {
                                                db.Media.remove({fieldName: photos[j].name}).exec(function (err, success) {
                                                    if (err || !success) {
                                                        callback(err, success);
                                                    }
                                                    else {
                                                        cont++;
                                                        if (cont == photos.length) {
                                                            db.Service.update({_id: service._doc._id}, {$set: {remove: true}}).exec(function (err, success) {
                                                                callback(err, success);
                                                            })
                                                        }
                                                    }
                                                })
                                            }
                                        }
                                        else {
                                            db.Service.update({_id: service._doc._id}, {$set: {remove: true}}).exec(function (err, success) {
                                                callback(err, success);
                                            })
                                        }
                                    }
                                })
                            } else {
                                var photos = service._doc.photos;
                                if (photos.length > 0) {
                                    var cont = 0;
                                    for (var j = 0; j < photos.length; j++) {
                                        db.Media.remove({fieldName: photos[j].name}).exec(function (err, success) {
                                            if (err || !success) {
                                                callback(err, success);
                                            }
                                            else {
                                                cont++;
                                                if (cont == photos.length) {
                                                    db.Service.update({_id: service._doc._id}, {$set: {remove: true}}).exec(function (err, success) {
                                                        callback(err, success);
                                                    })
                                                }
                                            }
                                        })
                                    }
                                }
                                else {
                                    db.Service.update({_id: service._doc._id}, {$set: {remove: true}}).exec(function (err, success) {
                                        callback(err, success);
                                    })
                                }
                            }
                        })
                    }, function (err, result) {
                        if (err || !result) {
                            return res.json({res: false});
                        } else {
                            var photo = enterprise._doc.photo;
                            if (photo) {
                                db.Media.remove({fieldName: photo.name}).exec(function (err, success) {
                                    if (err || !success) {
                                        return res.json({res: false});
                                    } else {
                                        db.Enterprise.update({_id: id}, {$set: {remove: true}}).exec(function (err, success) {
                                            if (err || !success) {
                                                return res.json({res: false});
                                            }
                                            else {
                                                return res.json({res: true});
                                            }
                                        })
                                    }
                                })
                            } else {
                                db.Enterprise.update({_id: id}, {$set: {remove: true}}).exec(function (err, success) {
                                    if (err || !success) {
                                        return res.json({res: false});
                                    }
                                    else {
                                        return res.json({res: true});
                                    }
                                })
                            }
                        }
                    })
                } else {
                    var photo = enterprise._doc.photo;
                    if (photo) {
                        db.Media.remove({fieldName: photo.name}).exec(function (err, success) {
                            if (err || !success) {
                                return res.json({res: false});
                            } else {
                                db.Enterprise.update({_id: id}, {$set: {remove: true}}).exec(function (err, success) {
                                    if (err || !success) {
                                        return res.json({res: false});
                                    }
                                    else {
                                        return res.json({res: true});
                                    }
                                })
                            }
                        })
                    } else {
                        db.Enterprise.update({_id: id}, {$set: {remove: true}}).exec(function (err, success) {
                            if (err || !success) {
                                return res.json({res: false});
                            }
                            else {
                                return res.json({res: true});
                            }
                        })
                    }
                }
            })
        } else {
            return res.json({res: false});
        }
    })
};

exports.getServicesByEnterprise = function (enterprise, pos, cb) {
    db.Service.find({enterprise: enterprise, remove: false}).exec(function (err, services) {
        if (err || !services) {
            if (cb) cb(new Array(), pos);
        }
        else {
            if (cb)cb(services, pos);
        }
    })
};

exports.getServicesAvailableByEnterprise = function (enterprise, pos, cb) {
    db.Service.find({enterprise: enterprise, status: true, remove: false}).exec(function (err, services) {
        if (err || !services) {
            if (cb) cb(new Array(), pos);
        }
        else {
            if (cb)cb(services, pos);
        }
    })
};