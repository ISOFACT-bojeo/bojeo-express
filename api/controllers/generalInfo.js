/**
 * Created by ernestomr87@gmail.com on 26/07/14.
 */
var htmlToText = require('html-to-text');
var utils = require('../utils');

exports.create = function (req, res) {
    // validate email

    var body = req.body;

    var aboutBojeo = {
        es: body.aboutBojeo_es,
        en: body.aboutBojeo_es
    };
    var payForms = {
        es: body.payForms_es,
        en: body.payForms_en
    };
    var devolutions = {
        es: body.devolutions_es,
        en: body.devolutions_en
    };
    var useTerms = {
        es: body.useTerms_es,
        en: body.useTerms_en
    };
    var searchUseText = {
        es: htmlToText.fromString(useTerms.es),
        en: htmlToText.fromString(useTerms.en)
    };
    var searchAboutText = {
        es: htmlToText.fromString(aboutBojeo.es),
        en: htmlToText.fromString(aboutBojeo.en)
    };

    var general = new db.GeneralInfo({
        aboutBojeo: aboutBojeo,
        payForms: payForms,
        devolutions: devolutions,
        useTerms: useTerms,
        searchUseText: searchUseText,
        searchAboutText: searchAboutText
    });

    general.save(function (err, success) {
        if (success) {
            return res.json(true);
        } else {
            return res.json(err);
        }
    });
};
exports.list = function (req, res) {
    db.GeneralInfo.find().exec(function (err, success) {
        if (success) {
            return res.json(success);
        } else {
            return res.json(err);
        }
    })

};
exports.update = function (req, res) {
    var body = req.body;
    var obj = {};

    if (body.aboutBojeo_es) {
        obj.aboutBojeo = {
            es: body.aboutBojeo_es,
            en: body.aboutBojeo_en
        };
        obj.searchAboutText = {
            es: htmlToText.fromString(obj.aboutBojeo.es),
            en: htmlToText.fromString(obj.aboutBojeo.en)
        }
    }
    if (body.payForms_es) {
        obj.payForms = {
            es: body.payForms_es,
            en: body.payForms_en
        }
    }

    if (body.devolutions_es) {
        obj.devolutions = {
            es: body.devolutions_es,
            en: body.devolutions_en
        }
    }

    if (body.useTerms_es) {
        obj.useTerms = {
            es: body.useTerms_es,
            en: body.useTerms_en
        };
        obj.searchUseText = {
            es: htmlToText.fromString(obj.useTerms.es),
            en: htmlToText.fromString(obj.useTerms.en)
        }
    }

    if (body.privacyPolicy_es) {
        obj.privacyPolicy = {
            es: body.privacyPolicy_es,
            en: body.privacyPolicy_en
        }
    }

    if (body.politicCookies_es) {
        obj.politicCookies = {
            es: body.politicCookies_es,
            en: body.politicCookies_en
        }
    }

    if (body.noticeCookies_es) {
        obj.noticeCookies = {
            es: body.noticeCookies_es,
            en: body.noticeCookies_en
        }
    }

    if (body.copyRights_es) {
        obj.copyRights = {
            es: body.copyRights_es,
            en: body.copyRights_en
        }
    }

    var id = body.ids;
    db.GeneralInfo.update({_id: id}, {$set: obj},
        function (err, success) {
            if (success) {
                return res.json(true);
            } else {
                return res.json(err);
            }
        });
};
exports.remove = function (req, res) {
    var id = req.params.id;

    db.GeneralInfo.remove({_id: id}, function (err, success) {

        if (success) {
            return res.json(success);
        } else {
            return res.json(err);
        }
    });
};
exports.publicated = function (req, res) {
    var id = req.body.id;
    var active = req.body.active;
    db.GeneralInfo.update({active: true}, {$set: {active: false}}).exec(function (err, success) {
        if (success != null) {
            db.GeneralInfo.update({_id: id}, {$set: {active: active}},
                function (err, success) {
                    if (success) {
                        return res.json(true);
                    } else {
                        return res.json(err);
                    }
                });
        }
        else return res.json(false);
    })

};
exports.getInfo = function (req, res) {
    try {
        db.GeneralInfo.findOne({active: true}).exec(function (err, success) {
            if (success) {
                if (req.body.type == 'aboutBojeo') {
                    var aux = {
                        aboutBojeo: success._doc.aboutBojeo,
                        photos: success._doc.photos
                    }
                    return res.json({res: aux});
                }
                else if (req.body.type == 'useTerms') {
                    return res.json({res: success._doc.useTerms});
                }
                else if (req.body.type == 'devolutions') {
                    return res.json({res: success._doc.devolutions});
                }
                else if (req.body.type == 'payForms') {
                    return res.json({res: success._doc.payForms});
                }
                else if (req.body.type == 'privacyPolicy') {
                    return res.json({res: success._doc.privacyPolicy});
                }
                else if (req.body.type == 'politicCookies') {
                    return res.json({res: success._doc.politicCookies});
                }
                else if (req.body.type == 'noticeCookies') {
                    return res.json({res: success._doc.noticeCookies});
                }
                else if (req.body.type == 'copyRights') {
                    return res.json({res: success._doc.copyRights});
                }
            }
            else {
                return res.json({res: false});
            }
        })
    }
    catch (err) {
        return res.json({res: false});
    }
};

exports.updateImg = function (req, res) {
    var idGeneralInfo = req.body.id;
    var nameImgDel = eval(req.body.nameImgDel);
    var files = req.files;

    var typeImgs = ["image/jpeg", "image/png"];

    if (nameImgDel.length != 0) {
        var cont = 0;
        for (var i = 0; i < nameImgDel.length; i++) {
            db.Media.remove({fieldName: nameImgDel[i]}).exec(function (err, success) {
                if (err ) {
                    return res.json({result: false});
                } else {
                    cont++;
                    if (cont == nameImgDel.length) {
                        db.GeneralInfo.findOne({_id: idGeneralInfo}).exec(function (err, generalInfo) {
                            if (err || !generalInfo) {
                                return res.json({result: false});
                            } else {
                                for (var i = 0; i < nameImgDel.length; i++) {
                                    var pos = 0;
                                    var flag = false;
                                    var cantidad = generalInfo.photos.length;
                                    while (!flag && pos < cantidad) {
                                        if (generalInfo.photos[pos].name == nameImgDel[i]) {
                                            generalInfo.photos.splice(pos, 1);
                                            flag = true;
                                        }
                                        pos++;
                                    }
                                }
                                var pos = 0;
                                while (eval("files.file" + pos)) {
                                    var tempFile = (eval("files.file" + pos));
                                    if (tempFile.mimetype != "" || tempFile.mimetype != null) {
                                        if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                                            var photo = {
                                                path: "/api/media/" + tempFile.name,
                                                name: tempFile.name,
                                                originalName: tempFile.originalname,
                                                extension: tempFile.extension
                                            };
                                            generalInfo.photos.push(photo);
                                        } else {
                                            utils.deleteFile(tempFile.name);
                                        }
                                    } else {
                                        utils.deleteFile(tempFile.name);
                                    }
                                    pos++;
                                }
                                db.GeneralInfo.update({_id: idGeneralInfo}, {$set: {photos: generalInfo.photos}}).exec(
                                    function (err, success) {
                                        if (err || !success) {
                                            return res.json({result: false});
                                        } else {
                                            return res.json({result: true});
                                        }
                                    })
                            }
                        })
                    }
                }
            })
        }
    } else {
        db.GeneralInfo.findOne({_id: idGeneralInfo}).exec(function (err, generalInfo) {
            if (err || !generalInfo) {
                return res.json({result: false});
            } else {
                var pos = 0;
                while (eval("files.file" + pos)) {
                    var tempFile = (eval("files.file" + pos));
                    if (tempFile.mimetype != "" || tempFile.mimetype != null) {
                        if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                            var photo = {
                                path: "/api/media/" + tempFile.name,
                                name: tempFile.name,
                                originalName: tempFile.originalname,
                                extension: tempFile.extension
                            };
                            generalInfo.photos.push(photo);
                        } else {
                            utils.deleteFile(tempFile.name);
                        }
                    } else {
                        utils.deleteFile(tempFile.name);
                    }
                    pos++;
                }
                db.GeneralInfo.update({_id: idGeneralInfo}, {$set: {photos: generalInfo.photos}}).exec(
                    function (err, success) {
                        if (err || !success) {
                            return res.json({result: false});
                        } else {
                            return res.json({result: true});
                        }
                    })
            }
        })
    }
};

exports.addMedia = function (req, res) {
    var attachBody = req.body;
    var files = req.files;
    var id = attachBody.id;
    var info = null;
    var images = [];
    var extImages = ['jpg', 'png', 'gif', 'bmp'];
    var photos = [];
    var pos = 0;
    while (eval("files.file" + pos)) {
        var tempFile = (eval("files.file" + pos));
        for (var j = 0; j < extImages.length; j++) {
            var image = {
                path: null
            }
            if (tempFile.extension.toLowerCase() == extImages[j]) {
                image.name = tempFile.name;
                image.path = "/api/media/" + tempFile.name;
                photos.push(image);
                images.push(image);
            }
        }
        pos++;
    }

    db.GeneralInfo.findOne({_id: id}).exec(function (err, success) {
        if (success) {
            info = success;
        }
        if (info.photos.length != 0) {
            for (var j = 0; j < info.photos.length; j++) {
                photos.push(info.photos[j]);
            }
        }

        db.GeneralInfo.update({_id: id}, {$set: {photos: photos}}).exec(
            function (err, success) {
                if (success) {
                    return res.json({result: images});
                } else {
                    return res.json({result: false});
                }
            });
    })
};
exports.removeOnlyMedia = function (req, res) {
    db.GeneralInfo.findOne({_id: req.body.id}).select("photos").exec(function (err, success) {
        if (success != null) {
            var photos = success.photos;
            var newPhotos = [];
            for (var i = 0; i < photos.length; i++) {
                if (photos[i].name != req.body.name) {
                    newPhotos.push(photos[i]);
                }
            }
            db.GeneralInfo.update({_id: req.body.id}, {$set: {photos: newPhotos}}).exec(function (err, success) {
                if (success != null) {
                    db.Media.remove({fieldName: req.body.name}).exec(function (err, success) {
                        if (success != null) {
                            return res.json({result: true});
                        }
                        else {
                            return res.json({result: false});
                        }
                    })

                }
                else {
                    return res.json({result: false});
                }
            })

        }
        else {
            return res.json({result: false});
        }
    })
};