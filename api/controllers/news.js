/**
 * Created by ernestomr87@gmail.com on 14/05/14.
 */

var utils = require('../utils');
var async = require('async');
var htmlToText = require('html-to-text');

exports.create = function (req, res) {
    // validate email
    var news = req.body;
    var date = new Date();

    var files = req.files;

    var photo = {
        path: "",
        name: "",
        originalName: "",
        extension: ""
    };
    var doc = {
        path: "",
        name: "",
        originalName: "",
        extension: ""
    };

    var typeImgs = ["image/jpeg", "image/png"];
    var typeDocs = new Array("application/pdf", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    var correct_photo = false;
    var correct_doc = false;

    if (files.file0.mimetype != "" || files.file0.mimetype != null) {
        if (utils.validateFile(files.file0.mimetype, typeImgs)) {
            photo.name = files.file0.name;
            photo.path = "/api/media/" + files.file0.name;
            photo.originalName = files.file0.originalname;
            photo.extension = files.file0.extension;
            correct_photo = true;
        }
    }

    if (!correct_photo) {
        utils.deleteFile(files.file0.name)
        return res.json({error: 'incorrect image!!!'});
    }

    if (files.file1 != null) {
        if (files.file1.mimetype != "" || files.file1.mimetype != null) {
            if (utils.validateFile(files.file1.mimetype, typeDocs)) {
                doc.name = files.file1.name;
                doc.path = "/api/media/" + files.file1.name;
                doc.originalName = files.file1.originalname;
                doc.extension = files.file1.extension;
                correct_doc = true;
            }
        }
        if (!correct_doc) {
            utils.deleteFile(files.file1.name)
            return res.json({error: 'incorrect documents!!!'});
        }
    }

    var title = {
        es: news.title_es,
        en: news.title_en
    };
    var intro = {
        es: news.introduction_es,
        en: news.introduction_en
    };
    var body = {
        es: news.body_es,
        en: news.body_en
    };
    var imageFooter = {
        es: null,
        en: null
    };
    if (news.imageFooter_es != 'undefined') {
        imageFooter.es = news.imageFooter_es;
    }
    if (news.imageFooter_en != 'undefined') {
        imageFooter.en = news.imageFooter_en;
    }
    var searchText = {
        es: htmlToText.fromString(body.es),
        en: htmlToText.fromString(body.en)
    };
    var originalAutor = null;
    if (news.originalAutor != 'undefined') {
        originalAutor = news.originalAutor;
    }
    var video = null;
    if (news.video != 'undefined') {
        video = news.video;
    }

    var newNews = new db.News({
        createDate: date,
        title: title,
        introduction: intro,
        body: body,
        imageFooter: imageFooter,
        video: video,
        autor: req.user.email,
        originalAutor: originalAutor,
        photo: photo,
        document: doc,
        searchText: searchText,
        slug: {
            en: utils.getSlug(title.en),
            es: utils.getSlug(title.es)
        }
    });
    newNews.save(function (err, success) {
        if (success) {
            return res.json({res: true});
        } else {
            return res.json({error: err});
        }
    });
};
exports.list = function (req, res) {
    var limit = req.query.limit,
        skip = req.query.skip,
        text = req.query.text;
    if (text.length > 0) {
        async.parallel([
            function (cb) {
                db.News.count({status: true}).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                text = new RegExp(text, 'i');
                db.News.find({
                    status: true,
                    $or: [
                        {'title.es': text},
                        {'title.en': text},
                        {'introduction.es': text},
                        {'introduction.en': text},
                        {'body.es': text},
                        {'body.en': text},
                        {video: text},
                        {autor: text},
                        {originalAutor: text},
                        {company: text}
                    ]
                }).sort('-createDate').limit(limit).skip(skip).exec(function (err, contacts) {
                    if (cb) cb(err, contacts);
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                return res.json({res: results[1], cont: results[0]});
            }
        });
    }
    else {
        async.parallel([
            function (cb) {
                db.News.count({status: true}).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                db.News.find({status: true}).sort('-createDate').limit(limit).skip(skip).exec(function (err, contacts) {
                    if (cb) cb(err, contacts);
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                return res.json({res: results[1], cont: results[0]});
            }
        });
    }
};
exports.listBack = function (req, res) {
    var limit = req.body.limit,
        skip = req.body.skip,
        text = req.body.text;
    if (text.length > 0) {
        async.parallel([
            function (cb) {
                db.News.count().exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                text = new RegExp(text, 'i');
                db.News.find({
                    $or: [
                        {'title.es': text},
                        {'title.en': text},
                        {'introduction.es': text},
                        {'introduction.en': text},
                        {'body.es': text},
                        {'body.en': text},
                        {video: text},
                        {autor: text},
                        {originalAutor: text},
                        {company: text}
                    ]
                }).sort('-createDate').limit(limit).skip(skip).exec(function (err, contacts) {
                    if (cb) cb(err, contacts);
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                return res.json({res: results[1], cont: results[0]});
            }
        });
    }
    else {
        async.parallel([
            function (cb) {
                db.News.count().exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                db.News.find().sort('-createDate').limit(limit).skip(skip).exec(function (err, contacts) {
                    if (cb) cb(err, contacts);
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                return res.json({res: results[1], cont: results[0]});
            }
        });
    }
};
exports.get = function (req, res) {
    var id = req.params.id;
    db.News.find({_id: id, status: true}, function (err, success) {
        if (err || !success) {
            return res.json(err);
        } else {
            if (success.length == 0) {
                return res.json({cont: 0});
            } else {
                return res.json(success[0]._doc);
            }
        }
    });
};
exports.update = function (req, res) {
    var u = req.body;

    var title = {
        es: u.title_es,
        en: u.title_en
    };
    var intro = {
        es: u.introduction_es,
        en: u.introduction_en
    };
    var body = {
        es: u.body_es,
        en: u.body_en
    };
    var imageFooter = {
        es: u.imageFooter_es,
        en: u.imageFooter_en
    };
    var searchText = {
        es: htmlToText.fromString(body.es),
        en: htmlToText.fromString(body.en)
    };
    var id = u.ids,
        title = title,
        introduction = intro,
        body = body,
        imageFooter = imageFooter,
        originalAutor = u.originalAutor,
        searchText = searchText,
        video = u.video;

    var slug = utils.getSlug(title.en);
    db.News.update({_id: id}, {
            $set: {
                title: title, introduction: introduction, body: body, video: video,
                originalAutor: originalAutor, imageFooter: imageFooter,
                searchText: searchText,
                slug: slug
            }
        },
        function (err, success) {
            if (success) {
                return res.json({res: true});
            } else {
                return res.json({res: false});
            }
        });
};
exports.remove = function (req, res) {
    var id = req.params.id;
    db.News.find({_id: id}).exec(function (err, noticia) {
        if (noticia) {
            var photo = noticia[0]._doc.photo;
            if (photo) {
                db.Media.remove({fieldName: photo.name}).exec(function (err, success) {
                    if (err || !success) {
                        return res.json({res: false});
                    } else {
                        db.News.remove({_id: noticia[0]._doc._id}, function (err, success) {
                            if (success) {
                                return res.json({res: true});
                            } else {
                                return res.json({res: false});
                            }
                        })
                    }
                })
            }
            else {
                db.News.remove({_id: noticia[0]._doc._id}, function (err, success) {
                    if (success) {
                        return res.json({res: true});
                    } else {
                        return res.json({res: false});
                    }
                })
            }
        }
        else {
            return res.json({res: false});
        }
    });


};
exports.publicated = function (req, res) {
    var id = req.body.id;
    var status = req.body.status;

    db.News.update({_id: id}, {$set: {status: status}},
        function (err, success) {
            if (err || !success) {
                return res.json({res: false})
            }
            else {
                return res.json({res: true})
            }
        });
};
exports.updateImgDoc = function (req, res) {
    var idNew = req.body.id;
    var nameImgDel = req.body.nameImgDel;
    var nameDocDel = req.body.nameDocDel;
    var files = req.files;

    var photo = {
        path: "",
        name: "",
        originalName: "",
        extension: ""
    };
    var doc = {
        path: "",
        name: "",
        originalName: "",
        extension: ""
    };

    var newPhotos = {
        path: "",
        name: "",
        originalName: "",
        extension: ""
    };
    var newDoc = {
        path: "",
        name: "",
        originalName: "",
        extension: ""
    };

    var typeImgs = ["image/jpeg", "image/png"];
    var typeDocs = new Array("application/pdf", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    var correct_photo = false;
    var correct_doc = false;

    if (files.file0.mimetype != "" || files.file0.mimetype != null) {
        if (utils.validateFile(files.file0.mimetype, typeImgs)) {
            photo.name = files.file0.name;
            photo.path = "/api/media/" + files.file0.name;
            photo.originalName = files.file0.originalname;
            photo.extension = files.file0.extension;
            correct_photo = true;
        }
    }

    if (!correct_photo) {
        utils.deleteFile(files.file0.name);
        return res.json({error: 'incorrect image!!!'});
    }

    if (files.file1 != null) {
        if (files.file1.mimetype != "" || files.file1.mimetype != null) {
            if (utils.validateFile(files.file1.mimetype, typeDocs)) {
                doc.name = files.file1.name;
                doc.path = "/api/media/" + files.file1.name;
                doc.originalName = files.file1.originalname;
                doc.extension = files.file1.extension;
                correct_doc = true;
            }
        }
        if (!correct_doc) {
            utils.deleteFile(files.file1.name);
            return res.json({error: 'incorrect documents!!!'});
        }
    }

    if (nameImgDel) {
        db.News.findOne({_id: idNew}).exec(function (err, success) {
            if (success != null) {
                if (nameDocDel) {
                    db.News.update({_id: idNew}, {
                        $set: {
                            photo: newPhotos,
                            document: newDoc
                        }
                    }).exec(function (err, success) {
                        if (success != null) {
                            db.Media.remove({fieldName: nameImgDel}).exec(function (err, success) {
                                if (success != null) {
                                    db.Media.remove({fieldName: nameDocDel}).exec(function (err, success) {
                                        if (success != null) {
                                            if (correct_photo) {
                                                if (correct_doc) {
                                                    db.News.update({_id: idNew}, {
                                                        $set: {
                                                            photo: photo,
                                                            document: doc
                                                        }
                                                    }).exec(
                                                        function (err, success) {
                                                            if (success) {
                                                                return res.json({res: true});
                                                            } else {
                                                                return res.json({res: false});
                                                            }
                                                        });
                                                } else {
                                                    db.News.update({_id: idNew}, {$set: {photo: photo}}).exec(
                                                        function (err, success) {
                                                            if (success) {
                                                                return res.json({res: true});
                                                            } else {
                                                                return res.json({res: false});
                                                            }
                                                        });
                                                }
                                            } else {
                                                if (correct_doc) {
                                                    db.News.update({_id: idNew}, {$set: {document: doc}}).exec(
                                                        function (err, success) {
                                                            if (success) {
                                                                return res.json({res: true});
                                                            } else {
                                                                return res.json({res: false});
                                                            }
                                                        });
                                                }
                                            }
                                        }
                                        else {
                                            return res.json({res: false})
                                        }
                                    })
                                }
                                else {
                                    return res.json({res: false})
                                }
                            })
                        }
                        else {
                            return res.json({res: false})
                        }
                    })
                } else {
                    if (correct_doc) {
                        db.News.update({_id: idNew}, {$set: {document: doc}}).exec(
                            function (err, success) {
                                if (success) {
                                    return res.json({res: true});
                                } else {
                                    return res.json({res: false});
                                }
                            });
                    }
                    db.News.update({_id: idNew}, {$set: {photo: newPhotos}}).exec(function (err, success) {
                        if (success != null) {
                            db.Media.remove({fieldName: nameImgDel}).exec(function (err, success) {
                                if (success != null) {
                                    if (correct_photo) {
                                        db.News.update({_id: idNew}, {$set: {photo: photo}}).exec(
                                            function (err, success) {
                                                if (success) {
                                                    return res.json({res: true});
                                                } else {
                                                    return res.json({res: false});
                                                }
                                            });
                                    }
                                }
                                else {
                                    return res.json({res: false})
                                }
                            })
                        }
                        else {
                            return res.json({res: false})
                        }
                    })
                }
            }
            else {
                return res.json({res: false})
            }
        })
    } else {
        if (nameDocDel) {
            db.News.update({_id: idNew}, {$set: {document: newDoc}}).exec(function (err, success) {
                if (success != null) {
                    db.Media.remove({fieldName: nameDocDel}).exec(function (err, success) {
                        if (success != null) {
                            if (correct_doc) {
                                db.News.update({_id: idNew}, {$set: {document: doc}}).exec(
                                    function (err, success) {
                                        if (success) {
                                            return res.json({res: true});
                                        } else {
                                            return res.json({res: false});
                                        }
                                    });
                            }
                        }
                        else {
                            return res.json({res: false})
                        }
                    })
                }
                else {
                    return res.json({res: false})
                }
            })
        } else {
            if (correct_doc) {
                db.News.update({_id: idNew}, {$set: {document: doc}}).exec(
                    function (err, success) {
                        if (success) {
                            return res.json({res: true});
                        } else {
                            return res.json({res: false});
                        }
                    });
            }
        }
    }
};
exports.updateDoc = function (req, res) {
    var idNew = req.body.id;
    var nameDocDel = req.body.nameDocDel;
    var files = req.files;

    var newDoc = {
        path: "",
        name: "",
        originalName: "",
        extension: ""
    };

    var doc = {
        path: "",
        name: "",
        originalName: "",
        extension: ""
    };

    var typeDocs = new Array("application/pdf", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    var correct_doc = false;

    if (files.file0 != null) {
        if (files.file0.mimetype != "" || files.file0.mimetype != null) {
            if (utils.validateFile(files.file0.mimetype, typeDocs)) {
                doc.name = files.file0.name;
                doc.path = "/api/media/" + files.file0.name;
                doc.originalName = files.file0.originalname;
                doc.extension = files.file0.extension;
                correct_doc = true;
            }
        }
        if (!correct_doc) {
            utils.deleteFile(files.file0.name)
            return res.json({error: 'incorrect documents!!!'});
        }
    }
    if (nameDocDel) {
        db.News.update({_id: idNew}, {$set: {document: newDoc}}).exec(function (err, success) {
            if (success != null) {
                db.Media.remove({fieldName: nameDocDel}).exec(function (err, success) {
                    if (success != null) {
                        if (correct_doc) {
                            db.News.update({_id: idNew}, {$set: {document: doc}}).exec(
                                function (err, success) {
                                    if (success) {
                                        return res.json({res: true});
                                    } else {
                                        return res.json({res: false});
                                    }
                                });
                        }
                    }
                    else {
                        return res.json({res: false})
                    }
                })
            }
            else {
                return res.json({res: false})
            }
        })
    } else {
        if (correct_doc) {
            db.News.update({_id: idNew}, {$set: {document: doc}}).exec(
                function (err, success) {
                    if (success) {
                        return res.json({res: true});
                    } else {
                        return res.json({res: false});
                    }
                });
        }
    }
};
exports.removeOnlyImg = function (req, res) {
    db.News.findOne({_id: req.body.id}).exec(function (err, success) {
        if (success != null) {
            var newPhotos = null;
            db.News.update({_id: req.body.id}, {$set: {photo: newPhotos}}).exec(function (err, success) {
                if (success != null) {
                    db.Media.remove({fieldName: req.body.name}).exec(function (err, success) {
                        if (success != null) {
                            return res.json({res: true})
                        }
                        else {
                            return res.json({res: false})
                        }
                    })
                }
                else {
                    return res.json({res: false})
                }
            })
        }
        else {
            return res.json({res: false})
        }
    })
};
exports.removeOnlyDoc = function (req, res) {
    db.News.findOne({_id: req.body.id}).exec(function (err, success) {
        if (success != null) {
            var newDoc = {
                path: "",
                name: ""
            };
            db.News.update({_id: req.body.id}, {$set: {document: newDoc}}).exec(function (err, success) {
                if (success != null) {
                    db.Media.remove({fieldName: req.body.name}).exec(function (err, success) {
                        if (success != null) {
                            return res.json({res: true})
                        }
                        else {
                            return res.json({res: false})
                        }
                    })
                }
                else {
                    return res.json({res: false})
                }
            })
        }
        else {
            return res.json({res: false})
        }
    })
};


