/**
 * Created by ernestomr87@gmail.com on 13/07/2014.
 */
var utils = require('../utils');
var async = require('async');
var shipController = require('./ship');
var tourController = require('./tours');
var mailController = require('./email');
var userController = require('./user');
var portController = require('./ports');
var enterpriseController = require('./enterprise');
var serviceController = require('./service');
var nodemailer = require("nodemailer");
var timeline = require('../../api/controllers/timeLine');
var Redsys = require('./../tpv-helper/redsys').Redsys;
var request = require('request');
var path = require('path');
var urlUtil = require('url');

exports.saveSession = function (req, res) {
    try {
        var data = JSON.parse(req.body.token);
        req.session.sale = data;
        db.Sale.count({user: req.user._id}, function (err, count) {
            if (err) {
                return res.json({res: false, env: global.production});
            }
            else {
                if (count) {
                    if (data.payform) {
                        return res.json({res: true, env: global.production});
                    }
                    else {
                        db.Sale.update({user: req.user._id}, {
                            $set: {
                                bodySale: data,
                                date: new Date(),
                                signature: false
                            }
                        }, function (err, success) {
                            if (err || !success) {
                                return res.json({res: false, env: global.production});
                            }
                            else {
                                return res.json({res: true, env: global.production});
                            }
                        })
                    }

                }
                else {
                    var sale = new db.Sale({
                        user: req.user._id,
                        bodySale: data,
                        date: new Date(),
                        signature: false
                    });

                    sale.save(function (err, success) {
                        if (err || !success) {
                            return res.json({res: false, env: global.production});
                        }
                        else {
                            return res.json({res: true, env: global.production});
                        }
                    });
                }
            }
        });
    }
    catch (err) {
        return res.json({res: false});
    }
};
exports.create = function (req, res) {
    if (req.user.complete) {
        getLastSale(req, function (err, sale) {
            if (err || !sale) {
                var url = urlUtil.resolve(global.bojeoServer, req.user.language+"/profile/complete");
                res.redirect(url);
            }
            else {
                req.session.sale = sale._doc.bodySale;
                var flag = req.session.sale.payFor || 'tours';
                if (flag != 'bonds') {
                    var cantPerson = req.session.sale.cantPerson;
                    var id = req.session.sale.pack ? req.session.sale.pack : req.session.sale.id;
                    tourController.getTour(id, function (tour, pack) {
                        if (pack) {
                            async.map(pack._doc.services, function (onePack, callback0) {
                                db.Service.findOne({_id: onePack}, function (err, packDoc) {
                                    callback0(err, packDoc._doc.price);
                                })
                            }, function (err, result) {
                                if (err || !result) {
                                    var url = urlUtil.resolve(global.bojeoServer, req.user.language+"/occupied_booking");

                                    res.redirect(url);
                                }
                                else {
                                    var price = 0;
                                    var day = 1;
                                    for (var i = 0; i < tour._doc.data.price.value.length; i++) {
                                        if (tour._doc.data.price.value[i].duration == req.session.sale.duration) {
                                            price = parseInt(tour._doc.data.price.value[i].price);
                                            if (tour._doc.data.price.type == 'day') {
                                                day = parseInt(tour._doc.data.price.value[i].duration);
                                            }
                                            break;
                                        }
                                    }
                                    if (tour._doc.data.capacity.type == 'person') {
                                        price = price * cantPerson * day;

                                    }
                                    for (var i = 0; i < result.length; i++) {
                                        price = price + (result[i] * cantPerson * day);
                                    }
                                    var percent = 100;
                                    if (tour._doc.data.price.toPay) {
                                        percent = tour._doc.data.price.toPay;
                                    }
                                    ;

                                    price = (percent * price) / 100;
                                    req.session.sale.monto = price;
                                    var tempPrice = price.toFixed(2);
                                    if (global.production) {
                                        var payment = {
                                            "intent": "sale",
                                            "payer": {
                                                "payment_method": "paypal"
                                            },
                                            "redirect_urls": {
                                                "return_url": urlUtil.resolve(global.bojeoServer, "/api/reservation/returnone"),
                                                "cancel_url": urlUtil.resolve(global.bojeoServer, "/api/reservation/payCancel")
                                            },
                                            "transactions": [
                                                {
                                                    "item_list": {
                                                        "items": [
                                                            {
                                                                "name": tour._doc.name.es,
                                                                "price": tempPrice,
                                                                "currency": "EUR",
                                                                "quantity": 1
                                                            }
                                                        ]
                                                    },
                                                    "amount": {
                                                        "total": tempPrice,
                                                        "currency": "EUR"
                                                    },
                                                    "description": "Bojeo Tours",
                                                    "related_resources": [
                                                        {
                                                            "sale": {
                                                                "id": "4RR959492F879224U",

                                                                "state": "completed",
                                                                "amount": {
                                                                    "total": tempPrice,
                                                                    "currency": "EUR"
                                                                },

                                                                "links": [
                                                                    {
                                                                        "href": "https://api.sandbox.paypal.com/v1/payments/sale/4RR959492F879224U",
                                                                        "rel": "self",
                                                                        "method": "GET"
                                                                    },
                                                                    {
                                                                        "href": "https://api.sandbox.paypal.com/v1/payments/sale/4RR959492F879224U/refund",
                                                                        "rel": "refund",
                                                                        "method": "POST"
                                                                    },
                                                                    {
                                                                        "href": "https://api.sandbox.paypal.com/v1/payments/payment/PAY-17S8410768582940NKEE66EQ",
                                                                        "rel": "parent_payment",
                                                                        "method": "GET"
                                                                    }
                                                                ]
                                                            }
                                                        }
                                                    ]
                                                }
                                            ]
                                        };
                                        utils.payWithPaypal(req, res, payment);
                                    }
                                    else {
                                        exports.reservationReturn(req, res);
                                    }


                                }
                            })
                        }
                        else {
                            if (tour._doc.data.payForm != 'bonds') {
                                var price = 0;
                                for (var i = 0; i < tour._doc.data.price.value.length; i++) {
                                    if (tour._doc.data.price.value[i].duration == req.session.sale.duration) {
                                        price = parseInt(tour._doc.data.price.value[i].price);
                                        break;
                                    }
                                }

                                if (tour._doc.data.capacity.type == 'person') {
                                    price = price * cantPerson;
                                }

                                var percent = 100;
                                if (tour._doc.data.price.toPay) {
                                    percent = tour._doc.data.price.toPay;
                                }
                                ;
                                price = (percent * price) / 100;
                                req.session.sale.monto = price;
                                var tempPrice = price.toFixed(2);

                                if (global.production) {
                                    var payment = {
                                        "intent": "sale",
                                        "payer": {
                                            "payment_method": "paypal"
                                        },
                                        "redirect_urls": {
                                            "return_url": urlUtil.resolve(global.bojeoServer, "/api/reservation/returnone"),
                                            "cancel_url": urlUtil.resolve(global.bojeoServer, "/api/reservation/payCancel")
                                        },
                                        "transactions": [
                                            {
                                                "item_list": {
                                                    "items": [
                                                        {
                                                            "name": tour._doc.name.es,
                                                            "price": tempPrice,
                                                            "currency": "EUR",
                                                            "quantity": 1
                                                        }
                                                    ]
                                                },
                                                "amount": {
                                                    "total": tempPrice,
                                                    "currency": "EUR"
                                                },
                                                "description": "Bojeo Tours",
                                                "related_resources": [
                                                    {
                                                        "sale": {
                                                            "id": "4RR959492F879224U",

                                                            "state": "completed",
                                                            "amount": {
                                                                "total": tempPrice,
                                                                "currency": "EUR"
                                                            },

                                                            "links": [
                                                                {
                                                                    "href": "https://api.sandbox.paypal.com/v1/payments/sale/4RR959492F879224U",
                                                                    "rel": "self",
                                                                    "method": "GET"
                                                                },
                                                                {
                                                                    "href": "https://api.sandbox.paypal.com/v1/payments/sale/4RR959492F879224U/refund",
                                                                    "rel": "refund",
                                                                    "method": "POST"
                                                                },
                                                                {
                                                                    "href": "https://api.sandbox.paypal.com/v1/payments/payment/PAY-17S8410768582940NKEE66EQ",
                                                                    "rel": "parent_payment",
                                                                    "method": "GET"
                                                                }
                                                            ]
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    };
                                    utils.payWithPaypal(req, res, payment);
                                }
                                else {
                                    exports.reservationReturn(req, res);
                                }
                            }
                            else {
                                exports.reservationReturn(req, res);
                            }
                        }


                    });
                }
                else {
                    db.Bonds.findOne({
                        _id: req.session.sale.bond,
                        available: true,
                        remove: false
                    }).exec(function (err, success) {
                        if (success) {
                            req.session.sale.price = success._doc.price;
                            req.session.sale.hours = success._doc.hours;
                            var tempPrice = req.session.sale.price.toFixed(2);
                            if (global.production) {
                                var payment = {
                                    "intent": "sale",
                                    "payer": {
                                        "payment_method": "paypal"
                                    },
                                    "redirect_urls": {
                                        "return_url": urlUtil.resolve(global.bojeoServer, "/api/reservation/bonus/return"),
                                        "cancel_url": urlUtil.resolve(global.bojeoServer, "/api/reservation/cancel")

                                    },
                                    "transactions": [
                                        {
                                            "item_list": {
                                                "items": [
                                                    {
                                                        "name": "item",
                                                        "sku": "item",
                                                        "price": "1.00",
                                                        "currency": "USD",
                                                        "quantity": 1
                                                    }
                                                ]
                                            },
                                            "amount": {
                                                "total": tempPrice,
                                                "currency": "EUR"
                                            },
                                            "description": "This is the payment description."
                                        }
                                    ]
                                };

                                utils.payWithPaypal(req, res, payment);
                            }
                            else {
                                exports.reservationBonoReturn(req, res);
                            }

                        }
                        else {
                            var url =  urlUtil.resolve(global.bojeoServer, req.user.language+"/error_conection");
                            res.redirect(url);
                        }
                    })
                }
            }
        })
    }
    else {
        var url = urlUtil.resolve(global.bojeoServer, req.user.language+"/profile/complete");

        res.redirect(url);
    }
};
exports.list = function (req, res) {
    var limit = req.body.limit,
        skip = req.body.skip,
        id = req.body.id,
        text = req.body.text;

    if (req.body.type) {
        var type = true;
    }
    else {
        var type = false;
    }

    async.parallel([
        function (cb) {
            db.ReservationClient.count({cancel: type}).exec(function (err, count) {
                if (cb) cb(err, count);
            })
        },
        function (cb) {
            db.ReservationClient.find({
                cancel: type
            }).populate({
                path: 'reservation',
                options: {sort: {start: -1}}
            }).populate('user').limit(limit).skip(skip).exec(function (err, resCs) {
                if (err || !resCs) {
                    cb(err, resCs);
                }
                else {
                    async.map(resCs, function (resC, callbackM) {
                            async.parallel([
                                function (callbackP) {
                                    db.Tour.findOne({_id: resC._doc.reservation._doc.tour}).populate('port').populate('group').exec(function (err, tour) {
                                        if (err || !tour) {
                                            callbackP(err, tour);
                                        }
                                        else {
                                            var aux = {
                                                _id: tour._doc._id,
                                                name: tour._doc.name,
                                                group: {
                                                    _id: tour._doc.group._doc._id,
                                                    payform: tour._doc.group._doc.payform,
                                                    color: tour._doc.group._doc.color,
                                                    name: tour._doc.group._doc.name
                                                },
                                                port: {
                                                    _id: tour._doc.port._doc._id,
                                                    name: tour._doc.port._doc.name
                                                }
                                            }
                                            callbackP(null, aux);
                                        }
                                    });
                                },
                                function (callbackP) {
                                    db.Ship.findOne({_id: resC._doc.reservation._doc.ship}).exec(function (err, ship) {
                                        if (err || !ship) {
                                            callbackP(err, ship);
                                        }
                                        else {
                                            var aux = {
                                                _id: ship._doc._id,
                                                name: ship._doc.name
                                            };
                                            callbackP(null, aux);
                                        }
                                    });
                                },
                                function (callbackP) {
                                    var today = new Date();
                                    today = new Date(today.getFullYear(), today.getMonth(), today.getDate());
                                    var state = 'booked';
                                    if (resC._doc.cancel) {
                                        state = 'canceled';
                                    }
                                    if (!resC._doc.cancel && today > resC._doc.reservation._doc.start) {
                                        state = 'done';
                                    }
                                    callbackP(null, state);

                                }
                            ], function (err, results) {
                                if (err || !results) {
                                    callbackM(err, results);
                                }
                                else {
                                    resC._doc.reservation._doc.tour = results[0];
                                    resC._doc.reservation._doc.ship = results[1];
                                    resC._doc.reservation._doc.state = results[2];
                                    callbackM(null, resC);
                                }
                            });
                        }, function (err, result) {
                            cb(err, result);
                        }
                    )
                }
            })
        }
    ], function (err, results) {
        if (err || !results) {
            return res.json({res: [], cont: 0});
        } else {
            return res.json({res: results[1], cont: results[0]});
        }
    })

    //if (text.length > 0) {
    //    async.parallel([
    //        function (cb) {
    //            db.ReservationClient.count({cancel: type}).exec(function (err, count) {
    //                if (cb) cb(err, count);
    //            })
    //        },
    //        function (cb) {
    //            text = new RegExp(text, 'i');
    //            db.ReservationClient.find({
    //                cancel: type,
    //                $or: [
    //                    {'token': text},
    //                    {'client': text},
    //                    {'reason': text}
    //                ]
    //            }).sort('-createDate').limit(limit).skip(skip).exec(function (err, resC) {
    //                if (cb) cb(err, resC);
    //            })
    //        }
    //    ], function (err, results) {
    //        if (err || !results) {
    //            return res.json({res: false, cont: 0});
    //        }
    //        else {
    //            var cont = 0;
    //            if (results[1].length > 0) {
    //                for (var i = 0; i < results[1].length; i++) {
    //                    getReservationTour(results[1][i]._doc.reservation, i, function (resT, pos) {
    //                        if (resT) {
    //                            tourController.getTour(resT._doc.tour, function (tour) {
    //                                if (tour) {
    //                                    getGroups(tour._doc.group.id, function (group) {
    //                                        if (group) {
    //                                            var aux = {
    //                                                tour: {
    //                                                    id: tour._doc._id,
    //                                                    name: tour._doc.name,
    //                                                    port: tour._doc.port,
    //                                                    group: group
    //                                                },
    //                                                date: resT._doc.date,
    //                                                ship: resT._doc.ship,
    //                                                departure: resT._doc.departure,
    //                                                cantPerson: resT._doc.cantPerson,
    //                                                pack: resT._doc.pack
    //                                            };
    //
    //                                            results[1][pos]._doc.reservation = aux;
    //                                            cont++;
    //                                            if (cont == results[1].length) {
    //                                                return res.json({res: results[1], cont: results[0]});
    //                                            }
    //                                        }
    //                                        else {
    //                                            return res.json({res: false, cont: results[0]});
    //                                        }
    //                                    })
    //                                }
    //                                else {
    //                                    return res.json({res: false, cont: results[0]});
    //                                }
    //                            })
    //                        }
    //                        else {
    //                            return res.json({res: false, cont: results[0]});
    //                        }
    //                    })
    //                }
    //            }
    //            else {
    //                return res.json({res: false, cont: results[0]});
    //            }
    //        }
    //    });
    //}
    //else {
    //    async.parallel([
    //        function (cb) {
    //            if (id.length > 0) {
    //                async.parallel([
    //                    function (cb1) {
    //                        db.ReservationTour.find({ship: id}).exec(function (err, resT) {
    //                            if (err || !resT || resT.length == 0) {
    //                                if (cb1) cb1(err, resT);
    //                            }
    //                            else {
    //                                var cont = 0;
    //                                var cantidad = 0;
    //                                for (var i = 0; i < resT.length; i++) {
    //                                    db.ReservationClient.count({
    //                                        cancel: type,
    //                                        state: 0,
    //                                        reservation: resT[i]._doc._id
    //                                    }).exec(function (err, count) {
    //                                        cont++;
    //                                        cantidad += count;
    //                                        if (cont == resT.length) {
    //                                            if (cb1) cb1(err, cantidad);
    //                                        }
    //                                    })
    //                                }
    //                            }
    //                        })
    //                    },
    //                    function (cb1) {
    //                        db.ReservationTour.find({tour: id}).exec(function (err, resT) {
    //                            if (err || !resT || resT.length == 0) {
    //                                if (cb1) cb1(err, resT);
    //                            }
    //                            else {
    //                                var cont = 0;
    //                                var cantidad = 0;
    //                                for (var i = 0; i < resT.length; i++) {
    //                                    db.ReservationClient.count({
    //                                        cancel: type,
    //                                        state: 0,
    //                                        reservation: resT[i]._doc._id
    //                                    }).exec(function (err, count) {
    //                                        cont++;
    //                                        cantidad += count;
    //                                        if (cont == resT.length) {
    //                                            if (cb1) cb1(err, cantidad);
    //                                        }
    //                                    })
    //                                }
    //                            }
    //                        })
    //                    },
    //                    function (cb1) {
    //                        db.Tour.find({group: id, remove: false}, function (err, tours) {
    //                            if (err || !tours || tours.length == 0) {
    //                                if (cb1) cb1(err, tours);
    //                            } else {
    //                                var cont = 0;
    //                                for (var j = 0; j < tours.length; j++) {
    //                                    var cantidad = 0;
    //                                    exports.getResevationClientByTour(tours[j], j, function (err, reservations, pos1) {
    //                                        cantidad += reservations.length;
    //                                        cont++;
    //                                        if (cont == tours.length) {
    //                                            if (cb1) cb1(err, cantidad);
    //                                        }
    //                                    })
    //                                }
    //                            }
    //                        })
    //                    },
    //                    function (cb1) {
    //                        portController.getTourByPortAll(id, 0, function (tour, pos) {
    //                            if (tour.length == 0) {
    //                                if (cb1) cb1(null, tour);
    //                            } else {
    //                                var cont = 0;
    //                                for (var j = 0; j < tour.length; j++) {
    //                                    var cantidad = 0;
    //                                    exports.getResevationClientByTour(tour[j], j, function (err, reservations, pos1) {
    //                                        cantidad += reservations.length;
    //                                        cont++;
    //                                        if (cont == tour.length) {
    //                                            if (cb1) cb1(err, cantidad);
    //                                        }
    //                                    })
    //                                }
    //                            }
    //
    //                        })
    //                    },
    //                    function (cb1) {
    //                        db.ReservationClient.count({
    //                            cancel: type,
    //                            state: 0,
    //                            pack: id
    //                        }).exec(function (err, count) {
    //                            if (cb1) cb1(err, count);
    //                        })
    //                    },
    //                    function (cb1) {
    //                        db.ReservationClient.find({
    //                            cancel: type,
    //                            state: 0
    //                        }).exec(function (err, reservClient) {
    //                            if (err || !reservClient || reservClient.length == 0) {
    //                                if (cb1) cb1(err, 0);
    //                            } else {
    //                                var array = [];
    //                                for (var i = 0; i < reservClient.length; i++) {
    //                                    if (reservClient[i]._doc.services.length > 0) {
    //                                        for (var j = 0; j < reservClient[i]._doc.services.length; j++) {
    //                                            if (reservClient[i]._doc.services[j].id.toString() == id) {
    //                                                array.push(reservClient[i]);
    //                                                break;
    //                                            }
    //                                        }
    //                                    }
    //                                }
    //                                if (cb1) cb1(null, array.length);
    //                            }
    //                        })
    //                    },
    //                    function (cb1) {
    //                        db.Enterprise.findOne({_id: id, remove: false}).exec(function (err, enterprise) {
    //                            if (err || !enterprise) {
    //                                if (cb1) cb1(err, 0);
    //                            } else {
    //                                enterpriseController.getServicesByEnterprise(enterprise._id, 0, function (services, pos) {
    //                                    if (services.length) {
    //                                        var cont = 0;
    //                                        var reservClient = [];
    //                                        for (var i = 0; i < services.length; i++) {
    //                                            serviceController.getReservationClientByIdService(services[i]._doc._id, i, function (reservsClient, pos) {
    //                                                if (reservsClient.length) {
    //                                                    for (var j = 0; j < reservsClient.length; j++) {
    //                                                        reservClient.push(reservsClient[i]);
    //                                                    }
    //                                                }
    //                                                cont++;
    //                                                if (cont == services.length) {
    //                                                    if (reservClient.length) {
    //                                                        if (cb1) cb1(null, reservClient.length);
    //                                                    } else {
    //                                                        if (cb1) cb1(null, 0);
    //                                                    }
    //                                                }
    //                                            })
    //                                        }
    //                                    } else {
    //                                        if (cb1) cb1(null, 0);
    //                                    }
    //                                })
    //                            }
    //                        })
    //                    }
    //                ], function (err, results) {
    //                    if (err || !results) {
    //                        cb(err, false);
    //                    } else {
    //                        if (results[0].length) {
    //                            cb(null, results[0]);
    //                        } else {
    //                            if (results[1].length) {
    //                                cb(null, results[1]);
    //                            } else {
    //                                if (results[2].length) {
    //                                    cb(null, results[2]);
    //                                } else {
    //                                    if (results[3].length) {
    //                                        cb(null, results[3]);
    //                                    } else {
    //                                        if (results[4].length) {
    //                                            cb(null, results[4]);
    //                                        } else {
    //                                            if (results[5].length) {
    //                                                cb(null, results[5]);
    //                                            } else {
    //                                                cb(null, results[6]);
    //                                            }
    //                                        }
    //                                    }
    //                                }
    //
    //                            }
    //                        }
    //                    }
    //                })
    //            }
    //            else {
    //                db.ReservationClient.count({cancel: type}).exec(function (err, count) {
    //                    if (cb) cb(err, count);
    //                })
    //            }
    //        },
    //        function (cb) {
    //            if (id.length > 0) {
    //                async.parallel([
    //                    function (cb2) {
    //                        db.ReservationTour.find({ship: id}).exec(function (err, resT) {
    //                            if (err || !resT || resT.length == 0) {
    //                                if (cb2) cb2(err, resT);
    //                            }
    //                            else {
    //                                var cont = 0;
    //                                var arrayResC = [];
    //                                for (var i = 0; i < resT.length; i++) {
    //                                    db.ReservationClient.find({
    //                                        cancel: type,
    //                                        state: 0,
    //                                        reservation: resT[i]._doc._id
    //                                    }).sort('-createDate').exec(function (err, resC) {
    //                                        cont++;
    //                                        for (var j = 0; j < resC.length; j++) {
    //                                            arrayResC.push(resC[j]);
    //                                        }
    //                                        if (cont == resT.length) {
    //                                            if (cb2) cb2(err, arrayResC);
    //                                        }
    //                                    })
    //                                }
    //                            }
    //                        })
    //                    },
    //                    function (cb2) {
    //                        db.ReservationTour.find({tour: id}).exec(function (err, resT) {
    //                            if (err || !resT || resT.length == 0) {
    //                                if (cb2) cb2(err, resT);
    //                            }
    //                            else {
    //                                var cont = 0;
    //                                var arrayResC = [];
    //                                for (var i = 0; i < resT.length; i++) {
    //                                    db.ReservationClient.find({
    //                                        cancel: type,
    //                                        state: 0,
    //                                        reservation: resT[i]._doc._id
    //                                    }).sort('-createDate').exec(function (err, resC) {
    //                                        cont++;
    //                                        for (var j = 0; j < resC.length; j++) {
    //                                            arrayResC.push(resC[j]);
    //                                        }
    //                                        if (cont == resT.length) {
    //                                            if (cb2) cb2(err, arrayResC);
    //                                        }
    //                                    })
    //                                }
    //                            }
    //                        })
    //                    },
    //                    function (cb2) {
    //                        db.Tour.find({group: id, remove: false}, function (err, tours) {
    //                            if (err || !tours || tours.length == 0) {
    //                                if (cb2) cb2(err, tours);
    //                            } else {
    //                                var cont = 0;
    //                                for (var j = 0; j < tours.length; j++) {
    //                                    var arrayResC = [];
    //                                    exports.getResevationClientByTour(tours[j], j, function (err, reservations, pos1) {
    //                                        for (var i = 0; i < reservations.length; i++) {
    //                                            arrayResC.push(reservations[i]);
    //                                        }
    //                                        cont++;
    //                                        if (cont == tours.length) {
    //                                            if (cb2) cb2(err, arrayResC);
    //                                        }
    //                                    })
    //                                }
    //                            }
    //                        })
    //                    },
    //                    function (cb2) {
    //                        portController.getTourByPortAll(id, 0, function (tour, pos) {
    //                            if (tour.length == 0) {
    //                                if (cb2) cb2(null, tour);
    //                            } else {
    //                                var cont = 0;
    //                                for (var j = 0; j < tour.length; j++) {
    //                                    var arrayResC = [];
    //                                    exports.getResevationClientByTour(tour[j], j, function (err, reservations, pos1) {
    //                                        for (var i = 0; i < reservations.length; i++) {
    //                                            arrayResC.push(reservations[i]);
    //                                        }
    //                                        cont++;
    //                                        if (cont == tour.length) {
    //                                            if (cb2) cb2(err, arrayResC);
    //                                        }
    //                                    })
    //                                }
    //                            }
    //
    //                        })
    //                    },
    //                    function (cb2) {
    //                        db.ReservationClient.find({
    //                            cancel: type,
    //                            state: 0,
    //                            pack: id
    //                        }).sort('-createDate').exec(function (err, resC) {
    //                            if (cb2) cb2(err, resC);
    //                        })
    //                    },
    //                    function (cb2) {
    //                        db.ReservationClient.find({
    //                            cancel: type,
    //                            state: 0
    //                        }).sort('-createDate').exec(function (err, reservClient) {
    //                            if (err || !reservClient || reservClient.length == 0) {
    //                                if (cb2) cb2(err, reservClient);
    //                            }
    //                            else {
    //                                var array = [];
    //                                for (var i = 0; i < reservClient.length; i++) {
    //                                    if (reservClient[i]._doc.services.length > 0) {
    //                                        for (var j = 0; j < reservClient[i]._doc.services.length; j++) {
    //                                            if (reservClient[i]._doc.services[j].id.toString() == id) {
    //                                                array.push(reservClient[i]);
    //                                                break;
    //                                            }
    //                                        }
    //                                    }
    //                                }
    //                                if (cb2) cb2(null, array);
    //                            }
    //                        })
    //                    },
    //                    function (cb2) {
    //                        db.Enterprise.findOne({_id: id, remove: false}).exec(function (err, enterprise) {
    //                            if (err || !enterprise) {
    //                                if (cb2) cb2(err, new Array());
    //                            } else {
    //                                enterpriseController.getServicesByEnterprise(enterprise._id, 0, function (services, pos) {
    //                                    if (services.length) {
    //                                        var cont = 0;
    //                                        var reservClient = [];
    //                                        for (var i = 0; i < services.length; i++) {
    //                                            serviceController.getReservationClientByIdService(services[i]._doc._id, i, function (reservsClient, pos) {
    //                                                if (reservsClient.length) {
    //                                                    for (var j = 0; j < reservsClient.length; j++) {
    //                                                        reservClient.push(reservsClient[j]);
    //                                                    }
    //                                                }
    //                                                cont++;
    //                                                if (cont == services.length) {
    //                                                    if (reservClient.length) {
    //                                                        if (cb2) cb2(null, reservClient);
    //                                                    } else {
    //                                                        if (cb2) cb2(null, reservClient);
    //                                                    }
    //                                                }
    //                                            })
    //                                        }
    //                                    } else {
    //                                        if (cb2) cb2(null, services);
    //                                    }
    //                                })
    //                            }
    //                        })
    //                    }
    //                ], function (err, results) {
    //                    if (err || !results) {
    //                        cb(err, false);
    //                    } else {
    //                        if (results[0].length) {
    //                            cb(null, results[0]);
    //                        } else {
    //                            if (results[1].length) {
    //                                cb(null, results[1]);
    //                            } else {
    //                                if (results[2].length) {
    //                                    cb(null, results[2]);
    //                                } else {
    //                                    if (results[3].length) {
    //                                        cb(null, results[3]);
    //                                    } else {
    //                                        if (results[4].length) {
    //                                            cb(null, results[4]);
    //                                        } else {
    //                                            if (results[5].length) {
    //                                                cb(null, results[5]);
    //                                            } else {
    //                                                cb(null, results[6]);
    //                                            }
    //                                        }
    //                                    }
    //                                }
    //                            }
    //                        }
    //                    }
    //                })
    //            }
    //            else {
    //                db.ReservationClient.find({cancel: type}).sort('-createDate').limit(limit).skip(skip).exec(function (err, resC) {
    //                    if (cb) cb(err, resC);
    //                })
    //            }
    //        }
    //    ], function (err, results) {
    //        if (err || !results) {
    //            return res.json({res: false, cont: 0});
    //        }
    //        else {
    //            var cont = 0;
    //            if (results[1].length > 0) {
    //                for (var i = 0; i < results[1].length; i++) {
    //                    getReservationTour(results[1][i]._doc.reservation, i, function (resT, pos) {
    //                        if (resT) {
    //                            tourController.getTour(resT._doc.tour, function (tour) {
    //                                if (tour) {
    //                                    getGroups(tour._doc.group.id, function (group) {
    //                                        if (group) {
    //                                            var aux = {
    //                                                tour: {
    //                                                    id: tour._doc._id,
    //                                                    name: tour._doc.name,
    //                                                    port: tour._doc.port,
    //                                                    group: group
    //                                                },
    //                                                date: resT._doc.date,
    //                                                ship: resT._doc.ship,
    //                                                departure: resT._doc.departure,
    //                                                cantPerson: resT._doc.cantPerson,
    //                                                pack: resT._doc.pack
    //                                            }
    //
    //                                            results[1][pos]._doc.reservation = aux;
    //                                            cont++;
    //                                            if (cont == results[1].length) {
    //                                                return res.json({res: results[1], cont: results[0]});
    //                                            }
    //                                        }
    //                                        else {
    //                                            return res.json({res: false, cont: results[0]});
    //                                        }
    //                                    })
    //                                }
    //                                else {
    //                                    return res.json({res: false, cont: results[0]});
    //                                }
    //                            })
    //                        }
    //                        else {
    //                            return res.json({res: false, cont: results[0]});
    //                        }
    //                    })
    //                }
    //            }
    //            else {
    //                return res.json({res: false, cont: results[0]});
    //            }
    //        }
    //    });
    //}
};
exports.reservationBonoReturn = function (req, res) {
    if (global.production) {
        utils.executepayWithPaypal(req, function (data) {
            if (data) {
                saveBono(req, res);
            }
            else {
                var url = urlUtil.resolve(global.bojeoServer+ req.user.language+"/occupied-booking");
                res.redirect(url);
            }
        })
    }
    else {
        saveBono(req, res);
    }
};
exports.reservationCancel = function (req, res) {
    res.redirect( urlUtil.resolve(global.bojeoServer, req.user.language));

};
exports.reservationReturn = function (req, res) {
    try {
        var id = req.session.sale.pack ? req.session.sale.pack : req.session.sale.id;
        tourController.getTour(id, function (tour) {
            validateResevedTour(req, function (data) {
                if (data) {
                    if (global.production) {
                        if (req.session.sale.method == 'paypal') {
                            utils.executepayWithPaypal(req, function (data) {
                                if (data != false) {
                                    //reservationReturnNext(tour, data, req.query.token);
                                    req.session.sale.receipt = data;
                                    reservationReturnNext(tour, req.query.token);
                                }
                                else {
                                    var url = urlUtil.resolve(global.bojeoServer, req.user.language+"/occupied-booking");
                                    res.redirect(url);
                                }
                            })
                        }
                        else if (req.session.sale.method == 'redsys') {
                                console.log("STEP 2 ----- OK");
                                console.log("STEP 3 ----- OK");
                                //reservationReturnNext(tour, req.body.Ds_Order, generateUid());
                                req.session.sale.receipt = req.body.Ds_Order;
                                reservationReturnNext(tour, generateUid());
                        }
                    }
                    else {
                        req.session.sale.receipt = 'saleId';
                        reservationReturnNext(tour, generateUid());
                    }
                } else {
                    var url = urlUtil.resolve(global.bojeoServer, req.user.language+"/occupied-booking");
                    res.redirect(url);
                }
            })
        });
    }
    catch (err) {
        if (req.session.sale.payform == 0) {
            var url =  urlUtil.resolve(global.bojeoServer, req.user.language+"/occupied-booking");;
            res.redirect(url);
        }
        else {
            var url = urlUtil.resolve(global.bojeoServer, req.user.language+"/error-conection");
            res.redirect(url);
        }

    }

    function reservationReturnNext(tour, token) {
        //tour, saleId, token
        var monto = req.session.sale.monto ? req.session.sale.monto : req.session.sale.hours;
        var pack = req.session.sale.pack ? req.session.sale.pack : null;
        if (pack) {
            async.waterfall([
                function (cb) {
                    db.Pack.findOne({_id: pack}).exec(function (err, packs) {
                        cb(err, packs);
                    })
                },
                function (packs, cb) {
                    async.map(packs._doc.services, function (service, callback0) {
                        db.Service.findOne({_id: service}, function (err, serv) {
                            callback0(err, serv);
                        })
                    }, function (err, result) {
                        if (err || !result) {
                            cb(err, result);
                        }
                        else {
                            for (var i = 0; i < result.length; i++) {
                                result[i] = result[i]._doc._id;
                            }
                            cb(null, result);
                        }
                    })
                }
            ], function (err, results) {
                if (err || !results) {
                    var url =   urlUtil.resolve(global.bojeoServer, req.user.language+"/occupied-booking");
                    res.redirect(url);
                }
                else {
                    var reservationClient = new db.ReservationClient({
                        user: req.user._id,
                        pack: pack,
                        reservation: null,
                        services: results,
                        cantPerson: req.session.sale.cantPerson,
                        payment: {
                            total: monto,
                            method: req.session.sale.method,
                            referenceNumber: 0,
                            token: token,
                            receipt: req.session.sale.receipt
                        }
                    });
                    console.log("STEP 3 PACK ----- OK");
                    saveReservationTour(req, res, reservationClient, tour);
                }
            });
        }
        else {


            var reservationClient = new db.ReservationClient({
                user: req.user._id,
                reservation: null,
                cantPerson: req.session.sale.cantPerson,
                payment: {
                    total: monto,
                    method: req.session.sale.method,
                    referenceNumber: 0,
                    token: token,
                    receipt: req.session.sale.receipt ? req.session.sale.receipt : token
                }
            });
            console.log("STEP 3 TOUR ----- OK");
            saveReservationTour(req, res, reservationClient, tour);
        }
    };
};
exports.get = function (req, res) {
    var id = req.params.id;
    db.ReservationClient.findOne({_id: id, user: req.user._id}).populate('reservation').exec(function (err, resC) {
        if (err || !resC) {
            return res.json({res: false, error: err});
        }
        else {
            async.parallel([
                function (callbackP) {
                    db.Tour.findOne({_id: resC._doc.reservation._doc.tour}).populate('port').populate('group').exec(function (err, tour) {
                        if (err || !tour) {
                            callbackP(err, tour);
                        }
                        else {
                            var aux = {
                                _id: tour._doc._id,
                                name: tour._doc.name,
                                group: {
                                    _id: tour._doc.group._doc._id,
                                    payform: tour._doc.group._doc.payform,
                                    color: tour._doc.group._doc.color,
                                    name: tour._doc.group._doc.name
                                },
                                port: {
                                    _id: tour._doc.port._doc._id,
                                    name: tour._doc.port._doc.name
                                }
                            }
                            callbackP(null, aux);
                        }
                    });
                },
                function (callbackP) {
                    db.Ship.findOne({_id: resC._doc.reservation._doc.ship}).exec(function (err, ship) {
                        if (err || !ship) {
                            callbackP(err, ship);
                        }
                        else {
                            var aux = {
                                _id: ship._doc._id,
                                name: ship._doc.name
                            };
                            callbackP(null, aux);
                        }
                    });
                },
                function (callbackP) {
                    var today = new Date();
                    today = new Date(today.getFullYear(), today.getMonth(), today.getDate());
                    var state = 'booked';
                    if (resC._doc.cancel) {
                        state = 'canceled';
                    }
                    if (!resC._doc.cancel && today > resC._doc.reservation._doc.start) {
                        state = 'done';
                    }
                    callbackP(null, state);

                }
            ], function (err, results) {
                if (err || !results) {
                    return res.json({res: false, error: err});
                }
                else {
                    resC._doc.reservation._doc.tour = results[0];
                    resC._doc.reservation._doc.ship = results[1];
                    resC._doc.reservation._doc.state = results[2];
                    return res.json({res: resC});
                }
            });
        }
    });

};
exports.refundReservation = function (req, res) {
    if (global.production) {
        var params = {
            'payment.token': req.body.token,
            cancel: false,
            user: req.user._id
        };
        if (req.user.role == 'admin') {
            params = {
                'payment.token': req.body.token,
                cancel: false
            };
        }
        db.ReservationClient.findOne(params).populate('reservation').exec(function (err, restC) {
            if (err || !restC) {
                return res.json({res: false});
            }
            else {
                executeRefund(restC, function (err, success) {
                    if (err || !success) {
                        return res.json({res: false});
                    }
                    else {
                        db.Tour.findOne({_id: restC._doc.reservation._doc.tour}).populate('port').populate('group').exec(function (err, tour) {
                            if (err || !tour) {
                                return res.json({res: false});
                            }
                            else {
                                if (tour._doc.data.capacity.type != 'person') {
                                    async.parallel([
                                        function (cb) {
                                            db.ReservationTour.update({
                                                _id: restC._doc.reservation._doc._id,
                                                remove: false
                                            }, {$set: {remove: true}}).exec(function (err, success) {
                                                cb(err, success);
                                            });
                                        },
                                        function (cb) {
                                            db.ReservationClient.update({
                                                _id: restC._doc._id,
                                                cancel: false
                                            }, {
                                                $set: {
                                                    reason: req.body.reason,
                                                    cancel: true
                                                }
                                            }).exec(function (err, success) {
                                                cb(err, success);
                                            });
                                        },
                                        function (cb) {
                                            db.User.update({
                                                _id: req.user._id
                                            }, {$inc: {totalAmount: -restC._doc.payment.total}}).exec(function (err, success) {
                                                cb(err, success);
                                            });
                                        },
                                    ], function (err, results) {
                                        if (err || !results) {
                                            return res.json({res: false});
                                        } else {
                                            sendEmailRefund(req, tour, restC, req.body.reason, function (mail) {
                                                return res.json({res: true});
                                            })
                                        }
                                    });
                                }
                                else {
                                    async.parallel([
                                        function (cb) {
                                            var remove = false;
                                            var available = false;
                                            var cantPerson = restC._doc.reservation._doc.cantPerson - restC._doc.cantPerson;
                                            if (!cantPerson) {
                                                remove = true;
                                            }
                                            else {
                                                available = true;
                                            }
                                            db.ReservationTour.update({
                                                _id: restC._doc.reservation._doc._id,
                                                remove: false
                                            }, {
                                                $set: {
                                                    remove: remove,
                                                    available: available
                                                }
                                            }).exec(function (err, success) {
                                                cb(err, success);
                                            });
                                        },
                                        function (cb) {
                                            db.ReservationClient.update({
                                                _id: restC._doc._id,
                                                cancel: false
                                            }, {
                                                $set: {
                                                    reason: req.body.reason,
                                                    cancel: true
                                                }
                                            }).exec(function (err, success) {
                                                cb(err, success);
                                            });
                                        },
                                        function (cb) {
                                            db.User.update({
                                                _id: req.user._id
                                            }, {$inc: {totalAmount: -restC._doc.payment.total}}).exec(function (err, success) {
                                                cb(err, success);
                                            });
                                        },
                                    ], function (err, results) {
                                        if (err || !results) {
                                            return res.json({res: false});
                                        } else {
                                            sendEmailRefund(req, tour, restC, req.body.reason, function (mail) {
                                                return res.json({res: true});
                                            })
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            }
        })
    }
    else {
        var params = {
            'payment.token': req.body.token,
            cancel: false,
            user: req.user._id
        };
        if (req.user.role == 'admin') {
            params = {
                'payment.token': req.body.token,
                cancel: false
            };
        }
        db.ReservationClient.findOne(params).populate('reservation').exec(function (err, restC) {
            if (err || !restC) {
                return res.json({res: false});
            }
            else {
                db.Tour.findOne({_id: restC._doc.reservation._doc.tour}).populate('port').populate('group').exec(function (err, tour) {
                    if (err || !tour) {
                        return res.json({res: false});
                    }
                    else {
                        if (tour._doc.data.capacity.type != 'person') {
                            async.parallel([
                                function (cb) {
                                    db.ReservationTour.update({
                                        _id: restC._doc.reservation._doc._id,
                                        remove: false
                                    }, {$set: {remove: true}}).exec(function (err, success) {
                                        cb(err, success);
                                    });
                                },
                                function (cb) {
                                    db.ReservationClient.update({
                                        _id: restC._doc._id,
                                        cancel: false
                                    }, {$set: {reason: req.body.reason, cancel: true}}).exec(function (err, success) {
                                        cb(err, success);
                                    });
                                },
                                function (cb) {
                                    db.User.update({
                                        _id: req.user._id
                                    }, {$inc: {totalAmount: -restC._doc.payment.total}}).exec(function (err, success) {
                                        cb(err, success);
                                    });
                                },
                            ], function (err, results) {
                                if (err || !results) {
                                    return res.json({res: false});
                                } else {
                                    sendEmailRefund(req, tour, restC, req.body.reason, function (mail) {
                                        return res.json({res: true});
                                    })
                                }
                            });
                        }
                        else {
                            async.parallel([
                                function (cb) {
                                    var remove = false;
                                    var available = false;
                                    var cantPerson = restC._doc.reservation._doc.cantPerson - restC._doc.cantPerson;
                                    if (!cantPerson) {
                                        remove = true;
                                    }
                                    else {
                                        available = true;
                                    }
                                    db.ReservationTour.update({
                                        _id: restC._doc.reservation._doc._id,
                                        remove: false
                                    }, {$set: {remove: remove, available: available}}).exec(function (err, success) {
                                        cb(err, success);
                                    });
                                },
                                function (cb) {
                                    db.ReservationClient.update({
                                        _id: restC._doc._id,
                                        cancel: false
                                    }, {$set: {reason: req.body.reason, cancel: true}}).exec(function (err, success) {
                                        cb(err, success);
                                    });
                                },
                                function (cb) {
                                    db.User.update({
                                        _id: req.user._id
                                    }, {$inc: {totalAmount: -restC._doc.payment.total}}).exec(function (err, success) {
                                        cb(err, success);
                                    });
                                },
                            ], function (err, results) {
                                if (err || !results) {
                                    return res.json({res: false});
                                } else {
                                    sendEmailRefund(req, tour, restC, req.body.reason, function (mail) {
                                        return res.json({res: true});
                                    })
                                }
                            });
                        }
                    }
                });

            }
        })
    }
};
exports.getReservationTourByShip = function (idShip, cb) {
    db.ReservationTour.find({
        ship: idShip,
        remove: false,
        tag: {$ne: ["canceled"]}
    }).populate('tour').exec(function (err, resTs) {
        if (err || !resTs) {
            cb(null, [])
        }
        else {
            if (resTs.length > 0) {
                async.map(resTs, function (resT, callback) {
                        if (resT.tour) {
                            db.Group.findOne({_id: resT.tour.group}).exec(function (err, group) {
                                if (err || !group) {
                                    callback(err, group);
                                }
                                else {
                                    resT._doc.tour._doc.group = group;
                                    callback(null, resT);
                                }
                            });
                        }
                        else {
                            callback(null, resT);
                        }

                    }, function (err, result) {
                        if (err || !result) {
                            cb(null, [])
                        }
                        else {
                            cb(null, result)
                        }
                    }
                )
            }
            else {
                cb(null, [])
            }

        }
    });
};
exports.getReservationTourByShip1 = function (req, res) {
    var shipId = req.body.ship;
    exports.getReservationTourByShip(shipId, function (err, data) {
        if (err || !data) {
            return res.json({res: err, error: data});
        }
        else {
            return res.json({res: data});
        }
    });

};
exports.getReservationTourByTour = function (tour, cb) {
    db.ReservationTour.find({tour: tour}).exact(function (err, resT) {
        if (err || !resT) {
            if (cb) cb(false);
        }
        else {
            if (cb) cb(tour);
        }
    });
};
exports.getReceipt = function (req, res) {
    var user = req.user;
    db.Sale.findOneAndRemove({user: user._id}).exec(function (err, sale) {
        if (err || !sale) {
            return res.json({res: false});
        }
        else {
            if (sale._doc.bodySale.payForm == 'bonds') {

                async.waterfall([
                    function (cb) {
                        db.Tour.findOne({_id: sale._doc.bodySale.id}).populate('group port').exec(function (err, tour) {
                            if (err || !tour) {
                                cb(err, tour);
                            }
                            else {
                                sale._doc.bodySale.tour = tour;
                                cb(null, sale);
                            }
                        });
                    },
                    function (sale, cb) {
                        db.Bonds.findOne({_id: sale._doc.bodySale.bond}).exec(function (err, bond) {
                            if (err || !bond) {
                                cb(err, bond);
                            }
                            else {
                                sale._doc.bodySale.bond = bond;
                                cb(null, sale);
                            }
                        });
                    }
                ], function (err, results) {
                    if (err || !results) {
                        return res.json({res: false});
                    } else {
                        return res.json({res: results});
                    }
                })


            }
            else {
                async.waterfall([
                    function (cb) {
                        db.Tour.findOne({_id: sale._doc.bodySale.id}).populate('group port').exec(function (err, tour) {
                            if (err || !tour) {
                                cb(err, tour);
                            }
                            else {
                                sale._doc.bodySale.tour = tour;
                                cb(null, sale);
                            }
                        });
                    },
                    function (sale, cb) {
                        if (sale._doc.bodySale.pack) {
                            db.Pack.findOne({_id: sale._doc.bodySale.pack}).populate('services').exec(function (err, pack) {
                                if (err || !pack) {
                                    cb(err, pack);
                                }
                                else {
                                    sale._doc.bodySale.pack = pack;
                                    cb(null, sale);
                                }
                            });
                        }
                        else {
                            cb(null, sale);
                        }

                    }
                ], function (err, results) {
                    if (err || !results) {
                        return res.json({res: false});
                    } else {
                        return res.json({res: results});
                    }
                })

            }
        }
    });
};
exports.tpvForm = function (req, res) {
    var description = req.body.description;
    return res.json({res: generateTPVFormDiferido(req.session.bookInfo.tourMonto, description, req)});
};
exports.saveBookingInfo = function (req, res) {
    if (req.body.info) {
        req.session.bookInfo = req.body.info;

        return res.json({res: true})
    }
    else {
        return res.json({res: false, error: "Icorrect information"})
    }
}
exports.getBookingInfo = function (req, res) {
    if (req.session.bookInfo) {
        return res.json({res: req.session.bookInfo})
    }
    else {
        return res.json({res: false, error: "Undefine"})
    }
}

function executeRefund(restC, cb) {
    if (restC._doc.payment.method == "paypal") {
        utils.paypalRefund( restC._doc.payment.total, restC._doc.payment.receipt, function (data) {
            if (data) {
                cb(null, data);
            }
            else {
                cb(null, false);
            }
        });
    }
    else {
        console.log("STEP 1 REFUND ----- GENERATE FORM_DATA");
        var form_data = generateTPVFormRefun(restC._doc.payment.total, 'Refund', restC._doc.payment.receipt);
        console.log("STEP 2 REFUND ----- ", form_data);
        postRefun(form_data, cb);

    }
};
function getTour(id, pos, cb) {
    db.Tour.findOne({_id: id}).exec(function (err, tour) {
        if (err || !tour) {
            if (cb) cb(false, pos);
        }
        else {
            if (cb) cb(tour, pos);
        }
    });
};
function getGroup(id, pos, cb) {
    db.Group.findOne({_id: id}).exec(function (err, group) {
        if (err || !group) {
            if (cb) cb(false, pos);
        }
        else {
            if (cb) cb(group, pos);
        }
    });
};
function validateResevedTour(req, cb) {
    var sale = req.session.sale;
    if (sale.payForm != 'bonds') {
        var event = {
            start: new Date(sale.date + departureTime(sale.departure)),
            end: new Date(sale.date + departureTime(sale.departure) + departureTime(sale.duration)),
            allDay: false
        }
    }
    else {
        var event = {
            start: new Date(sale.date),
            end: new Date(sale.date + 86400000),
            allDay: false
        }
    }
    db.Ship.findOne({_id: req.session.sale.ship.id}).populate({
        path: 'calendar.block',
        match: {remove: false}
    }).exec(function (err, ship) {
        if (err || !ship) {
            if (cb) cb(false);
        }
        else {
            var events = [];
            for (var i = 0; i < ship._doc.calendar.block.length; i++) {
                if (!ship._doc.calendar.block[i]._doc.available && ship._doc.calendar.block[i]._doc.tag != 'canceled') {
                    events.push(ship._doc.calendar.block[i]);
                }
            }

            if (validateAddEvent(events, event)) {
                if (cb) cb(true);
            }
            else {
                if (cb) cb(false);
            }
        }
    })
};
function saveBono(req, res) {
    var id = req.user._id;
    var idb = req.session.sale.bond;
    var hour = req.session.sale.hours;
    var price = req.session.sale.price;


    db.User.findOne({_id: id, remove: false}).exec(function (err, user) {
        if (err || !user) {
            res.redirect(urlUtil.resolve(global.bojeoServer, req.user.language+"/profile/bonu"));
        }
        else {
            var bondsUser = user._doc.bonds;
            if (bondsUser.length > 0) {
                var sem = false;
                for (var i = 0; i < bondsUser.length; i++) {
                    if (bondsUser[i].id == idb) {
                        bondsUser[i].value = bondsUser[i].value + hour;
                        sem = true;
                        break;
                    }
                }

                if (sem) {
                    db.User.update({_id: id, remove: false}, {
                        $set: {bonds: bondsUser},
                        $inc: {totalAmount: price}
                    }).exec(function (err, success) {
                        if (err || !success) {
                            res.redirect(urlUtil.resolve(global.bojeoServer, req.user.language+"/profile/bonu"))
                        }
                        else {
                            res.redirect(urlUtil.resolve(global.bojeoServer, req.user.language+"/profile/bonu"));
                        }
                    });
                }
                else {
                    var bond = {
                        id: idb,
                        value: hour
                    }
                    db.User.update({_id: id, remove: false}, {
                        $push: {bonds: bond},
                        $inc: {totalAmount: price}
                    }).exec(function (err, success) {
                        if (err || !success) {
                            res.redirect(urlUtil.resolve(global.bojeoServer, req.user.language+"/profile/bonu"));
                        }
                        else {
                            res.redirect(urlUtil.resolve(global.bojeoServer, req.user.language+"/profile/bonu"));
                        }
                    });
                }

            }
            else {
                var bond = {
                    id: idb,
                    value: hour
                }
                db.User.update({_id: id, remove: false}, {
                    $push: {bonds: bond},
                    $inc: {totalAmount: price}
                }).exec(function (err, success) {
                    if (err || !success) {
                        res.redirect(urlUtil.resolve(global.bojeoServer, req.user.language+"/profile/bonu"));
                    }
                    else {
                        var type = 3,
                            info = {
                                id: user._doc._id,
                                userName: user._doc.userName,
                                lastName: user._doc.lastName,
                                email: user._doc.email,
                                bond: bond,
                                price: price
                            };
                        timeline.create(type, info, function (err, success) {
                            if (err) {
                                var log = new db.ErrorLogs({
                                    error: err,
                                    date: new Date()
                                })
                                log.save()
                            }
                        });
                        res.redirect(urlUtil.resolve(global.bojeoServer, req.user.language+"/profile/bonu"));
                    }
                });
            }
        }
    })


};
function saveReservationTour(req, res, reservationClient, tour) {
    var departure = req.session.sale.departure ? req.session.sale.departure : '00:00';
    var duration = req.session.sale.duration ? req.session.sale.duration : '24:00';
    //var pack = req.session.sale.pack ? req.session.sale.pack : null;
    var start = new Date(req.session.sale.date + departureTime(departure));
    var end = new Date(req.session.sale.date + departureTime(departure) + departureTime(duration));
    db.ReservationTour.findOne({
        tour: req.session.sale.id,
        ship: req.session.sale.ship.id,
        start: start,
        end: end,
        departure: departure,
        duration: duration,
        available: true
    }).exec(function (err, resTour) {
        console.log("STEP 4 PACK ----- OK");
        if (err || !resTour) {
            if (resTour == null) {
                var cantPerson = 0;
                cantPerson = cantPerson + parseInt(req.session.sale.cantPerson);
                var maxPerson = tour._doc.data.capacity.maxPerson;
                if (cantPerson <= maxPerson) {
                    var available;
                    if (cantPerson == maxPerson) {
                        available = false;
                    }
                    else {
                        available = true;
                    }
                    var price = req.session.sale.monto || 0;

                    db.Ship.findOne({_id: req.session.sale.ship.id}, function (err, ship_success) {
                        if (err || !ship_success) {
                            var url =  urlUtil.resolve(global.bojeoServer, req.user.language+"/occupied_booking");
                            res.redirect(url);
                        }
                        else {
                            var tmp = req.session.sale.date;

                            if (req.session.sale.payForm != 'bonds') {
                                if (tour._doc.data.price.type == 'hours') {
                                    var duration = departureTime(req.session.sale.duration);
                                    var start = new Date(tmp + departureTime(req.session.sale.departure));
                                    var end = new Date(tmp + departureTime(req.session.sale.departure) + duration);
                                }
                                else {
                                    var duration = parseInt(req.session.sale.duration) * 86400000;
                                    var start = new Date(tmp + departureTime(req.session.sale.departure));
                                    var end = new Date(tmp + departureTime(req.session.sale.departure) + duration - 1);
                                }

                            }
                            else {
                                var duration = 86400000 - 1;
                                var start = new Date(tmp);
                                var end = new Date(tmp + duration);
                            }


                            var resT = db.ReservationTour({
                                tag: 'booked',
                                start: start,
                                end: end,
                                type: tour._doc.data.price.type,
                                tour: req.session.sale.id,
                                percent: tour._doc.data.price.toPay,
                                total: parseInt(price),
                                comitions: ship_success._doc.owner.comitions,
                                ship: req.session.sale.ship.id,
                                departure: departure,
                                duration: req.session.sale.duration || '24:00',
                                cantPerson: req.session.sale.cantPerson,
                                available: available
                            });

                            resT.save(function (err, resT) {
                                if (err || !resT) {
                                    var url =  urlUtil.resolve(global.bojeoServer, req.user.language+"/occupied_booking");
                                    res.redirect(url);
                                }
                                else {
                                    reservationClient.reservation = resT._doc._id;
                                    reservationClient.save(function (err, resClient) {
                                        if (err || !resClient) {
                                            return res.json({res: false});
                                        }
                                        else {
                                            shipController.writeCalendarByShip(req, resT, function (err, write) {
                                                if (err || !write) {
                                                    var url =  urlUtil.resolve(global.bojeoServer, req.user.language+"/occupied_booking");
                                                    res.redirect(url);

                                                }
                                                else {
                                                    if (tour._doc.data.payForm == 'currencies') {
                                                        changeUserAmount(req.user._id, null, req.session.sale.monto, function (successChange) {
                                                            if (successChange) {
                                                                //sendEmail(req,res);
                                                                sendEmailReserve(req, tour, resClient._doc.token, function (mail) {
                                                                    return res.redirect(urlUtil.resolve(global.bojeoServer, req.user.language+"/receipt"));
                                                                });
                                                            }
                                                            else {
                                                                var url =  urlUtil.resolve(global.bojeoServer, req.user.language+"/occupied_booking");
                                                                res.redirect(url);
                                                            }

                                                        })
                                                    }
                                                    else {
                                                        changeUserBond(req.user._id, req.session.sale.hours, tour._doc.data.bond.bond, function (successChange) {
                                                            if (successChange) {
                                                                sendEmailReserve(req, tour, resClient._doc.token, function (mail) {
                                                                    return res.redirect(urlUtil.resolve(global.bojeoServer, req.user.language+"/receipt"));
                                                                });
                                                            }
                                                            else {
                                                                var url = urlUtil.resolve(global.bojeoServer, req.user.language+"/occupied_booking");
                                                                res.redirect(url);
                                                            }
                                                        })
                                                    }
                                                }
                                            });

                                        }
                                    })
                                }
                            })
                        }
                    })
                }
                else {
                    return res.json({res: false});
                }
            }
        }
        else {
            var cantPerson = resTour._doc.cantPerson + parseInt(req.session.sale.cantPerson);
            if (cantPerson <= tour._doc.data.capacity.maxPerson) {
                var available = (tour._doc.data.capacity.maxPerson == cantPerson);
                db.ReservationTour.update({_id: resTour._doc._id}, {
                    $set: {
                        cantPerson: cantPerson,
                        available: !available
                    }
                }).exec(function (err, successs) {
                    if (err || !successs) {
                        var url = urlUtil.resolve(global.bojeoServer, req.user.language+"/occupied_booking");
                        res.redirect(url);
                    }
                    else {
                        reservationClient._doc.reservation = resTour._doc._id;
                        reservationClient._doc.payment.receipt = reservationClient._doc.payment.token;
                        reservationClient.save(function (err, resClient) {
                            if (err || !resClient) {
                                return res.json({res: false});
                            }
                            else {
                                shipController.writeCalendarByShip(req, resTour, function (err, write) {
                                    if (err || !write) {
                                        var url = urlUtil.resolve(global.bojeoServer, req.user.language+"/occupied_booking");
                                        res.redirect(url);

                                    }
                                    else {
                                        if (tour._doc.data.payForm == 'currencies') {
                                            changeUserAmount(req.user._id, null, req.session.sale.monto, function (successChange) {
                                                if (successChange) {
                                                    //sendEmail(req,res);
                                                    sendEmailReserve(req, tour, resClient._doc.token, function (mail) {
                                                        return res.redirect(urlUtil.resolve(global.bojeoServer, req.user.language+"/receipt"));
                                                    });
                                                }
                                                else {
                                                    var url = urlUtil.resolve(global.bojeoServer, req.user.language+"/occupied_booking");
                                                    res.redirect(url);
                                                }

                                            })
                                        }
                                        else {
                                            changeUserBond(req.user._id, req.session.sale.hours, tour._doc.data.bond.bond, function (successChange) {
                                                if (successChange) {
                                                    sendEmailReserve(req, tour, resClient._doc.token, function (mail) {
                                                        return res.redirect(urlUtil.resolve(global.bojeoServer, req.user.language+"/receipt"));

                                                    });
                                                }
                                                else {
                                                    var url = urlUtil.resolve(global.bojeoServer, req.user.language+"/occupied_booking");
                                                    res.redirect(url);
                                                }
                                            })
                                        }
                                    }
                                });

                            }
                        })
                    }
                });
            }
            else {
                var url = urlUtil.resolve(global.bojeoServer, req.user.language+"/occupied_booking");
                res.redirect(url);
            }

        }
    })
};
function getReservationTour(id, pos, cb) {
    db.ReservationTour.findOne({_id: id}).exec(function (err, resT) {
        if (err || !resT) {
            if (cb) cb(false, pos);
        }
        else {
            shipController.getShipByID(resT._doc.ship, function (data) {
                if (data) {
                    var aux = {
                        id: data._doc._id,
                        name: data._doc.name
                    }
                    resT._doc.ship = aux;
                    if (cb) cb(resT, pos);
                }
                else {
                    var aux = {
                        id: resT._doc.ship,
                        name: {
                            es: "Desconocido",
                            en: "Unknown"
                        }
                    }
                    resT._doc.ship = aux;
                    if (cb) cb(resT, pos);
                }
            })
        }
    })
};
function sendEmail(req, res) {
    db.User.findOne({_id: req.user._id, remove: false}).exec(function (err, success) {
        if (err == null && success != null) {
            req.user._id = success._doc.email;
            utils.prepareMail(res, 2, req.user._id);
        }
    })
};
function generateUid() {

    // http://www.ietf.org/rfc/rfc4122.txt
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;

};
function changeUserAmount(client, email, price, cb) {
    if (client) {

        db.User.update({_id: client, remove: false}, {$inc: {totalAmount: price}}).exec(function (err, success) {
            if (err || !success) {
                if (cb) cb(false);
            }
            else {
                if (cb) cb(true);
            }
        });
    }
    else {
        db.User.update({email: email, remove: false}, {$inc: {totalAmount: price}}).exec(function (err, success) {
            if (err || !success) {
                if (cb) cb(false);
            }
            else {
                if (cb) cb(true);
            }
        });
    }
};
function changeUserBond(client, hours, bonds, cb) {
    var horas = (0 - parseInt(hours) ) || 0;
    async.waterfall([
        function (callback) {
            db.User.findOne({_id: client, remove: false, available: true}).exec(function (err, user) {
                callback(err, user);
            })
        },
        function (user, callback) {
            for (var i = 0; i < user._doc.bonds.length; i++) {
                if (user._doc.bonds[i].id.toString() == bonds.toString()) {
                    user._doc.bonds[i].value += horas;
                    break;
                }
            }

            db.User.update({
                _id: client,
                remove: false,
                available: true
            }, {$set: {bonds: user._doc.bonds}}).exec(function (err, success) {
                callback(err, success);
            });
        }
    ], function (err, results) {
        if (err || !results) {
            if (cb) cb(false);
        } else {
            if (cb) cb(true);
        }
    });


    //var params = "{$inc: {'bonds." + bonds + ".value': -horas}}";
    //db.User.update({
    //    _id: client,
    //    remove: false,
    //    'bonds.id': bonds
    //}, params).exec(function (err, success) {
    //    if (err || !success) {
    //        if (cb) cb(false);
    //    }
    //    else {
    //        if (cb) cb(true);
    //    }
    //});
};
function updateStateTourReserved(cb) {
    var date = new Date();
    date = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() + 86400000;

    date = new Date(date);

    db.ReservationTour.find({date: {$lt: date}}).exec(function (err, resTours) {
        if (err || !resTours) {
            if (cb) cb(true);
        }
        else {
            var cont = 0;
            if (resTours.length > 0) {
                for (var i = 0; i < resTours.length; i++) {
                    db.ReservationClient.update({
                        reservation: resTours[i]._doc._id,
                        state: 0
                    }, {$set: {state: 1}}).exec(function () {
                        cont++;
                        if (cont == resTours.length) {
                            if (cb) cb(true);
                        }
                    })
                }
            }
            else {
                if (cb) cb(true);
            }


        }
    })

};
function changeRemoveReservationTour(id, remove, cb) {

    db.ReservationTour.update({_id: id}, {$set: {remove: remove}}).exec(function (err, success) {
        if (err || !success) {
            if (cb) cb(false);
        }
        else {
            if (cb) cb(true);
        }
    });
};
function changeStateReservation(restC, reason, cb) {

    db.ReservationClient.update({token: restC._doc.token}, {
        $set: {
            cancel: false,
            reason: reason
        }
    }).exec(function (err, success) {
        if (err || !success) {
            if (cb) cb(false);
        }
        else if (restC._doc.reservation.tour.priceBy != 'person') {
            changeRemoveReservationTour(restC._doc.reservation.id, true, function (data) {
                if (data) {
                    if (cb) cb(true);
                }
                else {
                    if (cb) cb(false);
                }
            })
        }
        else {
            async.waterfall([
                function (callback) {
                    db.ReservationTour.findOne({_id: restC._doc.reservation.id}, function (err, resT) {
                        callback(err, resT);
                    })
                },
                function (resT, callback) {
                    var aux = resT._doc.cantPerson - restC._doc.cantPerson;
                    if (aux) {
                        db.ReservationTour.update({_id: restC._doc.reservation.id}, {$inc: {cantPerson: -restC._doc.cantPerson}}).exec(function (err, success) {
                            if (err || !success) {
                                callback(err, success);
                            }
                            else {
                                callback(null, true);
                            }
                        });
                    }
                    else {
                        changeRemoveReservationTour(restC._doc.reservation.id, true, function (data) {
                            if (data) {
                                callback(null, true);
                            }
                            else {
                                callback(true, null);
                            }
                        })
                    }
                }
            ], function (err, result) {
                if (err || !result) {
                    cb(false);
                }
                else {
                    cb(true);
                }
            });


        }
    });


};
function sendMail(smtpTransport, mailOptions, cb) {
    var options = {
        from: mailOptions.from,
        to: mailOptions.to, // list of receivers
        subject: mailOptions.subject, // Subject line
        html: mailOptions.html
    }
    try {
        smtpTransport.sendMail(mailOptions, function (err) {
            if (err) {
                mailController.saveFailedMails(options, function (save) {
                    cb(save);
                })
            }
            else {
                cb(true);
            }
        });
    }
    catch (err) {
        if (err) if (cb) cb(true);
    }

};
function ReplaceAll(str, find, replace) {
    var repeat = true;
    while (repeat) {
        var newStr = str.replace(find, replace);
        if (str == newStr) {
            repeat = false;
        }
        else {
            str = newStr;
        }
    }
    return str;
};
function sendEmailReserve(req, tour, token, cb) {
    var date = new Date(req.session.sale.date).getDate() + '/' +
        new Date(req.session.sale.date).getMonth() + '/' + new Date(req.session.sale.date).getFullYear();

    getGroup(tour._doc.group.id, 0, function (group) {
        if (group) {
            var type = 1,
                info = {
                    date: req.session.sale.date,
                    userName: req.user.username,
                    userId: req.user._id,
                    tourName: tour._doc.name,
                    tourId: tour._doc._id,
                    portName: tour._doc.port.name,
                    shipName: req.session.sale.ship.name,
                    shipId: req.session.sale.ship.id,
                    departure: req.session.sale.departure,
                    cantPerson: req.session.sale.cantPerson,
                    monto: req.session.sale.monto,
                    group: group._doc.name
                };
            timeline.create(type, info, function (err, success) {
                if (err) {
                    var log = new db.ErrorLogs({
                        error: err,
                        date: new Date()
                    })
                    log.save();
                }
            });
        }
    })

    mailController.getEmail(function (data) {
        if (data) {
            if (data._doc.notificationReserved.active) {
                var smtpTransport = nodemailer.createTransport("SMTP", {
                    host: data._doc.mailServer, // hostname
                    secureConnection: true, // use SSL
                    connectionTimeout : 300000,
                    port: data._doc.mailSMTPPort, // port for secure SMTP
                    auth: {
                        user: data._doc.mailUser,
                        pass: data._doc.mailPass
                    }
                });

                var auxTmp = req.user.language == 'es' ? data._doc.notificationReserved.body.es : data._doc.notificationReserved.body.en,
                    auxSign = req.user.language == 'es' ? data._doc.sign.es : data._doc.sign.en;


                var template = ReplaceAll(auxTmp, '[email_user]', req.user.username);
                var template = ReplaceAll(template, '[tour_name]', tour._doc.name.es);
                var template = ReplaceAll(template, '[port_name]', tour._doc.port.name.es);
                var template = ReplaceAll(template, '[date]', date);
                var template = ReplaceAll(template, '[ship_name]', req.session.sale.ship.name);
                var template = ReplaceAll(template, '[departure]', req.session.sale.departure);
                var template = ReplaceAll(template, '[capacities]', req.session.sale.cantPerson);
                var template = ReplaceAll(template, '[days]', 1);
                var template = ReplaceAll(template, '[total]', req.session.sale.monto + ' €');
                var template = ReplaceAll(template, '[sale_id]', token);

                var sign = ReplaceAll(auxSign, '[logo]', '<img src=\"' + global.bojeoServer + '/img/email/bojeo_Logo.png\" alt=\"Logo Bojeo\" width=\"109\" height=\"72\" /><br />');
                var template = template + sign;

                var mailOptions = {
                    from: "Bojeo <" + data._doc.mailUser + ">", // sender address
                    to: req.user.email, // list of receivers
                    subject: data._doc.notificationReserved.subject.es, // Subject line
                    html: template
                }
                sendMail(smtpTransport, mailOptions, function (mail) {
                    cb(mail);
                });
            }
            else {
                if (cb) cb(true);
            }
        }
        else {
            if (cb) cb(true);
        }
    });
};
function sendEmailRefund(req, tour, resC, reason, cb) {

    userController.getUser(resC._doc.client, function (user) {
        if (user) {
            var date = new Date(req.session.sale.date).getDate() + '/' +
                new Date(req.session.sale.date).getMonth() + '/' + new Date(req.session.sale.date).getFullYear();
            getGroup(tour._doc.group.id, 0, function (group) {
                if (group) {
                    var type = 2,
                        info = {
                            date: req.session.sale.date,
                            userName: user._doc.userName,
                            userId: user._doc._id,
                            tourName: tour._doc.name,
                            tourId: tour._doc._id,
                            portName: tour._doc.port.name,
                            shipName: req.session.sale.ship.name,
                            shipId: req.session.sale.ship.id,
                            departure: req.session.sale.departure,
                            cantPerson: req.session.sale.cantPerson,
                            monto: req.session.sale.monto,
                            group: group._doc.name,
                            reason: reason
                        };
                    timeline.create(type, info, function (err, success) {
                        if (err) {
                            var log = new db.ErrorLogs({
                                error: err,
                                date: new Date()
                            })
                            log.save();
                        }
                    });
                }
            })

            mailController.getEmail(function (data) {
                if (data) {
                    if (data._doc.devolution.active) {
                        var smtpTransport = nodemailer.createTransport("SMTP", {
                            host: data._doc.mailServer, // hostname
                            secureConnection: true, // use SSL
                            connectionTimeout : 300000,
                            port: data._doc.mailSMTPPort, // port for secure SMTP
                            auth: {
                                user: data._doc.mailUser,
                                pass: data._doc.mailPass
                            }
                        });

                        var auxTmp = req.user.language == 'es' ? data._doc.devolution.body.es : data._doc.devolution.body.en,
                            auxSign = req.user.language == 'es' ? data._doc.sign.es : data._doc.sign.en;

                        var template = ReplaceAll(auxTmp, '[email_user]', user._doc.userName);
                        var template = ReplaceAll(template, '[tour_name]', tour._doc.name.es);
                        var template = ReplaceAll(template, '[port_name]', tour._doc.port.name.es);
                        var template = ReplaceAll(template, '[date]', date);
                        var template = ReplaceAll(template, '[ship_name]', req.session.sale.ship.name);
                        var template = ReplaceAll(template, '[departure]', req.session.sale.departure);
                        var template = ReplaceAll(template, '[capacities]', req.session.sale.cantPerson);
                        var template = ReplaceAll(template, '[days]', 1);
                        var template = ReplaceAll(template, '[total]', req.session.sale.monto + ' €');
                        var template = ReplaceAll(template, '[sale_id]', resC._doc.token);
                        var template = ReplaceAll(template, '[reason]', reason);
                        var sign = ReplaceAll(auxSign, '[logo]', '<img src=\"' + global.bojeoServer + '/img/email/bojeo_Logo.png\" alt=\"Logo Bojeo\" width=\"109\" height=\"72\" /><br />');
                        var template = template + sign;

                        var mailOptions = {
                            from: "Bojeo <" + data._doc.mailUser + ">", // sender address
                            to: user._doc.email, // list of receivers
                            subject: data._doc.devolution.subject.es, // Subject line
                            html: template
                        }
                        sendMail(smtpTransport, mailOptions, function (mail) {
                            cb(mail);
                        });

                    }
                    else {
                        if (cb) cb(true);
                    }
                }
                else {
                    if (cb) cb(true);
                }
            });
        }
        else {
            if (cb) cb(true);
        }
    })

};
function getGroups(id, cb) {
    db.Group.findOne({_id: id}).exec(function (err, group) {
        if (err || !group) {
            if (cb) cb(false);
        }
        else {
            if (cb) cb(group);
        }
    })
};
function getLastSale(req, cb) {
    db.Sale.findOne({user: req.user._id}, function (err, sale) {
        cb(err, sale);
    })
};
exports.getResevationClientByTour = function (tour, pos, cb) {
    async.waterfall([
        function (callback) {
            db.ReservationTour.find({tour: tour._doc._id, remove: false}, function (err, resTs) {
                callback(err, resTs);
            })
        },
        function (resTs, callback) {
            var array = [];
            async.map(resTs, function (resT, callback0) {
                db.ReservationClient.find({
                    reservation: resT._doc._id,
                    state: 0,
                    cancel: true
                }, function (err, resClient) {
                    callback0(err, resClient);
                })
            }, function (err, result) {
                if (err || !result) {
                    callback(err, result);
                }
                else {
                    if (result.length) {
                        for (var i = 0; i < result.length; i++) {
                            for (var j = 0; j < result[i].length; j++) {

                                array.push(result[i][j]);

                            }
                        }
                        callback(null, array);
                    } else {
                        callback(null, result);
                    }
                }
            })
        }
    ], function (err, result) {
        if (err) {
            if (cb) cb(err, null, pos);
        } else {
            if (cb) cb(false, result, pos);
        }
    })
};
function generateTPVFormDiferido(price, description, req) {
    var redsys = new Redsys({
        "merchant": {
            "code": redsysConfig.merchant.code,
            "terminal": redsysConfig.merchant.terminal,
            "titular": redsysConfig.merchant.titular,
            "name": redsysConfig.merchant.name,
            "secret": redsysConfig.merchant.secret
        },
        "language": "auto",
        "test": redsysConfig.test
    });

    var form_data = redsys.create_payment({
        total: price,
        currency: "EUR",
        order: ((new Date()).getTime() + "").substring(0, 12),
        description: description,
        //data: "CART DATA",
        transaction_type: '0',
        redirect_urls: {
            return_url: urlUtil.resolve(global.bojeoServer,"api/reservation/returnone"),
            cancel_url: urlUtil.resolve(global.bojeoServer, "/api/reservation/payCancel")
        }
    });
    console.log(form_data);
    return form_data;
};
function generateTPVFormDiferidoConfirm(price, description, orderId) {
    var redsys = new Redsys({
        "merchant": {
            "code": redsysConfig.merchant.code,
            "terminal": redsysConfig.merchant.terminal,
            "titular": redsysConfig.merchant.titular,
            "name": redsysConfig.merchant.name,
            "secret": redsysConfig.merchant.secret
        },
        "language": "auto",
        "test": redsysConfig.test
    });

    var form_data = redsys.create_payment({
        total: price,
        currency: "EUR",
        order: orderId,
        description: description,
        //data: "CART DATA",
        transaction_type: 'P',
        redirect_urls: {
            callback: global.bojeoServer + "/api/reservation/returnone",
            return_url: "/",
            cancel_url: urlUtil.resolve(global.bojeoServer, "/api/reservation/payCancel")
        }
    });
    console.log(form_data);
};
function generateTPVFormDiferidoCancel(price, description, orderId) {
    var redsys = new Redsys({
        "merchant": {
            "code": redsysConfig.merchant.code,
            "terminal": redsysConfig.merchant.terminal,
            "titular": redsysConfig.merchant.titular,
            "name": redsysConfig.merchant.name,
            "secret": redsysConfig.merchant.secret
        },
        "language": "auto",
        "test": redsysConfig.test
    });

    var form_data = redsys.create_payment({
        total: price,
        currency: "EUR",
        order: orderId,
        description: description,
        //data: "CART DATA",
        transaction_type: 'Q',
        redirect_urls: {
            callback: global.bojeoServer + "/api/reservation/returnone",
            return_url: "/",
            cancel_url: urlUtil.resolve(global.bojeoServer, "/api/reservation/payCancel")
        }
    });
    console.log(form_data);
};
function generateTPVFormRefun(price, description, orderId) {
    var redsys = new Redsys({
        "merchant": {
            "code": redsysConfig.merchant.code,
            "terminal": redsysConfig.merchant.terminal,
            "titular": redsysConfig.merchant.titular,
            "name": redsysConfig.merchant.name,
            "secret": redsysConfig.merchant.secret
        },
        "language": "auto",
        "test": redsysConfig.test
    });
    var form_data = redsys.create_payment({
        total: price,
        currency: "EUR",
        order: orderId,
        description: description,
        //data: "CART DATA",
        transaction_type: '3',
        redirect_urls: {
            callback: global.bojeoServer + "/api/tpv/notification",
            return_url:  urlUtil.resolve(global.bojeoServer, req.user.language+"/receipt"),
            cancel_url:  urlUtil.resolve(global.bojeoServer, req.user.language+"/error-conection")
        }
    });
    console.log(form_data);
    return form_data;
};
function postRefun(form_data, cb) {
    request.post({url: form_data.URL, form: form_data}, function (err, httpResponse, body) {
        if (err) {
            console.log(err);
            cb(err, body);
        }
        else {
            console.log("------------response tpv---------------");
            console.log(body);
            cb(err, body);
        }
    })
};
function validateAddEvent(array, event) {
    if (array.length) {
        for (var i = 0; i < array.length; i++) {
            var start = array[i].start;
            var end = array[i].end;
            if (array[i].end == null) {
                end = start;
            }
            if (array[i].allDay) {
                end = new Date(end.getFullYear(), end.getMonth(), end.getDate());
                end = new Date(end.getTime() + 86399999);
            }

            var eventEnd = event.end;
            if (event.end == null) {
                eventEnd = event.start;
            }
            if (event.allDay) {
                eventEnd = new Date(eventEnd.getFullYear(), eventEnd.getMonth(), eventEnd.getDate());
                eventEnd = new Date(eventEnd.getTime() + 86399999);
            }

            if (matchCalendarBlock(start, end, event.start, eventEnd)) {
                return false;
            }
        }
        return true;
    }
    else {
        return true;
    }
};
function matchCalendarBlock(a, b, c, d) {
    if (c < a && a < d) {
        return true;
    }
    if (c < b && b < d) {
        return true;
    }
    if (a < c && c < b) {
        return true;
    }
    if (a < d && d < b) {
        return true;
    }
    if (a.getTime() == c.getTime() && b.getTime() == d.getTime()) {
        return true;
    }
    return false;
}
function departureTime(time) {
    var array = time.split(':');
    if (array.length == 2) {
        var hour = parseInt(array[0]);
        var min = parseInt(array[1]);

        if (isNaN(hour) || isNaN(min)) {
            return false;
        }
        else {
            if (hour >= 0 && hour < 24 && min >= 0 && min < 60) {
                return (hour * 60 + min) * 60000;
            }
            else {
                return false;
            }

        }
    }
    else {
        return false;
    }
};