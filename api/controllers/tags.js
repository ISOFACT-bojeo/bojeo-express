/**
 * Created by Home on 04/06/2015.
 */

var _ = require('lodash');
var async = require('async');

exports.create = function (req, res) {
    var name = req.body.name;

    if (_.isEmpty(name.es) || _.isEmpty(name.en)) {
        return res.json({res: false});
    }
    else {
        var tag = new db.Tags({
            name: name
        });

        tag.save(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true});
            }
        });
    }
};

exports.get = function (req, res) {
    db.Tags.findOne({_id: req.params.id, remove: false}).exec(function (err, tag) {
        if (err || !tag) {
            return res.json({res: false});
        }
        else {
            return res.json({res: tag});
        }
    });
};

exports.update = function (req, res) {
    var id = req.body.id;
    var name = req.body.name;

    if (_.isEmpty(name.es) || _.isEmpty(name.en)) {
        return res.json({res: false});
    }
    else {
        db.Tags.update({_id: id, remove: false}, {
            $set: {
                name: name
            }
        }, function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true});
            }
        });
    }

};

exports.changeStatus = function (req, res) {
    var id = req.body.id;
    db.Tags.findOne({_id: id, remove: false}).exec(function (err, tag) {
        if (err || !tag) {
            return res.json({res: false});
        }
        else {
            db.Tags.update({_id: tag._doc._id}, {$set: {available: !tag._doc.available}}, function (err, success) {
                if (err || !success) {
                    return res.json({res: false});
                }
                else {
                    return res.json({res: success});
                }
            });
        }

    })
};

exports.list = function (req, res) {
    var limit = req.body.limit,
        skip = req.body.skip;
    async.parallel([
        function (cbb) {
            db.Tags.count({remove: false}).exec(function (err, count) {
                cbb(err, count);
            })
        },
        function (cbb) {
            db.Tags.find({
                remove: false
            }).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, tags) {
                if (err || !tags) {
                    cbb(err, tags);
                }
                else {
                    async.map(tags, function (tag, callback) {
                            tag.freeForRemove(function (err, array) {
                                if (err || !array) {
                                    callback(err, array);
                                }
                                else {
                                    tag._doc.dependencies = array;
                                    callback(err, tag);
                                }
                            });
                        }, function (err, result) {
                            cbb(err, result);
                        }
                    )
                }
            });
        }
    ], function (err, results) {
        if (err || !results) {
            return res.json({res: [], count: 0});
        } else {
            return res.json({res: results[1], count: results[0]});
        }
    });

};

exports.remove = function (req, res) {
    var id = req.params.id;
    if(!_.isEmpty(id)){
        db.Tags.update({remove: false, _id: id}, {$set: {remove: true}}, function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: success});
            }
        });
    }
    else{
        return res.json({res: false});
    }

};