/**
 * Created by ernestomr87@gmail.com on 16/07/14.
 */
var async = require('async');


exports.list = function (req, res) {
    var limit = req.query.limit,
        skip = req.query.skip,
        text = req.query.text;
    if (text.length > 0) {
        async.parallel([
            function (cb) {
                db.Contact.count().exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                text = new RegExp(text, 'i');
                db.Contact.find({$or: [{fullName: text}, {email: text}, {city: text}, {country: text}, {tel: text}, {comment: text}, {company: text}]}).sort('-createDate').limit(limit).skip(skip).exec(function (err, contacts) {
                    if (cb) cb(err, contacts);
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                return res.json({res: results[1], cont: results[0]});
            }
        });
    }
    else {
        async.parallel([
            function (cb) {
                db.Contact.count().exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                db.Contact.find().sort('-createDate').limit(limit).skip(skip).exec(function (err, contacts) {
                    if (cb) cb(err, contacts);
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                return res.json({res: results[1], cont: results[0]});
            }
        });
    }
};

exports.create = function (req, res) {
    var body = req.body;
    var date = new Date();
    var contact = new db.Contact({
        createDate: date,
        fullName: body.fullName,
        email: body.email,
        city: body.city,
        country: body.country,
        tel: body.tel,
        company: body.company,
        comment: body.comment
    });

    contact.save(function (err, success) {
        if (err || !success) {
            return res.json({res: false});
        }
        else {
            return res.json({res: true});
        }
    });
};
exports.remove = function (req, res) {
    var id = req.params.id;

    db.Contact.remove({_id: id}, function (err, success) {
        if (success) {
            return res.json(true);
        } else {
            return res.json(err);
        }
    });
};
exports.changeReading = function (req, res) {
    var id = req.body.id;
    var reading = req.body.reading;

    db.Contact.update({_id: id}, {$set: {reading: reading}}, function (err, success) {
        if (err || !success) {
            return res.json({res: false})
        }
        else {
            return res.json({res: true})
        }
    });
};
