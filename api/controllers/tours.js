/**
 * Created by ernestomr87@gmail.com on 04/06/2014.
 */
var utils = require('../utils');
var ships = require('./ship');
var reservationController = require('./reservations');
var async = require('async');
var htmlToText = require('html-to-text');
var _ = require('lodash');

exports.create = function (req, res) {

    var flag = true;
    if (req.body.data.payForm == 'bonds') {
        if (_.isEmpty(req.body.data.bond.bond) || _.isNaN(req.body.data.bond.minHour) || _.isNaN(req.body.data.bond.maxHour)) {
            flag = false;
        }
    }
    else {
        if (_.isEmpty(req.body.departureTxt.es) || _.isEmpty(req.body.departureTxt.en) || _.isEmpty(req.body.priceTxt.es) || _.isEmpty(req.body.priceTxt.es)) {
            flag = false;
        }
    }

    if (flag) {
        var searchText = {
            es: htmlToText.fromString(req.body.description.es),
            en: htmlToText.fromString(req.body.description.en)
        };
        var files = req.files;
        var photos = [];
        var typeImgs = ["image/jpeg", "image/png"];
        var correct_photo = false;
        var pos = 0;
        while (eval("files.file" + pos)) {
            var tempFile = (eval("files.file" + pos));
            if (tempFile.mimetype != "" || tempFile.mimetype != null) {
                var image = {
                    path: "",
                    name: "",
                    originalName: "",
                    extension: ""
                };
                if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                    image.name = tempFile.name;
                    image.path = "/api/media/" + tempFile.name;
                    image.originalName = tempFile.originalname;
                    image.extension = tempFile.extension;
                    photos.push(image);
                    correct_photo = true;
                }
                if (!correct_photo) {
                    utils.deleteFile(tempFile.name);
                    return res.json({error: 'incorrect image!!!'});
                }
            }
            pos++;
        }
        var tour = new db.Tour({
            group: req.body.group,
            port: req.body.port,
            name: req.body.name,
            description: req.body.description,
            daysWeek: req.body.daysWeek,
            daysTxt: req.body.daysTxt,
            priceTxt: req.body.priceTxt,
            departureTxt: req.body.departureTxt,
            recommendations: req.body.recommendations,
            forReserve: req.body.forReserve,
            transport: req.body.transport,
            conditions: req.body.conditions,
            language: req.body.language,
            extras: req.body.extras,
            noIncludes: req.body.noIncludes,
            data: req.body.data,
            photos: [],
            searchText: searchText,
            seo: typeof req.body.seo == "string" ? JSON.parse(req.body.seo) : req.body.seo,
            slug: {
                es: utils.getSlug(req.body.name.es),
                en: utils.getSlug(req.body.name.en)
            }


        });
        tour.save(function (err, tour) {
            if (err || !tour) {
                return res.json({res: false})
            }
            else {
                return res.json({res: true})
            }
        });
    }
    else {
        return res.json({res: false})
    }

};
exports.list = function (req, res) {
    db.Tour.find({remove: false}, function (err, tours) {
        if (err || !tours) {
            return res.json({res: false});
        }
        else {

            var toursArray = tours;
            getPorts(null, function (portsData) {
                if (portsData) {
                    var ports = portsData;
                    getGroups(function (groupsData) {
                        if (groupsData) {
                            var groups = groupsData;
                            for (var i = 0; i < toursArray.length; i++) {
                                for (var j = 0; j < ports.length; j++) {
                                    if (toursArray[i]._doc.port.toString() == ports[j]._doc._id.toString()) {
                                        var aux = {
                                            name: ports[j]._doc.name,
                                            id: ports[j]._doc._id
                                        }
                                        toursArray[i]._doc.port = aux;
                                    }
                                }
                                for (var j = 0; j < groups.length; j++) {
                                    if (toursArray[i]._doc.group.toString() == groups[j]._doc._id.toString()) {
                                        var aux = {
                                            name: groups[j]._doc.name,
                                            id: groups[j]._doc._id,
                                            color: groups[j]._doc.color
                                        }
                                        toursArray[i]._doc.group = aux;
                                    }
                                }

                            }
                            return res.json({res: toursArray});
                        }
                        else {
                            return res.json({res: false});
                        }
                    })
                }
                else {
                    return res.json({res: false});
                }
            })


        }
    })
};
exports.listBack = function (req, res) {
    var limit = req.body.limit,
        skip = req.body.skip,
        id = req.body.id,
        text = req.body.text;

    async.parallel([
        function (cb) {
            db.Tour.count({remove: false}).exec(function (err, count) {
                if (cb) cb(err, count);
            })
        },
        function (cb) {

            if (id.length > 0) {
                var params = {
                    remove: false, $or: [
                        {_id: id},
                        {port: id},
                        {group: id}
                    ]
                };
                if (text.length) {
                    var word = new RegExp(text, 'i');
                    params = {
                        remove: false, $or: [
                            {_id: id},
                            {port: id},
                            {group: id}
                        ],
                        'name.es': word,
                        'name.en': word
                    };
                }
                db.Tour.find(params).populate('port').populate('group').exec(function (err, tours) {
                    if (err || !tours) {
                        async.map(tours, function (tour, callback) {
                                tour.freeForRemove(function (err, data) {
                                    if (err || !data) {
                                        callback(null, tour);
                                    }
                                    else {
                                        tour._doc.dependencies = data;
                                    }
                                })
                            }, function (err, result) {
                                cb(err, result);
                            }
                        )
                    }
                    else {
                        async.map(tours, function (tour, callback) {
                                tour.freeForRemove(function (err, data) {
                                    if (err || !data) {
                                        callback(null, tour);
                                    }
                                    else {
                                        tour._doc.dependencies = data;
                                    }
                                })
                            }, function (err, result) {
                                cb(err, result);
                            }
                        )
                    }
                })
            }
            else {
                var params = {
                    remove: false
                };
                if (text.length) {
                    var word = new RegExp(text, 'i');
                    params = {
                        remove: false,
                        'name.es': word,
                        'name.en': word
                    };
                }
                db.Tour.find(params).sort('-name').limit(limit).skip(skip).populate('port').populate('group').exec(function (err, tours) {
                    if (err || !tours) {
                        cb(err, tours);
                    }
                    else {
                        async.map(tours, function (tour, callback) {
                                tour.freeForRemove(function (err, data) {
                                    if (err || !data) {
                                        callback(null, tour);
                                    }
                                    else {
                                        tour._doc.dependencies = data;
                                        callback(null, tour);
                                    }
                                })
                            }, function (err, result) {
                                cb(err, result);
                            }
                        )
                    }
                })
            }

        }
    ], function (err, results) {
        if (err || !results) {
            return res.json({res: false, cont: 0});
        }
        else {
            return res.json({res: results[1], cont: results[0]});
            //var toursArray = results[1];
            //getPorts(null, function (portsData) {
            //    if (portsData) {
            //        var ports = portsData;
            //        getGroups(function (groupsData) {
            //            if (groupsData) {
            //                var groups = groupsData;
            //                for (var i = 0; i < toursArray.length; i++) {
            //                    for (var j = 0; j < ports.length; j++) {
            //                        if (toursArray[i]._doc.port.toString() == ports[j]._doc._id.toString()) {
            //                            var aux = {
            //                                name: ports[j]._doc.name,
            //                                id: ports[j]._doc._id
            //                            };
            //                            toursArray[i]._doc.port = aux;
            //                        }
            //                    }
            //                    for (var j = 0; j < groups.length; j++) {
            //                        if (toursArray[i]._doc.group.toString() == groups[j]._doc._id.toString()) {
            //                            var aux = {
            //                                name: groups[j]._doc.name,
            //                                id: groups[j]._doc._id,
            //                                color: groups[j]._doc.color
            //                            };
            //                            toursArray[i]._doc.group = aux;
            //                        }
            //                    }
            //
            //                }
            //                async.map(toursArray, function (tour, callback) {
            //                    async.parallel([
            //                        function (cb) {
            //                            getShipByTour(tour, i, function (ship, pos) {
            //                                if (ship.length > 0) {
            //                                    tour._doc.hasShip = true;
            //                                }
            //                                else {
            //                                    tour._doc.hasShip = false;
            //                                }
            //                                cb(null, tour);
            //                            })
            //                        },
            //                        function (cb) {
            //                            getPackByTour(tour, function (packs) {
            //                                if (packs.length > 0) {
            //                                    tour._doc.hasPack = true;
            //                                } else {
            //                                    tour._doc.hasPack = false;
            //                                }
            //                                cb(null, tour);
            //                            })
            //                        },
            //                        function (cb) {
            //                            if (tour._doc.price.payform == "bonds") {
            //                                getBondById(tour._doc.price.bond, function (bond) {
            //                                    if (bond) {
            //                                        tour._doc.price.hours = bond._doc.hours;
            //                                        tour._doc.price.price = bond._doc.price;
            //                                    } else {
            //                                        tour._doc.price.hours = 0;
            //                                        tour._doc.price.price = 0.00;
            //                                    }
            //                                })
            //                            }
            //                            cb(null, tour);
            //                        }
            //                    ], function (err, datos) {
            //                        callback(null, datos[2]);
            //                    })
            //                }, function (err, toursArray1) {
            //                    return res.json({res: toursArray1, cont: results[0]});
            //                })
            //            }
            //            else {
            //                return res.json({res: false, cont: 0});
            //            }
            //        })
            //    }
            //    else {
            //        return res.json({res: false, cont: 0});
            //    }
            //})
        }
    });

};
exports.listAvailable = function (req, res) {
    db.Tour.find({remove: false, available: true}, function (err, tours) {
        if (err || !tours) {
            return res.json({res: false});
        }
        else {
            var toursArray = tours;
            getPorts(null, function (portsData) {
                if (portsData) {
                    var ports = portsData;
                    getGroups(function (groupsData) {
                        if (groupsData) {
                            var groups = groupsData;
                            for (var i = 0; i < toursArray.length; i++) {
                                for (var j = 0; j < ports.length; j++) {
                                    if (toursArray[i]._doc.port.toString() == ports[j]._doc._id.toString()) {
                                        var aux = {
                                            name: ports[j]._doc.name,
                                            id: ports[j]._doc._id
                                        }
                                        toursArray[i]._doc.port = aux;
                                    }
                                }
                                for (var j = 0; j < groups.length; j++) {
                                    if (toursArray[i]._doc.group.toString() == groups[j]._doc._id.toString()) {
                                        var aux = {
                                            name: groups[j]._doc.name,
                                            id: groups[j]._doc._id,
                                            color: groups[j]._doc.color
                                        }
                                        toursArray[i]._doc.group = aux;
                                    }
                                }

                            }
                            return res.json({res: toursArray});
                        }
                        else {
                            return res.json({res: false});
                        }
                    })
                }
                else {
                    return res.json({res: false});
                }
            })
        }
    })
};
exports.get = function (req, res) {
    var slug = req.params.id;

    async.parallel([
        function (cb) {
            db.Tour.findOne(
                {
                    $or: [{'slug.en': slug}, {'slug.es': slug}],
                    available: true,
                    remove: false
                }
            ).exec(function (err, tour) {
                    cb(err, tour);
                })
        },
        function (cb) {
            db.Pack.findOne({$or: [{'slug.en': slug}, {'slug.es': slug}], remove: false}).exec(function (err, pack) {
                cb(err, pack);
            })
        }
    ], function (err, results) {
        if (err || !results) {
            return res.json({res: false});
        }
        else {
            if (results[0] != null) {
                moreTourInfo(res, results[0]);
            }
            else if (results[1] != null) {
                db.Tour.findOne({_id: results[1]._doc.tour, remove: false, available: true}, function (err, tour) {
                    if (err || !tour) {
                        return res.json({res: false});
                    }
                    else {
                        moreTourInfo(res, tour);
                    }
                })
            }
            else {
                return res.json({res: false});
            }
        }
    });

    function moreTourInfo(res, tour) {
        getPorts(null, function (portsData) {
            if (portsData) {
                var ports = portsData;
                getGroups(function (groupsData) {
                    if (groupsData) {
                        var groups = groupsData;
                        for (var j = 0; j < ports.length; j++) {
                            if (tour._doc.port.toString() == ports[j]._doc._id.toString()) {
                                var aux = {
                                    address: ports[j]._doc.address,
                                    name: ports[j]._doc.name,
                                    id: ports[j]._doc._id
                                }
                                tour._doc.port = aux;
                            }
                        }
                        for (var j = 0; j < groups.length; j++) {
                            if (tour._doc.group.toString() == groups[j]._doc._id.toString()) {
                                var aux = {
                                    name: groups[j]._doc.name,
                                    id: groups[j]._doc._id,
                                    slug: groups[j]._doc.slug
                                }
                                tour._doc.group = aux;
                            }
                        }
                        return res.json({res: tour});
                    }
                    else {
                        return res.json({res: false});
                    }
                })
            }
            else {
                return res.json({res: false});
            }
        })
    }
};
exports.status = function (req, res) {
    db.Tour.update({
        _id: req.body.id,
        available: req.body.available
    }, {$set: {available: !req.body.available}}).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false})
        }
        else {
            return res.json({res: true})
        }
    })
    //if (req.body.action == 3) {
    //    var idPort = req.body.idPort;
    //    db.Port.update({_id: idPort}, {$set: {available: true}},
    //        function (err, success) {
    //            if (err || !success) {
    //                return res.json({res: false})
    //            }
    //            else {
    //                db.Tour.update({
    //                    _id: req.body.id,
    //                    available: req.body.available
    //                }, {$set: {available: !req.body.available}}).exec(function (err, success) {
    //                    if (err || !success) {
    //                        return res.json({res: false})
    //                    }
    //                    else {
    //                        return res.json({res: true, complete: true});
    //                    }
    //                })
    //            }
    //        })
    //} else {
    //    if (req.body.action == 2) {
    //        var idPort = req.body.idPort;
    //        db.Port.count({_id: idPort, remove: false, available: true}, function (err, count) {
    //            if (err) {
    //                return res.json({res: false})
    //            } else {
    //                if (count != 0) {
    //                    db.Tour.update({
    //                        _id: req.body.id,
    //                        available: req.body.available
    //                    }, {$set: {available: !req.body.available}}).exec(function (err, success) {
    //                        if (err || !success) {
    //                            return res.json({res: false})
    //                        }
    //                        else {
    //                            return res.json({res: true, complete: true});
    //                        }
    //                    })
    //                } else {
    //                    return res.json({res: true, complete: false});
    //                }
    //            }
    //        })
    //    } else {
    //        async.waterfall([
    //            function (callback) {
    //                db.ReservationTour.find({tour: req.body.id, remove: false}, function (err, resTs) {
    //                    callback(err, resTs);
    //                })
    //            },
    //            function (resTs, callback) {
    //                async.map(resTs, function (resT, callback0) {
    //                    db.ReservationClient.count({
    //                        reservation: resT._doc._id,
    //                        state: 0,
    //                        cancel: true
    //                    }, function (err, count) {
    //                        callback0(err, count);
    //                    })
    //                }, function (err, result) {
    //                    if (err || !result) {
    //                        callback(err, result);
    //                    }
    //                    else {
    //                        var cant = 0;
    //                        for (var i = 0; i < result.length; i++) {
    //                            cant = cant + result[i];
    //                        }
    //                        callback(null, cant);
    //                    }
    //                })
    //            }
    //        ], function (err, result) {
    //            if (err) {
    //                return res.json({res: false})
    //            }
    //            else {
    //                var reservations = result;
    //                async.waterfall([
    //                    function (callback) {
    //                        db.Pack.find({tour: req.body.id, status: true, remove: false}, function (err, paks) {
    //                            callback(err, paks);
    //                        })
    //                    },
    //                    function (paks, callback) {
    //                        async.map(paks, function (pack, callback0) {
    //                            async.map(pack.services, function (service, callback1) {
    //                                db.Service.findOne({_id: service}).select('name').exec(function (err, servs) {
    //                                    callback1(err, servs);
    //                                })
    //                            }, function (err, result) {
    //                                if (err || !result) {
    //                                    callback0(err, result);
    //                                }
    //                                else {
    //                                    var aux = {
    //                                        id: pack._id,
    //                                        services: result
    //                                    };
    //                                    callback0(null, aux);
    //                                }
    //
    //                            })
    //                        }, function (err, result) {
    //                            callback(err, result);
    //                        })
    //                    }
    //                ], function (err, result) {
    //                    if (err || !result) {
    //                        return res.json({res: false});
    //                    }
    //                    else {
    //                        if (req.body.action == 1) {
    //                            async.map(result, function (pack, callback1) {
    //                                db.Pack.update({_id: pack.id}, {$set: {status: false}}, function (err, success) {
    //                                    callback1(err, success)
    //                                })
    //                            }, function (err, result) {
    //                                if (err || !result) {
    //                                    return res.json({res: false, complete: false});
    //                                }
    //                                else {
    //                                    db.Tour.update({
    //                                        _id: req.body.id,
    //                                        available: req.body.available
    //                                    }, {$set: {available: !req.body.available}}).exec(function (err, success) {
    //                                        if (err || !success) {
    //                                            return res.json({res: false})
    //                                        }
    //                                        else {
    //                                            return res.json({res: true, complete: true});
    //                                        }
    //                                    })
    //                                }
    //                            })
    //                        }
    //                        else {
    //                            if (result.length) {
    //                                exports.getTour(req.body.id, function (tour) {
    //                                    if (tour) {
    //                                        var aux = {
    //                                            reservations: reservations,
    //                                            packs: result,
    //                                            tour: {
    //                                                name: tour._doc.name,
    //                                                group: tour._doc.group
    //                                            }
    //                                        };
    //                                        return res.json({res: aux, complete: false});
    //                                    }
    //                                    else {
    //                                        return res.json({res: false});
    //                                    }
    //                                })
    //                            }
    //                            else {
    //                                db.Tour.findOneAndUpdate({
    //                                    _id: req.body.id,
    //                                    available: req.body.available
    //                                }, {$set: {available: !req.body.available}}).exec(function (err, tour) {
    //                                    if (err || !tour) {
    //                                        return res.json({res: false})
    //                                    }
    //                                    else {
    //                                        if (tour._doc.price.bond) {
    //                                            db.Bonds.update({_id: tour._doc.price.bond}, {$set: {available: false}}, function () {
    //                                                return res.json({res: true, complete: true});
    //                                            })
    //                                        }
    //                                        else {
    //                                            return res.json({res: true, complete: true});
    //                                        }
    //                                    }
    //                                })
    //                            }
    //                        }
    //                    }
    //                });
    //            }
    //        });
    //    }
    //}
};
exports.remove = function (req, res) {
    async.waterfall([
        function (callback) {
            db.Pack.find({tour: req.params.id, remove: false}, function (err, paks) {
                callback(err, paks);
            })
        },
        function (paks, callback) {
            async.map(paks, function (pack, callback0) {
                db.Pack.update({_id: pack._id}, {$set: {remove: true}}).exec(function (err, success) {
                    callback0(err, success)
                })
            }, function (err, result) {
                callback(err, result);
            })
        }
    ], function (err, result) {
        if (err || !result) {
            return res.json({res: false});
        }
        else {
            db.Tour.findOne({_id: req.params.id}).exec(function (err, success) {
                if (err || !success) {
                    return res.json({res: false});
                } else {
                    var photos = success._doc.photos;
                    if (photos.length > 0) {
                        var cont = 0;
                        for (var j = 0; j < photos.length; j++) {
                            db.Media.remove({fieldName: photos[j].name}).exec(function (err, success) {
                                cont++;
                                if (cont == photos.length) {
                                    db.Tour.update({_id: req.params.id}, {$set: {remove: true}}).exec(function (err, success) {
                                        if (err || !success) {
                                            return res.json({res: false})
                                        } else {
                                            return res.json({res: true})
                                        }
                                    })
                                }
                            })
                        }
                    }
                    else {
                        db.Tour.update({_id: req.params.id}, {$set: {remove: true}}).exec(function (err, success) {
                            if (err || !success) {
                                return res.json({res: false})
                            } else {
                                return res.json({res: true})
                            }
                        })
                    }
                }
            })
        }
    })
};
exports.update = function (req, res) {
    var searchText = {
        es: htmlToText.fromString(req.body.description.es),
        en: htmlToText.fromString(req.body.description.en)
    };
    db.Tour.update({_id: req.body.id}, {
        $set: {
            group: req.body.group._id || req.body.group,
            name: req.body.name,
            description: req.body.description,
            port: req.body.port,
            daysWeek: req.body.daysWeek,
            daysTxt: req.body.daysTxt,
            priceTxt: req.body.priceTxt,
            departureTxt: req.body.departureTxt,
            recommendations: req.body.recommendations,
            forReserve: req.body.forReserve,
            transport: req.body.transport,
            conditions: req.body.conditions,
            language: req.body.language,
            extras: req.body.extras,
            noIncludes: req.body.noIncludes,
            data: req.body.data,
            searchText: searchText,
            seo: typeof req.body.seo == "string" ? JSON.parse(req.body.seo) : req.body.seo,
            slug: {
                es: utils.getSlug(req.body.name.es),
                en: utils.getSlug(req.body.name.en)
            }
        }
    }).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false})
        }
        else {
            return res.json({res: true})
        }
    })
};
exports.updateImg = function (req, res) {
    var idTour = req.body.id;
    var nameImgDel = eval(req.body.nameImgDel);
    var files = req.files;

    var typeImgs = ["image/jpeg", "image/png"];

    if (nameImgDel.length != 0) {
        var cont = 0;
        for (var i = 0; i < nameImgDel.length; i++) {
            db.Media.remove({fieldName: nameImgDel[i]}).exec(function (err, success) {
                if (err || !success) {
                    return res.json({result: false});
                } else {
                    cont++;
                    if (cont == nameImgDel.length) {
                        db.Tour.findOne({_id: idTour}).exec(function (err, tour) {
                            if (err || !tour) {
                                return res.json({result: false});
                            } else {
                                for (var i = 0; i < nameImgDel.length; i++) {
                                    var pos = 0;
                                    var flag = false;
                                    var cantidad = tour.photos.length;
                                    while (!flag && pos < cantidad) {
                                        if (tour.photos[pos].name == nameImgDel[i]) {
                                            tour.photos.splice(pos, 1);
                                            flag = true;
                                        }
                                        pos++;
                                    }
                                }
                                var pos = 0;
                                while (eval("files.file" + pos)) {
                                    var tempFile = (eval("files.file" + pos));
                                    if (tempFile.mimetype != "" || tempFile.mimetype != null) {
                                        if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                                            var photo = {
                                                path: "/api/media/" + tempFile.name,
                                                name: tempFile.name,
                                                originalName: tempFile.originalname,
                                                extension: tempFile.extension
                                            };
                                            tour.photos.push(photo);
                                        } else {
                                            utils.deleteFile(tempFile.name);
                                        }
                                    } else {
                                        utils.deleteFile(tempFile.name);
                                    }
                                    pos++;
                                }
                                db.Tour.update({_id: idTour}, {$set: {photos: tour.photos}}).exec(
                                    function (err, success) {
                                        if (err || !success) {
                                            return res.json({result: false});
                                        } else {
                                            return res.json({result: true});
                                        }
                                    })
                            }
                        })
                    }
                }
            })
        }
    } else {
        db.Tour.findOne({_id: idTour}).exec(function (err, tour) {
            if (err || !tour) {
                return res.json({result: false});
            } else {
                var pos = 0;
                while (eval("files.file" + pos)) {
                    var tempFile = (eval("files.file" + pos));
                    if (tempFile.mimetype != "" || tempFile.mimetype != null) {
                        if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                            var photo = {
                                path: "/api/media/" + tempFile.name,
                                name: tempFile.name,
                                originalName: tempFile.originalname,
                                extension: tempFile.extension
                            };
                            tour.photos.push(photo);
                        } else {
                            utils.deleteFile(tempFile.name);
                        }
                    } else {
                        utils.deleteFile(tempFile.name);
                    }
                    pos++;
                }
                db.Tour.update({_id: idTour}, {$set: {photos: tour.photos}}).exec(
                    function (err, success) {
                        if (err || !success) {
                            return res.json({result: false});
                        } else {
                            return res.json({result: true});
                        }
                    })
            }
        })
    }
};
exports.removeOnlyMedia = function (req, res) {
    var id = req.body.id,
        photo = req.body.photo;

    db.Tour.update({_id: id}, {$pull: {photos: photo}}).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false})
        }
        else {
            db.Media.remove({fieldName: photo.name}).exec(function (err, success) {
                if (err || !success) {
                    return res.json({res: false})
                }
                else {
                    return res.json({res: true})
                }
            })
        }
    });
};
exports.addOnlyMedia = function (req, res) {
    var extImages = ['jpg', 'png', 'gif', 'bmp'];
    var tmp = false;
    for (var i = 0; i < extImages.length; i++) {
        if (req.files.file0.extension.toLowerCase() != extImages[i]) {
            tmp = true;
        }
    }
    if (!tmp) {
        return res.json({res: false});
    }
    else {
        var aux = {
            path: "/api/media/" + req.files.file0.name,
            "name": req.files.file0.name
        };

        db.Tour.update({_id: req.body.id}, {$push: {photos: aux}}).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: aux});
            }
        });
    }
};
exports.listByPort = function (req, res, cb) {
    db.Tour.find({
        port: req.body.port,
        available: true,
        remove: false
    }).sort({'price.price': 1}).exec(function (err, tours) {
        if (err || !tours) {
            return res.json({res: false});
        }
        else {
            var toursArray = tours;
            getPorts(null, function (portsData) {
                if (portsData) {
                    var ports = portsData;
                    getGroups(function (groupsData) {
                        if (groupsData) {
                            var groups = groupsData;
                            for (var i = 0; i < toursArray.length; i++) {
                                for (var j = 0; j < ports.length; j++) {
                                    if (toursArray[i]._doc.port.toString() == ports[j]._doc._id.toString()) {
                                        var aux = {
                                            name: ports[j]._doc.name,
                                            id: ports[j]._doc._id
                                        }
                                        toursArray[i]._doc.port = aux;
                                    }
                                }
                                for (var j = 0; j < groups.length; j++) {
                                    if (toursArray[i]._doc.group.toString() == groups[j]._doc._id.toString()) {
                                        var aux = {
                                            name: groups[j]._doc.name,
                                            id: groups[j]._doc._id,
                                            front: groups[j]._doc.front,
                                            color: groups[j]._doc.color,
                                            photos: groups[j]._doc.photos
                                        }
                                        toursArray[i]._doc.group = aux;
                                    }
                                }
                            }
                            if (req.body.local) {
                                cb(toursArray);
                            }
                            else {
                                return res.json({res: toursArray});
                            }

                        }
                        else {
                            if (req.body.local) {
                                cb(false);
                            }
                            else {
                                return res.json({res: false});
                            }
                        }
                    })
                }
                else {
                    if (req.body.local) {
                        cb(false);
                    }
                    else {
                        return res.json({res: false});
                    }
                }
            })
        }
    })
};
exports.listByGroup = function (req, res) {
    db.Tour.find({
        port: req.body.port,
        available: true,
        remove: false
    }).exec(function (err, tours) {
        if (err || !tours) {
            return res.json({res: false});
        }
        else {
            var toursArray = tours;
            getPorts(null, function (portsData) {
                if (portsData) {
                    var ports = portsData;
                    getGroups(function (groupsData) {
                        if (groupsData) {
                            var groups = groupsData;
                            for (var i = 0; i < toursArray.length; i++) {
                                for (var j = 0; j < ports.length; j++) {
                                    if (toursArray[i]._doc.port.toString() == ports[j]._doc._id.toString()) {
                                        var aux = {
                                            name: ports[j]._doc.name,
                                            id: ports[j]._doc._id
                                        }
                                        toursArray[i]._doc.port = aux;
                                    }
                                }
                                for (var j = 0; j < groups.length; j++) {
                                    if (toursArray[i]._doc.group.toString() == groups[j]._doc._id.toString()) {
                                        var aux = {
                                            name: groups[j]._doc.name,
                                            id: groups[j]._doc._id,
                                            front: groups[j]._doc.front,
                                            color: groups[j]._doc.color,
                                            slug: groups[j]._doc.slug
                                        }
                                        toursArray[i]._doc.group = aux;
                                    }
                                }

                            }
                            var toursByGroup = [];
                            var groupId = null;
                            for (var i = 0; i < toursArray.length; i++) {
                                if (toursArray[i]._doc.group.slug.es == req.body.group || toursArray[i]._doc.group.slug.en == req.body.group) {
                                    toursByGroup.push(toursArray[i]._doc);
                                    if (!groupId) {
                                        groupId = toursArray[i]._doc.group.id;
                                    }
                                }
                            }

                            getPackByGroup(groupId, req.body.port, function (pack) {
                                if (pack) {
                                    for (var i = 0; i < pack.length; i++) {
                                        toursByGroup.push(pack[i]);
                                    }
                                    return res.json({res: toursByGroup});
                                }
                                else {
                                    return res.json({res: toursByGroup});
                                }
                            });


                        }
                        else {
                            return res.json({res: false});
                        }
                    })
                }
                else {
                    return res.json({res: false});
                }
            })


        }
    })
};
exports.listByPortGroup = function (req, res) {
    var text = req.body.text;
    var limit = req.body.limit;
    var skip = req.body.skip;
    if (text.length > 0) {
        async.parallel([
            function (cb) {
                db.Tour.count({
                    port: req.body.idPort,
                    group: req.body.idGroup,
                    available: true,
                    remove: false
                }).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                text = new RegExp(text, 'i');
                db.Tour.find({
                    port: req.body.idPort, group: req.body.idGroup, available: true, remove: false, $or: [
                        {'name.es': text}
                    ]
                }).sort('-name').limit(limit).skip(skip).exec(function (err, tours) {
                    if (cb) cb(err, tours);
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            } else {
                return res.json({res: results[1], cont: results[0]});
            }
        })
    } else {
        async.parallel([
            function (cb) {
                db.Tour.count({
                    port: req.body.idPort,
                    group: req.body.idGroup,
                    available: true,
                    remove: false
                }).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                db.Tour.find({
                    port: req.body.idPort, group: req.body.idGroup, available: true, remove: false
                }).sort('-name').limit(limit).skip(skip).exec(function (err, tours) {
                    if (cb) cb(err, tours);
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            } else {
                return res.json({res: results[1], cont: results[0]});
            }
        })
    }
};
exports.getTourForBooking = function (req, res) {
    async.parallel([
        function (cb) {
            db.Tour.findOne({
                $or: [{'slug.en': req.body.slug}, {'slug.es': req.body.slug}],
                available: true,
                remove: false
            }).exec(function (err, tour) {
                cb(err, tour);
            })
        },
        function (cb) {
            db.Pack.findOne({
                $or: [{'slug.en': req.body.slug}, {'slug.es': req.body.slug}],
                remove: false,
                status: true
            }, function (err, pack) {
                cb(err, pack);
            })
        }
    ], function (err, results) {
        if (err || !results) {
            return res.json({res: false});
        }
        else {
            if (results[0] != null) {
                req.body.id = results[0]._doc._id.toString();
                exports.getTourForBookingBody(req, res, results[0], null);
            }
            else {
                req.body.id = results[1]._doc._id.toString();
                db.Tour.findOne({_id: results[1]._doc.tour, remove: false, available: true}, function (err, tour) {
                    if (err || !tour) {
                        return res.json({res: false});
                    }
                    else {
                        exports.getTourForBookingBody(req, res, tour, results[1]);
                    }
                })
            }
        }
    });
};
exports.getTourForBookingBody = function (req, res, tour, pack) {
    var dt;
    var todayDt = new Date();
    if (req.body.dt) {
        dt = new Date(req.body.dt.y, req.body.dt.m, req.body.dt.d);
        if (dt < todayDt) {
            //return res.json({res: tour, occupied: true, today: todayDt});
            var aux = new Date(dt.getTime() + 86400000);
            var tmp = {
                d: aux.getDate(),
                m: aux.getMonth(),
                y: aux.getFullYear()
            }
            req.body.dt = tmp;
            return this.getTourForBookingBody(req, res, tour, pack);
        }

    }
    else {
        var aux = new Date();
        var aux = new Date(aux.getTime() + 86400000);
        dt = new Date(aux.getFullYear(), aux.getMonth(), aux.getDate());
    }
    var id_tour = req.body.id;
    if (pack) {
        id_tour = tour._doc._id.toString();
    }
    async.waterfall([
        function (callback) {
            ships.hasShips(id_tour, function (_ships) {
                callback(null, _ships);
            });
        },
        function (_ships, callback) {
            if (req.body.ship) {
                for (var i = 0; i < _ships.length; i++) {
                    if (req.body.ship == _ships[i]._doc._id.toString()) {
                        var aux = _ships[0];
                        _ships[0] = _ships[i];
                        _ships[i] = aux;
                        break;

                    }
                }
            }


            if (_ships.length > 0) {
                async.map(_ships, function (ship, callback0) {
                    if (dt >= ship._doc.calendar.start && dt <= ship._doc.calendar.end) {
                        var departures = [];
                        if (tour._doc.data.payForm != 'bonds') {
                            for (var q = 0; q < tour._doc.data.departures.length; q++) {
                                var aux = {
                                    departure: tour._doc.data.departures[q],
                                    duration: []
                                }
                                for (var i = 0; i < tour._doc.data.price.value.length; i++) {
                                    var aux1 = {
                                        price: tour._doc.data.price.value[i]._doc.price,
                                        duration: tour._doc.data.price.value[i]._doc.duration,
                                        maxPerson: tour._doc.data.capacity.maxPerson
                                    }
                                    aux.duration.push(aux1);
                                }
                                departures.push(aux);
                            }
                        }
                        else {
                            var aux = {
                                departure: '00:00',
                                duration: [{price: 0, duration: '23:59', maxPerson: tour._doc.data.capacity.maxPerson}]
                            }
                            departures.push(aux);
                        }


                        filterInfoByDay(dt, departures, ship, tour, function (err, success) {
                            if (err || !success) {
                                var aux = {
                                    ship: ship,
                                    occupied: true
                                }
                                callback0(null, aux);
                            }
                            else {
                                addShip(success, ship, tour, function (data) {
                                    if (data) {
                                        callback0(null, data);
                                    }
                                    else {
                                        callback0(10, false);
                                    }
                                });
                            }
                        });
                    }
                    else {
                        var aux = {
                            ship: ship,
                            occupied: true,
                            finish: true
                        }
                        callback0(null, aux);
                    }
                }, function (err, result) {
                    if (err || !result) {
                        callback(err, null);
                    }
                    else {
                        var array = [];
                        _.forEach(result, function (ship) {
                            if (ship && !ship.occupied) {
                                array.push(ship);
                            }
                            else {
                                if (ship.finish) {
                                    array.push(ship);
                                }
                            }
                        });
                        if (array.length) {
                            callback(null, array);
                        }
                        else {
                            callback(11, null);
                        }

                    }
                })
            }
            else {
                callback(11, null);
            }
        }
    ], function (err, result) {
        if (err || !result) {
            var dAux = new Date();
            var todayDt = {
                y: dAux.getFullYear(),
                m: dAux.getMonth(),
                d: dAux.getDate()
            }
            tour._doc.dt = {
                y: dt.getFullYear(),
                m: dt.getMonth(),
                d: dt.getDate()
            };
            var toursController = require('./tours');
            if (err == 11) {
                var aux = new Date(dt.getTime() + 86400000);
                var tmp = {
                    d: aux.getDate(),
                    m: aux.getMonth(),
                    y: aux.getFullYear()
                }
                req.body.dt = tmp;
                //return res.json({res: tour, occupied: true, ships: false, today: todayDt});
                return toursController.getTourForBookingBody(req, res, tour, pack);
            }
            else {
                var aux = new Date(dt.getTime() + 86400000);
                var tmp = {
                    d: aux.getDate(),
                    m: aux.getMonth(),
                    y: aux.getFullYear()
                }
                req.body.dt = tmp;

                return toursController.getTourForBookingBody(req, res, tour, pack);
            }
        }
        else {
            returnReservation(req, res, result, dt, pack);
        }
    });
};
exports.getDisablesDay = function (tour, ship, cb) {
    var daysDisables = [];
    if (ship._doc.calendar.block.length) {
        if (tour._doc.data.price.type == 'day') {
            for (var j = 0; j < ship._doc.calendar.block.length; j++) {
                if (ship._doc.calendar.block[j].tag != 'canceled') {

                    var dif = ship._doc.calendar.block[j].end - ship._doc.calendar.block[j].start;
                    dif = parseInt(dif / 86400000);

                    var day = new Date(ship._doc.calendar.block[j].start);
                    day = new Date(day.getFullYear(), day.getMonth(), day.getDate()).getTime();
                    daysDisables = addDaysDisables(daysDisables, new Date(day - 86400000));
                    while (dif >= 0) {
                        daysDisables = addDaysDisables(daysDisables, new Date(day + (dif * 86400000)));
                        dif--;
                    }
                }
            }
            if (cb) cb(daysDisables);
        }
        else if (tour._doc.data.price.type == 'hours') {
            for (var j = 0; j < ship._doc.calendar.block.length; j++) {
                if (ship._doc.calendar.block[j].tag != 'canceled') {

                    var dif = ship._doc.calendar.block[j].end - ship._doc.calendar.block[j].start;
                    dif = parseInt(dif / 86400000);

                    if (dif >= 1) {
                        var day = new Date(ship._doc.calendar.block[j].start);
                        day = new Date(day.getFullYear(), day.getMonth(), day.getDate()).getTime();
                        daysDisables = addDaysDisables(daysDisables, new Date(day - 86400000));
                        while (dif >= 0) {
                            daysDisables = addDaysDisables(daysDisables, new Date(day + (dif * 86400000)));
                            dif--;
                        }
                    }
                    else {
                        var day = new Date(ship._doc.calendar.block[j].start);
                        day = new Date(day.getFullYear(), day.getMonth(), day.getDate()).getTime();

                        var flag = false;
                        for (var i = 0; i < tour._doc.data.departures.length; i++) {
                            var dep = departureTime(tour._doc.data.departures[i]);
                            for (var k = 0; k < tour._doc.data.price.value.length; k++) {
                                var dur = departureTime(tour._doc.data.price.value[k].duration);
                                var event = {
                                    start: new Date(day + dep),
                                    end: new Date(day + dep + dur)
                                }
                                if (validateAddEvent(ship._doc.calendar.block, event)) {
                                    flag = true;
                                    break;
                                }
                            }
                            if (flag) {
                                break;
                            }
                        }
                        if (!flag) {
                            daysDisables = addDaysDisables(daysDisables, day);
                        }
                    }


                }
            }
            if (cb) cb(daysDisables);
        }
    }
    else {
        if (cb) cb(daysDisables);
    }

};
exports.bonds = function (req, res) {
    db.Tour.find({'data.payForm': 'bonds', remove: false}).exec(function (err, tours) {
        if (err || !tours) {
            return res.json({res: false});
        }
        else {
            if (tours.length > 0) {
                async.map(tours, function (tour, callback0) {
                    db.Bonds.findOne({
                        _id: tour._doc.data.bond.bond,
                        remove: false,
                        available: true
                    }).exec(function (err, bond) {
                        if (err || !bond) {
                            callback0(err, bond);
                        }
                        else {
                            var aux = {
                                name: tour._doc.name,
                                bond: bond
                            };
                            callback0(null, aux);
                        }
                    });
                }, function (err, result) {
                    if (err || !result) {
                        return res.json({res: []});
                    }
                    else {
                        var aux = [];
                        for (var i = 0; i < result.length; i++) {
                            if (result[i]) {
                                aux.push(result[i]);
                            }
                        }
                        return res.json({res: aux});
                    }
                });
            }
            else {
                return res.json({res: []});
            }
        }
    })
};
exports.getRecomendTour = function (req, res) {


    var port = req.body.port,
        tour = req.body.tour;


    db.Tour.find({port: port, _id: {$ne: tour}, available: true, remove: false}, function (err, results) {
        if (err || !results) {
            return res.json({res: []})
        }
        else {
            var getTour = [];
            var max = results.length > 4 ? 4 : results.length;
            for (var i = 0; i < max; i++) {
                var num = generateNumber(results.length);
                var aux = results[num];
                getTour.push(aux);
                var list = [];
                for (var j = 0; j < results.length; j++) {
                    var auxId = aux._doc._id.toString();
                    var resId = results[j]._doc._id.toString();

                    if (aux._doc.hasPack) {
                        auxId = aux._doc.hasPack.id;
                    }
                    if (results[j]._doc.hasPack) {
                        resId = results[j]._doc.hasPack.id;
                    }
                    if (auxId.toString() != resId.toString()) {
                        list.push(results[j]);
                    }
                }
                results = list;
            }
            async.map(getTour, function (tour, callback0) {
                db.Group.findOne({_id: tour._doc.group.toString()}, function (err, group) {
                    tour._doc.group = {
                        color: group._doc.color,
                        slug: group._doc.slug
                    }
                    callback0(null, tour);
                })
            }, function (err, result) {
                return res.json({res: result})
            })
        }
    });
};
exports.listPacksByTour = function (req, res) {
    exports.getResevationByIdTour(req.body.id, 0, function (reservations, pos) {
        if (reservations > 0) {
            var result = {
                reservation: true,
                quantity: reservations
            };
            return res.json({res: result});
        } else {
            async.waterfall([
                function (callback) {
                    db.Pack.find({tour: req.body.id, remove: false}, function (err, paks) {
                        callback(err, paks);
                    })
                },
                function (paks, callback) {
                    async.map(paks, function (pack, callback0) {
                        async.map(pack.services, function (service, callback1) {
                            db.Service.findOne({_id: service}).select('name').exec(function (err, servs) {
                                callback1(err, servs);
                            })
                        }, function (err, result) {
                            if (err || !result) {
                                callback0(err, result);
                            }
                            else {
                                var aux = {
                                    id: pack._id,
                                    services: result
                                };
                                callback0(null, aux);
                            }

                        })
                    }, function (err, result) {
                        callback(err, result);
                    })
                }
            ], function (err, result) {
                if (err || !result) {
                    return res.json({res: false})
                }
                else {
                    return res.json({res: result});
                }
            })
        }
    })
};

exports.getTour = function (id, callback) {
    var pack = null;
    async.parallel([
        function (cb) {
            db.Tour.findOne({_id: id}).exec(function (err, tour) {
                cb(err, tour);
            })
        },
        function (cb) {
            db.Pack.findOne({_id: id}).exec(function (err, pack) {
                if (pack) {
                    db.Tour.findOne({_id: pack._doc.tour}).exec(function (err, tour) {
                        var aux = {
                            tour: tour,
                            pack: pack
                        }
                        cb(err, aux);
                    })
                }
                else {
                    cb(err, pack);
                }
            })
        }
    ], function (err, results) {
        if (err || !results) {
            callback(false);
        }
        else {
            var tour;

            if (results[0]) {
                tour = results[0];
            }
            else {
                tour = results[1].tour;
                pack = results[1].pack;

            }
            getPorts(tour._doc.port, function (data) {
                if (data) {
                    var aux = {
                        id: tour._doc.port,
                        name: data._doc.name
                    };
                    tour._doc.port = aux;

                    getGroupsById(tour._doc.group, function (group) {
                        if (group) {
                            tour._doc.group = {
                                id: group._doc._id,
                                name: group._doc.name
                            }
                            callback(tour, pack);
                        }
                        else {
                            callback(tour, pack);
                        }
                    })
                }
                else {
                    var aux = {
                        id: tour._doc.port,
                        name: {
                            es: "Desconocido",
                            en: "Unknown"
                        }
                    };
                    tour._doc.port = aux;
                    callback(tour, pack);
                }
            })
        }
    });
};
exports.getTourGroups = function (id, pos, cb) {
    db.Tour.findOne({_id: id}).exec(function (err, tour) {
        if (err || !tour) {
            if (cb) cb(false);
        }
        else {
            getGroupsById(tour._doc.group, function (data) {
                if (data) {
                    var aux = {
                        id: tour._doc.group,
                        name: data._doc.name
                    };
                    tour._doc.group = aux;
                    if (cb) cb(tour, pos);
                }
                else {
                    var aux = {
                        id: tour._doc.group,
                        name: {
                            es: "Desconocido",
                            en: "Unknown"
                        }
                    };
                    tour._doc.group = aux;
                    if (cb) cb(tour, pos);
                }
            })
        }
    })
};
exports.getTourGroupsAvailable = function (id, pos, cb) {
    db.Tour.findOne({_id: id, available: true, remove: false}).exec(function (err, tour) {
        if (err || !tour) {
            if (cb) cb(false);
        }
        else {
            getGroupsById(tour._doc.group, function (data) {
                if (data) {
                    var aux = {
                        id: tour._doc.group,
                        name: data._doc.name
                    };
                    tour._doc.group = aux;
                    if (cb) cb(tour, pos);
                }
                else {
                    var aux = {
                        id: tour._doc.group,
                        name: {
                            es: "Desconocido",
                            en: "Unknown"
                        }
                    };
                    tour._doc.group = aux;
                    if (cb) cb(tour, pos);
                }
            })
        }
    })
};
exports.getResevationByTour = function (tour, pos, cb) {
    async.waterfall([
        function (callback) {
            db.ReservationTour.find({tour: tour._doc._id, remove: false}, function (err, resTs) {
                callback(err, resTs);
            })
        },
        function (resTs, callback) {
            async.map(resTs, function (resT, callback0) {
                db.ReservationClient.count({reservation: resT._doc._id, cancel: false}, function (err, count) {
                    callback0(err, count);
                })
            }, function (err, result) {
                if (err || !result) {
                    callback(err, result);
                }
                else {
                    var cant = 0;
                    for (var i = 0; i < result.length; i++) {
                        cant = cant + result[i];
                    }
                    callback(null, cant);
                }
            })
        }
    ], function (err, result) {
        if (err) {
            if (cb) cb(0, pos);
        } else {
            if (cb) cb(result, pos);
        }
    })
};
exports.getResevationByIdTour = function (idTour, pos, cb) {
    async.waterfall([
        function (callback) {
            db.ReservationTour.find({tour: idTour, remove: false}, function (err, resTs) {
                callback(err, resTs);
            })
        },
        function (resTs, callback) {
            async.map(resTs, function (resT, callback0) {
                db.ReservationClient.count({reservation: resT._doc._id, state: 0, cancel: true}, function (err, count) {
                    callback0(err, count);
                })
            }, function (err, result) {
                if (err || !result) {
                    callback(err, result);
                }
                else {
                    var cant = 0;
                    for (var i = 0; i < result.length; i++) {
                        cant = cant + result[i];
                    }
                    callback(null, cant);
                }
            })
        }
    ], function (err, result) {
        if (err) {
            if (cb) cb(0, pos);
        } else {
            if (cb) cb(result, pos);
        }
    })
};


exports.getAviableTours = function (cb) {
    db.Tour.find({remove: false, available: true}, function (err, tours) {
        if (err || !tours) {
            return cb(true, false);
        }
        else {
            var toursArray = tours;
            getPorts(null, function (portsData) {
                if (portsData) {
                    var ports = portsData;
                    getGroups(function (groupsData) {
                        if (groupsData) {
                            var groups = groupsData;
                            for (var i = 0; i < toursArray.length; i++) {
                                for (var j = 0; j < ports.length; j++) {
                                    if (toursArray[i]._doc.port.toString() == ports[j]._doc._id.toString()) {
                                        var aux = {
                                            name: ports[j]._doc.name,
                                            id: ports[j]._doc._id,

                                        }
                                        toursArray[i]._doc.port = aux;
                                    }
                                }
                                for (var j = 0; j < groups.length; j++) {
                                    if (toursArray[i]._doc.group.toString() == groups[j]._doc._id.toString()) {
                                        var aux = {
                                            name: groups[j]._doc.name,
                                            id: groups[j]._doc._id,
                                            color: groups[j]._doc.color,
                                            slug: groups[j]._doc.slug
                                        }
                                        toursArray[i]._doc.group = aux;
                                    }
                                }

                            }
                            return cb(false, toursArray);
                        }
                        else {
                            return cb(true, false);
                        }
                    })
                }
                else {
                    return cb(true, false);
                }
            })
        }
    })
}

function filterInfoByDay(dt, departures, ship, tour, cb) {
    async.map(departures, function (dep, callback0) {
        async.map(dep.duration, function (dur, callback1) {
            var start = new Date(dt.getTime() + departureTime(dep.departure));
            if (tour._doc.data.payForm == 'bonds') {
                var end = new Date(dt.getTime() + departureTime(dep.departure) + departureTime('23:59'));
            }
            else {
                if (tour._doc.data.price.type == 'hours') {
                    var end = new Date(dt.getTime() + departureTime(dep.departure) + departureTime(dur.duration));
                }
                else {
                    var end = new Date(dt.getTime() + departureTime(dep.departure) + (parseInt(dur.duration) * 86400000));
                }
            }

            db.ReservationTour.findOne({
                start: start,
                end: end,
                ship: ship,
                tour: tour,
                duration: dur.duration
            }).exec(function (err, resT) {
                if (err || !resT) {
                    var event = {
                        start: start,
                        end: end,
                        allDay: false
                    }
                    var events = [];
                    for (var i = 0; i < ship._doc.calendar.block.length; i++) {
                        if (ship._doc.calendar.block[i]._doc.tag != 'canceled') {
                            events.push(ship._doc.calendar.block[i]);
                        }
                    }
                    if (validateAddEvent(events, event)) {
                        callback1(null, dur);
                    }
                    else {
                        callback1(null, null);
                    }
                }
                else {
                    if (resT._doc.available) {
                        dur.maxPerson = dur.maxPerson - resT._doc.cantPerson;
                        if (dur.maxPerson) {
                            callback1(null, dur);
                        }
                        else {
                            callback1(null, null);
                        }
                    }
                    else {
                        callback1(null, null);
                    }
                }
            });

        }, function (err, result) {
            if (err || !result) {
                callback0(null, null);
            }
            else {
                var array = [];
                _.forEach(result, function (n) {
                    if (n) {
                        array.push(n);
                    }
                });
                if (array.length) {
                    dep.duration = array;
                    callback0(null, dep);
                }
                else {
                    callback0(null, null);
                }
            }
        });
    }, function (err, result) {
        if (err || !result) {
            cb(null, null);
        }
        else {
            var array = [];
            _.forEach(result, function (n) {
                if (n) {
                    array.push(n);
                }
            });
            if (array.length) {
                departures = array;
                cb(null, departures);
            }
            else {
                cb(null, null);
            }
        }
    });
};
function generateNumber(max) {
    var aux = Math.floor((Math.random() * max));
    return aux;
};
function getPorts(id, cb) {
    if (!id) {
        db.Port.find().select('name').select('address').exec(function (err, ports) {
            if (err || !ports) {
                if (cb) cb(false);
            }
            else {
                if (cb) cb(ports);
            }
        })
    }
    else {
        db.Port.findOne({_id: id}).select('name').select('address').exec(function (err, ports) {

            if (err || !ports) {
                if (cb) cb(false);
            }
            else {
                if (cb) cb(ports);
            }
        })
    }

};
function getGroups(cb) {
    db.Group.find().exec(function (err, groups) {
        if (err || !groups) {
            if (cb) cb(false);
        }
        else {
            if (cb) cb(groups);
        }
    })
};
function getGroupsById(id, cb) {
    db.Group.findOne({_id: id}).exec(function (err, groups) {
        if (err || !groups) {
            if (cb) cb(false);
        }
        else {
            if (cb) cb(groups);
        }
    })
};
function getCapacity(tour) {
    var capacity;
    if (tour._doc.data.payForm == 'bonds') {
        capacity = {
            minHour: tour._doc.data.bond.minHour,
            maxHour: tour._doc.data.bond.maxHour,
            maxPerson: tour._doc.data.capacity.maxPerson,
            priceBy: tour._doc.data.capacity.type
        }
    }
    else {
        var price = [];
        for (var i = 0; i < tour._doc.data.price.value.length; i++) {
            price.push({duration: tour._doc.data.price.value[i].duration, price: tour._doc.data.price.value[i].price});
        }
        capacity = {
            priceBy: tour._doc.data.capacity.type,
            price: price
        }
    }
    return capacity;
};
function returnReservation(req, res, ship, dt, pack) {
    if (ship.length > 0) {
        if (pack) {
            var occupied = false;
            var aux = {
                ship: ship,
                dt: {
                    y: dt.getFullYear(),
                    m: dt.getMonth(),
                    d: dt.getDate()
                }
            }
            var daux = new Date();
            var today = {
                y: daux.getFullYear(),
                m: daux.getMonth(),
                d: daux.getDate()
            }
            var services = [];
            var cont = 0;
            for (var i = 0; i < pack._doc.services.length; i++) {
                db.Service.findOne({
                    _id: pack._doc.services[i],
                    remove: false,
                    status: true
                }, function (err, service) {
                    cont++;
                    if (err || !service) {
                    }
                    else {
                        services.push(service);
                    }
                    if (cont == pack._doc.services.length) {
                        return res.json({
                            res: aux,
                            occupied: occupied,
                            today: today,
                            pack: {
                                id: pack._doc._id,
                                services: services,
                                color: pack._doc.color
                            }
                        });
                    }
                })
            }
        }
        else {
            var occupied = false;

            try {
                if (ship[0].finish) {
                    occupied = true;
                }
            }
            catch (err) {
                console.log(err);
            }

            var aux = {
                ship: ship,
                dt: {
                    y: dt.getFullYear(),
                    m: dt.getMonth(),
                    d: dt.getDate()
                }
            }
            var daux = new Date();
            var today = {
                y: daux.getFullYear(),
                m: daux.getMonth(),
                d: daux.getDate()
            }

            return res.json({res: aux, occupied: occupied, today: today});
        }

    }
    else {

        dt = dt.getTime() + 86400000;
        dt = new Date(dt);
        var auxdt = {
            d: dt.getDate(),
            m: dt.getMonth(),
            y: dt.getFullYear()
        }
        req.body.dt = auxdt;
        return exports.getTourForBooking(req, res);
    }

};
function addShip(departures, ship, tour, cb) {
    exports.getDisablesDay(tour, ship, function (data) {
        var aux = {
            id: ship._doc._id,
            name: ship._doc.name,
            photos: ship._doc.photos,
            description: ship._doc.description,
            departures: departures,
            price: tour._doc.data.price.value,
            capacity: getCapacity(tour),
            calendar: {
                start: ship._doc.calendar.start.getTime(),
                end: ship._doc.calendar.end.getTime()
            },
            disables: data

        }
        if (cb) cb(aux);
    })
};
function addDaysDisables(array, day) {
    day = new Date(day);
    day = day.getTime();
    if (array.length == 0) {
        array.push(day);
    }
    else {
        var aux = false;
        for (var i = 0; i < array.length; i++) {
            if (day == array[i]) {
                aux = true;
                break;
            }
        }
        if (!aux)array.push(day);
    }
    return array;
};
function getPackByGroup(group, port, cb) {
    async.waterfall([
        function (callback) {
            db.Pack.find({remove: false, status: true}, function (err, packs) {
                callback(err, packs);
            });
        },
        function (packs, callback) {
            if (packs.length > 0) {
                async.map(packs, function (pack, callback0) {
                    getTourByPack(pack, group, function (tour, total) {
                        console.log("STEP 2");
                        if (tour) {
                            tour._doc.hasPack = {
                                id: pack._doc._id,
                                color: pack._doc.color,
                                total: total,
                                slug: pack._doc.slug

                            }
                            if (tour._doc.port.toString() == port) {
                                callback0(null, tour);
                            }
                            else {
                                callback0(null, false);
                            }

                        }
                        else {
                            callback0(null, false);
                        }
                    })

                }, function (err, result) {
                    callback(null, result);
                });
            }
            else {
                callback(null, []);
            }
        }
    ], function (err, results) {
        if (err || !results) {
            cb(false);
        }
        else {
            var toursArray = [];
            _.forEach(results, function (tour) {
                if (tour) {
                    toursArray.push(tour);
                }
            })

            if (toursArray.length > 0) {
                getPorts(null, function (portsData) {
                    if (portsData) {
                        var ports = portsData;
                        getGroups(function (groupsData) {
                            if (groupsData) {
                                var groups = groupsData;
                                for (var i = 0; i < toursArray.length; i++) {
                                    for (var j = 0; j < ports.length; j++) {

                                        if (toursArray[i]._doc.port.toString() == ports[j]._doc._id.toString()) {
                                            var aux = {
                                                name: ports[j]._doc.name,
                                                id: ports[j]._doc._id
                                            }
                                            toursArray[i]._doc.port = aux;
                                        }
                                    }
                                    for (var j = 0; j < groups.length; j++) {
                                        if (toursArray[i]._doc.group.toString() == groups[j]._doc._id.toString()) {
                                            var aux = {
                                                name: groups[j]._doc.name,
                                                id: groups[j]._doc._id,
                                                front: groups[j]._doc.front,
                                                color: groups[j]._doc.color
                                            }
                                            toursArray[i]._doc.group = aux;
                                        }
                                    }
                                }

                                cb(toursArray);
                            }
                            else {
                                cb(false);
                            }
                        })
                    }
                    else {
                        cb(false);
                    }
                })
            }
            else {
                cb(toursArray);
            }
        }
    });
};
function getTourByPack(pack, group, cbb) {
    async.parallel([
        function (cb) {
            db.Tour.findOne({_id: pack._doc.tour, group: group}, function (err, tour) {
                cb(err, tour);
            });
        },
        function (cb) {
            async.map(pack._doc.services, function (service, callback0) {
                db.Service.findOne({_id: service}, function (err, serv) {
                    callback0(err, serv);
                })
            }, function (err, result) {
                cb(err, result);
            })
        }
    ], function (err, results) {
        if (err || !results) {
            cbb(false, 0);
        }
        else {
            if (results[0] == null) {
                cbb(false);
            }
            else {
                if (results[1].length) {
                    var total = 0;
                    for (var i = 0; i < results[1].length; i++) {
                        total = total + results[1][i]._doc.price;
                    }
                    if (results[0]._doc.data.capacity.type == 'boat') {
                        total = total * results[0]._doc.data.capacity.maxPerson;
                    }

                }
                cbb(results[0], total);
            }
        }
    });
};
function matchCalendarBlock(a, b, c, d) {
    if (c <= a && a < d) {
        return true;
    }
    if (c <= b && b < d) {
        return true;
    }
    if (a <= c && c < b) {
        return true;
    }
    if (a <= d && d < b) {
        return true;
    }
    return false;
}
function validateAddEvent(array, event) {
    if (array.length) {
        for (var i = 0; i < array.length; i++) {
            var start = array[i].start;
            var end = array[i].end;
            if (array[i].end == null) {
                end = start;
            }
            if (array[i].allDay) {
                end = new Date(end.getFullYear(), end.getMonth(), end.getDate());
                end = new Date(end.getTime() + 86399999);
            }

            var eventEnd = event.end;
            if (event.end == null) {
                eventEnd = event.start;
            }
            if (event.allDay) {
                eventEnd = new Date(eventEnd.getFullYear(), eventEnd.getMonth(), eventEnd.getDate());
                eventEnd = new Date(eventEnd.getTime() + 86399999);
            }

            if (matchCalendarBlock(start, end, event.start, eventEnd)) {
                return false;
            }
        }
        return true;
    }
    else {
        return true;
    }
};
function departureTime(time) {
    var array = time.split(':');
    if (array.length == 2) {
        var hour = parseInt(array[0]);
        var min = parseInt(array[1]);

        if (isNaN(hour) || isNaN(min)) {
            return false;
        }
        else {
            return (hour * 60 + min) * 60000;
        }
    }
    else {
        return false;
    }
};
