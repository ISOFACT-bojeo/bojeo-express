/**
 * Created by ernestomr87@gmail.com on 16/07/14.
 */

var async = require('async');


exports.listBack = function (req, res) {
    var limit = req.body.limit,
        skip = req.body.skip;

    async.parallel([
        function (cb) {
            db.Timeline.count().exec(function (err, count) {
                if (cb) cb(err, count);
            })
        },
        function (cb) {
            db.Timeline.find().sort('-date').limit(limit).skip(skip).exec(function (err, time) {
                if (cb) cb(err, time);
            })
        }
    ], function (err, results) {
        if (err || !results) {
            return res.json({res: false, cont: 0});
        }
        else {
            if (results[0] > 0) {
                var day = new Array();
                var aux = new Array();
                aux.push(results[1][0]);
                for (var i = 1; i < results[1].length; i++) {
                    if (results[1][i - 1]._doc.date.toLocaleDateString() == results[1][i]._doc.date.toLocaleDateString()) {
                        aux.push(results[1][i]);
                    }
                    else {
                        var tmp = {
                            date: new Date(results[1][i - 1]._doc.date.toLocaleDateString()),
                            event: aux
                        };
                        day.push(tmp);
                        aux = new Array();
                        aux.push(results[1][i]);
                    }
                }
                if (aux.length > 0) {
                    var tmp = {
                        date: new Date(aux[0]._doc.date.toLocaleDateString()),
                        event: aux
                    };
                    day.push(tmp);
                }


                return res.json({res: day, cont: results[0]});
            }
            else {
                return res.json({res: results[1], cont: results[0]});
            }

        }
    });


};
exports.create = function (type, info, cb) {

    var timeLine = new db.Timeline({
        date: new Date(),
        type: type,
        info: JSON.stringify(info)
    });

    timeLine.save(function (err, success) {
        if (cb) cb(err, success);
    });
};
