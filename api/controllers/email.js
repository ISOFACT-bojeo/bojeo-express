/**
 * Created by ernestomr87@gmail.com on 16/07/14.
 */

var utils = require('../utils');
var nodemailer = require("nodemailer");
var util = require('util');
var async = require('async');

exports.getConfigurationsServer = function (req, res) {
    db.Email.findOne().exec(function (err, email) {
        if (err || !email) {
            return res.json({res: false})
        }
        else {
            return res.json({res: email})
        }
    })
};
exports.saveConfigurationsEmailServer = function (req, res) {
    try {
        db.Email.update({_id: req.body.id}, {
            $set: {
                mailServer: req.body.mailServer,
                mailSMTPPort: req.body.mailSMTPPort,
                mailUser: req.body.mailUser,
                mailPass: req.body.mailPass
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true});
            }
        })
    }
    catch (err) {
        return res.json({res: false});
    }
};
exports.createConfigurationsEmailServer = function (req, res) {
    var email = db.Email({
        mailServer: 'localhost',
        mailSMTPPort: 25,
        mailUser: 'correo.testing@company.mail',
        mailPass: 'Isofact2014',
        notificationReserved: false,
        notificationChangePass: false,
        devolution: false,

        notificationRegister: {
            subject: {
                es: "Email de Notificación de Registro",
                en: "Sing-in notification email"
            },
            body: {
                es: "Su cuenta de usuario de usuario ha sido creada satifactoriamente",
                en: "Your Bojeo.com user account has been created successfully."
            },
            active: false
        },
        bulletin: {
            subject: {
                es: "Boletin",
                en: "Bulletin"
            },
            body: {
                es: "Cuerpo del boletin",
                en: "Bulletin body"
            },
            active: false
        },
        promo: {
            subject: {
                es: "Email promocional",
                en: "Promotional email"
            },
            body: {
                es: "Email promocional",
                en: "Promotional email"
            },
            active: false
        }
    })
    email.save(function (err, em) {
        if (err || !em) {
            return res.json({res: false});
        }
        else {
            return res.json({res: true});
        }
    })
};
exports.changeNoty = function (req, res) {

    if (req.body.type == 0) {
        db.Email.update({_id: req.body.id}, {
            $set: {
                'notificationReserved.active': req.body.active
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true});
            }
        })
    }
    else if (req.body.type == 1) {
        db.Email.update({_id: req.body.id}, {
            $set: {
                notificationChangePass: req.body.active
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true});
            }
        })
    }
    else if (req.body.type == 2) {
        db.Email.update({_id: req.body.id}, {
            $set: {
                "devolution.active": req.body.active
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true});
            }
        })
    }
    else if (req.body.type == 3) {
        db.Email.update({_id: req.body.id}, {
            $set: {
                'promo.active': req.body.active
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true});
            }
        })
    }
    else if (req.body.type == 4) {
        db.Email.update({_id: req.body.id}, {
            $set: {
                'bulletin.active': req.body.active
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true});
            }
        })
    }
    else {
        db.Email.update({_id: req.body.id}, {
            $set: {
                'notificationRegister.active': req.body.active
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true});
            }
        })
    }

};
exports.saveNotyBody = function (req, res) {
    if (req.body.type == 0) {
        db.Email.update({_id: req.body.id}, {
            $set: {
                notificationRegister: req.body.noty
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false})
            }
            else {
                return res.json({res: true})
            }
        })
    }
    else if (req.body.type == 1) {
        db.Email.update({_id: req.body.id}, {
            $set: {
                promo: req.body.noty
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false})
            }
            else {
                return res.json({res: true})
            }
        })
    }
    else if (req.body.type == 2) {
        db.Email.update({_id: req.body.id}, {
            $set: {
                notificationReserved: req.body.noty
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false})
            }
            else {
                return res.json({res: true})
            }
        })
    }
    else if (req.body.type == 3) {
        db.Email.update({_id: req.body.id}, {
            $set: {
                bulletin: req.body.noty
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false})
            }
            else {
                return res.json({res: true})
            }
        })
    }
    else if (req.body.type == 4) {
        db.Email.update({_id: req.body.id}, {
            $set: {
                devolution: req.body.noty
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false})
            }
            else {
                return res.json({res: true})
            }
        })
    }
    else if (req.body.type == 5) {
        db.Email.update({_id: req.body.id}, {
            $set: {
                sign: req.body.noty
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false})
            }
            else {
                return res.json({res: true})
            }
        })
    }

};
exports.sendEmail = function (req, res) {
    var type = req.body.type;

    if (type == 0) {
        var arrayMailclients = [];
        db.User.find({remove: false}).exec(function (err, success) {
                if (err == null && success != null && success.length > 0) {
                    for (var i = 0; i < success.length; i++) {
                        arrayMailclients.push(success[i]._doc.email);
                    }
                    utils.prepareMail(res, type, arrayMailclients);
                }
                else {
                    return res.json({result: false})
                }

            }
        )
    }
    else if (type == 1) {
        var arrayMailclients = [];
        db.User.find({subscription: true, remove: false}).exec(function (err, success) {
                if (err == null && success != null && success.length > 0) {
                    for (var i = 0; i < success.length; i++) {
                        arrayMailclients.push(success[i]._doc.email);
                    }
                    utils.prepareMail(res, type, arrayMailclients);
                }
                else {
                    return res.json({result: false})
                }

            }
        )
    }
    else {
        return res.json({result: false})
    }
};
exports.testConection = function (req, res) {
    db.Email.findOne().exec(function (err, email) {
        if (err || !email) {
            return res.json({res: false});
        }
        else {
            var smtpTransport = nodemailer.createTransport("SMTP", {
                host: email._doc.mailServer, // hostname
                secureConnection: true, // use SSL
                connectionTimeout : 300000,
                port: email._doc.mailSMTPPort, // port for secure SMTP
                auth: {
                    user: email._doc.mailUser,
                    pass: email._doc.mailPass
                }
            });
            var mailOptions = {
                from: "Bojeo <" + email._doc.mailUser + ">", // sender address
                to: email._doc.mailUser, // list of receivers
                subject: 'Prueba de conexión', // Subject line
                html: '<h1> Correo satisfactorio</h1>'
            }


            try {
                smtpTransport.sendMail(mailOptions, function (err) {
                    if (err) {
                        return res.json({res: false});
                    }
                    else {
                        smtpTransport.close();
                        return res.json({res: true});
                    }

                });
            }
            catch (err) {
                return res.json({res: false});
            }
        }
    });
};
exports.sendNotyEmail = function (req, res) {
    db.Email.findOne().exec(function (err, email) {
        if (err || !email) {
            return res.json({res: false});
        }
        else {
            var smtpTransport = nodemailer.createTransport("SMTP", {
                host: email._doc.mailServer, // hostname
                secureConnection: true, // use SSL
                connectionTimeout : 300000,
                port: email._doc.mailSMTPPort, // port for secure SMTP
                auth: {
                    user: email._doc.mailUser,
                    pass: email._doc.mailPass
                }
            });

            if (req.body.type == 1) {
                db.User.find({subscription: true, remove: false}, 'email language').exec(function (err, users) {
                    if (err || !users) {
                        return res.json({res: false})
                    }
                    else {
                        var client_es = new Array();
                        var client_en = new Array();
                        if (users.length > 0) {
                            for (var i = 0; i < users.length; i++) {
                                if (users[i]._doc.language == 'es') {
                                    client_es.push(users[i]._doc.email);
                                }
                                else {
                                    client_en.push(users[i]._doc.email);
                                }

                            }
                            var mailOptionsEs = {
                                from: "Bojeo <" + email._doc.mailUser + ">", // sender address
                                to: client_es, // list of receivers
                                subject: email._doc.bulletin.subject.es, // Subject line
                                html: email._doc.bulletin.body.es//util.format(email._doc.promo.body.es, email._doc.mailUser)
                            }

                            var mailOptionsEn = {
                                from: "Bojeo <" + email._doc.mailUser + ">", // sender address
                                to: client_en, // list of receivers
                                subject: email._doc.bulletin.subject.en, // Subject line
                                html: email._doc.bulletin.body.en//util.format(email._doc.promo.body.es, email._doc.mailUser)
                            }

                            async.parallel([
                                function (cb) {
                                    try {
                                        smtpTransport.sendMail(mailOptionsEs, function (err) {
                                            if (err) {
                                                cb(err);
                                            }
                                            else {
                                                smtpTransport.close()
                                                cb(null);
                                            }
                                        });
                                    }
                                    catch (err) {
                                        cb(err);
                                    }
                                },
                                function (cb) {
                                    try {
                                        smtpTransport.sendMail(mailOptionsEn, function (err) {
                                            if (err) {
                                                cb(err);
                                            }
                                            else {
                                                smtpTransport.close()
                                                cb(null);
                                            }
                                        });
                                    }
                                    catch (err) {
                                        cb(err);
                                    }
                                }
                            ], function (err) {
                                if (err) return res.json({res: false})
                                else return res.json({res: false})
                            });

                        }
                        else {
                            return res.json({res: true});
                        }

                    }
                });
            }
            else {
                db.User.find({remove: false}, 'email language').exec(function (err, users) {
                    if (err || !users) {
                        return res.json({res: false})
                    }
                    else {
                        var client_es = new Array();
                        var client_en = new Array();
                        if (users.length > 0) {
                            for (var i = 0; i < users.length; i++) {
                                if (users[i]._doc.language == 'es') {
                                    client_es.push(users[i]._doc.email);
                                }
                                else {
                                    client_en.push(users[i]._doc.email);
                                }

                            }
                            var mailOptionsEs = {
                                from: "Bojeo <" + email._doc.mailUser + ">", // sender address
                                to: client_es, // list of receivers
                                subject: email._doc.promo.subject.es, // Subject line
                                html: email._doc.promo.body.es//util.format(email._doc.promo.body.es, email._doc.mailUser)
                            }

                            var mailOptionsEn = {
                                from: "Bojeo <" + email._doc.mailUser + ">", // sender address
                                to: client_en, // list of receivers
                                subject: email._doc.promo.subject.en, // Subject line
                                html: email._doc.promo.body.en//util.format(email._doc.promo.body.es, email._doc.mailUser)
                            }

                            async.parallel([
                                function (cb) {
                                    try {
                                        smtpTransport.sendMail(mailOptionsEs, function (err) {
                                            if (err) {
                                                cb(err);
                                            }
                                            else {
                                                smtpTransport.close()
                                                cb(null);
                                            }
                                        });
                                    }
                                    catch (err) {
                                        cb(err);
                                    }
                                },
                                function (cb) {
                                    try {
                                        smtpTransport.sendMail(mailOptionsEn, function (err) {
                                            if (err) {
                                                cb(err);
                                            }
                                            else {
                                                smtpTransport.close()
                                                cb(null);
                                            }
                                        });
                                    }
                                    catch (err) {
                                        cb(err);
                                    }
                                }
                            ], function (err) {
                                if (err) return res.json({res: false})
                                else return res.json({res: false})
                            });

                        }
                        else {
                            return res.json({res: true});
                        }

                    }
                });
            }


        }
    });
};
exports.getEmail = function (cb) {
    db.Email.findOne().exec(function (err, email) {
        if (err || !email) {
            if (cb) cb(false);
        }
        else {
            if (cb) cb(email);
        }
    });
};
exports.saveFailedMails = function (options, cb) {
    var mail = new db.FailedMails({
        mailOptions: options
    });

    mail.save(function (err, mail) {
        if (err || !mail) {
            var log = new db.ErrorLogs({
                error: err,
                date: new Date()
            })
            log.save(function () {
                cb(true);
            });
        }
        else {
            cb(true);
        }
    })
}