 /**
 * Created by ernestomr87@gmail.com on 16/07/14.
 */
var async = require('async');
var tourController = require('./tours');

exports.listBack = function (req, res) {
    var limit = req.body.limit,
        skip = req.body.skip;

    async.parallel([
        function (cb) {
            db.Bonds.count({remove:false}).exec(function (err, count) {
                if (cb) cb(err, count);
            })
        },
        function (cb) {
            db.Bonds.find({remove:false}).sort('-price').limit(limit).skip(skip).exec(function (err, bonds) {
                if (cb) cb(err, bonds);
            })
        }
    ], function (err, results) {
        if (err || !results) {
            return res.json({res: false, cont: 0});
        }
        else {
            var bonds = new Array();
            if (results[1].length > 0) {
                var cont = 0;
                for (var i = 0; i < results[1].length; i++) {
                    exports.occupiedBond(results[1][i]._doc._id, i, function (data, pos) {
                        var aux = {
                            _id: results[1][pos]._doc._id,
                            hours: results[1][pos]._doc.hours,
                            price: results[1][pos]._doc.price,
                            available: results[1][pos]._doc.available,
                            occupied: data
                        };
                        bonds.push(aux);
                        cont++;
                        if (cont == results[1].length) {
                            return res.json({res:bonds, cont: results[0]});
                        }
                    });
                }
            }
            else {
                return res.json({res: bonds, cont: 0});
            }
        }
    });
};
exports.list = function (req, res) {
    db.Bonds.find({remove: false}).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false})
        }
        else {
            var bonds = new Array();
            if (success.length > 0) {
                var cont = 0;
                for (var i = 0; i < success.length; i++) {
                    exports.occupiedBond(success[i]._doc._id, i, function (data, pos) {
                        var aux = {
                            _id: success[pos]._doc._id,
                            hours: success[pos]._doc.hours,
                            price: success[pos]._doc.price,
                            available: success[pos]._doc.available,
                            occupied: data
                        }
                        bonds.push(aux);
                        cont++;
                        if (cont == success.length) {
                            return res.json({res: bonds});
                        }
                    });
                }
            }
            else {
                return res.json({res: bonds})
            }
        }
    })
};
exports.create = function (req, res) {

    var newBonds = new db.Bonds({
        hours: req.body.hours,
        price: req.body.price
    });

    newBonds.save(function (err, success) {
        if (err || !success) {
            return res.json({res: false})
        }
        else {
            return res.json({res: true})
        }
    });
};
exports.changeStatus = function (req, res) {
    var id = req.body.id;
    var available = req.body.available;
    if (req.body.action == 2) {
        db.Bonds.update({_id: id, available: available}, {$set: {available: !available}}).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true, complete: true})
            }
        })
    }else{
        db.Tour.find({'price.bond': id, remove: false, available: false}).exec(function (err, tour) {
            if (err || !tour) {
                return res.json({res: false});
            }
            else {
                if(tour.length == 0){
                    db.Bonds.update({_id: id, available: available}, {$set: {available: !available}}).exec(function (err, success) {
                        if (err || !success) {
                            return res.json({res: false});
                        }
                        else {
                            return res.json({res: true, complete: true})
                        }
                    })
                }else{
                    var aux = {
                        tour: {
                            id: tour[0]._doc._id,
                            name: tour[0]._doc.name
                        }
                    };
                    db.Group.findOne({_id: tour[0]._doc.group}).exec(function (err, groups) {
                        if (err || !groups) {
                            return res.json({res: false});
                        }else {
                            if (groups.length == 0) {
                                aux.tour.group = false;
                            }else {
                                aux.tour.group = groups._doc.name;
                            }
                            if (req.body.action == 1) {
                                db.Tour.update({_id: tour[0]._doc._id, available: available}, {$set: {available: !available}}).exec(function (err, success) {
                                    if (err || !success) {
                                        return res.json({res: false})
                                    }
                                    else {
                                        db.Bonds.update({_id: id, available: available}, {$set: {available: !available}}).exec(function (err, success) {
                                            if (err || !success) {
                                                return res.json({res: false});
                                            }
                                            else {
                                                return res.json({res: true, complete: true})
                                            }
                                        })
                                    }
                                })
                            }else{
                                return res.json({res: aux, complete: false});
                            }
                        }
                    })
                }
            }
        })
    }
};
exports.remove = function (req, res) {
    db.Tour.find({'price.bond': req.body.id, remove: false}).exec(function (err, tour) {
        if (err || !tour) {
            return res.json({res: false});
        }else{
            if(tour.length == 0){
                db.Bonds.update({_id: req.body.id}, {$set: {remove: true}}).exec(function (err, success) {
                    if (err || !success) {
                        return res.json({res: false})
                    }
                    else {
                        return res.json({res: true})
                    }
                })
            }else{
                var photos = tour[0]._doc.photos;
                if (photos.length > 0) {
                    var cont = 0;
                    for (var j = 0; j < photos.length; j++) {
                        db.Media.remove({fieldName: photos[j].name}).exec(function (err, success) {
                            cont++;
                            if (cont == photos.length) {
                                db.Tour.update({_id: tour[0]._doc._id}, {$set: {remove: true}}).exec(function (err, success) {
                                    if (err || !success) {
                                        return res.json({res: false})
                                    } else {
                                        db.Bonds.update({_id: req.body.id}, {$set: {remove: true}}).exec(function (err, success) {
                                            if (err || !success) {
                                                return res.json({res: false})
                                            }
                                            else {
                                                return res.json({res: true})
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                }
                else {
                    db.Tour.update({_id: tour[0]._doc._id}, {$set: {remove: true}}).exec(function (err, success) {
                        if (err || !success) {
                            return res.json({res: false})
                        } else {
                            db.Bonds.update({_id: req.body.id}, {$set: {remove: true}}).exec(function (err, success) {
                                if (err || !success) {
                                    return res.json({res: false})
                                }
                                else {
                                    return res.json({res: true})
                                }
                            })
                        }
                    })
                }

            }
        }
    })
};
exports.edit = function (req, res) {
    db.Bonds.update({_id: req.body.id}, {
        $set: {
            hours: req.body.hours,
            price: req.body.price
        }
    }).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false})
        }
        else {
            return res.json({res: true})
        }
    })
};
exports.occupiedBond = function (bond, pos, cb) {
    var aux = {
        tour: {}
    };
    async.parallel([
        function(cb2){
            db.Tour.findOne({'data.bond.bond': bond.toString(), remove: false}).exec(function (err, tour) {
                if (err || !tour || tour.length == 0) {
                    aux.tour = false;
                    cb2(null, aux);
                }
                else {
                    aux.tour.id = tour._doc._id;
                    aux.tour.name = tour._doc.name;
                    async.parallel([
                        function (cb1) {
                            db.Group.findOne({_id: tour._doc.group}).exec(function (err, groups) {
                                if (err || !groups || groups.length == 0) {
                                    aux.tour.group = false;
                                    cb1(null,aux);
                                }
                                else {
                                    aux.tour.group = groups._doc.name.es;
                                    cb1(null,aux);
                                }
                            })
                        },
                        function (cb1) {
                            tourController.getResevationByTour(tour, pos, function (reservations, pos) {
                                if (reservations > 0) {
                                    aux.tour.hasReservation = true;
                                } else {
                                    aux.tour.hasReservation = false;
                                }
                                cb1(null,aux);
                            })
                        }
                    ], function (err, results) {
                        if (err || !results) {
                            cb2(err, false);
                        }
                        else {
                            cb2(null, results[1]);
                        }
                    });
                }
            })
        },
        function(cb2){
            db.User.find({remove: false, 'bonds.id': bond.toString()}).exec(function (err, users) {
                if (err || !users || users.length == 0) {
                    aux.user = false;
                }else{
                    // aqui hay que verificar que tenga cantidad en el bono
                    var isbonos = false;
                    for(var i = 0; i < users.length; i++){
                        if(users[i]._doc.bonds.length > 0){
                            for(var j = 0; j < users[i]._doc.bonds.length; j++){
                                if(users[i]._doc.bonds[j].id == bond.toString() && users[i]._doc.bonds[j].cant != 0){
                                    isbonos = true;
                                }
                            }
                        }
                    }
                    aux.user = isbonos;
                }
                cb2(null, aux);
            })
        }
    ], function (err, results) {
        if (err || !results) {
            cb(false, pos);
        }
        else {
            cb(results[1], pos);
        }
    })
};
exports.tourByBond = function(req, res){
    db.Tour.find({'price.bond': req.body.id, remove: false}).exec(function (err, tour) {
        if (err || !tour) {
            return res.json({res: false});
        }else{
            if(tour.length == 0){
                var aux = {tour: false};
                return res.json({res: aux});
            }else{
                var aux = {
                    tour: {
                        id: tour[0]._doc._id,
                        name: tour[0]._doc.name
                    }
                };
                db.Group.findOne({_id: tour[0]._doc.group}).exec(function (err, groups) {
                    if (err || !groups) {
                        return res.json({res: false});
                    } else {
                        if (groups.length == 0) {
                            aux.tour.group = false;
                        } else {
                            aux.tour.group = groups._doc.name;
                        }
                        return res.json({res: aux});
                    }
                })
            }
        }
    })
};
