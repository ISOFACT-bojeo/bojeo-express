/**
 * Created by ernestomr87@gmail.com on 04/06/2014.
 */
var utils = require('../utils');
var userController = require('./user');
var reservationController = require('./reservations');
var async = require('async');
var _ = require('lodash');

exports.create = function (req, res) {
    var name = req.body.name,
        patron = req.body.patron;

    var description = {
        es: req.body.description_es,
        en: req.body.description_en
    };

    var owner = {
        id: req.body.owner_id,
        comitions: req.body.owner_comitions
    };
    var tagObj = typeof req.body.tag == "string" ? JSON.parse(req.body.tag) : req.body.tag;
    var tag = tagObj._id;


    var files = req.files;
    var photos = [];
    var typeImgs = ["image/jpeg", "image/png"];
    var correct_photo = false;
    var pos = 0;

    var aux = (
        name.length >= 1
        && description.es.length >= 1
        && description.en.length >= 1
        && patron
        && !isNaN(owner.comitions)
        && owner.id
        && !_.isEmpty(tag)

        )
        ;

    if (aux) {
        while (eval("files.file" + pos)) {
            var tempFile = (eval("files.file" + pos));
            if (tempFile.mimetype != "" || tempFile.mimetype != null) {
                var image = {
                    path: "",
                    name: "",
                    originalName: "",
                    extension: ""
                };
                if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                    image.name = tempFile.name;
                    image.path = "/api/media/" + tempFile.name;
                    image.originalName = tempFile.originalname;
                    image.extension = tempFile.extension;
                    photos.push(image);
                    correct_photo = true;

                }
                if (!correct_photo) {
                    utils.deleteFile(tempFile.name);
                    return res.json({error: 'incorrect image!!!'});
                }
            }
            pos++;
        }

        var ship = new db.Ship({
            name: name,
            slug: utils.getSlug(name),
            description: description,
            patron: patron,
            owner: owner,
            photos: photos,
            tag: tag,
            calendar: {
                start: new Date(),
                end: new Date(),
                block: []
            }
        });

        ship.save(function (err, ship) {
            if (err || !ship) {
                return res.json({res: false})
            }
            else {
                return res.json({res: true})
            }
        });
    }
    else {
        return res.json({res: false});
    }
};
exports.listBack = function (req, res) {
    var limit = req.body.limit,
        skip = req.body.skip,
        id = req.body.id,
        text = req.body.text;
    if (text.length > 0) {
        async.parallel([
            function (cb) {
                db.Ship.count({remove: false}).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                text = new RegExp(text, 'i');
                db.Ship.find({
                    remove: false,
                    $or: [
                        {name: text},
                        {tours: id},
                        {'description.es': text},
                        {tel: text},
                        {'description.en': text}
                    ]
                }).populate('tag').sort('-name').limit(limit).skip(skip).exec(function (err, ships) {
                    if (err || !ships) {
                        cb(err, ships);
                    }
                    else {
                        async.map(ships, function (ship, callback) {
                                ship.freeForRemove(function (err, array) {
                                    if (err || !array) {
                                        callback(err, array);
                                    }
                                    else {
                                        ship._doc.dependencies = array;
                                        callback(err, ship);
                                    }
                                });
                            }, function (err, result) {
                                cb(err, result);
                            }
                        )
                    }
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                var ships = results[1];
                var list = new Array();
                if (ships.length > 0) {
                    var cont = 0;
                    for (var i = 0; i < ships.length; i++) {
                        userController.getOwnerAndPatron(ships[i]._doc.owner.id, ships[i]._doc.patron, function (data) {
                            if (data) {
                                var aux = {
                                    id: ships[cont]._doc.owner.id,
                                    comitions: ships[cont]._doc.owner.comitions,
                                    email: (data.owner == 'vacio' ? 'vacio' : data.owner._doc.email)
                                }
                                ships[cont]._doc.owner = aux;

                                var tmp = {
                                    id: ships[cont]._doc.patron,
                                    email: (data.patron == 'vacio' ? 'vacio' : data.patron._doc.email)
                                }
                                ships[cont]._doc.patron = tmp;
                            }
                            cont++;
                            if (cont == ships.length) {
                                var contS = 0;
                                for (var j = 0; j < ships.length; j++) {
                                    getReservationByShip(ships[j], j, function (resT, pos) {
                                        if (resT.length > 0) {
                                            ships[pos]._doc.hasRes = true;
                                        }
                                        else {
                                            ships[pos]._doc.hasRes = false;
                                        }
                                        contS++;
                                        if (contS == ships.length) {
                                            return res.json({res: ships, cont: results[0]});
                                        }

                                    })
                                }

                            }
                        })
                    }
                }
                else {
                    return res.json({res: list, cont: results[0]});
                }
            }
        });
    }
    else {
        async.parallel([
            function (cb) {
                db.Ship.count({remove: false}).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                if (id.length > 0) {
                    db.Ship.find({
                        remove: false, $or: [
                            {_id: id},
                            {tours: id},
                            {'owner.id': id},
                            {patron: id}
                        ]
                    }).populate('tag').exec(function (err, ship) {
                        if (cb) cb(err, ship);
                    })
                }
                else {
                    db.Ship.find({
                        remove: false
                    }).populate('tag').sort('-name').limit(limit).skip(skip).exec(function (err, ships) {
                        if (err || !ships) {
                            cb(err, ships);
                        }
                        else {
                            async.map(ships, function (ship, callback) {
                                    ship.freeForRemove(function (err, array) {
                                        if (err || !array) {
                                            callback(err, array);
                                        }
                                        else {
                                            ship._doc.dependencies = array;
                                            callback(err, ship);
                                        }
                                    });
                                }, function (err, result) {
                                    cb(err, result);
                                }
                            )
                        }
                    })
                }
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                var ships = results[1];
                var list = new Array();
                if (ships.length > 0) {
                    var cont = 0;
                    for (var i = 0; i < ships.length; i++) {
                        userController.getOwnerAndPatron(ships[i]._doc.owner.id, ships[i]._doc.patron, function (data) {
                            if (data) {
                                var aux = {
                                    id: ships[cont]._doc.owner.id,
                                    comitions: ships[cont]._doc.owner.comitions,
                                    email: (data.owner == 'vacio' ? 'vacio' : data.owner._doc.email)
                                }
                                ships[cont]._doc.owner = aux;

                                var tmp = {
                                    id: ships[cont]._doc.patron,
                                    email: (data.patron == 'vacio' ? 'vacio' : data.patron._doc.email)
                                }
                                ships[cont]._doc.patron = tmp;
                            }
                            cont++;
                            if (cont == ships.length) {
                                var contS = 0;
                                for (var j = 0; j < ships.length; j++) {
                                    getReservationByShip(ships[j], j, function (resT, pos) {
                                        if (resT.length > 0) {
                                            ships[pos]._doc.hasRes = true;
                                        }
                                        else {
                                            ships[pos]._doc.hasRes = false;
                                        }
                                        contS++;
                                        if (contS == ships.length) {
                                            return res.json({res: ships, cont: results[0]});
                                        }

                                    })
                                }

                            }
                        })
                    }
                }
                else {
                    return res.json({res: list, cont: results[0]});
                }
            }
        });
    }
};
exports.list = function (req, res) {
    var limit = req.query.limit;
    var skip = req.query.skip;
    db.Ship.find({available: true, remove: false}).populate('tag').exec(function (err, ships) {
        if (err || !ships) {
            return res.json({res: false});
        }
        else {
            var list = new Array();
            if (ships.length > 0) {
                var cont = 0;
                for (var i = 0; i < ships.length; i++) {
                    userController.getOwnerAndPatron(ships[i]._doc.owner.id, ships[i]._doc.patron, function (data) {
                        if (data) {
                            var aux = {
                                id: ships[cont]._doc.owner.id,
                                comitions: ships[cont]._doc.owner.comitions,
                                email: (data.owner == 'vacio' ? 'vacio' : data.owner._doc.email)
                            }
                            ships[cont]._doc.owner = aux;

                            var tmp = {
                                id: ships[cont]._doc.patron,
                                email: (data.patron == 'vacio' ? 'vacio' : data.patron._doc.email)
                            }
                            ships[cont]._doc.patron = tmp;
                        }
                        cont++;
                        if (cont == ships.length) {

                        }
                        return res.json({res: ships});
                    })
                }

            }
            else {
                return res.json({res: list});
            }
        }
    })
};
exports.get = function (req, res) {
    db.Ship.findOne({_id: req.params.id}).exec(function (err, sucesss) {
        if (err || !sucesss) {
            return res.json({res: false});
        }
        else {
            reservationController.getReservationTourByShip(sucesss._doc._id, function (err, reservations) {
                if (err || !reservations) {
                    return res.json({res: false});
                }
                else {
                    sucesss._doc.calendar.block = reservations;
                    return res.json({res: sucesss});
                }
            });

        }
    })
};
exports.remove = function (req, res) {
    var id = req.params.id;
    db.Ship.findOne({_id: id, available: false, remove: false}).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false});
        }
        else {
            var photos = success._doc.photos;
            if (photos.length > 0) {
                var cont = 0;
                for (var j = 0; j < photos.length; j++) {
                    db.Media.remove({fieldName: photos[j].name}).exec(function (err, success) {
                        cont++;
                        if (cont == photos.length) {
                            db.Ship.update({
                                _id: id
                            }, {$set: {remove: true, photos: [], tours: []}}).exec(function (err, success) {
                                if (err || !success) {
                                    return res.json({res: false});
                                }
                                else {
                                    return res.json({res: true});
                                }
                            })
                        }
                    })
                }
            }
            else {
                db.Ship.update({_id: id}, {$set: {remove: true, tours: []}}).exec(function (err, success) {
                    if (err || !success) {
                        return res.json({res: false});
                    }
                    else {
                        return res.json({res: true});
                    }
                })
            }
        }
    })
};
exports.update = function (req, res) {
    var tag = req.body.tag._id;
    if (_.isEmpty(tag) || _.isEmpty(req.body.patron) || _.isEmpty(req.body.owner)) {
        return res.json({res: false});
    }
    else {
        var tagObj = typeof req.body.tag == "string" ? JSON.parse(req.body.tag) : req.body.tag;
        var tag = tagObj._id;
        db.Ship.update({_id: req.body.id, remove: false}, {
                $set: {
                    name: req.body.name,
                    description: req.body.description,
                    patron: req.body.patron,
                    owner: req.body.owner,
                    tag: tag,
                    slug: utils.getSlug(req.body.name)
                }
            },
            function (err, success) {
                if (err || !success) {
                    return res.json({res: false})
                }
                else {
                    return res.json({res: true})
                }
            });
    }

};
exports.removeOnlyMedia = function (req, res) {
    var id = req.body.id,
        photo = req.body.photo;

    db.Ship.update({_id: id, available: true, remove: false}, {$pull: {photos: photo}}).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false})
        }
        else {
            db.Media.remove({fieldName: photo.name}).exec(function (err, success) {
                if (err || !success) {
                    return res.json({res: false})
                }
                else {
                    return res.json({res: true})
                }
            })
        }
    });
};
exports.addOnlyMedia = function (req, res) {
    var extImages = ['jpg', 'png', 'gif', 'bmp'];
    var tmp = false;
    for (var i = 0; i < extImages.length; i++) {
        if (req.files.file0.extension.toLowerCase() != extImages[i]) {
            tmp = true;
        }
    }
    if (!tmp) {
        return res.json({res: false});
    }
    else {
        var aux = {
            path: "/api/media/" + req.files.file0.name,
            "name": req.files.file0.name
        };

        db.Ship.update({
            _id: req.body.id,
            available: true,
            remove: false
        }, {$push: {photos: aux}}).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: aux});
            }
        });

    }


};
exports.addTour = function (req, res) {

    db.Ship.update({_id: req.body.id, remove: false}, {$push: {tours: req.body.tour_id}}).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false});
        }
        else {
            return res.json({res: true});
        }
    })
};
exports.delTour = function (req, res) {
    db.Ship.update({_id: req.body.id, remove: false}, {$pull: {tours: req.body.tour_id}}).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false});
        }
        else {
            return res.json({res: true});
        }
    })
};
exports.createCalendar = function (req, res) {
    var id = req.body.id;
    var calendar = req.body.calendar;

    var events = req.body.calendar.block;


    async.series([
        function (cb) {
            db.ReservationTour.find({available: false, tag: "locked", ship: id}).exec(function (err, resTs) {
                if (err || !resTs) {
                    cb(err, resTs);
                }
                else {
                    async.map(resTs, function (resT, callback) {
                            async.parallel([
                                function (cbp) {
                                    db.ReservationTour.remove({_id: resT._doc._id}).exec(function (err, success) {
                                        cbp(err, success);
                                    });
                                },
                                function (cbp) {
                                    db.Ship.update({_id: id}, {$pop: {'calendar.block': resT._doc._id}}).exec(function (err, success) {
                                        cbp(err, success);
                                    });
                                }
                            ], function (err, results) {
                                callback(err, results);
                            })
                        }, function (err, result) {
                            cb(err, result);
                        }
                    )
                }
            });
        },
        function (cb) {
            async.map(events, function (event, callback) {
                var rest = new db.ReservationTour({
                    ship: id,
                    type: 'undefined',
                    start: new Date(event.start),
                    end: event.end ? new Date(event.end) : new Date(event.start + 86400000),
                    departure: 'undefined',
                    duration: 'undefined',
                    percent: 0,
                    total: 0,
                    comitions: 0,
                    cantPerson: 0,
                    title: event.title,
                    description: event.description,
                    available: false

                });
                rest.save(function (err, event) {
                    callback(err, event);
                });
            }, function (err, result) {
                if (err || !result) {
                    cb(err, result);
                }
                else {
                    async.map(result, function (resT, callbackM) {
                            db.Ship.update({_id: id}, {$push: {'calendar.block': resT._doc._id}}).exec(function (err, success) {
                                callbackM(err, success);
                            });
                        }, function (err, result) {
                            cb(err, result);
                        }
                    )
                }
            });
        }
    ], function (err, results) {
        if (err || !results) {
            return res.json({result: false});
        } else {
            db.Ship.update({_id: id}, {
                $set: {
                    'calendar.start': new Date(calendar.start),
                    'calendar.end': new Date(calendar.end)
                }
            }).exec(function (err, success) {
                if (err || !success) {
                    return res.json({res: false});
                }
                else {
                    return res.json({res: true});
                }
            });
        }
    });


};
exports.updateImg = function (req, res) {
    var idShip = req.body.id;
    var nameImgDel = eval(req.body.nameImgDel);
    var files = req.files;

    var typeImgs = ["image/jpeg", "image/png"];

    if (nameImgDel.length != 0) {
        var cont = 0;
        for (var i = 0; i < nameImgDel.length; i++) {
            db.Media.remove({fieldName: nameImgDel[i]}).exec(function (err, success) {
                if (err || !success) {
                    return res.json({result: false});
                } else {
                    cont++;
                    if (cont == nameImgDel.length) {
                        db.Ship.findOne({_id: idShip, remove: false}).exec(function (err, ship) {
                            if (err || !ship) {
                                return res.json({result: false});
                            } else {
                                for (var i = 0; i < nameImgDel.length; i++) {
                                    var pos = 0;
                                    var flag = false;
                                    var cantidad = ship.photos.length;
                                    while (!flag && pos < cantidad) {
                                        if (ship.photos[pos].name == nameImgDel[i]) {
                                            ship.photos.splice(pos, 1);
                                            flag = true;
                                        }
                                        pos++;
                                    }
                                }
                                var pos = 0;
                                while (eval("files.file" + pos)) {
                                    var tempFile = (eval("files.file" + pos));
                                    if (tempFile.mimetype != "" || tempFile.mimetype != null) {
                                        if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                                            var photo = {
                                                path: "/api/media/" + tempFile.name,
                                                name: tempFile.name,
                                                originalName: tempFile.originalname,
                                                extension: tempFile.extension
                                            };
                                            ship.photos.push(photo);
                                        } else {
                                            utils.deleteFile(tempFile.name);
                                        }
                                    } else {
                                        utils.deleteFile(tempFile.name);
                                    }
                                    pos++;
                                }
                                db.Ship.update({_id: idShip, remove: false}, {$set: {photos: ship.photos}}).exec(
                                    function (err, success) {
                                        if (err || !success) {
                                            return res.json({result: false});
                                        } else {
                                            return res.json({result: true});
                                        }
                                    })
                            }
                        })
                    }
                }
            })
        }
    } else {
        db.Ship.findOne({_id: idShip, remove: false}).exec(function (err, ship) {
            if (err || !ship) {
                return res.json({result: false});
            } else {
                var pos = 0;
                while (eval("files.file" + pos)) {
                    var tempFile = (eval("files.file" + pos));
                    if (tempFile.mimetype != "" || tempFile.mimetype != null) {
                        if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                            var photo = {
                                path: "/api/media/" + tempFile.name,
                                name: tempFile.name,
                                originalName: tempFile.originalname,
                                extension: tempFile.extension
                            };
                            ship.photos.push(photo);
                        } else {
                            utils.deleteFile(tempFile.name);
                        }
                    } else {
                        utils.deleteFile(tempFile.name);
                    }
                    pos++;
                }
                db.Ship.update({_id: idShip, remove: false}, {$set: {photos: ship.photos}}).exec(
                    function (err, success) {
                        if (err || !success) {
                            return res.json({result: false});
                        } else {
                            return res.json({result: true});
                        }
                    })
            }
        })
    }
};
exports.status = function (req, res) {
    var id = req.body.id;
    db.Ship.findOne({_id: id, remove: false}).exec(function (err, ship) {
        if (err || !ship) {
            return res.json({res: false});
        }
        else {
            var available = !ship._doc.available;
            db.Ship.update({_id: id, remove: false}, {$set: {available: available}}).exec(function (err, success) {
                if (err || !success) {
                    return res.json({res: false});
                }
                else {
                    return res.json({res: true})
                }
            });
        }
    });


};

/*FUCTIONS*/
exports.writeCalendarByShip = function (req, tour, cb) {
    try {
        req.session.bookInfo = null;
        db.Ship.findOne({_id: req.session.sale.ship.id}).exec(function (err, ship) {
            if (err || !ship) {
                if (cb) cb(false);
            }
            else {
                var flag = false;
                _.forEach(ship._doc.calendar.block, function (block) {
                    if (block.toString() == tour._doc._id.toString()) {
                        flag = false;
                    }
                });
                if (!flag) {
                    db.Ship.update({_id: ship._doc._id}, {$push: {'calendar.block': tour._doc._id}}).exec(function (err, success) {
                        if (cb) cb(err, success);
                    });
                }
                else {
                    if (cb) cb(null, true);
                }

            }
        })
    }
    catch
        (err) {
        return false;
    }
};

exports.deleteCalendarByShip = function (resC, cb) {
    if ((resC._doc.reservation.cantPerson - resC._doc.cantPerson) < 1) {
        db.Ship.findOne({_id: resC._doc.reservation.ship.id, remove: false}, function (err, ship) {
            if (err || !ship) {
                cb(false);
            }
            else {
                if (resC._doc.reservation.tour.type == 'hours') {
                    var duration = parseFloat(resC._doc.reservation.tour.duration) * 3600000,
                        departure = resC._doc.reservation.departure * 3600000,
                        start = new Date(resC._doc.reservation.date.getTime() + departure),
                        end = new Date(start.getTime() + duration),
                        allDay = false,
                        editable = false;
                }
                else if (resC._doc.reservation.tour.type == 'agree') {
                    var start = resC._doc.reservation.date,
                        end = null,
                        allDay = true,
                        editable = false;
                }
                else if (resC._doc.reservation.tour.type == 'day') {
                    var start = resC._doc.reservation.date,
                        end = new Date(resC._doc.reservation.date.getTime() + 86400000),
                        allDay = true,
                        editable = false;
                }

                db.Ship.findOne({_id: resC._doc.reservation.ship.id, remove: false}, function (err, ship) {
                    if (err || !ship) {
                        cb(false);
                    }
                    else {
                        var blocks = new Array();
                        for (var i = 0; i < ship.calendar.block.length; i++) {
                            if (resC._doc.reservation.tour.type != 'agree') {
                                var sem = (start.getTime() == ship.calendar.block[i].start.getTime() && end.getTime() == ship.calendar.block[i].end.getTime() && ship.calendar.block[i].allDay == allDay && ship.calendar.block[i].editable == editable);

                            }
                            else if (resC._doc.reservation.tour.type == 'agree') {
                                var sem = (start.getTime() == ship.calendar.block[i].start.getTime() && end == ship.calendar.block[i].end && ship.calendar.block[i].allDay == allDay && ship.calendar.block[i].editable == editable);

                            }
                            if (!sem) {
                                blocks.push(ship.calendar.block[i]);
                            }
                        }

                        db.Ship.update({_id: resC._doc.reservation.ship.id}, {$set: {'calendar.block': blocks}}, function (err, success) {
                            if (err || !success) {
                                cb(false);
                            }
                            else {
                                cb(true);
                            }
                        })
                    }
                });
            }

        });
    }
    else {
        cb(true);
    }

};
exports.hasShips = function (id, cb) {
    db.Ship.find({tours: id, remove: false, available: true}).populate({
        path: 'calendar.block',
        match: {remove: false}
    }).exec(function (err, ship) {
        if (err || !ship) {
            cb([]);
        }
        else {
            cb(ship);
        }
    })
};
exports.getShipByID = function (id, cb) {
    db.Ship.findOne({_id: id}).exec(function (err, ship) {
        if (err || !ship) {
            if (cb) cb(false);
        }
        else {
            if (cb) cb(ship);
        }
    })
};
exports.getShipByOwner = function (owner, aux, cb) {
    db.Ship.findOne({'owner.id': owner, remove: false}).exec(function (err, ship) {
        if (err || !ship) {
            if (cb) cb(aux);
        }
        else {
            aux.ship = true;
            if (cb) cb(aux);
        }
    })
};
exports.getShipByPatron = function (patron, aux, cb) {
    db.Ship.findOne({patron: patron, remove: false}).exec(function (err, ship) {
            if (err || !ship) {
                if (cb) cb(aux);
            }
            else {
                aux.ship = true;
                if (cb) cb(aux);
            }
        }
    )
};
function getHoursArray(start, length) {
    var hours = [];
    for (var i = start; i < length; i++) {
        if (i <= 24) {
            hours.push(i);
        }

    }
    return hours;
};
function removeDaysBlock(id, block, cb) {
    var cont = 0;
    if (block.length > 0) {
        for (var i = 0; i < block.length; i++) {
            db.Ship.update({
                _id: id,
                remove: false
            }, {$pull: {'calendar.block': block[i]}}).exec(function (err, success) {
                if (err || !success) {
                    if (cb) cb(false);
                }
                else {
                    cont++;
                    if (cont == block.length)
                        if (cb) cb(true);
                }
            })
        }
    }
    else {
        if (cb) cb(true);
    }


};
function ifNoExist(array, item) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] == item) {
            return false;
        }
    }
    return true;
};
function ifNoExistBlock(array, day) {
    for (var i = 0; i < array.length; i++) {
        if (array[i].day == day) {
            return false;
        }
    }
    return true;
};
function orderAsc(array) {
    for (var i = 0; i < array.length - 1; i++) {
        for (var j = i + 1; j < array.length; j++) {
            if (array[i] > array[j]) {
                var aux = array[i];
                array[i] = array[j];
                array[j] = aux;
            }
        }
    }
    return array;
};
function getReservationByShip(ship, pos, cb) {
    var date = new Date();
    db.ReservationTour.find({remove: false, ship: ship._id, date: {$gt: date}}).exec(function (err, res) {
        if (err || !res) {
            if (cb) cb(new Array(), pos);
        }
        else {
            if (cb) cb(res, pos);
        }
    })
};
function matchCalendarBlock(a, b, c, d) {
    if (c <= a && a < d) {
        return true;
    }
    if (c <= b && b < d) {
        return true;
    }
    if (a <= c && c < b) {
        return true;
    }
    if (a <= d && d < b) {
        return true;
    }
    return false;
}


