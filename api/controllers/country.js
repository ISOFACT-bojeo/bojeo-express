/**
 * Created by ernestomr87@gmail.com on 05/11/2014.
 */
var async = require('async');
exports.create = function (req, res) {
    var newCountry = new db.Country({
        iso: req.body.iso,
        name: req.body.name,
        cities: req.body.cities
    });

    newCountry.save(function (err, country) {
        if (err || !country) {
            return res.json({res: false});
        }
        else {
            return res.json({res: true});
        }
    })
};
exports.delete = function (req, res) {
    db.Country.findOne({_id: req.params.id}).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false});
        }
        else {
            getPortByCountry(success._doc.name, 0, function (ports, pos) {
                if (ports.length > 0) {
                    return res.json({res: false});
                }
                else {
                    db.Country.remove({_id: req.params.id}).exec(function (err, success) {
                        if (err || !success) {
                            return res.json({res: false});
                        }
                        else {
                            return res.json({res: true});
                        }
                    });
                }
            })
        }
    })
};
exports.update = function (req, res) {

    db.Country.findOne({_id: req.body.id}).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false});
        }
        else {
            getPortByCountry(success._doc.name, 0, function (ports, pos) {
                if (ports.length > 0) {
                    return res.json({res: false});
                }
                else {
                    db.Country.update({_id: req.body.id}, {
                        $set: {
                            iso: req.body.iso,
                            name: req.body.name,
                            cities: req.body.cities
                        }
                    }).exec(function (err, success) {
                        if (err || !success) {
                            return res.json({res: false});
                        }
                        else {
                            return res.json({res: true});
                        }
                    })
                }
            })
        }
    })


};
exports.listFront = function (req, res) {
    db.Country.find().exec(function (err, objs) {
        if (err || !objs) {
            return res.json({res: false});
        }
        else {
            return res.json({res: objs});
        }
    })

};
exports.list = function (req, res) {
    var limit = req.query.limit,
        skip = req.query.skip,
        text = req.query.text;
    if (text.length > 0) {
        async.parallel([
            function (cb) {
                db.Country.count().exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                text = new RegExp(text, 'i');
                db.Country.find({$or: [
                    {'name.es': text},
                    {'name.en': text},
                    {tel: text},
                    {'cities.es': text},
                    {'cities.en': text}
                ]}).sort('-name.es').limit(limit).skip(skip).exec(function (err, countries) {
                    var cont = 0;
                    for (var i = 0; i < countries.length; i++) {
                        getPortByCountry(countries[i]._doc.name, i, function (ports, pos) {

                            if (ports.length > 0) {
                                countries[pos]._doc.has = true;
                            }
                            else {
                                countries[pos]._doc.has = false;
                            }
                            cont++;
                            if (cont == countries.length) {
                                if (cb) cb(err, countries);
                            }
                        });
                    }

                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                var conti = 0;
                for (var i = 0; i < results[1].length; i++) {
                    var contj = 0;
                    if (results[1][i]._doc.has) {

                        for (var j = 0; j < results[1][i].cities.length; j++) {
                            getPortByCountryAndCity(results[1][i].name, results[1][i].cities[j], i, j, function (ports, posi, posj) {
                                if (ports.length > 0) {
                                    results[1][posi].cities[posj].has = true;
                                }
                                else {
                                    results[1][posi].cities[posj].has = false;
                                }
                                contj++;
                                if (contj == results[1][posi].cities.length) {
                                    conti++;
                                    if (conti == results[1].length) {
                                        return res.json({res: results[1], cont: results[0]});
                                    }
                                }

                            })
                        }
                    }
                    else {
                        conti++;
                        if (conti == results[1].length) {
                            return res.json({res: results[1], cont: results[0]});
                        }
                    }
                }


            }
        });
    }
    else {
        async.parallel([
            function (cb) {
                db.Country.count().exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                db.Country.find().sort('-name').limit(limit).skip(skip).exec(function (err, countries) {
                    var cont = 0;
                    if (countries.length) {
                        for (var i = 0; i < countries.length; i++) {
                            getPortByCountry(countries[i]._doc.name, i, function (ports, pos) {

                                if (ports.length > 0) {
                                    countries[pos]._doc.has = true;
                                }
                                else {
                                    countries[pos]._doc.has = false;
                                }
                                cont++;
                                if (cont == countries.length) {
                                    if (cb) cb(err, countries);
                                }
                            });
                        }
                    }
                    else {
                        cb(null, new Array());
                    }

                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                var conti = 0;
                if (results[1].length) {
                    for (var i = 0; i < results[1].length; i++) {
                        var contj = 0;
                        if (results[1][i]._doc.has) {
                            for (var j = 0; j < results[1][i].cities.length; j++) {
                                getPortByCountryAndCity(results[1][i].name, results[1][i].cities[j], i, j, function (ports, posi, posj) {
                                    if (ports.length > 0) {
                                        results[1][posi].cities[posj]._doc.has = true;
                                    }
                                    else {
                                        results[1][posi].cities[posj]._doc.has = false;
                                    }
                                    contj++;
                                    if (contj == results[1][posi].cities.length) {
                                        contj = 0;
                                        conti++;
                                        if (conti == results[1].length) {
                                            return res.json({res: results[1], cont: results[0]});
                                        }
                                    }

                                })
                            }
                        }
                        else {
                            conti++;
                            if (conti == results[1].length) {
                                return res.json({res: results[1], cont: results[0]});
                            }
                        }
                    }
                }

                else {
                    return res.json({res: new Array(), cont: 0});
                }


            }
        });
    }


};
exports.get = function (req, res) {
    db.Country.findOne({_id: req.params.id}).exec(function (err, obj) {
        if (err || !obj) {
            return res.json({res: false});
        }
        else {
            return res.json({res: obj});
        }

    })
};
exports.addCity = function (req, res) {
    var idCountry = req.body.id;
    var city = req.body.city;
    db.Country.findOne({_id: idCountry}).exec(function (err, obj) {
        if (err || !obj) {
            return res.json({res: false, code: 0});
        } else {
            var exist = false;
            for (var i = 0; i < obj.cities.length; i++) {
                if (city.es.toLowerCase() == obj.cities[i].es.toLowerCase() && city.en.toLowerCase() == obj.cities[i].en.toLowerCase()) {
                    exist = true;
                    break
                }
            }
            if (exist) {
                return res.json({res: false, code: 1});
            } else {
                db.Country.update({_id: req.body.id}, {$push: {cities: req.body.city}}).exec(function (err, success) {
                    if (err || !success) {
                        return res.json({res: false, code: 0});
                    }
                    else {
                        return res.json({res: true});
                    }
                })
            }
        }
    })
};

exports.delCity = function (req, res) {
    db.Country.findOne({_id: req.body.id}).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false});
        }
        else {
            var city = {
                es: req.body.city.es,
                en: req.body.city.en
            };
            getPortByCountryAndCity(success._doc.name, city, 0, 0, function (ports, pos) {
                if (ports.length > 0) {
                    return res.json({res: false});
                }
                else {
                    db.Country.update({_id: req.body.id}, {$pull: {cities: req.body.city}}).exec(function (err, success) {
                        if (err || !success) {
                            return res.json({res: false});
                        }
                        else {
                            return res.json({res: true});
                        }
                    })
                }
            })
        }
    });
};

exports.updateCity = function (req, res) {
    db.Country.findOne({_id: req.body.id}).exec(function (err, country) {
        if (err || !country) {
            return res.json({res: false, code: 0});
        } else {
            getPortByCountryAndCity(country._doc.name, req.body.cityOld, 0, 0, function (ports) {
                if (ports.length > 0) {
                    return res.json({res: false, code: 0});
                }
                else {
                    var exist = false;
                    for (var i = 0; i < country._doc.cities.length; i++) {
                        if (req.body.city.es.toLowerCase() ==  country._doc.cities[i].es.toLowerCase() && req.body.city.en.toLowerCase() ==  country._doc.cities[i].en.toLowerCase()) {
                            if(req.body.city.id != country._doc.cities[i]._id){
                                exist = true;
                                break;
                            }
                        }
                    }
                    if(!exist){
                        for (var i = 0; i < country._doc.cities.length; i++) {
                            if (country._doc.cities[i].es == req.body.cityOld.es && country._doc.cities[i].en == req.body.cityOld.en) {
                                country._doc.cities[i] = req.body.city;
                                break;
                            }
                        }
                        db.Country.update({_id: req.body.id}, {$set: {cities: country._doc.cities}}).exec(function (err, success) {
                            if (err || !success) {
                                return res.json({res: false, code: 0});
                            }
                            else {
                                return res.json({res: true});
                            }
                        });
                    }else{
                        return res.json({res: false, code: 1});
                    }
                }
            })
        }
    });
};

function getPortByCountry(country, pos, cb) {
    db.Port.find({'country.es': country.es, 'country.en': country.en, remove: false}).exec(function (err, objs) {
        if (err || !objs) {
            if (cb) cb(new Array(), pos);
        }
        else {
            if (cb) cb(objs, pos);
        }
    })
};

function getPortByCountryAndCity(country, city, posi, posj, cb) {
    db.Port.find({
        'country.es': country.es, 'country.en': country.en,
        'city.es': city.es, 'city.en': city.en, remove: false
    }).exec(function (err, objs) {
        if (err || !objs) {
            cb(new Array(), posi, posj);
        }
        else {
            cb(objs, posi, posj);
        }
    })

};

