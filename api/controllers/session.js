/**
 * Created by ernestomr87@gmail.com on 23/05/2014.
 */
'use strict';

exports.auth = function (req, res, next) {
    return req.user ? next() : res.status(403).json({error: 'Requires login'});
}
exports.authRedirect = function (req, res, next) {
    return req.user ? next() : res.redirect('/login');
    ;
}
exports.admin = function (req, res, next) {
    var is_admin = (req.user && req.user.role == 'admin') || false;
    return is_admin ? next() : res.redirect('/backoffice/login');
}

exports.nocache = function (req, res, next) {
    res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    res.setHeader("Pragma", "no-cache");
    res.setHeader("Expires", 0);
    next();
}

exports.generateGuid = function () {

    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;

};
exports.getStatus = function (sid) {
    return db.Sessions.find({'session.user._id': sid}).exec(function (err, succes) {
        if (err) return err;
        if (succes) return true;
    });
};



