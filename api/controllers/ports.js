/**
 * Created by ernestomr87@gmail.com on 7/05/14.
 */
var async = require('async');
var utils = require('../utils');
var toursController = require('./tours');

exports.create = function (req, res) {

    var name = {
        es: req.body.name_es,
        en: req.body.name_en
    };
    var address = {
        es: req.body.address_es,
        en: req.body.address_en
    };

    var city = {
        es: req.body.city_es,
        en: req.body.city_en
    };

    var country = {
        id: req.body.country_id,
        es: req.body.country_es,
        en: req.body.country_en
    };

    var location = {
        x: req.body.location_x,
        y: req.body.location_y
    };

    var files = req.files;

    var photos = [];
    var typeImgs = ["image/jpeg", "image/png"];
    var correct_photo = false;
    var pos = 0;

    if (validateForm(name, address, city, country, location)) {

        while (eval("files.file" + pos)) {
            var tempFile = (eval("files.file" + pos));
            if (tempFile.mimetype != "" || tempFile.mimetype != null) {
                var image = {
                    path: "",
                    name: "",
                    originalName: "",
                    extension: ""
                };
                if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                    image.name = tempFile.name;
                    image.path = "/api/media/" + tempFile.name;
                    image.originalName = tempFile.originalname;
                    image.extension = tempFile.extension;
                    photos.push(image);
                    correct_photo = true;

                }
                if (!correct_photo) {
                    utils.deleteFile(tempFile.name);
                    return res.json({error: 'incorrect image!!!'});
                }
            }
            pos++;
        }

        var port = new db.Port({
            name: name,
            address: address,
            location: location,
            city: city,
            photos: photos,
            country: country,
            slug: utils.getSlug(name.en)
        });

        port.save(function (err, port) {
            if (err || !port) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true});
            }
        });
    }
    else {
        return res.json({res: false});
    }


};
exports.list = function (req, res) {
    var limit = req.query.limit;
    var skip = req.query.skip;
    db.Port.find({remove: false}).sort().limit(limit).skip(skip).exec(function (err, ports) {
        if (err || !ports) {
            return res.json({res: false});
        } else {
            if (ports.length > 0) {
                var cont = 0;
                for (var i = 0; i < ports.length; i++) {
                    getTourByPort(ports[i]._id, i, function (tour, pos) {
                        var arrayTours = new Array();
                        for (var j = 0; j < tour.length; j++) {
                            arrayTours.push(tour[j]._doc._id);
                        }
                        ports[pos]._doc.tour = arrayTours;
                        cont++;
                        if (cont == ports.length) {
                            return res.json({res: ports});
                        }
                    })
                }
            } else {
                return res.json({res: new Array()});
            }
        }
    })
};
exports.listBack = function (req, res) {
    var limit = req.body.limit,
        skip = req.body.skip,
        text = req.body.text;
    if (text.length > 0) {
        async.parallel([
            function (cb) {
                db.Port.count({remove: false}).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                text = new RegExp(text, 'i');
                db.Port.find(
                    {
                        remove: false,
                        $or: [
                            {'name.es': text},
                            {'name.en': text},
                            {'address.es': text},
                            {'address.es': text},
                            {'city.es': text},
                            {'city.es': text},
                            {'country.es': text},
                            {'country.es': text}
                        ]
                    }).sort('-createDate').limit(limit).skip(skip).exec(function (err, ports) {

                        if (ports.length > 0) {
                            var cont = 0;
                            for (var i = 0; i < ports.length; i++) {
                                exports.getTourByPortAll(ports[i]._id, i, function (tour, pos) {
                                    if (tour.length > 0) {
                                        var cont1 = 0;
                                        for (var j = 0; j < tour.length; j++) {
                                            var reserva = false;
                                            var arrayTours = new Array();
                                            toursController.getResevationByTour(tour[j], j, function (reservations, pos1) {
                                                if (reservations > 0) {
                                                    reserva = true;
                                                }
                                                cont1++;
                                                arrayTours.push(tour[pos1]._doc._id);
                                                if (cont1 == tour.length) {
                                                    ports[pos]._doc.hasReservation = reserva;
                                                    cont++;
                                                    ports[pos]._doc.tour = arrayTours;
                                                    if (cont == ports.length) {
                                                        if (cb) cb(err, ports);
                                                    }
                                                }
                                            })
                                        }
                                    } else {
                                        cont++;
                                        ports[pos]._doc.tour = [];
                                        if (cont == ports.length) {
                                            if (cb) cb(err, ports);
                                        }
                                    }
                                })
                            }
                        } else {
                            if (cb) cb(err, ports);
                        }
                    })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                return res.json({res: results[1], cont: results[0]});
            }
        });
    }
    else {
        async.parallel([
            function (cb) {
                db.Port.count({remove: false}).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                db.Port.find({remove: false}).sort('-createDate').limit(limit).skip(skip).exec(function (err, ports) {
                    if (ports.length > 0) {
                        var cont = 0;
                        for (var i = 0; i < ports.length; i++) {
                            exports.getTourByPortAll(ports[i]._id, i, function (tour, pos) {
                                if (tour.length > 0) {
                                    var cont1 = 0;
                                    for (var j = 0; j < tour.length; j++) {
                                        var reserva = false;
                                        var arrayTours = new Array();
                                        toursController.getResevationByTour(tour[j], j, function (reservations, pos1) {
                                            if (reservations > 0) {
                                                reserva = true;
                                            }
                                            cont1++;
                                            arrayTours.push(tour[pos1]._doc._id);
                                            if (cont1 == tour.length) {
                                                ports[pos]._doc.hasReservation = reserva;
                                                cont++;
                                                ports[pos]._doc.tour = arrayTours;
                                                if (cont == ports.length) {
                                                    if (cb) cb(err, ports);
                                                }
                                            }
                                        })
                                    }
                                } else {
                                    cont++;
                                    ports[pos]._doc.tour = [];
                                    if (cont == ports.length) {
                                        if (cb) cb(err, ports);
                                    }
                                }
                            })
                        }
                    } else {
                        if (cb) cb(err, ports);
                    }
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            }
            else {
                return res.json({res: results[1], cont: results[0]});
            }
        });
    }
};
exports.get = function (req, res) {
    var id = req.params.id;
    db.Port.find({_id: id, remove: false}, function (err, success) {
        if (success) {
            return res.json({res: success});
        } else {
            return res.json({res: false});
        }
    });
};
exports.update = function (req, res) {
    var id = req.body.id,
        name = req.body.name,
        address = req.body.address,
        city = req.body.city,
        country = req.body.country,
        location = req.body.location,
        slug = utils.getSlug(name.en);

    if (validateForm(name, address, city, country)) {
        db.Port.update({_id: id, remove: false}, {
            $set: {
                name: name,
                address: address,
                location: location,
                city: city,
                country: country,
                slug: slug
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true});
            }
        })
    }
    else {
        return res.json({res: false});
    }
};
exports.remove = function (req, res) {
    var id = req.params.id;
    async.waterfall([
        function (callback) {
            db.Tour.find({port: id, remove: false}, function (err, tours) {
                callback(err, tours);
            })
        },
        function (tours, callback) {
            async.map(tours, function (tour, callback0) {
                async.waterfall([
                    function (callback1) {
                        db.Pack.find({tour: tour._id, remove: false}, function (err, paks) {
                            callback1(err, paks);
                        })
                    },
                    function (paks, callback1) {
                        async.map(paks, function (pack, callback2) {
                            db.Pack.update({_id: pack._id}, {$set: {remove: true}}).exec(function (err, success) {
                                callback2(err, success)
                            })
                        }, function (err, result) {
                            callback1(err, result);
                        })
                    }
                ], function (err, result) {
                    if (err || !result) {
                        callback0(err, result);
                    }
                    else {
                        var photos = tour._doc.photos;
                        if (photos.length > 0) {
                            var cont = 0;
                            for (var j = 0; j < photos.length; j++) {
                                db.Media.remove({fieldName: photos[j].name}).exec(function (err, success) {
                                    cont++;
                                    if (cont == photos.length) {
                                        db.Tour.update({_id: tour._id}, {$set: {remove: true}}).exec(function (err, success) {
                                            callback0(err, success);
                                        })
                                    }
                                })
                            }
                        }
                        else {
                            db.Tour.update({_id: tour._id}, {$set: {remove: true}}).exec(function (err, success) {
                                callback0(err, success);
                            })
                        }
                    }
                })
            }, function (err, result) {
                callback(err, result);
            })
        }
    ], function (err, result) {
        if (err || !result) {
            return res.json({res: false})
        }
        else {
            db.Port.findOne({_id: id, available: false}).exec(function (err, success) {
                if (err || !success) {
                    return res.json({res: false});
                }
                else {
                    var photos = success._doc.photos;
                    if (photos.length > 0) {
                        var cont = 0;
                        for (var j = 0; j < photos.length; j++) {
                            db.Media.remove({fieldName: photos[j].name}).exec(function (err, success) {
                                cont++;
                                if (cont == photos.length) {
                                    db.Port.update({
                                        _id: id,
                                        available: false
                                    }, {$set: {remove: true, photos: []}}).exec(function (err, success) {
                                        if (err || !success) {
                                            return res.json({res: false});
                                        }
                                        else {
                                            return res.json({res: true});
                                        }
                                    })
                                }
                            })
                        }
                    }
                    else {
                        db.Port.update({_id: id}, {$set: {remove: true}}).exec(function (err, success) {
                            if (err || !success) {
                                return res.json({res: false});
                            }
                            else {
                                return res.json({res: true});
                            }
                        })
                    }
                }
            })
        }
    })
};
exports.removeOnlyMedia = function (req, res) {
    var id = req.body.id,
        photo = req.body.photo;

    db.Port.update({_id: id}, {$pull: {photos: photo}}).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false})
        }
        else {
            db.Media.remove({fieldName: photo.name}).exec(function (err, success) {
                if (err || !success) {
                    return res.json({res: false})
                }
                else {
                    return res.json({res: true})
                }
            })
        }
    });
};
exports.addOnlyMedia = function (req, res) {
    var extImages = ['jpg', 'png', 'gif', 'bmp'];
    var tmp = false;
    for (var i = 0; i < extImages.length; i++) {
        if (req.files.file0.extension.toLowerCase() != extImages[i]) {
            tmp = true;
        }
    }
    if (!tmp) {
        return res.json({res: false});
    }
    else {
        var aux = {
            path: "/api/media/" + req.files.file0.name,
            "name": req.files.file0.name
        };

        db.Port.update({_id: req.body.id}, {$push: {photos: aux}}).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: aux});
            }
        });

    }


};
exports.filterByCountry = function (req, res) {
    db.Port.find({'country.en': req.body.country}).exec(function (err, ports) {
        if (err || !ports) {
            return res.json({res: false});
        }
        else {
            return res.json({res: ports});
        }
    })
};
exports.filterByCity = function (req, res, cb) {
    db.Port.find({
        'country.en': req.body.country,
        'city.en': req.body.city,
        available: true,
        remove: false
    }).exec(function (err, ports) {
        if (req.body.local) {
            if (err || !ports || ports.length == 0) {
                cb(false);
            }
            else {
                cb(ports);
            }
        }
        else {
            if (err || !ports || ports.length == 0) {
                return res.json({res: false});
            }
            else {
                return res.json({res: ports});
            }
        }

    })
};
exports.getAvailable = function (req, res, cb) {

    db.Port.find({available: true, remove: false}).exec(function (err, ports) {
        if (req.body.local) {
            if (err || !ports) {
                cb(false);
            }
            else {
                cb(ports);
            }
        }
        else {
            if (err || !ports) {
                return res.json({res: false});
            }
            else {
                return res.json({res: ports});
            }
        }
    })
};
exports.setAvailable = function (req, res) {
    var id = req.body.id;
    var available = req.body.available;
    if (req.body.action == 2) {
        db.Port.update({_id: id, available: available}, {$set: {available: !available}},
            function (err, success) {
                if (err || !success) {
                    return res.json({res: false})
                }
                else {
                    return res.json({res: true, complete: true})
                }
            })
    } else {
        var tours = req.body.tours;
        if (req.body.action == 0) {
            async.map(tours, function (tour, callback) {
                toursController.getTourGroupsAvailable(tour, 0, function (data) {
                    if (data) {
                        async.waterfall([
                            function (cb) {
                                db.Pack.find({tour: tour, status: true, remove: false}, function (err, paks) {
                                    cb(err, paks);
                                })
                            },
                            function (paks, cb) {
                                async.map(paks, function (pack, callback0) {
                                    async.map(pack.services, function (service, callback1) {
                                        db.Service.findOne({_id: service}).select('name').exec(function (err, servs) {
                                            callback1(err, servs);
                                        })
                                    }, function (err, result) {
                                        if (err || !result) {
                                            callback0(err, result);
                                        }
                                        else {
                                            var aux = {
                                                id: pack._id,
                                                services: result
                                            };
                                            callback0(null, aux);
                                        }

                                    })
                                }, function (err, result) {
                                    cb(err, result);
                                })
                            }
                        ], function (err, result) {
                            if (err || !result) {
                                callback(err, result);
                            } else {
                                var aux = {
                                    id: data._doc._id,
                                    name: data._doc.name,
                                    group: data._doc.group,
                                    packs: result
                                };
                                callback(null, aux);
                            }
                        })
                    } else {
                        callback(null, null);
                    }
                })
            }, function (err, result) {
                if (err || !result) {
                    return res.json({res: false});
                }
                else {
                    var temp = [];
                    for (var i = 0; i < result.length; i++) {
                        if (result[i] != null) {
                            temp.push(result[i]);
                        }
                    }
                    if (temp.length == 0) {
                        db.Port.update({_id: id, available: available}, {$set: {available: !available}},
                            function (err, success) {
                                if (err || !success) {
                                    return res.json({res: false})
                                }
                                else {
                                    return res.json({res: true, complete: true})
                                }
                            })
                    } else {
                        return res.json({res: temp, complete: false});
                    }
                }
            })
        } else {
            async.map(tours, function (tour, callback) {
                async.map(tour.packs, function (pack, callback0) {
                    db.Pack.update({_id: pack.id}, {$set: {status: false}}, function (err, success) {
                        callback0(err, success)
                    })
                }, function (err, result) {
                    if (err || !result) {
                        callback(err, result);
                    } else {
                        db.Tour.update({
                            _id: tour.id
                        }, {$set: {available: false}}).exec(function (err, success) {
                            callback(err, success);
                        })
                    }
                })
            }, function (err, result) {
                if (err || !result) {
                    return res.json({res: false, complete: false});
                }
                else {
                    db.Port.update({_id: id, available: available}, {$set: {available: !available}},
                        function (err, success) {
                            if (err || !success) {
                                return res.json({res: false})
                            }
                            else {
                                return res.json({res: true, complete: true})
                            }
                        })
                }
            })
        }
    }
};
exports.updateImg = function (req, res) {
    var idPort = req.body.id;
    var nameImgDel = eval(req.body.nameImgDel);
    var files = req.files;

    var typeImgs = ["image/jpeg", "image/png"];

    if (nameImgDel.length != 0) {
        async.map(nameImgDel, function (obj, callback) {
            db.Media.remove({fieldName: obj}).exec(function (err, success) {
                callback(err, success);
            });
        }, function (err, result) {
            if (err || !result) {
                return res.json({result: false});
            }
            else {
                db.Port.findOne({_id: idPort}).exec(function (err, port) {
                    if (err || !port) {
                        return res.json({result: false});
                    } else {
                        for (var i = 0; i < nameImgDel.length; i++) {
                            var pos = 0;
                            var flag = false;
                            var cont = port.photos.length;
                            while (!flag && pos < cont) {
                                if (port.photos[pos].name == nameImgDel[i]) {
                                    port.photos.splice(pos, 1);
                                    flag = true;
                                }
                                pos++;
                            }
                        }
                        var pos = 0;
                        while (eval("files.file" + pos)) {
                            var tempFile = (eval("files.file" + pos));
                            if (tempFile.mimetype != "" || tempFile.mimetype != null) {
                                if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                                    var photo = {
                                        path: "/api/media/" + tempFile.name,
                                        name: tempFile.name,
                                        originalName: tempFile.originalname,
                                        extension: tempFile.extension
                                    };
                                    port.photos.push(photo);
                                } else {
                                    utils.deleteFile(tempFile.name);
                                }
                            } else {
                                utils.deleteFile(tempFile.name);
                            }
                            pos++;
                        }
                        db.Port.update({_id: idPort}, {$set: {photos: port.photos}}).exec(
                            function (err, success) {
                                if (err || !success) {
                                    return res.json({result: false});
                                } else {
                                    return res.json({result: true});
                                }
                            })
                    }
                })
            }

        });


    } else {
        db.Port.findOne({_id: idPort}).exec(function (err, port) {
            if (err || !port) {
                return res.json({result: false});
            } else {
                var pos = 0;
                while (eval("files.file" + pos)) {
                    var tempFile = (eval("files.file" + pos));
                    if (tempFile.mimetype != "" || tempFile.mimetype != null) {
                        if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                            var photo = {
                                path: "/api/media/" + tempFile.name,
                                name: tempFile.name,
                                originalName: tempFile.originalname,
                                extension: tempFile.extension
                            };
                            port.photos.push(photo);
                        } else {
                            utils.deleteFile(tempFile.name);
                        }
                    } else {
                        utils.deleteFile(tempFile.name);
                    }
                    pos++;
                }

                db.Port.update({_id: idPort}, {$set: {photos: port.photos}}).exec(
                    function (err, success) {
                        if (err || !success) {
                            return res.json({result: false});
                        } else {
                            return res.json({result: true});
                        }
                    })
            }
        })
    }
};
exports.getFilterAvailable = function (req, res, cb) {
    async.waterfall([
        function (cb) {
            db.Port.find({available: true, remove: false}, function (err, ports) {
                if (err || !ports) {
                    cb(err, ports);
                }
                else {
                    if (ports.length) {
                        async.map(ports, function (port, callback0) {
                            db.Tour.find({port: port._doc._id}, function (err, tours) {
                                var aux = {
                                    port: port,
                                    tours: tours
                                }
                                callback0(err, aux)
                            })
                        }, function (err, result) {
                            if (err || !result) {
                                cb(err, ports);
                            }
                            else {
                                var listPorts = new Array();
                                for (var i = 0; i < result.length; i++) {
                                    if (result[i].tours.length) {
                                        listPorts.push(result[i].port);
                                    }
                                }
                                cb(err, listPorts);
                            }
                        })
                    }
                    else {
                        cb(err, ports);
                    }

                }
            })
        },
        function (ports, cb) {
            db.Country.find(function (err, countries) {
                if (err || !countries) {
                    cb(err, countries);
                }
                else {
                    var auxCountries = new Array();
                    for (var i = 0; i < countries.length; i++) {
                        var hascity = new Array();
                        for (var j = 0; j < countries[i]._doc.cities.length; j++) {
                            for (var k = 0; k < ports.length; k++) {
                                if (ports[k]._doc.city.es == countries[i]._doc.cities[j].es && ports[k]._doc.city.en == countries[i]._doc.cities[j].en) {
                                    var exist = existElemnt(hascity, ports[k]._doc.city);
                                    if (!exist) {
                                        hascity.push(ports[k]._doc.city);
                                    }

                                }
                            }
                        }
                        if (hascity.length) {
                            var aux = {
                                name: countries[i]._doc.name,
                                cities: hascity
                            }
                            auxCountries.push(aux);
                        }
                    }
                    cb(null, auxCountries);
                }
            })
        }
    ], function (err, results) {
        if (req.body.local) {
            if (err || !results) {
                cb(new Array());
            }
            else {
                cb(results);
            }
        }
        else {
            if (err || !results) {
                return res.json({res: new Array()})
            }
            else {
                return res.json({res: results})
            }
        }

    });
}
exports.listToursByPort = function (req, res) {
    async.waterfall([
        function (callback) {
            db.Tour.find({port: req.body.id, remove: false}, function (err, tours) {
                callback(err, tours);
            })
        },
        function (tours, callback) {
            async.map(tours, function (tour, callback0) {
                toursController.getTourGroups(tour, 0, function (data) {
                    if (data) {
                        async.waterfall([
                            function (cb) {
                                db.Pack.find({tour: tour, remove: false}, function (err, paks) {
                                    cb(err, paks);
                                })
                            },
                            function (paks, cb) {
                                async.map(paks, function (pack, callback1) {
                                    async.map(pack.services, function (service, callback2) {
                                        db.Service.findOne({_id: service}).select('name').exec(function (err, servs) {
                                            callback2(err, servs);
                                        })
                                    }, function (err, result) {
                                        if (err || !result) {
                                            callback1(err, result);
                                        }
                                        else {
                                            var aux = {
                                                id: pack._id,
                                                services: result
                                            };
                                            callback1(null, aux);
                                        }

                                    })
                                }, function (err, result) {
                                    cb(err, result);
                                })
                            }
                        ], function (err, result) {
                            if (err || !result) {
                                callback0(err, result);
                            } else {
                                var aux = {
                                    id: data._doc._id,
                                    name: data._doc.name,
                                    group: data._doc.group,
                                    packs: result
                                };
                                callback0(null, aux);
                            }
                        })
                    } else {
                        callback0(null, null);
                    }
                })
            }, function (err, result) {
                if (err || !result) {
                    callback(err, result);
                }
                else {
                    var temp = [];
                    for (var i = 0; i < result.length; i++) {
                        if (result[i] != null) {
                            temp.push(result[i]);
                        }
                    }
                    return res.json({res: temp, complete: false});
                }
            })
        }
    ], function (err, result) {
        if (err || !result) {
            return res.json({res: false})
        }
        else {
            return res.json({res: result});
        }
    })
};

function validateNumber(num) {
    return isNaN(num);
};
function validateForm(name, address, city, country) {
    return (name.es.length >= 1
    && name.en.length >= 1
    && address.es.length >= 1
    && address.en.length >= 1
    && city.es.length >= 1
    && city.en.length >= 1
    && country.es.length >= 1
    && country.en.length >= 1
    );
};
function getTourByPort(port, pos, cb) {

    db.Tour.find({port: port, remove: false, available: true}).exec(function (err, tours) {
        if (err || !tours) {
            if (cb) cb(new Array(), pos);
        }
        else {
            if (cb)cb(tours, pos);
        }
    })
}

exports.getTourByPortAll = function (port, pos, cb) {

    db.Tour.find({port: port, remove: false}).exec(function (err, tours) {
        if (err || !tours) {
            if (cb) cb(new Array(), pos);
        }
        else {
            if (cb)cb(tours, pos);
        }
    })
};

function existElemnt(array, obj) {
    if (array.length) {
        for (var i = 0; i < array.length; i++) {
            if (obj.en == array[i].en && obj.es == array[i].es) {
                return true;
            }
        }
        return false;
    }
    else {
        return false;
    }
}








