/**
 * Created by ernestomr87@gmail.com on 05/08/2014.
 */
var etag = require('etag');
var fresh = require('fresh');
var request = require('request');
var fs = require('fs-extra');

exports.get = function (req, res) {

    var download = function (uri, filename, callback) {

        request.head(uri, function (err, response, body) {

            console.log('content-type:', response.headers['content-type']);
            console.log('content-length:', response.headers['content-length']);

            var r = request(uri).pipe(fs.createWriteStream(filename));
            r.on('close', function () {
                callback(response.headers['content-type']);
            });
        });
    };

    db.Media.findOne({fieldName: req.params.name}).exec(function (err, success) {
        if (err || !success) {

            console.log("download: "+'http://ws-bojeo-express.azurewebsites.net/api/media/' + req.params.name)
            download('http://ws-bojeo-express.azurewebsites.net/api/media/' + req.params.name, req.params.name, function (contentType) {
                console.log("read full: " + req.params.name)
                var a = new db.Media({});
                fs.readFile(req.params.name, function (err, data) {
                    a.contentType = contentType;
                    a.fieldName = req.params.name;
                    a.data = data;
                    a.save(function (err, a) {
                        if (err) console.log(err);
                        fs.unlink(req.params.name, function (err) {
                            if (err)
                                console.log(err);
                            res.end(data, "binary");
                        });
                    });
                });
            })

        }
        else {
            res.setHeader('ETag', etag(success._doc.data));
            res.contentType(success._doc.contentType);

            if (fresh(req.headers, res._headers)) {
                res.statusCode = 304;
                res.end();
                return;
            }
            res.end(success._doc.data, "binary");
        }
    })

};
