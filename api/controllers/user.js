/**
 * Created by ernestomr87@gmail.com on 7/05/14.
 */
var bcrypt = require('bcrypt-nodejs');
var session = require('./session.js');
var utils = require('../utils');
var nodemailer = require("nodemailer");
var mailController = require('./email');
var resController = require('./reservations');
var shipController = require('./ship');
var timeline = require('../../api/controllers/timeLine');
var async = require('async');
var serviceController = require('./service');
var reservationController = require('./reservations');

exports.login = function (req, res, next) {

    db.User.findOne({email: req.body.email, active: true, remove: false}).exec(function (err, user) {
        if (err || !user)
            return res.json({error: 'Email inv&aacute;lido'});

        user.validPassword(req.body.password, function (err, isValid) {
            if (err || !isValid)
                return res.json({error: 'Contrase&ntilde;a inv&aacute;lida'});

            var us = {
                role: user.role,
                _id: user._id.toString(),
                username: user.userName,
                email: user.email
            };
            req.session.user = us;
            return res.json(us);
        })
    })
};

/*
 ERROR REQUEST
 0   datos incorrectos
 1   email registrado
 2   user no registrado
 */
exports.create = function (req, res) {
    var userName = req.body.userName,
        lastName = req.body.lastName,
        sex = req.body.sex,
        birthday = req.body.birthday,
        email = req.body.email,
        phoneNumber = req.body.phoneNumber,
        enterprise = req.body.enterprise,
        address = req.body.address,
        city = req.body.city,
        postalCode = req.body.postalCode,
        country = req.body.country,
        password = req.body.password,
        role = req.body.role,
        language = req.body.language;

    var date = new Date();
    date.setDate(date.getDate() + 1);
    var aux = (
    password.length >= 6 &&
    userName.length > 0 &&
    lastName.length > 0 &&
    address.length > 0 &&
    city.length > 0 &&
    country.length > 0
    );
    if (aux) {
        db.User.findOne({email: email}).exec(function (err, user) {
            if (err || user) {
                return res.json({res: false, error: 1});
            } else {
                var newUser = new db.User({
                    userName: userName,
                    lastName: lastName,
                    email: email,
                    sex: sex,
                    birthday: birthday,
                    phoneNumber: phoneNumber,
                    enterprise: enterprise,
                    address: address,
                    city: city,
                    postalCode: postalCode,
                    country: country,
                    subscription: false,
                    createDate: new Date(),
                    expireTime: date,
                    token: exports.generateUid(),
                    role: role,
                    totalAmount: 0,
                    password: bcrypt.hashSync(password, bcrypt.genSaltSync(8), null),
                    language: language,
                    slug: utils.getSlug(userName + ' ' + lastName)
                });
                newUser.save(function (err, user) {
                    if (err || !user) {
                        return res.json({res: false, error: 2});
                    }
                    else {
                        mailController.getEmail(function (data) {
                            if (data) {
                                if (data._doc.notificationRegister.active) {
                                    var smtpTransport = nodemailer.createTransport("SMTP", {
                                        host: data._doc.mailServer, // hostname
                                        secureConnection: true, // use SSL
                                        connectionTimeout : 300000,
                                        port: data._doc.mailSMTPPort, // port for secure SMTP
                                        auth: {
                                            user: data._doc.mailUser,
                                            pass: data._doc.mailPass
                                        }
                                    });

                                    var auxTmp = language == 'es' ? data._doc.notificationRegister.body.es : data._doc.notificationRegister.body.en;
                                    var singTmp = language == 'es' ? data._doc.sign.es : data._doc.sign.en;

                                    var template = ReplaceAll(auxTmp, '[email_user]', user._doc.userName);
                                    var template = ReplaceAll(template, '[bojeo_link]', global.bojeoServer + "/"+language+"/login/" + user._doc.token);
                                    var sign = ReplaceAll(singTmp, '[logo]', '<img src=\"' + global.bojeoServer + '/img/email/bojeo_Logo.png\" alt=\"Logo Bojeo\" width=\"109\" height=\"72\" /><br />');
                                    var template = template + sign;
                                    var mailOptions = {
                                        from: "Bojeo <" + data._doc.mailUser + ">", // sender address
                                        to: user._doc.email, // list of receivers
                                        subject: language == 'es' ? data._doc.notificationRegister.subject.es : data._doc.notificationRegister.subject.en, // Subject line
                                        html: template
                                    };
                                    sendMail(smtpTransport, mailOptions, function (mail) {
                                        return res.json({res: mail});
                                    });
                                }
                                else {
                                    return res.json({res: true});
                                }
                            }
                            else {
                                return res.json({res: true});
                            }
                        })
                    }
                })
            }
        });
    }
    else {
        return res.json({res: false, error: 0});
    }
};
/*
 ERROR REQUEST
 0   datos incorrectos
 1   email registrado
 2   user no registrado
 */
exports.createBack = function (req, res) {
    var userName = req.body.userName,
        lastName = req.body.lastName,
        sex = req.body.sex,
        birthday = req.body.birthday,
        email = req.body.email,
        phoneNumber = req.body.phoneNumber,
        enterprise = req.body.enterprise,
        address = req.body.address,
        city = req.body.city,
        postalCode = req.body.postalCode,
        country = req.body.country,
        password = req.body.password,
        role = req.body.role;

    var date = new Date();
    date.setDate(date.getDate() + 1);
    var aux = (
    password.length >= 6 &&
    userName.length > 0 &&
    lastName.length > 0 &&
    address.length > 0 &&
    city.length > 0 &&
    country.length > 0
    );
    if (aux) {
        db.User.findOne({email: email}).exec(function (err, user) {
            if (err || user) {
                return res.json({res: false, error: 1});
            } else {
                var newUser = new db.User({
                    userName: userName,
                    lastName: lastName,
                    email: email,
                    sex: sex,
                    birthday: birthday,
                    phoneNumber: phoneNumber,
                    enterprise: enterprise,
                    address: address,
                    city: city,
                    postalCode: postalCode,
                    country: country,
                    subscription: false,
                    createDate: new Date(),
                    expireTime: date,
                    token: exports.generateUid(),
                    role: role,
                    totalAmount: 0,
                    password: bcrypt.hashSync(password, bcrypt.genSaltSync(8), null),
                    slug: utils.getSlug(userName + ' ' + lastName)
                });
                newUser.save(function (err, user) {
                    if (err || !user) {
                        return res.json({res: false, error: 2});
                    }
                    else {
                        return res.json({res: true});
                    }
                })
            }
        });
    }
    else {
        return res.json({res: false, error: 0});
    }
};
exports.createSocialUser = function (profile, cb) {
    var email = "";
    if (profile.emails) {
        email = profile.emails[0].value;
    }
    var newUser = new db.User({
        socialId: profile.id,
        userName: profile.name.givenName || "",
        lastName: profile.name.familyName || "",
        email: email,
        subscription: false,
        createDate: new Date(),
        complete: false,
        available: true,
        slug: utils.getSlug(profile.name.givenName + ' ' + profile.name.familyName)
    });
    newUser.save(function (err, user) {
        if (err || !user) {
            return cb(err, null);
        }
        else {
            return cb(null, user);
        }
    })
};
exports.list = function (req, res) {
    var limit = req.body.limit;
    var skip = req.body.skip;
    var word = req.body.word;
    var id_user = req.body.id;
    if (word.length > 0) {
        async.parallel([
            function (cb) {
                db.User.count({remove: false}).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                word = new RegExp(word, 'i');
                db.User.find({
                    remove: false, $or: [
                        {'userName': word},
                        {'lastName': word},
                        {'email': word},
                        {'country': word}
                    ]
                }).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, data) {
                    if (cb) cb(err, data);
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            } else {
                var list = new Array();
                var cantidad = results[0];
                var users = results[1];
                if (users.length > 0) {
                    async.map(users, function (user, callback) {
                            getReservationClientsByClient(user._doc.email, function (err, resC) {

                                if (err || !resC) {
                                    var reservations = new Array();
                                }
                                else {
                                    var reservations = resC;
                                }
                                var aux = {
                                    _id: user._doc._id,
                                    userName: user._doc.userName,
                                    lastName: user._doc.lastName,
                                    sex: user._doc.sex,
                                    birthday: user._doc.birthday,
                                    email: user._doc.email,
                                    phoneNumber: user._doc.phoneNumber,
                                    enterprise: user._doc.enterprise,
                                    address: user._doc.address,
                                    city: user._doc.city,
                                    postalCode: user._doc.postalCode,
                                    country: user._doc.country,
                                    role: user._doc.role,
                                    subscription: user._doc.subscription,
                                    abonos: user._doc.abonos,
                                    totalAmount: user._doc.totalAmount,
                                    createDate: user._doc.createDate,
                                    available: user._doc.available,
                                    ship: false,
                                    service: false,
                                    reservations: reservations
                                };
                                if (user._doc.role == 'owner') {
                                    async.waterfall([
                                        function (cb) {
                                            shipController.getShipByOwner(user._doc._id, aux, function (hasShip) {
                                                cb(null, hasShip);
                                            })
                                        },
                                        function (hasShip, cb) {
                                            serviceController.getServiceByOwner(user._doc._id, hasShip, function (hasService) {
                                                cb(null, hasService);
                                            })
                                        }
                                    ], function (err, results) {
                                        callback(null, results);
                                    });
                                }
                                else if (user._doc.role == 'patron') {
                                    shipController.getShipByPatron(user._doc._id, aux, function (result) {
                                        callback(null, result);
                                    })
                                }
                                else {
                                    callback(null, aux);
                                }
                            })
                        }, function (err, result) {
                            return res.json({res: result, cont: cantidad});
                        }
                    )
                }
                else {
                    return res.json({res: list, cont: cantidad});
                }
            }
        })
    } else {
        if (id_user.length > 0) {
            async.parallel([
                function (cb) {
                    db.User.count({remove: false}).exec(function (err, count) {
                        if (cb) cb(err, count);
                    })
                },
                function (cb) {
                    db.User.find({
                        remove: false, $or: [
                            {_id: id_user},
                            {'abonos.id': id_user}
                        ]
                    }).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, data) {
                        if (cb) cb(err, data);
                    })
                }
            ], function (err, results) {
                if (err || !results) {
                    return res.json({res: false, cont: 0});
                } else {
                    var list = new Array();
                    var cantidad = results[0];
                    var users = results[1];
                    var usersTemp = new Array();
                    if (users.length > 0) {
                        if (users[0]._doc._id.toString() == id_user) {
                            usersTemp.push(users[0]);
                        } else {
                            for (var i = 0; i < users.length; i++) {
                                if (users[i]._doc.abonos.length > 0) {
                                    for (var j = 0; j < users[i]._doc.abonos.length; j++) {
                                        if (users[i]._doc.abonos[j].id == id_user && users[i]._doc.abonos[j].cant != 0) {
                                            usersTemp.push(users[i]);
                                        }
                                    }
                                }
                            }
                        }
                        async.map(usersTemp, function (user, callback) {
                            getReservationClientsByClient(user._doc.email, function (err, resC) {

                                if (err || !resC) {
                                    var reservations = new Array();
                                }
                                else {
                                    var reservations = resC;
                                }
                                var aux = {
                                    _id: user._doc._id,
                                    userName: user._doc.userName,
                                    lastName: user._doc.lastName,
                                    sex: user._doc.sex,
                                    birthday: user._doc.birthday,
                                    email: user._doc.email,
                                    phoneNumber: user._doc.phoneNumber,
                                    enterprise: user._doc.enterprise,
                                    address: user._doc.address,
                                    city: user._doc.city,
                                    postalCode: user._doc.postalCode,
                                    country: user._doc.country,
                                    role: user._doc.role,
                                    subscription: user._doc.subscription,
                                    abonos: user._doc.abonos,
                                    totalAmount: user._doc.totalAmount,
                                    createDate: user._doc.createDate,
                                    available: user._doc.available,
                                    ship: false,
                                    service: false,
                                    reservations: reservations
                                };
                                if (user._doc.role == 'owner') {

                                    async.waterfall([
                                        function (cb) {
                                            shipController.getShipByOwner(user._doc._id, aux, function (hasShip) {
                                                cb(null, hasShip);
                                            })
                                        },
                                        function (hasShip, cb) {
                                            serviceController.getServiceByOwner(user._doc._id, hasShip, function (hasService) {
                                                cb(null, hasService);
                                            })
                                        }
                                    ], function (err, results) {
                                        callback(null, results);
                                    });


                                }
                                else if (user._doc.role == 'patron') {
                                    shipController.getShipByPatron(user._doc._id, aux, function (result) {
                                        callback(null, result);
                                    })
                                }
                                else {
                                    callback(null, aux);
                                }
                            })
                        }, function (err, result) {
                            return res.json({res: result, cont: cantidad});
                        });
                    }
                    else {
                        return res.json({res: list, cont: cantidad});
                    }
                }
            });
        }
        else {
            async.parallel([
                function (cb) {
                    db.User.count({remove: false}).exec(function (err, count) {
                        if (cb) cb(err, count);
                    })
                },
                function (cb) {
                    db.User.find({remove: false}).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, data) {
                        if (cb) cb(err, data);
                    })
                }
            ], function (err, results) {
                if (err || !results) {
                    return res.json({res: false, cont: 0});
                } else {
                    var list = new Array();
                    var cantidad = results[0];
                    var users = results[1];
                    if (users.length > 0) {
                        async.map(users, function (user, callback) {
                                getReservationClientsByClient(user._doc.email, function (err, resC) {

                                    if (err || !resC) {
                                        var reservations = new Array();
                                    }
                                    else {
                                        var reservations = resC;
                                    }
                                    var aux = {
                                        _id: user._doc._id,
                                        userName: user._doc.userName,
                                        lastName: user._doc.lastName,
                                        sex: user._doc.sex,
                                        birthday: user._doc.birthday,
                                        email: user._doc.email,
                                        phoneNumber: user._doc.phoneNumber,
                                        enterprise: user._doc.enterprise,
                                        address: user._doc.address,
                                        city: user._doc.city,
                                        postalCode: user._doc.postalCode,
                                        country: user._doc.country,
                                        role: user._doc.role,
                                        subscription: user._doc.subscription,
                                        abonos: user._doc.abonos,
                                        totalAmount: user._doc.totalAmount,
                                        createDate: user._doc.createDate,
                                        available: user._doc.available,
                                        ship: false,
                                        service: false,
                                        reservations: reservations
                                    };
                                    if (user._doc.role == 'owner') {

                                        async.waterfall([
                                            function (cb) {
                                                shipController.getShipByOwner(user._doc._id, aux, function (hasShip) {
                                                    cb(null, hasShip);
                                                })
                                            },
                                            function (hasShip, cb) {
                                                serviceController.getServiceByOwner(user._doc._id, hasShip, function (hasService) {
                                                    cb(null, hasService);
                                                })
                                            }
                                        ], function (err, results) {
                                            callback(null, results);
                                        });


                                    }
                                    else if (user._doc.role == 'patron') {
                                        shipController.getShipByPatron(user._doc._id, aux, function (result) {
                                            callback(null, result);
                                        })
                                    }
                                    else {
                                        callback(null, aux);
                                    }
                                })
                            }, function (err, result) {
                                return res.json({res: result, cont: cantidad});
                            }
                        )

                    }
                    else {
                        return res.json({res: list, cont: cantidad});
                    }
                }
            });
        }
    }
};
exports.update = function (req, res) {
    var id = req.body.id,
        userName = req.body.userName,
        lastName = req.body.lastName,
        sex = req.body.sex,
        birthday = req.body.birthday,
        email = req.body.email,
        phoneNumber = req.body.phoneNumber,
        enterprise = req.body.enterprise,
        address = req.body.address,
        city = req.body.city,
        postalCode = req.body.postalCode,
        country = req.body.country,
        role = req.body.role;


    var aux = (
    userName.length > 0 &&
    lastName.length > 0 &&
    address.length > 0 &&
    city.length > 0 &&
    country.length > 0
    );

    if (aux) {
        db.User.update({_id: id}, {
            $set: {
                userName: userName,
                lastName: lastName,
                sex: sex,
                birthday: birthday,
                email: email,
                phoneNumber: phoneNumber,
                enterprise: enterprise,
                address: address,
                city: city,
                postalCode: postalCode,
                country: country,
                role: role,
                slug: utils.getSlug(userName + ' ' + lastName)
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true});
            }
        })
    }
    else {
        return res.json({res: false});
    }
};
exports.remove = function (req, res) {
    var user_id = req.body.id,
        action = req.body.action,
        role = null,
        change = false,
        userAux = null;

    if (action == 0) {
        async.waterfall([
            function (cb) {
                db.User.findOne({_id: user_id}, function (err, user) {
                    cb(err, user);
                })
            },
            function (user, cb) {
                userAux = user;
                role = user._doc.role;
                if (!user._doc.available) {
                    if (user._doc.role == 'admin') {
                        db.User.count({role: 'admin', _id: {$ne: user._doc._id}}, function (err, count) {
                            if (err) {
                                cb(null, false);
                            }
                            else {
                                change = true;
                                cb(null, count);
                            }
                        })
                    }
                    else if (user._doc.role == 'patron') {
                        db.Ship.findOne({
                            patron: user._doc._id,
                            remove: false
                        }, function (err, ship) {
                            if (err || !ship) {
                                change = true;
                                cb(null, true);
                            }
                            else {
                                var temp = {
                                    id: ship._doc._id,
                                    name: ship._doc.name
                                };
                                cb(null, temp);

                            }
                        })
                    }
                    else if (user._doc.role == 'owner') {
                        async.parallel([
                            function (callback) {
                                db.Ship.find({
                                    'owner.id': user._doc._id,
                                    remove: false
                                }, function (err, ships) {
                                    if (err || !ships) {
                                        callback(null, false);
                                    }
                                    else {
                                        var aux = new Array();
                                        for (var i = 0; i < ships.length; i++) {
                                            var temp = {
                                                id: ships[i]._doc._id,
                                                name: ships[i]._doc.name
                                            };
                                            aux.push(temp);
                                        }
                                        callback(null, aux);

                                    }
                                })
                            },
                            function (callback) {
                                db.Service.find({
                                    owner: user._doc._id,
                                    remove: false
                                }, function (err, services) {
                                    if (err || !services) {
                                        callback(null, false);
                                    }
                                    else {
                                        var aux = new Array();
                                        for (var i = 0; i < services.length; i++) {
                                            var temp = {
                                                id: services[i]._doc._id,
                                                name: services[i]._doc.name
                                            };
                                            aux.push(temp);
                                        }
                                        callback(null, aux);
                                    }
                                })
                            },
                            function (callback) {
                                db.Service.find({
                                    owner: user._doc._id,
                                    remove: false
                                }, function (err, services) {
                                    if (err || !services) {
                                        callback(null, false);
                                    }
                                    else {
                                        async.map(services, function (service, cbMap) {
                                            db.Pack.find({
                                                'services.id': service._doc._id.toString(),
                                                remove: false
                                            }, function (err, packs) {
                                                if (err || !packs) {
                                                    cbMap(err, packs);
                                                }
                                                else {
                                                    async.map(packs, function (pack, cbMM) {
                                                        pack.getName(function (name) {
                                                            cbMM(null, {id: pack._doc._id, name: name});
                                                        })
                                                    }, function (err, result) {
                                                        cbMap(null, result);
                                                    });
                                                }
                                            })
                                        }, function (err, result) {
                                            if (err || !result) {
                                                callback(err, result);
                                            }
                                            else {
                                                var aux = new Array();
                                                for (var i = 0; i < result.length; i++) {
                                                    for (var j = 0; j < result[i].length; j++) {
                                                        if (existElement(aux, result[i][j])) {
                                                            var temp = {
                                                                id: result[i][j].id,
                                                                name: result[i][j].name
                                                            };
                                                            aux.push(temp);
                                                        }
                                                    }
                                                }
                                                callback(null, aux);
                                            }

                                        });
                                    }
                                })
                            }
                        ], function (err, result) {
                            if (!result[0].length && !result[1].length && !result[2].length) {
                                change = true;
                                cb(null, result);

                            }
                            else {
                                cb(null, result);
                            }

                        });
                    } else {
                        change = true;
                        cb(null, true);
                    }
                }
                else {
                    cb(null, false);
                }


            }
        ], function (err, result) {
            if (err || !result) {
                return res.json({res: false});
            }
            else {
                if (role == 'admin') {
                    return res.json({res: result, complete: false, role: role});
                }
                else if (role == 'patron') {
                    return res.json({res: result, complete: false, role: role});
                }
                else if (role == 'owner') {
                    //ship services packs
                    return res.json({res: result, complete: false, role: role});
                } else {
                    return res.json({res: result, complete: false, role: role});
                }
            }
        });
    }
    else {
        async.waterfall([
            function (cb) {
                db.User.findOne({_id: user_id}, function (err, user) {
                    cb(err, user);
                })
            },
            function (user, cb) {
                userAux = user;
                role = user._doc.role;

                if (user._doc.role == 'patron') {
                    db.Ship.update({
                        patron: user._doc._id,
                        remove: false
                    }, {$set: {remove: true}}, function (err, ship) {
                        cb(err, ship);
                    })
                }
                else if (user._doc.role == 'owner') {
                    async.parallel([
                        function (callback) {
                            db.Ship.find({
                                'owner.id': user._doc._id,
                                remove: false
                            }, function (err, ships) {
                                if (err || !ships) {
                                    callback(err, ships);
                                }
                                else {
                                    async.map(ships, function (ship, cbM) {
                                        db.Ship.update({
                                            _id: ship._doc._id
                                        }, {$set: {remove: true}}, function (err, ships) {
                                            cbM(err, ships);
                                        })
                                    }, function (err, result) {
                                        callback(err, result);

                                    });
                                }
                            })
                        },
                        function (callback) {
                            db.Service.update({
                                owner: user._doc._id,
                                remove: false
                            }, {$set: {remove: true}}, function (err, services) {
                                callback(err, services);
                            })
                        },
                        function (callback) {
                            db.Service.find({
                                owner: user._doc._id
                            }, function (err, services) {
                                if (err || !services) {
                                    callback(err, result);
                                }
                                else {
                                    async.map(services, function (service, cbMap) {
                                        db.Pack.find({
                                            'services.id': service._doc._id.toString()
                                        }, function (err, packs) {
                                            if (err || !packs) {
                                                cbMap(err, packs);
                                            }
                                            else {
                                                async.map(packs, function (obj, cbMMap) {
                                                    db.Pack.update({
                                                        _id: obj._doc._id.toString()
                                                    }, {$set: {remove: true}}, function (err, packs) {
                                                        cbMMap(err, packs);
                                                    })
                                                }, function (err, result) {
                                                    cbMap(err, result);
                                                });
                                            }
                                        })
                                    }, function (err, result) {
                                        callback(err, result);
                                    });
                                }
                            })
                        }
                    ], function (err, result) {
                        cb(err, result);
                    });
                }
                else if (user._doc.role == 'user') {
                    cb(null, true);
                }

            }
        ], function (err) {
            if (err) {
                return res.json({res: false});
            }
            else {
                db.User.update({_id: userAux._doc._id}, {$set: {remove: true}}, function (err, success) {
                    if (err || !success) {
                        return res.json({res: false});
                    }
                    else {
                        return res.json({res: true, complete: true, role: role});
                    }
                })
            }
        });
    }


    //var id = req.params.id;
    //db.User.remove({_id: id}, function (err, success) {
    //    if (err || !success) {
    //        return res.json({res: false});
    //    } else {
    //        return res.json({res: true});
    //    }
    //});
};
exports.status = function (req, res) {
    var user_id = req.body.id,
        action = req.body.action,
        role = null,
        change = false,
        userAux = null;

    if (action == 0) {
        async.waterfall([
            function (cb) {
                db.User.findOne({_id: user_id}, function (err, user) {
                    cb(err, user);
                })
            },
            function (user, cb) {
                userAux = user;
                if (user._doc.available) {
                    role = user._doc.role;
                    if (user._doc.available) {
                        if (user._doc.role == 'admin') {
                            db.User.count({role: 'admin', _id: {$ne: user._doc._id}}, function (err, count) {
                                if (err) {
                                    cb(null, false);
                                }
                                else {
                                    change = true;
                                    cb(null, count);
                                }
                            })
                        }
                        else if (user._doc.role == 'patron') {
                            db.Ship.findOne({
                                patron: user._doc._id,
                                available: true,
                                remove: false
                            }, function (err, ship) {
                                if (err || !ship) {
                                    change = true;
                                    cb(null, true);
                                }
                                else {
                                    var temp = {
                                        id: ship._doc._id,
                                        name: ship._doc.name
                                    };
                                    cb(null, temp);

                                }
                            })
                        }
                        else if (user._doc.role == 'owner') {
                            async.parallel([
                                function (callback) {
                                    db.Ship.find({
                                        'owner.id': user._doc._id,
                                        available: true,
                                        remove: false
                                    }, function (err, ships) {
                                        if (err || !ships) {
                                            callback(null, false);
                                        }
                                        else {
                                            var aux = new Array();
                                            for (var i = 0; i < ships.length; i++) {
                                                var temp = {
                                                    id: ships[i]._doc._id,
                                                    name: ships[i]._doc.name
                                                };
                                                aux.push(temp);
                                            }
                                            callback(null, aux);

                                        }
                                    })
                                },
                                function (callback) {
                                    db.Service.find({
                                        owner: user._doc._id,
                                        remove: false,
                                        status: true
                                    }, function (err, services) {
                                        if (err || !services) {
                                            callback(null, false);
                                        }
                                        else {
                                            var aux = new Array();
                                            for (var i = 0; i < services.length; i++) {
                                                var temp = {
                                                    id: services[i]._doc._id,
                                                    name: services[i]._doc.name
                                                };
                                                aux.push(temp);
                                            }
                                            callback(null, aux);
                                        }
                                    })
                                },
                                function (callback) {
                                    db.Service.find({
                                        owner: user._doc._id,
                                        remove: false,
                                        status: true
                                    }, function (err, services) {
                                        if (err || !services) {
                                            callback(null, false);
                                        }
                                        else {
                                            async.map(services, function (service, cbMap) {
                                                db.Pack.find({
                                                    'services.id': service._doc._id.toString(),
                                                    remove: false,
                                                    status: true
                                                }, function (err, packs) {
                                                    if (err || !packs) {
                                                        cbMap(err, packs);
                                                    }
                                                    else {
                                                        async.map(packs, function (pack, cbMM) {
                                                            pack.getName(function (name) {
                                                                cbMM(null, {id: pack._doc._id, name: name});
                                                            })
                                                        }, function (err, result) {
                                                            cbMap(null, result);
                                                        });
                                                    }
                                                })
                                            }, function (err, result) {
                                                if (err || !result) {
                                                    callback(err, result);
                                                }
                                                else {
                                                    var aux = new Array();
                                                    for (var i = 0; i < result.length; i++) {
                                                        for (var j = 0; j < result[i].length; j++) {
                                                            if (existElement(aux, result[i][j])) {
                                                                var temp = {
                                                                    id: result[i][j].id,
                                                                    name: result[i][j].name
                                                                };
                                                                aux.push(temp);
                                                            }
                                                        }
                                                    }
                                                    callback(null, aux);
                                                }

                                            });
                                        }
                                    })
                                }
                            ], function (err, result) {
                                if (!result[0].length && !result[1].length && !result[2].length) {
                                    change = true;
                                    cb(null, result);

                                }
                                else {
                                    cb(null, result);
                                }
                                cb(err, result);
                            });
                        } else {
                            change = true;
                            cb(null, true);
                        }
                    }
                }
                else {
                    change = true;
                    cb(null, true);

                }

            }
        ], function (err, result) {
            if (err || !result) {
                if (role == 'admin')
                    return res.json({res: false, role: role});
                return res.json({res: false});

            }
            else {
                if (change) {
                    db.User.update({
                        _id: userAux._doc._id,
                        remove: false
                    }, {$set: {available: !userAux._doc.available}}, function (err, success) {
                        if (err || !success) {
                            return res.json({res: false, complete: false, role: role});
                        }
                        else {
                            return res.json({res: true, complete: true, role: role});
                        }

                    })
                }
                else {
                    if (role == 'admin') {
                        return res.json({res: result, complete: false, role: role});
                    }
                    else if (role == 'patron') {
                        return res.json({res: result, complete: false, role: role});
                    }
                    else if (role == 'owner') {
                        //ship services packs
                        return res.json({res: result, complete: false, role: role});
                    }
                }


            }
        });
    }
    else {
        async.waterfall([
            function (cb) {
                db.User.findOne({_id: user_id}, function (err, user) {
                    cb(err, user);
                })
            },
            function (user, cb) {
                userAux = user;
                role = user._doc.role;
                if (user._doc.available) {
                    if (user._doc.role == 'patron') {
                        db.Ship.update({
                            patron: user._doc._id,
                            available: true,
                            remove: false
                        }, {$set: {available: false}}, function (err, ship) {
                            cb(err, ship);
                        })
                    }
                    else if (user._doc.role == 'owner') {
                        async.parallel([
                            function (callback) {
                                db.Ship.find({
                                    'owner.id': user._doc._id,
                                    available: true,
                                    remove: false
                                }, function (err, ships) {
                                    if (err || !ships) {
                                        callback(err, ships);
                                    }
                                    else {
                                        async.map(ships, function (ship, cbM) {
                                            db.Ship.update({
                                                _id: ship._doc._id
                                            }, {$set: {available: false}}, function (err, ships) {
                                                cbM(err, ships);
                                            })
                                        }, function (err, result) {
                                            callback(err, result);

                                        });

                                    }

                                })


                            },
                            function (callback) {
                                db.Service.update({
                                    owner: user._doc._id,
                                    remove: false,
                                    status: true
                                }, {$set: {status: false}}, function (err, services) {
                                    callback(err, services);
                                })
                            },
                            function (callback) {
                                db.Service.find({
                                    owner: user._doc._id,
                                    remove: false
                                }, function (err, services) {
                                    if (err || !services) {
                                        callback(err, result);
                                    }
                                    else {


                                        async.map(services, function (service, cbMap) {
                                            db.Pack.find({
                                                'services.id': service._doc._id.toString(),
                                                remove: false
                                            }, function (err, packs) {
                                                if (err || !packs) {
                                                    cbMap(err, packs);
                                                }
                                                else {
                                                    async.map(packs, function (obj, cbMMap) {
                                                        db.Pack.update({
                                                            _id: obj._doc._id.toString()
                                                        }, {$set: {status: false}}, function (err, packs) {
                                                            cbMMap(err, packs);
                                                        })
                                                    }, function (err, result) {
                                                        cbMap(err, result);
                                                    });
                                                }
                                            })


                                        }, function (err, result) {
                                            callback(err, result);
                                        });
                                    }
                                })
                            }
                        ], function (err, result) {
                            cb(err, result);
                        });
                    }
                }
            }
        ], function (err, result) {
            if (err || !result) {
                return res.json({res: false});
            }
            else {
                db.User.update({_id: userAux._doc._id}, {$set: {available: false}}, function (err, success) {
                    if (err || !success) {
                        return res.json({res: false});
                    }
                    else {
                        return res.json({res: true, complete: true, role: role});
                    }
                })


            }
        });
    }


};
exports.profile = function (req, res) {
    async.waterfall([
        function (callback) {
            db.User.findOne({_id: req.user._id, remove: false}).exec(function (err, user) {
                callback(err, user);
            });
        },
        function (user, callback) {
            async.parallel([
                function (cb) {
                    db.ReservationClient.find({user: req.user._id}).populate('reservation').exec(function (err, resCs) {
                        if (err || !resCs) {
                            cb(err, resCs);
                        }
                        else {
                            async.map(resCs, function (resC, callbackM) {
                                    async.parallel([
                                        function (callbackP) {
                                            db.Tour.findOne({_id: resC._doc.reservation._doc.tour}).populate('port').populate('group').exec(function (err, tour) {
                                                if (err || !tour) {
                                                    callbackP(err, tour);
                                                }
                                                else {
                                                    var aux = {
                                                        _id: tour._doc._id,
                                                        name: tour._doc.name,
                                                        group: {
                                                            _id: tour._doc.group._doc._id,
                                                            payform: tour._doc.group._doc.payform,
                                                            color: tour._doc.group._doc.color,
                                                            name: tour._doc.group._doc.name
                                                        },
                                                        port: {
                                                            _id: tour._doc.port._doc._id,
                                                            name: tour._doc.port._doc.name
                                                        }
                                                    }
                                                    callbackP(null, aux);
                                                }
                                            });
                                        },
                                        function (callbackP) {
                                            db.Ship.findOne({_id: resC._doc.reservation._doc.ship}).exec(function (err, ship) {
                                                if (err || !ship) {
                                                    callbackP(err, ship);
                                                }
                                                else {
                                                    var aux = {
                                                        _id: ship._doc._id,
                                                        name: ship._doc.name
                                                    };
                                                    callbackP(null, aux);
                                                }
                                            });
                                        },
                                        function (callbackP) {
                                            var today = new Date();
                                            today = new Date(today.getFullYear(), today.getMonth(), today.getDate());
                                            var state = 'booked';
                                            if (resC._doc.cancel) {
                                                state = 'canceled';
                                            }
                                            if (!resC._doc.cancel && today > resC._doc.reservation._doc.start) {
                                                state = 'done';
                                            }
                                            callbackP(null, state);

                                        }
                                    ], function (err, results) {
                                        if (err || !results) {
                                            callbackM(err, results);
                                        }
                                        else {
                                            resC._doc.reservation._doc.tour = results[0];
                                            resC._doc.reservation._doc.ship = results[1];
                                            resC._doc.reservation._doc.state = results[2];
                                            callbackM(null, resC);
                                        }
                                    });
                                }, function (err, result) {
                                    cb(err, result);
                                }
                            )
                        }
                    });
                },
                function (cb) {
                    async.map(user._doc.bonds, function (bond, callback0) {
                        db.Tour.findOne({'data.bond.bond': bond.id}).exec(function (err, tour) {
                            var aux = {
                                id: bond.id,
                                cant: bond.value,
                                name: tour._doc.name
                            };
                            callback0(err, aux);
                        })
                    }, function (err, result) {
                        cb(err, user, result);
                    })
                }
            ], function (err, results) {
                callback(err, results);
            });
        }
        /*function (resAndBonds, callback) {
         var user = resAndBonds[1][0];
         if (user._doc.role == "patron") {

         }
         }*/
    ], function (err, result) {
        if (err || !result) {
            return res.json({res: false});
        }
        else {
            var date = new Date();
            var dt = {
                y: date.getFullYear(),
                m: date.getMonth(),
                d: date.getDate()
            };
            var user = result[1][0],
                bonds = result[1][1],
                reservations = result[0];
            getActivitiesByUser(user, function (activities) {
                var response = {
                    userName: user._doc.userName,
                    lastName: user._doc.lastName,
                    sex: user._doc.sex,
                    birthday: user._doc.birthday,
                    email: user._doc.email,
                    phoneNumber: user._doc.phoneNumber,
                    fax: user._doc.fax,
                    enterprise: user._doc.enterprise,
                    address: user._doc.address,
                    city: user._doc.city,
                    postalCode: user._doc.postalCode,
                    country: user._doc.country,
                    role: user._doc.role,
                    subscription: user._doc.subscription,
                    abonos: bonds,
                    totalAmount: user._doc.totalAmount,
                    createDate: user._doc.createDate,
                    reservations: reservations,
                    today: dt,
                    activities: activities,
                    socialId: user._doc.socialId || null
                };
                return res.json({res: response, date: date, reserved: true});

            });
        }
    });
};
exports.profileChangePass = function (req, res) {
    /**
     * Error
     * 1:   invalid text pass
     * 2:   pass incorrect
     * */
    if (req.body.oldPass.length > 2 && req.body.newPass.length > 2) {
        var password = bcrypt.hashSync(req.body.newPass, bcrypt.genSaltSync(8), null);
        db.User.findOne({_id: req.user._id, remove: false}).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false, error: 2})
            }
            else {
                success.validPassword(req.body.oldPass, function (err, isValid) {
                    if (err || !isValid)
                        return res.json({res: false, error: 2});
                    else
                        db.User.update({
                            _id: req.user._id,
                            remove: false
                        }, {$set: {password: password}}).exec(function (err, succ) {
                            if (err || !succ) {
                                return res.json({res: false, error: 2});
                            }
                            else {
                                return res.json({res: true});
                            }
                        })
                })
            }
        })
    }
    else {
        return res.json({res: false, error: 1});
    }
};
exports.profileUpdate = function (req, res) {

    var userName = req.body.userName,
        lastName = req.body.lastName,
        sex = req.body.sex,
        birthday = req.body.birthday,
        email = req.body.email,
        phoneNumber = req.body.phoneNumber,
        enterprise = req.body.enterprise,
        address = req.body.address,
        city = req.body.city,
        postalCode = req.body.postalCode,
        country = req.body.country,
        subscription = req.body.subscription;


    var aux = (
    userName.length > 0 &&
    lastName.length > 0 &&
    emailValidator(email) &&
    address.length > 0 &&
    city.length > 0 &&
    country.length > 0);


    if (aux) {
        //Validar campos
        db.User.update({_id: req.user._id, remove: false}, {
            $set: {
                userName: userName,
                lastName: lastName,
                sex: sex,
                birthday: birthday,
                email: email,
                phoneNumber: phoneNumber,
                enterprise: enterprise,
                address: address,
                city: city,
                postalCode: postalCode,
                country: country,
                complete: true,
                subscription: subscription,
                slug: utils.getSlug(userName + ' ' + lastName)
            }
        }).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true});
            }
        })
    }
    else {
        return res.json({res: false});
    }


};
exports.changePassByToken = function (req, res) {
    var token = req.body.token;
    var pass = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);

    db.User.update({token: token}, {$set: {password: pass}},
        function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true});
            }
        });
};
exports.changePassByAdmin = function (req, res) {
    var id = req.body.id,
        password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
    db.User.update({_id: id}, {$set: {password: password}}).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false});
        }
        else {
            return res.json({res: true});
        }
    })


};
exports.getOwnerAndPatron = function (owner, patron, cb) {
    db.User.find({
        remove: false,
        $or: [
            {_id: owner},
            {_id: patron}
        ]
    }).exec(function (err, users) {
        if (err || !users) {
            if (cb) cb(false);
        }
        else {
            if (users.length == 2) {
                var aux = {
                    owner: (users[0]._doc._id.toString() == owner.toString() ? users[0] : users[1]),
                    patron: (users[1]._doc._id.toString() == patron.toString() ? users[1] : users[0])
                };
            }
            else {
                var aux = {
                    owner: 'vacio',
                    patron: 'vacio'
                };
            }
            if (cb) cb(aux);
        }
    })
};
exports.getOwnerAndPatronAvailable = function (owner, patron, cb) {
    db.User.find({
        remove: false,
        available: false,
        $or: [
            {_id: owner},
            {_id: patron}
        ]
    }).exec(function (err, users) {
        if (err || !users) {
            if (cb) cb(false);
        }
        else {
            if (users.length == 2) {
                var aux = {
                    owner: (users[0]._doc._id.toString() == owner.toString() ? users[0] : users[1]),
                    patron: (users[1]._doc._id.toString() == patron.toString() ? users[1] : users[0])
                };
            }
            else {
                if (users.length == 1) {
                    var aux = {
                        owner: (users[0]._doc._id.toString() == owner.toString() ? users[0] : false),
                        patron: (users[0]._doc._id.toString() == patron.toString() ? users[0] : false)
                    };
                } else {
                    var aux = {
                        owner: false,
                        patron: false
                    };
                }
            }
            if (cb) cb(aux);
        }
    })
};
exports.getActivitiesByOwner = function (req, res) {
    if (req.user.role == 'owner') {
        var id = req.user._id,
            year = req.body.year;
        var start = new Date(year, 0, 1);
        var end = new Date(year + 1, 0, 1);

        async.parallel([
            function (cbp) {
                async.waterfall([
                    function (cbw) {
                        db.Ship.find({'owner.id': id, remove: false}, function (err, ships) {
                            cbw(err, ships);
                        })
                    },
                    function (ships, cbw) {
                        async.map(ships, function (ship, cbm) {
                            db.ReservationTour.find({
                                date: {$gte: start, $lt: end},
                                ship: ship,
                                remove: false
                            }).exec(function (err, resTs) {
                                if (err || !resTs) {
                                    cbw(1, null);
                                }
                                else {
                                    var sp = 0,
                                        others = 0,
                                        total = 0,
                                        monthArrayTour = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                        monthArrayTotal = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

                                    for (var i = 0; i < resTs.length; i++) {
                                        var mont = resTs[i]._doc.date.getMonth();
                                        if (resTs[i]._doc.type == 'agree') {
                                            sp++;
                                        }
                                        else {
                                            others++;
                                            var tmp = (resTs[i]._doc.total * resTs[i]._doc.comitions) / 100;
                                            total = total + tmp;
                                            monthArrayTotal[mont] = monthArrayTotal[mont] + tmp;
                                        }
                                        monthArrayTour[mont]++;
                                    }
                                    var aux = {
                                        id: ship._doc._id,
                                        name: ship._doc.name,
                                        sp: sp,
                                        others: others,
                                        total: total,
                                        monthArray: monthArrayTour,
                                        monthArrayTotal: monthArrayTotal
                                    }
                                    cbm(null, aux);
                                }
                            })
                        }, function (err, result) {
                            if (err || !result) {
                                cbw(null, new Array());
                            }
                            else {
                                cbw(null, result);
                            }
                        })
                    }
                ], function (err, results) {
                    if (err || !results) {
                        cbp(null, new Array());
                    }
                    else {
                        cbp(null, results);
                    }
                });
            },
            function (cbp) {
                async.waterfall([
                    function (cbw) {
                        db.Service.find({owner: id, remove: false}, function (err, services) {
                            cbw(err, services);
                        })
                    },
                    function (services, cbw) {
                        async.map(services, function (service, cbm) {
                            db.Pack.find({
                                'services.id': service._doc._id.toString(),
                                remove: false
                            }, function (err, packs) {
                                if (err || !packs) {
                                    cbm(err, packs);
                                }
                                else {
                                    async.map(packs, function (pack, cbmm) {
                                        db.ReservationClient.find({
                                            pack: pack._doc._id,
                                            state: 1,
                                            date: {$gte: start, $lt: end},
                                            'services.id': service._doc._id.toString()
                                        }).exec(function (err, resCs) {
                                            if (err || !resCs) {
                                                cbmm(null, new Array());
                                            }
                                            else {
                                                if (resCs.length) {
                                                    var reservationsC = removeReservationRepeat(resCs);
                                                    cbmm(null, reservationsC);
                                                } else {
                                                    cbmm(null, new Array());
                                                }
                                            }
                                        })
                                    }, function (err, result) {
                                        var tmp = new Array();
                                        for (var i = 0; i < result.length; i++) {
                                            if (result[i].length) {
                                                tmp.push(result[i][0]);
                                            }
                                        }
                                        if (tmp.length) {
                                            var cont = 0;
                                            var total = 0;
                                            var monthArray = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                                monthArrayTotal = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                                            for (var i = 0; i < tmp.length; i++) {
                                                var mont = tmp[i]._doc.date.getMonth();
                                                for (var j = 0; j < tmp[i].services.length; j++) {
                                                    if (tmp[i].services[j].id.toString() == service._doc._id.toString()) {
                                                        cont++;
                                                        total = total + (((tmp[i].services[j].comition * tmp[i].services[j].price) / 100) * tmp[i].cantPerson * tmp[i].days);
                                                        monthArrayTotal[mont] = monthArrayTotal[mont] + (((tmp[i].services[j].comition * tmp[i].services[j].price) / 100) * tmp[i].cantPerson * tmp[i].days);
                                                    }
                                                }
                                                monthArray[mont]++;
                                            }
                                            var aux = {
                                                id: service._doc._id,
                                                name: service._doc.name,
                                                cant: cont,
                                                total: total,
                                                monthArray: monthArray,
                                                monthArrayTotal: monthArrayTotal
                                            };
                                            cbm(null, aux);
                                        }
                                        else {
                                            cbm(null, new Array());
                                        }
                                    });
                                }
                            })
                        }, function (err, result) {
                            var tmp = new Array();
                            for (var i = 0; i < result.length; i++) {
                                if (result[i].length != 0) {
                                    tmp.push(result[i]);
                                }
                            }
                            cbw(err, tmp);
                        })
                    }
                ], function (err, results) {
                    cbp(err, results);
                });
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: new Array()});
            }
            else {
                var aux = {
                    ships: results[0],
                    services: results[1]
                }
                return res.json({res: aux});
            }
        });

    }
    else {
        return res.json({res: false});
    }
};
exports.changeLanguage = function (req, res) {
    if (req.body.language) {
        db.User.update({_id: req.user._id}, {$set: {language: req.body.language}}).exec(function (err, success) {
            if (err || !success) {
                return res.json({res: false});
            }
            else {
                return res.json({res: true});
            }
        })
    }
    else {
        return res.json({res: false});
    }
}
function emailValidator(email) {
    if (!(/\w{1,}[@][\w\-]{1,}([.]([\w\-]{1,})){1,3}$/.test(email))) {
        return false;
    }
    else {
        return true;
    }
};
exports.getUser = function (email, cb) {
    db.User.findOne({email: email, remove: false}).exec(function (err, success) {
        if (err || !success) {
            if (cb) cb(false);
        }
        else {
            if (cb) cb(success);
        }
    })
};
exports.getUserForId = function (id, pos, cb) {
    db.User.findOne({_id: id, remove: false}).exec(function (err, data) {
        if (err || !data) {
            if (cb) cb(false, pos);
        }
        else {
            if (cb) cb(data, pos);
        }
    })
};
exports.listUsers = function (req, res) {
    var limit = parseInt(req.query.limit);
    var skip = parseInt(req.query.skip);
    var word = req.query.word;

    var newText = new RegExp(word, 'i');

    db.User.count({remove: false}).exec(function (err, success) {
        if (err || !success) {
            return res.json({response: false, error: 0});
        }
        else {
            db.User.find({remove: false}).where('email').regex(newText).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, users) {
                if (err || !users) {
                    return res.json({response: false, error: 0});
                }
                else {
                    var response = [];
                    var count = success;
                    for (var i = 0; i < users.length; i++) {
                        var aux = {
                            _id: users[i]._doc._id,
                            userName: users[i]._doc.userName,
                            lastName: users[i]._doc.lastName,
                            email: users[i]._doc.email,
                            role: users[i]._doc.role,
                            country: users[i]._doc.country,
                            profession: users[i]._doc.profession,
                            phoneNumber: users[i]._doc.phoneNumber,
                            avatar: users[i]._doc.avatar,
                            status: users[i]._doc.status,
                            CreateDate: new Date(users[i]._doc.CreateDate)
                        }
                        response.push(aux);
                    }
                    return res.json({response: response, count: count});
                }
            })
        }
    })
};
exports.listUsersOwners = function (req, res) {
    var limit = req.query.limit;
    var skip = req.query.skip;
    var word = req.query.word;

    if (word.length > 0) {
        db.User.count({'role': 'owner', 'available': 'true', remove: false}).exec(function (err, success) {
            if (err || !success) {
                return res.json({response: false, error: 0});
            }
            else {
                var newText = new RegExp(word, 'i');
                db.User.find({
                        'role': 'owner',
                        'available': 'true',
                        remove: false,
                        $or: [
                            {userName: newText},
                            {lastName: newText},
                            {email: newText}
                        ]
                    }, '_id userName lastName email phoneNumber'
                ).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, users) {
                        if (err || !users) {
                            return res.json({response: false, error: 0});
                        }
                        else {
                            var count = success;
                            return res.json({response: users, count: count});
                        }
                    })
            }
        })
    }
    else {
        db.User.count({'role': 'owner', 'available': 'true', 'remove': false}).exec(function (err, success) {
            if (err || !success) {
                return res.json({response: false, error: 0});
            }
            else {
                db.User.find({
                        'role': 'owner',
                        'remove': false,
                        'available': 'true'
                    }, '_id userName lastName email phoneNumber'
                ).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, users) {
                        if (err || !users) {
                            return res.json({response: false, error: 0});
                        }
                        else {
                            var count = success;
                            return res.json({response: users, count: count});
                        }
                    })
            }
        })
    }
};
exports.listUsersPatrons = function (req, res) {
    var limit = req.query.limit;
    var skip = req.query.skip;
    var word = req.query.word;
    if (word.length) {
        db.Ship.find({remove: false}).distinct("patron").exec(function (err, patrones) {
            if (err || !patrones) {
                return res.json({response: false, error: 0});
            } else {
                db.User.count({'role': 'patron', 'available': 'true', 'remove': false}).exec(function (err, success) {
                    if (err || !success) {
                        return res.json({response: false, error: 0});
                    } else {
                        var newText = new RegExp(word, 'i');
                        db.User.find({
                            'role': 'patron',
                            'available': 'true',
                            'remove': false,
                            $or: [
                                {userName: newText},
                                {lastName: newText},
                                {email: newText}
                            ]
                        }, '_id userName lastName email phoneNumber').sort({_id: 1}).limit(limit).skip(skip).exec(function (err, users) {
                            if (err || !users) {
                                return res.json({response: false, error: 0});
                            }
                            else {
                                for (var i = 0; i < patrones.length; i++) {
                                    for (var j = 0; j < users.length; j++) {
                                        if (patrones[i].equals(users[j]._id)) {
                                            users.splice(j, 1);
                                            break;
                                        }
                                    }
                                }
                                var count = success - patrones.length;
                                return res.json({response: users, count: count});
                            }
                        })
                    }
                })
            }
        })
    }
    else {
        db.Ship.find({remove: false}).distinct("patron").exec(function (err, patrones) {
            if (err || !patrones) {
                return res.json({response: false, error: 0});
            } else {
                db.User.count({'role': 'patron', 'available': 'true', 'remove': false}).exec(function (err, success) {
                    if (err || !success) {
                        return res.json({response: false, error: 0});
                    } else {
                        db.User.find({
                            'role': 'patron',
                            'remove': false,
                            'available': 'true'
                            //}, '_id userName lastName email phoneNumber').sort({_id: 1}).limit(limit).skip(skip).exec(function (err, users) {
                        }, '_id userName lastName email phoneNumber').sort({_id: 1}).exec(function (err, users) {
                            if (err || !users) {
                                return res.json({response: false, error: 0});
                            }
                            else {
                                for (var i = 0; i < patrones.length; i++) {
                                    for (var j = 0; j < users.length; j++) {
                                        if (patrones[i].equals(users[j]._id)) {
                                            users.splice(j, 1);
                                            break;
                                        }
                                    }
                                }
                                var count = success - patrones.length;
                                return res.json({response: users, count: count});
                            }
                        })
                    }
                })
            }
        })
    }
};
exports.verifyPassword = function (req, res, next) {
    db.User.findOne({email: req.body.email, active: true, remove: false}).exec(function (err, user) {
        if (err || !user)
            return res.json({error: 'Invalid email'});

        user.validPassword(req.body.password, function (err, isValid) {
            if (err || !isValid)
                return res.json(false);
            else
                return res.json(true);
        })
    })
};
exports.activate = function (req, res) {
    var token = req.params.token;
    db.User.findOneAndUpdate({token: token, remove: false}, {
            $set: {
                available: true,
                token: null,
                createDate: new Date()
            }
        },
        function (err, user) {
            if (err || !user) {
                return res.json({res: false});
            }
            else {
                var type = 0,
                    info = {
                        id: user._doc._id,
                        userName: user._doc.userName,
                        lastName: user._doc.lastName,
                        email: user._doc.email
                    };
                timeline.create(type, info, function (err, success) {
                    if (err) {
                        var log = new db.ErrorLogs({
                            error: err,
                            date: new Date()
                        });
                        log.save();
                    }
                });
                return res.json({res: true});
            }
        });
};
exports.generateUid = function () {

    // http://www.ietf.org/rfc/rfc4122.txt
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;

};
exports.forgivePass = function (req, res) {

    var email = req.body.email;
    var token = exports.generateUid();

    db.User.update({email: email}, {$set: {token: token}}).exec(function (err, user) {
            if (err || !user) {
                return res.json({res: false});
            }
            else {
                mailController.getEmail(function (data) {
                    if (data) {
                        var smtpTransport = nodemailer.createTransport("SMTP", {
                            host: data._doc.mailServer, // hostname
                            secureConnection: true, // use SSL
                            connectionTimeout : 300000,
                            port: data._doc.mailSMTPPort, // port for secure SMTP
                            auth: {
                                user: data._doc.mailUser,
                                pass: data._doc.mailPass
                            }
                        });
                        var mailOptions = {
                            from: "Bojeo <" + data._doc.mailUser + ">", // sender address
                            to: email, // list of receivers
                            subject: 'Cambio de contraseña', // Subject line
                            html: templateForgivePass(email, token)
                        };
                        utils.sendMail(res, smtpTransport, mailOptions);

                    }
                    else {
                        return res.json({res: true});
                    }
                });
            }
        }
    )
};
exports.generatePass = function (len) {
    var pwd = [], cc = String.fromCharCode, R = Math.random, rnd, i;
    pwd.push(cc(48 + (0 | R() * 10))); // push a number
    pwd.push(cc(65 + (0 | R() * 26))); // push an upper case letter

    for (i = 2; i < len; i++) {
        rnd = 0 | R() * 62; // generate upper OR lower OR number
        pwd.push(cc(48 + rnd + (rnd > 9 ? 7 : 0) + (rnd > 35 ? 6 : 0)));
    }

    // shuffle letters in password
    return pwd.sort(function () {
        return R() - .5;
    }).join('');
};
exports.getUserByToken = function (req, res) {
    var token = req.params.token;
    db.User.findOne({token: token, remove: false}).exec(function (err, success) {
        if (success)
            return res.json(true);
        else
            return res.json(false);

    })
};
exports.updateSuscription = function (req, res) {
    db.User.update({_id: req.body.id}, {$set: {subscription: req.body.subscription}}).exec(function (err, success) {
        if (success) res.json(true);
        if (err) res.json(err);
    })
};
exports.generateUid = function () {

    // http://www.ietf.org/rfc/rfc4122.txt
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;

};
function ReplaceAll(str, find, replace) {
    var repeat = true;
    while (repeat) {
        var newStr = str.replace(find, replace);
        if (str == newStr) {
            repeat = false;
        }
        else {
            str = newStr;
        }
    }
    return str;
};
function getActivitiesByUser(user, cb) {
    if (user._doc.role == "patron") {
        async.waterfall([
            function (callback) {
                db.Ship.findOne({patron: user._doc._id, remove: false}).exec(function (err, ship) {
                    callback(err, ship);
                });
            },
            function (ship, callback) {
                db.ReservationTour.find({
                    ship: ship._doc._id,
                    remove: false
                }).sort('date').exec(function (err, res) {
                    callback(err, res, ship);
                })
            },
            function (resT, ship, callback) {
                async.map(resT, function (res, callback0) {
                    async.waterfall([
                        function (callback1) {
                            db.Tour.findOne({_id: res._doc.tour, remove: false}, function (err, tour) {
                                callback1(err, tour);
                            })
                        },
                        function (tour, callback1) {
                            db.Group.findOne({_id: tour._doc.group}).exec(function (err, group) {
                                if (err || !group) {
                                    callback1(err);
                                }
                                else {
                                    var aux = {
                                        name: group._doc.name,
                                        color: group._doc.color
                                    };
                                    tour._doc.group = aux;
                                    callback1(null, tour);
                                }
                            });
                        },
                        function (tour, callback1) {
                            db.Port.findOne({_id: tour._doc.port, remove: false}, function (err, port) {
                                if (err || !port) {
                                    callback1(err);
                                }
                                else {
                                    var aux = {
                                        id: port._doc._id,
                                        name: port._doc.name
                                    };
                                    tour._doc.port = aux;
                                    callback1(null, tour);
                                }
                            })
                        }
                    ], function (err, result) {
                        if (err || !result) {
                            callback(1);
                        }
                        else {
                            var aux = {
                                id: result._doc._id,
                                name: result._doc.name,
                                hours: (result._doc.hours.type == 'hours' ? result._doc.hours.duration : 24),
                                group: result._doc.group,
                                port: result._doc.port
                            };
                            res._doc.tour = aux;
                            var aux = {
                                id: ship._doc._id,
                                name: ship._doc.name
                            };
                            res._doc.ship = aux;

                            callback0(null, res);
                        }
                    });

                }, function (err, result) {
                    callback(err, result);
                })
            }
        ], function (err, result) {
            if (err || !result) {
                cb(new Array());
            }
            else {
                cb(result);
            }
        });
    }
    else {
        cb(new Array());
    }

};
function sendMail(smtpTransport, mailOptions, cb) {
    var options = {
        from: mailOptions.from,
        to: mailOptions.to, // list of receivers
        subject: mailOptions.subject, // Subject line
        html: mailOptions.html
    };
    try {
        smtpTransport.sendMail(mailOptions, function (err) {
            if (err) {
                mailController.saveFailedMails(options, function (save) {
                    cb(save);
                })
            }
            else {
                cb(true);
            }
        });
    }
    catch (err) {
        cb(true);
    }

};
function removeReservationRepeat(array) {
    var res = new Array();
    if (array.length > 1) {
        for (var i = 0; i < array.length - 1; i++) {
            var aux = true;
            for (var j = i + 1; j < array.length; j++) {
                if (array[i]._doc.reservation.toString() == array[j]._doc.reservation.toString()) {
                    aux = false;
                    break;
                }
            }
            if (aux) {
                res.push(array[i]);
                if (i == array.length - 1) {
                    res.push(array[i + 1]);
                }
            }

        }
        return res;
    }
    else {
        return array;
    }

};
function existElement(array, obj) {
    if (array.length) {
        for (var i = 0; i < array.length; i++) {
            if (array[i].id.toString() == obj.id.toString()) {
                return false;
            }
        }
        return true;
    }
    else {
        return true;
    }
};
function getReservationClientsByClient(email, cb) {
    updateStateTourReserved(function () {
        db.ReservationClient.find({client: email, cancel: true, state: 0}, function (err, success) {
            cb(err, success);
        })
    })
}
function updateStateTourReserved(cb) {
    var date = new Date();
    date = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() + 86400000;
    date = new Date(date);
    db.ReservationTour.find({date: {$lt: date}}).exec(function (err, resTours) {
        if (err || !resTours) {
            if (cb) cb(true);
        }
        else {
            var cont = 0;
            if (resTours.length > 0) {
                for (var i = 0; i < resTours.length; i++) {
                    db.ReservationClient.update({
                        reservation: resTours[i]._doc._id,
                        state: 0
                    }, {$set: {state: 1}}).exec(function () {
                        cont++;
                        if (cont == resTours.length) {
                            if (cb) cb(true);
                        }
                    })
                }
            }
            else {
                if (cb) cb(true);
            }
        }
    })

};
function templateForgivePass(email, token) {
    var url = global.bojeoServer + "/changePass/" + token;
    return "\"<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n" +
        "xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns:m=\"http://schemas.microsoft.com/office/2004/12/omml\"\n" +
        "xmlns=\"http://www.w3.org/TR/REC-html40\">\n" +
        "<head>\n" +
        "<meta http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">\n" +
        "<meta name=Generator content=\"Microsoft Word 15 (filtered medium)\" \n>" +
        "<!--[if !mso]><style>v\: * {behavior: url(#default#VML);}o\: * {behavior: url(#default#VML);}w\: * {behavior: url(#default#VML);}.shape {behavior: url(#default#VML);}</style><![endif]-->\n" +
        "<style>@font-face {font-family: \"Cambria Math\";panose-1: 0 0 0 0 0 0 0 0 0 0;}\n" +
        "@font-face {font-family: Calibri;panose-1: 2 15 5 2 2 2 4 3 2 4;}@font-face {font-family: \"Segoe UI Semilight\";panose-1: 2 11 4 2 4 2 4 2 2 3;}\n" +
        "p.MsoNormal, li.MsoNormal, div.MsoNormal {margin: 0cm;margin-bottom: .0001pt;font-size: 11.0pt;font-family: \"Calibri\", \"sans-serif\";mso-fareast-language: EN-US;}\n" +
        "a:link, span.MsoHyperlink {mso-style-priority: 99;color: #0563C1;text-decoration: underline;}a:visited, span.MsoHyperlinkFollowed {mso-style-priority: 99;color: #954F72;text-decoration: underline;}\n" +
        "span.EstiloCorreo17 {mso-style-type: personal-compose;font-family: \"Calibri\", \"sans-serif\";color: windowtext;}\n" +
        ".MsoChpDefault {mso-style-type: export-only;mso-fareast-language: EN-US;}@page WordSection1 {size: 612.0pt 792.0pt;margin: 70.85pt 3.0cm 70.85pt 3.0cm;}div.WordSection1 {page: WordSection1;}\n" +
        "--></style>\n" +

        "<!--[if gte mso 9]><xml><o:shapedefaults v:ext=\"edit\" spidmax=\"1026\"/></xml><![endif]-->\n" +
        "<!--[if gte mso 9]><xml><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></xml><![endif]-->\n" +
        "</head>\n" +

        "<body lang=ES link=\"#0563C1\" vlink=\"#954F72\">\n" +
        "<div class=WordSection1>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Hola," + email + "<o:p></o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Acceda al siguiente enlance para cambiar su contraseña.<br><a href=\"" + url + "\">url</a><o:p></o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Descripción<o:p></o:p></span></p>\n" +


        "<p class=MsoNormal style='margin-bottom:8.0pt;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>¡Ven a &#8220;Bojear&#8221; y a disfrutar de la ciudad desde el mar!<o:p></o:p></span></p>\n" +
        "<p class=MsoNormal style='margin-bottom:8.0pt;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal style='margin-bottom:8.0pt;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'><o:p>&nbsp;</o:p></span></p>\n" +
        "<p class=MsoNormal align=center style='margin-bottom:8.0pt;text-align:center;line-height:107%'><span style='font-family:\"Segoe UI Semilight\",\"sans-serif\"'>Bojeo Tours L.S. Bercelona, España<o:p></o:p></span></p><p class=MsoNormal><o:p>&nbsp;</o:p></p>\n" +
        "</div></body></html>\n";
};
