/**
 * Created by ernestomr87@gmail.com on 06/10/2014.
 */
var async = require('async');
var tourController = require('./tours');
var htmlToText = require('html-to-text');
var utils = require('../utils');
exports.create = function (req, res) {

    var name = {
            es: req.body.es,
            en: req.body.en
        },
        slug = {
            es: utils.getSlug(name.es),
            en: utils.getSlug(name.en)
        },
        description = {
            es: req.body.descriptions_es,
            en: req.body.descriptions_en
        },
        front = req.body.front,
        color = req.body.color,
        payform = req.body.payform;

    var seo = req.body.seo;

    var searchText = {
        es: htmlToText.fromString(description.es),
        en: htmlToText.fromString(description.en)
    }
    var files = req.files;
    var photos = [];
    var typeImgs = ["image/jpeg", "image/png"];
    var correct_photo = false;
    var pos = 0;
    while (eval("files.file" + pos)) {
        var tempFile = (eval("files.file" + pos));
        if (tempFile.mimetype != "" || tempFile.mimetype != null) {
            var image = {
                path: "",
                name: "",
                originalName: "",
                extension: ""
            };
            if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                image.name = tempFile.name;
                image.path = "/api/media/" + tempFile.name;
                image.originalName = tempFile.originalname;
                image.extension = tempFile.extension;
                photos.push(image);
                correct_photo = true;

            }
            if (!correct_photo) {
                utils.deleteFile(tempFile.name);
                return res.json({error: 'incorrect image!!!'});
            }
        }
        pos++;
    }


    var group = new db.Group({
        name: name,
        slug: slug,
        description: description,
        payform: payform,
        front: front,
        color: color,
        searchText: searchText,
        photos: photos,
        seo: typeof seo == "string" ? JSON.parse(seo) : seo
    })

    group.save(function (err, group) {
        if (err || !group) {
            return res.json({res: false})
        }
        else {
            return res.json({res: true})
        }
    })
};
exports.getByGroup = function (req, res) {
    db.Group.findOne({
            $or: [{'slug.es' : req.params.group}, {'slug.en': req.params.group}]
        }
    ).
        exec(function (err, group) {
            if (err || !group) {
                return res.json({res: false})
            }
            else {
                return res.json({res: group})
            }
        })
}
;
exports.getGroupPack = function (req, res) {
    db.Group.find({payform: "currencies"}).sort({front: 1}).exec(function (err, groups) {
        if (err || !groups) {
            return res.json({res: false})
        }
        else {
            return res.json({res: groups})
        }
    })
};
exports.delete = function (req, res) {
    var id = req.params.id;
    async.waterfall([
        function (callback) {
            db.Tour.find({group: id, remove: false}, function (err, tours) {
                callback(err, tours);
            })
        },
        function (tours, callback) {
            async.map(tours, function (tour, callback0) {
                async.waterfall([
                    function (callback1) {
                        db.Pack.find({tour: tour._id, remove: false}, function (err, paks) {
                            callback1(err, paks);
                        })
                    },
                    function (paks, callback1) {
                        async.map(paks, function (pack, callback2) {
                            db.Pack.update({_id: pack._id}, {$set: {remove: true}}).exec(function (err, success) {
                                callback2(err, success)
                            })
                        }, function (err, result) {
                            callback1(err, result);
                        })
                    }
                ], function (err, result) {
                    if (err || !result) {
                        callback0(err, result);
                    }
                    else {
                        var photos = tour._doc.photos;
                        if (photos.length > 0) {
                            var cont = 0;
                            for (var j = 0; j < photos.length; j++) {
                                db.Media.remove({fieldName: photos[j].name}).exec(function (err, success) {
                                    cont++;
                                    if (cont == photos.length) {
                                        db.Tour.update({_id: tour._id}, {$set: {remove: true}}).exec(function (err, success) {
                                            callback0(err, success);
                                        })
                                    }
                                })
                            }
                        }
                        else {
                            db.Tour.update({_id: tour._id}, {$set: {remove: true}}).exec(function (err, success) {
                                callback0(err, success);
                            })
                        }
                    }
                })
            }, function (err, result) {
                callback(err, result);
            })
        }
    ], function (err, result) {
        if (err || !result) {
            return res.json({res: false})
        }
        else {
            db.Group.remove({_id: id}).exec(function (err, success) {
                if (err || !success) {
                    return res.json({res: false})
                }
                else {
                    return res.json({res: true})
                }
            })
        }
    })
};
exports.update = function (req, res) {
    var name = {
            es: req.body.es,
            en: req.body.en
        },
        description = {
            es: req.body.descriptions_es,
            en: req.body.descriptions_en
        },
        color = req.body.color,
        payform = req.body.payform,
        seo = typeof req.body.seo == "string" ? JSON.parse(req.body.seo) : req.body.seo,
        slug = {
            es: utils.getSlug(name.es),
            en: utils.getSlug(name.en)
        };

    var searchText = {
        es: htmlToText.fromString(description.es),
        en: htmlToText.fromString(description.en)
    }

    db.Group.update({_id: req.body.id}, {
        $set: {
            name: name,
            description: description,
            payform: payform,
            color: color,
            searchText: searchText,
            seo: seo,
            slug: slug
        }
    }).exec(function (err, success) {
        if (err || !success) {
            return res.json({res: false})
        }
        else {
            return res.json({res: true})
        }
    })
};
exports.list = function (req, res) {
    db.Group.find().sort({front: 1}).exec(function (err, groups) {
        if (err || !groups) {
            return res.json({res: false})
        }
        else {
            if (groups.length > 0) {
                async.map(groups, function (group, cb) {
                    getTourByGroup(group._doc._id, function (err, data) {
                        if (data) {
                            if (data.length > 0) {
                                group._doc.tour = true;
                                group._doc.hasReservation = false;
                                for (var i = 0; i < data.length; i++) {
                                    if (data[i]._doc.hasReservation) {
                                        group._doc.hasReservation = true;
                                        break;
                                    }
                                }
                            } else {
                                group._doc.tour = false;
                                group._doc.hasReservation = false;
                            }
                            cb(null, group);
                        } else {
                            cb(err, false);
                        }
                    })
                }, function (err, result) {
                    if (err || !result) {
                        return res.json({res: false})
                    } else {
                        return res.json({res: result});
                    }
                })
            } else {
                return res.json({res: groups})
            }
        }
    })
};
exports.upFront = function (req, res) {
    var front = req.body.front;
    var query = { 'front': { $lt: front }};
    var update = { 'front': front };
    db.Group.findOneAndUpdate(query, update).sort('-front').exec(function (err, groups) {
        if (err || !groups) {
            return res.json({res: false});
        }
        else {
            db.Group.update({_id: req.body.id}, {front: front - 1}).exec(function (err, success) {
                if (err || !success) {
                    return res.json({res: false});
                }
                else {
                    return res.json({res: true});
                }
            })
        }
    });

};
exports.downFront = function (req, res) {
    var front = req.body.front;
    var query = { 'front': { $gt: front }};
    var update = { 'front': front };
    db.Group.findOneAndUpdate(query, update).sort('front').exec(function (err, groups) {
        if (err || !groups) {
            return res.json({res: false});
        }
        else {
            db.Group.update({_id: req.body.id}, {front: front + 1}).exec(function (err, success) {
                if (err || !success) {
                    return res.json({res: false});
                }
                else {
                    return res.json({res: true});
                }
            })
        }
    });

};
exports.listToursByGroup = function (req, res) {
    async.waterfall([
        function (callback) {
            db.Tour.find({group: req.body.id, remove: false}, function (err, tours) {
                callback(err, tours);
            })
        },
        function (tours, callback) {
            async.map(tours, function (tour, callback0) {
                async.parallel([
                    function (cb1) {
                        tourController.getTourGroups(tour, 0, function (data) {
                            if (data) {
                                async.waterfall([
                                    function (cb) {
                                        db.Pack.find({tour: tour, remove: false}, function (err, paks) {
                                            cb(err, paks);
                                        })
                                    },
                                    function (paks, cb) {
                                        async.map(paks, function (pack, callback1) {
                                            async.map(pack.services, function (service, callback2) {
                                                db.Service.findOne({_id: service}).select('name').exec(function (err, servs) {
                                                    callback2(err, servs);
                                                })
                                            }, function (err, result) {
                                                if (err || !result) {
                                                    callback1(err, result);
                                                }
                                                else {
                                                    var aux = {
                                                        id: pack._id,
                                                        services: result
                                                    };
                                                    callback1(null, aux);
                                                }

                                            })
                                        }, function (err, result) {
                                            cb(err, result);
                                        })
                                    }
                                ], function (err, result) {
                                    if (err || !result) {
                                        cb1(err, result);
                                    } else {
                                        var aux = {
                                            id: data._doc._id,
                                            name: data._doc.name,
                                            group: data._doc.group,
                                            packs: result
                                        };
                                        cb1(null, aux);
                                    }
                                })
                            } else {
                                cb1(null, null);
                            }
                        })
                    },
                    function (cb1) {
                        tourController.getResevationByTour(tour, 0, function (reservations, pos) {
                            var aux = {hasReservation: reservations};
                            cb1(null, aux);
                        })
                    }
                ], function (err, results) {
                    if (err || !results) {
                        callback0(err, results);
                    } else {
                        var aux = {
                            id: results[0].id,
                            name: results[0].name,
                            group: results[0].group,
                            packs: results[0].packs,
                            reservation: results[1].hasReservation
                        };
                        callback0(null, aux);
                    }
                })
            }, function (err, result) {
                if (err || !result) {
                    callback(err, result);
                }
                else {
                    var temp = [];
                    var reservation = 0;
                    for (var i = 0; i < result.length; i++) {
                        if (result[i] != null) {
                            temp.push(result[i]);
                            reservation += result[i].reservation;
                        }
                    }
                    return res.json({res: temp, complete: false, cantReservation: reservation});
                }
            })
        }
    ], function (err, result) {
        if (err || !result) {
            return res.json({res: false})
        }
        else {
            return res.json({res: result});
        }
    })
};
function getTourByGroup(group, cb) {
    db.Tour.find({group: group, remove: false}, function (err, tours) {
        if (err || !tours) {
            if (cb) cb(err, false);
        }
        else {
            if (tours.length > 0) {
                async.map(tours, function (tour, callback) {
                    tourController.getResevationByTour(tour, 0, function (reservations, pos) {
                        if (reservations > 0) {
                            tour._doc.hasReservation = true;
                        } else {
                            tour._doc.hasReservation = false;
                        }
                        callback(null, tour);
                    })
                }, function (err, result) {
                    if (cb) cb(null, result);
                })
            } else {
                if (cb) cb(null, tours);
            }
        }
    })
};
exports.updateImg = function (req, res) {
    var idGroup = req.body.id;
    var nameImgDel = eval(req.body.nameImgDel);
    var files = req.files;

    var typeImgs = ["image/jpeg", "image/png"];

    if (nameImgDel.length != 0) {
        var cont = 0;
        for (var i = 0; i < nameImgDel.length; i++) {
            db.Media.remove({fieldName: nameImgDel[i]}).exec(function (err, success) {
                if (err || !success) {
                    return res.json({result: false});
                } else {
                    cont++;
                    if (cont == nameImgDel.length) {
                        db.Group.findOne({_id: idGroup}).exec(function (err, group) {
                            if (err || !group) {
                                return res.json({result: false});
                            } else {
                                for (var i = 0; i < nameImgDel.length; i++) {
                                    var pos = 0;
                                    var flag = false;
                                    var cantidad = group.photos.length;
                                    while (!flag && pos < cantidad) {
                                        if (group.photos[pos].name == nameImgDel[i]) {
                                            group.photos.splice(pos, 1);
                                            flag = true;
                                        }
                                        pos++;
                                    }
                                }
                                var pos = 0;
                                while (eval("files.file" + pos)) {
                                    var tempFile = (eval("files.file" + pos));
                                    if (tempFile.mimetype != "" || tempFile.mimetype != null) {
                                        if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                                            var photo = {
                                                path: "/api/media/" + tempFile.name,
                                                name: tempFile.name,
                                                originalName: tempFile.originalname,
                                                extension: tempFile.extension
                                            };
                                            group.photos.push(photo);
                                        } else {
                                            utils.deleteFile(tempFile.name);
                                        }
                                    } else {
                                        utils.deleteFile(tempFile.name);
                                    }
                                    pos++;
                                }
                                db.Group.update({_id: idGroup}, {$set: {photos: group.photos}}).exec(
                                    function (err, success) {
                                        if (err || !success) {
                                            return res.json({result: false});
                                        } else {
                                            return res.json({result: true});
                                        }
                                    })
                            }
                        })
                    }
                }
            })
        }
    } else {
        db.Group.findOne({_id: idGroup}).exec(function (err, group) {
            if (err || !group) {
                return res.json({result: false});
            } else {
                var pos = 0;
                while (eval("files.file" + pos)) {
                    var tempFile = (eval("files.file" + pos));
                    if (tempFile.mimetype != "" || tempFile.mimetype != null) {
                        if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                            var photo = {
                                path: "/api/media/" + tempFile.name,
                                name: tempFile.name,
                                originalName: tempFile.originalname,
                                extension: tempFile.extension
                            };
                            group.photos.push(photo);
                        } else {
                            utils.deleteFile(tempFile.name);
                        }
                    } else {
                        utils.deleteFile(tempFile.name);
                    }
                    pos++;
                }
                db.Group.update({_id: idGroup}, {$set: {photos: group.photos}}).exec(
                    function (err, success) {
                        if (err || !success) {
                            return res.json({result: false});
                        } else {
                            return res.json({result: true});
                        }
                    })
            }
        })
    }
};


exports.getGroupList = function (callback) {
    db.Group.find().sort({front: 1}).exec(function (err, groups) {
        callback(err, groups);
    })
}