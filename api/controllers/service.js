/**
 * Created by Nesto on 19/11/2014.
 */

var utils = require('../utils');
var userController = require('./user');
var enterpriseController = require('./enterprise');
var toursController = require('./tours');
var async = require('async');
var serviceController = require('./service');

exports.create = function (req, res) {

    var type = req.body.type;
    var price = req.body.price;
    var enterprise = req.body.enterprise;
    var owner = req.body.owner;
    var commission = req.body.commission;
    var url = req.body.url;
    var name = {
        es: req.body.name_es,
        en: req.body.name_en
    };
    var description = {
        es: req.body.description_es,
        en: req.body.description_en
    };
    var files = req.files;

    var photos = [];
    var typeImgs = ["image/jpeg", "image/png"];
    var correct_photo = false;
    var pos = 0;

    while (eval("files.file" + pos)) {
        var tempFile = (eval("files.file" + pos));
        if (tempFile.mimetype != "" || tempFile.mimetype != null) {
            var image = {
                path: "",
                name: "",
                originalName: "",
                extension: ""
            };
            if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                image.name = tempFile.name;
                image.path = "/api/media/" + tempFile.name;
                image.originalName = tempFile.originalname;
                image.extension = tempFile.extension;
                photos.push(image);
                correct_photo = true;

            }
            if (!correct_photo) {
                utils.deleteFile(tempFile.name);
                return res.json({error: 'incorrect image!!!'});
            }
        }
        pos++;
    }

    var service = new db.Service({
        type: type,
        name: name,
        description: description,
        price: price,
        enterprise: enterprise,
        owner: owner,
        commission: commission,
        photos: photos,
        url: url,
        slug: utils.getSlug(name.en)
    });
    service.save(function (err, data) {
        if (err || !data) {
            return res.json({res: false});
        }
        else {
            return res.json({res: true});
        }
    });

};

exports.update = function (req, res) {

    var id = req.body.id,
        type = req.body.type,
        name = req.body.name,
        description = req.body.description,
        price = req.body.price,
        enterprise = req.body.enterprise,
        commission = req.body.commission,
        owner = req.body.owner,
        url = req.body.url;


    db.Service.update({_id: id}, {
        $set: {
            type: type,
            name: name,
            description: description,
            price: price,
            enterprise: enterprise,
            commission: commission,
            owner: owner,
            url: url,
            slug: utils.getSlug(name.en)
        }
    }).exec(function (err, data) {
        if (err || !data) {
            return res.json({res: false});
        }
        else {
            return res.json({res: true});
        }
    });

};

exports.delete = function (req, res) {
    var id = req.params.id;
    exports.getPackagesByService(id, 0, function (packages, pos) {
        if (packages.length == 0) {
            db.Service.findOne({_id: id}).exec(function (err, success) {
                if (err || !success) {
                    return res.json({res: false});
                } else {
                    var photos = success._doc.photos;
                    if (photos.length > 0) {
                        var cont = 0;
                        for (var j = 0; j < photos.length; j++) {
                            db.Media.remove({fieldName: photos[j].name}).exec(function (err, success) {
                                cont++;
                                if (cont == photos.length) {
                                    db.Service.update({_id: id}, {$set: {remove: true}}).exec(function (err, success) {
                                        if (err || !success) {
                                            return res.json({res: false});
                                        }
                                        else {
                                            return res.json({res: true});
                                        }
                                    })
                                }
                            })
                        }
                    }
                    else {
                        db.Service.update({_id: id}, {$set: {remove: true}}).exec(function (err, success) {
                            if (err || !success) {
                                return res.json({res: false});
                            }
                            else {
                                return res.json({res: true});
                            }
                        })
                    }
                }
            })
        } else {
            async.map(packages, function (pack, callback) {
                db.Pack.update({_id: pack._id}, {$set: {remove: true}}).exec(function (err, success) {
                    callback(err, success);
                })
            }, function (err, result) {
                if (err || !result) {
                    return res.json({res: false});
                } else {
                    db.Service.findOne({_id: id}).exec(function (err, success) {
                        if (err || !success) {
                            return res.json({res: false});
                        } else {
                            var photos = success._doc.photos;
                            if (photos.length > 0) {
                                var cont = 0;
                                for (var j = 0; j < photos.length; j++) {
                                    db.Media.remove({fieldName: photos[j].name}).exec(function (err, success) {
                                        cont++;
                                        if (cont == photos.length) {
                                            db.Service.update({_id: id}, {$set: {remove: true}}).exec(function (err, success) {
                                                if (err || !success) {
                                                    return res.json({res: false});
                                                }
                                                else {
                                                    return res.json({res: true});
                                                }
                                            })
                                        }
                                    })
                                }
                            }
                            else {
                                db.Service.update({_id: id}, {$set: {remove: true}}).exec(function (err, success) {
                                    if (err || !success) {
                                        return res.json({res: false});
                                    }
                                    else {
                                        return res.json({res: true});
                                    }
                                })
                            }
                        }
                    })
                }
            })
        }
    })
};

exports.list = function (req, res) {
    var limit = req.query.limit;
    var skip = req.query.skip;
    async.parallel([
        function (cb) {
            db.Service.count({remove: false}).exec(function (err, count) {
                if (cb) cb(err, count);
            })
        },
        function (cb) {
            db.Service.find({remove: false}).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, services) {
                if (cb) cb(err, services);
            })
        }
    ], function (err, results) {
        if (err || !results) {
            return res.json({res: false, cont: 0});
        } else {
            var list = new Array();
            var services = results[1];
            if (services.length > 0) {
                var cont = 0;
                for (var i = 0; i < services.length; i++) {
                    userController.getUserForId(services[i]._doc.owner, i, function (user, pos) {
                        if (user) {
                            var owner = {
                                id: services[pos]._doc.owner,
                                email: user._doc.email
                            };
                            services[pos]._doc.owner = owner;

                            enterpriseController.getEnterpriseForId(services[pos]._doc.enterprise, pos, function (enterprise, pos) {
                                if (enterprise) {
                                    var tmp = {
                                        id: services[pos]._doc.enterprise,
                                        name: enterprise._doc.name.es
                                    }
                                    services[pos]._doc.enterprise = tmp;
                                }
                                cont++;
                                if (cont == services.length) {
                                    return res.json({res: services, cont: results[0]});
                                }
                            })
                        }
                    })
                }
            } else {
                return res.json({res: list, cont: results[0]});
            }
        }
    })
};

exports.listBack = function (req, res) {
    var limit = req.body.limit;
    var skip = req.body.skip;
    var id = req.body.id;
    async.parallel([
        function (cb) {
            db.Service.count({remove: false}).exec(function (err, count) {
                if (cb) cb(err, count);
            })
        },
        function (cb) {
            if (id.length > 0) {
                db.Service.find({
                    remove: false, $or: [
                        {_id: id},
                        {enterprise: id},
                        {owner: id}
                    ]
                }).exec(function (err, services) {
                    if (services.length > 0) {
                        var cont = 0;
                        for (var i = 0; i < services.length; i++) {
                            exports.getPackagesByService(services[i]._id, i, function (packages, pos) {
                                var arrayPackages = new Array();
                                for (var j = 0; j < packages.length; j++) {
                                    arrayPackages.push(packages[j]._doc._id);
                                }
                                services[pos]._doc.packages = arrayPackages;
                                cont++;
                                if (cont == services.length) {
                                    if (cb) cb(err, services);
                                }
                            })
                        }
                    } else {
                        if (cb) cb(err, services);
                    }
                })
            } else {
                db.Service.find({remove: false}).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, services) {
                    if (services.length > 0) {
                        var cont = 0;
                        for (var i = 0; i < services.length; i++) {
                            exports.getPackagesByService(services[i]._id, i, function (packages, pos) {
                                var arrayPackages = new Array();
                                for (var j = 0; j < packages.length; j++) {
                                    arrayPackages.push(packages[j]._doc._id);
                                }
                                services[pos]._doc.packages = arrayPackages;
                                cont++;
                                if (cont == services.length) {
                                    if (cb) cb(err, services);
                                }
                            })
                        }
                    } else {
                        if (cb) cb(err, services);
                    }
                })
            }
        }
    ], function (err, results) {
        if (err || !results) {
            return res.json({res: false, cont: 0});
        } else {
            var list = new Array();
            var services = results[1];
            if (services.length > 0) {
                var cont = 0;
                for (var i = 0; i < services.length; i++) {
                    userController.getUserForId(services[i]._doc.owner, i, function (user, pos) {
                        if (user) {
                            var owner = {
                                id: services[pos]._doc.owner,
                                email: user._doc.email
                            };
                            services[pos]._doc.owner = owner;

                            enterpriseController.getEnterpriseForId(services[pos]._doc.enterprise, pos, function (enterprise, pos) {
                                if (enterprise) {
                                    var tmp = {
                                        id: services[pos]._doc.enterprise,
                                        name: enterprise._doc.name.es
                                    }
                                    services[pos]._doc.enterprise = tmp;
                                }
                                exports.getReservationClientByIdService(services[pos]._doc._id, pos, function (reservsClient, pos) {
                                    services[pos]._doc.hasReservation = false;
                                    if (reservsClient.length) {
                                        services[pos]._doc.hasReservation = true;
                                    }
                                    cont++;
                                    if (cont == services.length) {
                                        return res.json({res: services, cont: results[0]});
                                    }
                                })
                            })
                        }
                    })
                }
            } else {
                return res.json({res: list, cont: results[0]});
            }
        }
    })
};

exports.listAvailable = function (req, res) {
    var limit = req.body.limit,
        skip = req.body.skip,
        text = req.body.text;
    if (text.length > 0) {
        async.parallel([
            function (cb) {
                db.Service.count({remove: false, status: true}).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                text = new RegExp(text, 'i');
                db.Service.find({
                    remove: false, status: true, $or: [
                        {'name.es': text},
                        {type: text}
                    ]
                }).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, services) {
                    if (cb) cb(err, services);
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            } else {
                var services = results[1];
                var list = new Array();
                if (services.length > 0) {
                    var cont = 0;
                    for (var i = 0; i < services.length; i++) {
                        userController.getUserForId(services[i]._doc.owner, i, function (user, pos) {
                            if (user) {
                                var owner = {
                                    id: services[pos]._doc.owner,
                                    email: user._doc.email
                                };
                                services[pos]._doc.owner = owner;

                                enterpriseController.getEnterpriseForId(services[pos]._doc.enterprise, pos, function (enterprise, pos) {
                                    if (enterprise) {
                                        var tmp = {
                                            id: services[pos]._doc.enterprise,
                                            name: enterprise._doc.name.es
                                        }
                                        services[pos]._doc.enterprise = tmp;
                                    }
                                    cont++;
                                    if (cont == services.length) {
                                        return res.json({res: services, cont: results[0]});
                                    }
                                })
                            }
                        })
                    }
                } else {
                    return res.json({res: list, cont: results[0]});
                }
            }
        })
    } else {
        async.parallel([
            function (cb) {
                db.Service.count({remove: false, status: true}).exec(function (err, count) {
                    if (cb) cb(err, count);
                })
            },
            function (cb) {
                db.Service.find({
                    remove: false,
                    status: true
                }).sort({_id: 1}).limit(limit).skip(skip).exec(function (err, services) {
                    if (cb) cb(err, services);
                })
            }
        ], function (err, results) {
            if (err || !results) {
                return res.json({res: false, cont: 0});
            } else {
                var services = results[1];
                var list = new Array();
                if (services.length > 0) {
                    var cont = 0;
                    for (var i = 0; i < services.length; i++) {
                        userController.getUserForId(services[i]._doc.owner, i, function (user, pos) {
                            if (user) {
                                var owner = {
                                    id: services[pos]._doc.owner,
                                    email: user._doc.email
                                };
                                services[pos]._doc.owner = owner;

                                enterpriseController.getEnterpriseForId(services[pos]._doc.enterprise, pos, function (enterprise, pos) {
                                    if (enterprise) {
                                        var tmp = {
                                            id: services[pos]._doc.enterprise,
                                            name: enterprise._doc.name.es
                                        }
                                        services[pos]._doc.enterprise = tmp;
                                    }
                                    cont++;
                                    if (cont == services.length) {
                                        return res.json({res: services, cont: results[0]});
                                    }
                                })
                            }
                        })
                    }
                } else {
                    return res.json({res: list, cont: results[0]});
                }
            }
        })
    }
};

exports.get = function (req, res) {
    db.Service.find({_id: req.params.id, remove: false}).exec(function (err, data) {
        if (err || !data) {
            return res.json({res: false});
        }
        else {
            return res.json({res: data});
        }
    })
};

exports.status = function (req, res) {
    var id = req.body.id;
    var status = req.body.status;
    if (req.body.action == 3) {
        db.Service.findOne({_id: id, remove: false}).exec(function (err, service) {
            if (err || !service) {
                return res.json({res: false});
            }
            else {
                async.parallel([
                    function (cb) {
                        db.User.update({
                            _id: service._doc.owner,
                            remove: false
                        }, {$set: {available: status}}, function (err, success) {
                            if (cb) cb(err, success);
                        })
                    },
                    function (cb) {
                        db.Enterprise.update({
                            _id: service._doc.enterprise,
                            remove: false
                        }, {$set: {available: status}}, function (err, success) {
                            if (cb) cb(err, success);
                        })
                    }
                ], function (err, results) {
                    if (err || !results) {
                        return res.json({res: false});
                    } else {
                        db.Service.update({_id: id}, {$set: {status: status}},
                            function (err, success) {
                                if (err || !success) {
                                    return res.json({res: false});
                                }
                                else {
                                    return res.json({res: true, complete: true});
                                }
                            })
                    }
                })
            }
        })
    } else {
        if (req.body.action == 2) {
            db.Service.findOne({_id: id, remove: false}).exec(function (err, service) {
                if (err || !service) {
                    return res.json({res: false});
                }
                else {
                    async.parallel([
                        function (cb) {
                            db.User.findOne({
                                _id: service._doc.owner,
                                available: false,
                                remove: false
                            }).exec(function (err, user) {
                                if (err || !user) {
                                    if (cb) cb(err, false);
                                } else {
                                    var aux = {
                                        id: user._doc._id,
                                        userName: user._doc.userName,
                                        lastName: user._doc.lastName,
                                        email: user._doc.email
                                    };
                                    if (cb) cb(false, aux);
                                }
                            })
                        },
                        function (cb) {
                            db.Enterprise.findOne({
                                _id: service._doc.enterprise,
                                available: false,
                                remove: false
                            }).exec(function (err, enterprise) {
                                if (err || !enterprise) {
                                    if (cb) cb(err, false);
                                } else {
                                    var aux = {
                                        id: enterprise._doc._id,
                                        name: enterprise._doc.name
                                    };
                                    if (cb) cb(false, aux);
                                }
                            })
                        }
                    ], function (err, results) {
                        if (err || !results) {
                            return res.json({res: false});
                        } else {
                            if (results[0] == false && results[1] == false) {
                                db.Service.update({_id: id}, {$set: {status: status}},
                                    function (err, success) {
                                        if (err || !success) {
                                            return res.json({res: false});
                                        }
                                        else {
                                            return res.json({res: true, complete: true});
                                        }
                                    })
                            } else {
                                var aux = {
                                    user: results[0],
                                    enterprise: results[1]
                                };
                                return res.json({res: aux, complete: false});
                            }
                        }
                    })
                }
            })
        } else {
            exports.getPackagesAvailablesByService(id, 0, function (packages, pos) {
                if (packages.length == 0) {
                    db.Service.update({_id: id}, {$set: {status: status}},
                        function (err, success) {
                            if (err || !success) {
                                return res.json({res: false});
                            }
                            else {
                                return res.json({res: true, complete: true});
                            }
                        })
                } else {
                    if (req.body.action == 0) {
                        async.map(packages, function (pack, callback) {
                            async.parallel([
                                function (cb) {
                                    toursController.getTour(pack._doc.tour, function (tour) {
                                        if (tour) {
                                            var aux = {
                                                tour: {
                                                    name: tour._doc.name,
                                                    group: tour._doc.group
                                                }
                                            };
                                            cb(null, aux);
                                        } else {
                                            cb(null, false);
                                        }
                                    })
                                },
                                function (cb) {
                                    async.map(pack._doc.services, function (service, callback1) {
                                        db.Service.findOne({_id: service}).select('name').exec(function (err, servs) {
                                            callback1(err, servs);
                                        })
                                    }, function (err, result) {
                                        if (err || !result) {
                                            cb(err, result);
                                        }
                                        else {
                                            var aux = {
                                                id: pack._id,
                                                services: result
                                            };
                                            cb(null, aux);
                                        }
                                    })
                                }
                            ], function (err, result) {
                                if (err || !result) {
                                    callback(err, result);
                                } else {
                                    pack._doc.datTour = result[0];
                                    pack._doc.datService = result[1];
                                    callback(null, pack);
                                }
                            })
                        }, function (err, result) {
                            if (err || !result) {
                                return res.json({res: false});
                            }
                            else {
                                var temp = [];
                                for (var i = 0; i < result.length; i++) {
                                    if (result[i] != null) {
                                        temp.push(result[i]);
                                    }
                                }
                                if (temp.length == 0) {
                                    db.Service.update({
                                        _id: id
                                    }, {$set: {status: status}}).exec(function (err, success) {
                                        if (err || !success) {
                                            return res.json({res: false})
                                        }
                                        else {
                                            return res.json({res: true, complete: true});
                                        }
                                    })
                                } else {
                                    return res.json({res: temp, complete: false});
                                }
                            }
                        })
                    } else {
                        async.map(packages, function (pack, callback) {
                            db.Pack.update({_id: pack._doc._id}, {$set: {status: false}}, function (err, success) {
                                callback(err, success)
                            })
                        }, function (err, result) {
                            if (err || !result) {
                                return res.json({res: false, complete: false});
                            }
                            else {
                                db.Service.update({
                                    _id: id
                                }, {$set: {status: status}}).exec(function (err, success) {
                                    if (err || !success) {
                                        return res.json({res: false})
                                    }
                                    else {
                                        return res.json({res: true, complete: true});
                                    }
                                })
                            }
                        })
                    }
                }
            })
        }
    }
};

exports.updateImg = function (req, res) {
    var idService = req.body.id;
    var nameImgDel = eval(req.body.nameImgDel);
    var files = req.files;

    var typeImgs = ["image/jpeg", "image/png"];

    if (nameImgDel.length != 0) {
        async.map(nameImgDel, function (nameImg, callback0) {
            db.Media.remove({fieldName: nameImg}).exec(function (err, success) {
                callback0(err, success);
            })
        }, function (err, result) {
            if (err || !result) {
                return res.json({result: false});
            }
            else {
                db.Service.findOne({_id: idService}).exec(function (err, service) {
                    if (err || !service) {
                        return res.json({result: false});
                    } else {
                        for (var i = 0; i < nameImgDel.length; i++) {
                            var pos = 0;
                            var flag = false;
                            var cantidad = service.photos.length;
                            while (!flag && pos < cantidad) {
                                if (service.photos[pos].name == nameImgDel[i]) {
                                    service.photos.splice(pos, 1);
                                    flag = true;
                                }
                                pos++;
                            }
                        }
                        var pos = 0;
                        while (eval("files.file" + pos)) {
                            var tempFile = (eval("files.file" + pos));
                            if (tempFile.mimetype != "" || tempFile.mimetype != null) {
                                if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                                    var photo = {
                                        path: "/api/media/" + tempFile.name,
                                        name: tempFile.name,
                                        originalName: tempFile.originalname,
                                        extension: tempFile.extension
                                    };
                                    service.photos.push(photo);
                                } else {
                                    utils.deleteFile(tempFile.name);
                                }
                            } else {
                                utils.deleteFile(tempFile.name);
                            }
                            pos++;
                        }
                        db.Service.update({_id: idService}, {$set: {photos: service.photos}}).exec(
                            function (err, success) {
                                if (err || !success) {
                                    return res.json({result: false});
                                } else {
                                    return res.json({result: true});
                                }
                            })
                    }
                })
            }
        });
    } else {
        db.Service.findOne({_id: idService}).exec(function (err, service) {
            if (err || !service) {
                return res.json({result: false});
            } else {
                var pos = 0;
                while (eval("files.file" + pos)) {
                    var tempFile = (eval("files.file" + pos));
                    if (tempFile.mimetype != "" || tempFile.mimetype != null) {
                        if (utils.validateFile(tempFile.mimetype, typeImgs)) {
                            var photo = {
                                path: "/api/media/" + tempFile.name,
                                name: tempFile.name,
                                originalName: tempFile.originalname,
                                extension: tempFile.extension
                            };
                            service.photos.push(photo);
                        } else {
                            utils.deleteFile(tempFile.name);
                        }
                    } else {
                        utils.deleteFile(tempFile.name);
                    }
                    pos++;
                }
                db.Service.update({_id: idService}, {$set: {photos: service.photos}}).exec(
                    function (err, success) {
                        if (err || !success) {
                            return res.json({result: false});
                        } else {
                            return res.json({result: true});
                        }
                    })
            }
        })
    }
};

exports.getServices = function (arrayID, pos, cb) {
    var cont = 0;
    var result = [];
    for (var i = 0; i < arrayID.length; i++) {
        db.Service.findOne({_id: arrayID[i].id, remove: false}).exec(function (err, data) {
            if (data) {
                var aux = {
                    id: arrayID[cont].id,
                    nameService: data._doc.name
                }
                result[cont] = aux;
            }
            cont++;
            if (cont == arrayID.length) {
                if (cb) cb(result, pos);
            }
        })
    }
};

exports.getServiceByOwner = function (owner, aux, cb) {
    db.Service.findOne({owner: owner, remove: false}).exec(function (err, service) {
        if (err || !service) {
            if (cb) cb(aux);
        }
        else {
            aux.service = true;
            if (cb) cb(aux);
        }
    })
};

exports.getServiceByPack = function (req, res) {
    db.Pack.findOne({_id: req.body.id}, function (err, pack) {
        if (err || !pack) {
            return res.json({res: new Array()});
        }
        else {
            async.map(pack._doc.services, function (services, callback0) {
                db.Service.findOne({_id: services.id}, function (err, serv) {
                    callback0(err, serv);
                })
            }, function (err, result) {
                if (err || !result) {
                    return res.json({res: new Array()});
                }
                else {
                    return res.json({res: result});
                }
            })
        }
    })
};

exports.getPacksByService = function (req, res) {
    exports.getPackagesByService(req.body.id, 0, function (packages, pos) {
        if (packages.length == 0) {
            return res.json({res: packages});
        } else {
            async.parallel([
                function (cb) {
                    var cont = 0;
                    for (var i = 0; i < packages.length; i++) {
                        toursController.getTourGroups(packages[i]._doc.tour, i, function (data, pos) {
                            if (data) {
                                var aux = {
                                    id: packages[pos]._doc.tour,
                                    idGroup: data.group.id,
                                    name: data.name,
                                    nameGroup: (data.group.name == 'vacio' ? 'vacio' : data.group.name)
                                };
                                packages[pos]._doc.tour = aux;
                            }
                            cont++;
                            if (cont == packages.length) {
                                cb(null, packages);
                            }
                        })
                    }
                },
                function (cb) {
                    var cont = 0;
                    for (var i = 0; i < packages.length; i++) {
                        serviceController.getServices(packages[i]._doc.services, i, function (data, pos) {
                            if (data) {
                                var services = [];
                                for (var j = 0; j < data.length; j++) {
                                    services.push(data[j]);
                                }
                                packages[pos]._doc.services = services;
                            }
                            cont++;
                            if (cont == packages.length) {
                                cb(null, packages);
                            }
                        })
                    }
                }
            ], function (err, results) {
                if (err || !results) {
                    return res.json({res: false});
                }
                else {
                    return res.json({res: results[1]});
                }
            });
        }
    })
};

exports.getPackagesByService = function (service, pos, cb) {
    db.Pack.find({remove: false}).exec(function (err, packages) {
        if (err || !packages) {
            if (cb) cb(new Array(), pos);
        }
        else {
            var arrayPack = [];
            for (var i = 0; i < packages.length; i++) {
                if (packages[i].services.length > 1) {
                    for (var j = 0; j < packages[i].services.length; j++) {
                        if (packages[i].services[j].id == service) {
                            arrayPack.push(packages[i]);
                            break;
                        }
                    }
                } else {
                    if (packages[i].services[0].id == service) {
                        arrayPack.push(packages[i]);
                    }
                }
            }
            if (cb)cb(arrayPack, pos);
        }
    })
};

exports.getPackagesAvailablesByService = function (service, pos, cb) {
    db.Pack.find({status: true, remove: false}).exec(function (err, packages) {
        if (err || !packages) {
            if (cb) cb(new Array(), pos);
        }
        else {
            var arrayPack = [];
            for (var i = 0; i < packages.length; i++) {
                if (packages[i].services.length > 1) {
                    for (var j = 0; j < packages[i].services.length; j++) {
                        if (packages[i].services[j].id == service) {
                            arrayPack.push(packages[i]);
                            break;
                        }
                    }
                } else {
                    if (packages[i].services[0].id == service) {
                        arrayPack.push(packages[i]);
                    }
                }
            }
            if (cb)cb(arrayPack, pos);
        }
    })
};

exports.getReservationClientByIdService = function (idService, pos, cb) {
    db.ReservationClient.find({'services.id': idService, state: 0, cancel: true}).exec(function (err, resvClient) {
        if (err || !resvClient) {
            if (cb) cb(new Array(), pos);
        } else {
            if (cb)cb(resvClient, pos);
        }
    })
};