/**
 * Created by Home on 04/06/2015.
 */
var async = require('async');
module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var tagSchema = new Schema({
        name: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        available: {type: Boolean, required: true, default: false},
        remove: {type: Boolean, required: true, default: false}
    }, {versionKey: false});

    tagSchema.methods.freeForRemove = function (cb) {
        var tag = this._doc;
        db.Ship.find({remove: false, tag: tag._id}).exec(function (err, ships) {
            if (err || !ships) {
                cb(err, ships);
            }
            else {
                var array = [];
                for (var i = 0; i < ships.length; i++) {
                    var aux = {
                        _id: ships[i]._doc._id,
                        name: ships[i]._doc.name,
                        type: 'ship'
                    };
                    array.push(aux);
                }
                cb(null, array);
            }
        });
    };

    return mongoose.model('Tag', tagSchema);
};