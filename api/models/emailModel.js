/**
 * Created by ernestomr87@gmail.com on 05/08/2014.
 */


module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var emailSchema = new Schema({
        mailServer: { type: String, required: true, index: { unique: true } },
        mailSMTPPort: { type: Number, required: true },
        mailUser: { type: String, required: true },
        mailPass: { type: String },
        notificationChangePass: {
            subject: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            },
            body: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            },
            active: {type: Boolean, required: true}
        },
        devolution: {
            subject: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            },
            body: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            },
            active: {type: Boolean, required: true}
        },
        notificationReserved: {
            subject: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            },
            body: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            },
            active: {type: Boolean, required: true}
        },
        notificationRegister: {
            subject: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            },
            body: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            },
            active: {type: Boolean, required: true}
        },
        bulletin: {
            subject: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            },
            body: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            },
            active: {type: Boolean, required: true}
        },
        promo: {
            subject: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            },
            body: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            },
            active: {type: Boolean, required: true}
        },
        sign: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        }
    }, { versionKey: false });

    return mongoose.model('Email', emailSchema);
};