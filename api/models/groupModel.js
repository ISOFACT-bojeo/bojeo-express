/**
 * Created by ernestomr87@gmail.com on 06/10/2014.
 */
module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var groupSchema = new Schema({
        name: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },

        slug: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },

        description: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        searchText: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        payform: {type: String, required: true},
        front: {type: Number, required: true},
        color: {type: String, required: true},
        seo: {
            title: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            },
            description: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            }
        },
        photos: []
    }, {versionKey: false});

    return mongoose.model('Group', groupSchema);
};