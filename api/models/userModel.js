var bcrypt = require('bcrypt-nodejs');

module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var userSchema = new Schema({
        userName: {type: String, required: false},

        slug: {type: String, required: true},

        lastName: {type: String, required: false},
        sex: {type: String, enum: ['male', 'female'], default: 'male'},
        birthday: {type: Date, required: false},
        email: {type: String, required: false},
        phoneNumber: {type: Number, required: false},
        enterprise: {type: String},
        address: {type: String, required: false},
        city: {type: String, required: false},
        postalCode: {type: String},
        country: {type: String, required: false},
        password: {type: String, required: false},
        role: {type: String, enum: ['user', 'admin', 'patron', 'owner', 'partner'], default: 'user'},
        subscription: {type: Boolean, required: false, default: false},
        bonds: [{
            id: {type: Schema.Types.ObjectId, ref: 'Bond', required: true},
            value: {type: Number, required: false, default: 0}
        }],
        totalAmount: {type: Number, required: false, default: 0},
        createDate: {type: Date, required: false},
        available: {type: Boolean, required: false, default: false},
        token: {type: String, default: null},
        complete: {type: Boolean, required: false, default: true},
        expireTime: {type: Date, required: false},
        socialId: {type: String, required: false},
        remove: {type: Boolean, required: true, default: false},
        language: {type: String, enum: ['es', 'en'], default: 'es'}
    }, {versionKey: false});

    // checking if password is valid
    userSchema.methods.validPassword = function (password, next) {
        //return bcrypt.compareSync(password, this.password);
        bcrypt.compare(password, this.password, function (err, valid) {
            if (err) return next(err)
            next(null, valid)
        })
    };
    userSchema.methods.nameComplete = function (cb) {
        cb(this.userName + ' ' + this.lastName);
    };

    return mongoose.model('User', userSchema);
};
