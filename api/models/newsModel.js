/**
 * Created by ernestomr87@gmail.com on 14/05/14.
 */
module.exports = function (mongoose) {

    var Schema = mongoose.Schema;


    var newsSchema = new Schema({
        createDate: {type: Date, required: true},
        title: {
            es: { type: String, required: true, index: { unique: true }},
            en: { type: String, required: true, index: { unique: true }}
        },

        slug: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },

        introduction: {
            es: { type: String, required: true },
            en: { type: String, required: true }
        },
        body: {
            es: { type: String, required: true },
            en: { type: String, required: true }
        },
        imageFooter: {
            es: { type: String, required: false },
            en: { type: String, required: false }
        },
        searchText: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        status: {type: Boolean, default: false},
        photo: {type: Schema.Types.Mixed},
        document: {type: Schema.Types.Mixed, required: false, default: false},
        video: { type: String, required: false },
        autor: { type: String, required: true },
        originalAutor: { type: String, required: false }
    }, { versionKey: false });

    return mongoose.model('News', newsSchema);
}
