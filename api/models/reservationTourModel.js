/**
 * Created by ernestomr87@gmail.com on 05/06/2014.
 */
module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var reservationTourSchema = new Schema({
        tag: {type: String, enum: ['locked', 'booked', 'canceled'], default: 'locked'},
        type: {type: String, required: true},
        tour: {type: Schema.Types.ObjectId, ref: 'Tour'},
        ship: {type: Schema.Types.ObjectId, ref: 'Ship'},
        start: {type: Date, required: true},
        end: {type: Date, required: true},
        departure: {type: String, required: true},
        duration: {type: String, required: true},
        percent: {type: Number, required: true},
        total: {type: Number, required: true},
        comitions: {type: Number, required: true},
        cantPerson: {type: Number, required: true},
        available: {type: Boolean, required: true, default: true},
        remove: {type: Boolean, required: true, default: false},
        title: {type: String},
        description: {type: String}


    }, {versionKey: false});


    return mongoose.model('ReservationTour', reservationTourSchema);
};