/**
 * Created by ernestomr87@gmail.com on 26/07/14.
 */

module.exports = function (mongoose) {

    var Schema = mongoose.Schema;
        //, ObjectId = Schema.ObjectId;

    var generalInfoSchema = new Schema({
        aboutBojeo: {
            es: { type: String, required: true },
            en: { type: String, required: true }
        },
        photos: [],
        payForms: {
            es: { type: String, required: true },
            en: { type: String, required: true }
        },
        devolutions: {
            es: { type: String, required: true },
            en: { type: String, required: true }
        },
        useTerms: {
            es: { type: String, required: true },
            en: { type: String, required: true }
        },
        searchUseText: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        searchAboutText: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        privacyPolicy: {
            es: { type: String, required: true },
            en: { type: String, required: true }
        },
        politicCookies: {
            es: { type: String, required: true },
            en: { type: String, required: true }
        },
        noticeCookies: {
            es: { type: String, required: true },
            en: { type: String, required: true }
        },
        copyRights: {
            es: { type: String, required: true },
            en: { type: String, required: true }
        }
    }, { versionKey: false });

    return mongoose.model('GeneralInfo', generalInfoSchema);
};