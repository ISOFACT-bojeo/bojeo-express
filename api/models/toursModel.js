/**
 * Created by ernestomr87@gmail.com on 06/10/2014.
 */
var async = require('async');
module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var tourSchema = new Schema({
        group: {type: Schema.Types.ObjectId, ref: 'Group'},
        port: {type: Schema.Types.ObjectId, ref: 'Port'},
        name: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },

        slug: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },

        description: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        available: {type: Boolean, required: true, default: false},
        daysWeek: [],
        daysTxt: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        recommendations: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        forReserve: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        transport: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        conditions: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        language: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        extras: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        noIncludes: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        priceTxt: {
            es: {type: String},
            en: {type: String}
        },
        departureTxt: {
            es: {type: String},
            en: {type: String}
        },
        seo: {
            title: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            },
            description: {
                es: {type: String, required: true},
                en: {type: String, required: true}
            }
        },
        data: {
            bond: {
                bond: {type: Schema.Types.ObjectId, ref: 'Bond'},
                minHour: {type: Number},
                maxHour: {type: Number}

            },
            capacity: {
                maxPerson: {type: Number},
                type: {type: String}
            },
            price: {
                value: [
                    {
                        duration: {type: String, required: true},
                        price: {type: Number, required: true}
                    }
                ],
                type: {type: String, required: true},
                toPay: {type: Number, required: true}

            },
            payForm: {type: String, required: true},
            departures: [{type: String, required: true}]
        },
        photos: {type: Schema.Types.Mixed},
        remove: {type: Boolean, required: true, default: false},
        available: {type: Boolean, required: true, default: false},
        searchText: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        }
    }, {versionKey: false});

    tourSchema.methods.getName = function (callbackM) {
        var tour = this._doc;

        db.Group.findOne({_id: tour.group}, 'name color', function (err, group) {
            if (err || !group) {
                callbackM(false);
            }
            else {
                var name = {
                    es: group._doc.name.es + ' - ' + tour.name.es,
                    en: group._doc.name.en + ' - ' + tour.name.en,
                    color: group.color
                }
                callbackM(name);
            }

        });
    };

    tourSchema.methods.freeForRemove = function (cb) {
        var tour = this._doc;
        async.parallel([
            function (callback) {
                db.Ship.find({remove: false, tours: tour._id}).exec(function (err, ships) {
                    if (err || !ships) {
                        callback(err, ships);
                    }
                    else {
                        var array = [];
                        for (var i = 0; i < ships.length; i++) {
                            var aux = {
                                _id: ships[i]._doc._id,
                                name: ships[i]._doc.name,
                                type: 'ship'
                            };
                            array.push(aux);
                        }
                        callback(null, array);
                    }
                });
            },
            function (callback) {
                db.Pack.find({remove: false, tour: tour._id}).populate('services').exec(function (err, tour) {
                    if (err || !tour) {
                        callback(err, tour);
                    }
                    else {
                        var array = [];
                        for (var i = 0; i < tour.length; i++) {
                            var aux = {
                                _id: tour[i]._doc._id,
                                services: tour[i]._doc.services,
                                type: 'pack'
                            };
                            array.push(aux);
                        }
                        callback(null, array);
                    }
                });
            },
            function (callback) {
                var today = new Date();
                db.ReservationTour.find({
                    remove: false,
                    tour: tour._id,
                    start: {$gt: today}
                }).populate('services').exec(function (err, services) {
                    if (err || !services) {
                        callback(err, services);
                    }
                    else {
                        var array = [];
                        for (var i = 0; i < services.length; i++) {
                            var aux = {
                                _id: services[i]._doc._id,
                                type: 'reservation'
                            };
                            array.push(aux);
                        }
                        callback(null, array);
                    }
                });
            }
        ], function (err, results) {
            if (err || !results) {
                cb(err, results);
            }
            else {
                var aux = {
                    ships: results[0],
                    packs: results[1],
                    bookings: results[2]
                }
                cb(err, aux);
            }

        })
    };

    return mongoose.model('Tour', tourSchema);
};