/**
 * Created by ernestomr87@gmail.com on 16/07/14.
 */

module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var citySchema = new Schema({
        es: {type: String, required: true},
        en: {type: String, required: true}
    });
    var countrySchema = new Schema({
        iso: {type: String, required: true},
        name: {
            es: {type: String, required: true, index: {unique: true}},
            en: {type: String, required: true, index: {unique: true}}
        },
        cities: [citySchema]
    }, {versionKey: false});

    return mongoose.model('Country', countrySchema);
};
