module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var portSchema = new Schema({
        name: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },

        slug: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },

        address: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        city: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        country: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        location: {
            x: {type: Number, required: true},
            y: {type: Number, required: true}
        },
        available: {type: Boolean, required: true, default: false},
        remove: {type: Boolean, required: true, default: false},
        photos: []
    }, {versionKey: false});

    return mongoose.model('Port', portSchema);
};
