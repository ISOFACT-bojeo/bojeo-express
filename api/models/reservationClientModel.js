/**
 * Created by ernestomr87@gmail.com on 05/06/2014.
 */
module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var reservationClientSchema = new Schema({
        user: {type: Schema.Types.ObjectId, ref: 'User', required: true},
        pack: {type: Schema.Types.ObjectId, ref: 'Pack'},
        reservation: {type: Schema.Types.ObjectId, ref: 'ReservationTour', required: true},
        reason: {type: String, required: true, default: 'none'},
        services: [{type: Schema.Types.ObjectId, ref: 'Service'}],
        cantPerson: {type: Number, required: true},
        payment: {
            total: {type: String, required: true},
            method: {type: String, required: true},
            referenceNumber: {type: Number, required: true},
            token: {type: String, required: true},
            receipt: {type: Schema.Types.Mixed, required: true}
        },
        cancel: {type: Boolean, required: true, default: false}

    }, {versionKey: false});


    return mongoose.model('ReservationClient', reservationClientSchema);
};