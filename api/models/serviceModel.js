/**
 * Created by ernestomr87@gmail.com on 16/07/14.
 */

module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var serviceSchema = new Schema({
        type: {type: String, enum: ['tourist', 'gastronomic', 'adventurer'], required: true},
        name: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },

        slug: {type: String, required: true, unique: true},

        description: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        price: {type: Number, required: true},
        enterprise: {type: Schema.Types.ObjectId, required: true},
        owner: {type: Schema.Types.ObjectId, required: true},
        commission: {type: Number, required: true},
        photos: [],
        status: {type: Boolean, required: true, default: false},
        url: { type: String, required: false },
        remove: {type: Boolean, required: true, default: false}
    }, {versionKey: false});

    return mongoose.model('Service', serviceSchema);
};
