/**
 * Created by ernestomr87@gmail.com on 16/07/14.
 */

module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var saleSchema = new Schema({
        user: {type: String, required: true},
        signature: {type: Schema.Types.Mixed, required: true},
        bodySale: {type: Schema.Types.Mixed, required: true},
        finish: {type: Boolean, required: true, default: false},
        date: {type: Date, required: true}
    }, {versionKey: false});

    return mongoose.model('Sale', saleSchema);
};
