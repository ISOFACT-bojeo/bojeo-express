/**
 * Created by ernestomr87@gmail.com on 16/07/14.
 */

module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var logsSchema = new Schema({
        error: [],
        date: {type: Date, required: true}
    }, {versionKey: false});

    return mongoose.model('ErrorLogs', logsSchema);
};
