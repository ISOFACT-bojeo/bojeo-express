/**
 * Created by ernestomr87@gmail.com on 04/06/2014.
 */
module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var shipSchema = new Schema({
        name: {type: String, required: true, index: {unique: true}},

        slug: {type: String, required: true},

        description: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        patron: {type: Schema.Types.ObjectId, required: false},
        owner: {
            id: {type: Schema.Types.ObjectId, required: true},
            comitions: {type: Number, required: false}
        },
        tag: {type: Schema.Types.ObjectId, ref: 'Tag', required: true},
        tours: [{type: Schema.Types.ObjectId, ref: 'Tour', required: true}],
        photos: [],
        calendar: {
            start: {type: Date, required: true},
            end: {type: Date, required: true},
            block: [{type: Schema.Types.ObjectId, ref: 'ReservationTour', required: true}]
        },
        remove: {type: Boolean, required: true, default: false},  //cuando se elimina el barco cambia su estado
        available: {type: Boolean, required: true, default: false}
    }, {versionKey: false});

    shipSchema.methods.freeForRemove = function (cb) {
        var ship = this._doc;
        var today = new Date();
        db.ReservationTour.find({remove: false, ship: ship._id, start: {$gt: today}}).exec(function (err, resTs) {
            if (err || !resTs) {
                cb(err, resTs);
            }
            else {
                var array = [];
                for (var i = 0; i < resTs.length; i++) {
                    var aux = {
                        _id: resTs[i]._doc._id,
                        start: resTs[i]._doc.start,
                        end: resTs[i]._doc.end,
                        type: 'Reservation'
                    };
                    array.push(aux);
                }
                cb(null, array);
            }
        });
    };

    return mongoose.model('Ship', shipSchema);
};