/**
 * Created by ernestomr87@gmail.com on 16/07/14.
 */

module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var failedMailsSchema = new Schema({
        mailOptions: {type: Schema.Types.Mixed},
        remove: {type: Boolean, required: true, default: false}
    }, {versionKey: false});

    return mongoose.model('failedMail', failedMailsSchema);
};
