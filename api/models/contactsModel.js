/**
 * Created by ernestomr87@gmail.com on 16/07/14.
 */

module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var contacsSchema = new Schema({
        fullName: { type: String, required: true },
        email: { type: String, required: true },
        city: { type: String, required: true },
        country: { type: String, required: true},
        tel: { type: String, required: false },
        company: { type: String, required: false },
        comment: { type: String, required: true },
        createDate: {type: Date, required: true},
        reading: {type: Boolean, default: false}
    }, { versionKey: false });

    return mongoose.model('Contact', contacsSchema);
};
