/**
 * Created by ernestomr87@gmail.com on 16/07/14.
 */
var async = require('async');
module.exports = function (mongoose) {

    var Schema = mongoose.Schema
        , ObjectId = Schema.ObjectId;

    var packSchema = new Schema({
        tour: {type: ObjectId, required: true},
        slug: {type: String, required: true, unique: true},
        services: [{type: Schema.Types.ObjectId, ref: 'Service', required: true}],
        status: {type: Boolean, required: true, default: false},
        color: {type: String, required: true},
        remove: {type: Boolean, required: true, default: false}
    }, {versionKey: false});

    packSchema.methods.getName = function (callbackM) {
        console.log(this);
        var pack = this._doc;
        async.parallel([
            function (cb) {
                db.Tour.findOne({_id: pack.tour}, function (err, tour) {
                    if (err || !tour) {
                        cb(false);
                    }
                    else {
                        tour.getName(function (name) {
                            cb(null, name);
                        })
                    }
                });
            },
            function (cb) {
                async.map(pack.services, function (service, callback) {
                    db.Service.findOne({_id: service}, 'name', function (err, service) {
                        if (err || !service) {
                            callback(err);
                        }
                        else {
                            callback(null, service._doc.name);
                        }
                    })
                }, function (err, result) {
                    if (err || !result) {
                        cb(err);
                    }
                    else {
                        var name = {
                            es: '',
                            en: ''
                        }
                        for (var i = 0; i < result.length; i++) {
                            name.es = name.es + ' - ' + result[i].es;
                            name.en = name.en + ' - ' + result[i].en;
                        }
                        cb(null, name);

                    }
                });
            }
        ], function (err, result) {
            if (err || !result) {
                callbackM(false);
            }
            else {
                var name = {
                    es: result[0].es + result[1].es,
                    en: result[0].en + result[1].en
                }
                callbackM(name);
            }
        });
    }

    return mongoose.model('Pack', packSchema);
};
