/**
 * Created by Juan Javier on 19/11/2014.
 */
module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var enterpriseSchema = new Schema({
        name: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        slug: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        description: {
            es: {type: String, required: true},
            en: {type: String, required: true}
        },
        photo: {type: Schema.Types.Mixed},
        url: {type: String, required: false},
        remove: {type: Boolean, required: true, default: false},
        available: {type: Boolean, required: true, default: false}
    }, {versionKey: false});

    return mongoose.model('Enterprise', enterpriseSchema);
};