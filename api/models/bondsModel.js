/**
 * Created by ernestomr87@gmail.com on 16/07/14.
 */

module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var bondSchema = new Schema({
        hours: {type: Number, required: true},
        price: {type: Number, required: true},
        available: {type: Boolean, required: true, default: false},
        remove: {type: Boolean, required: true, default: false}
    }, {versionKey: false});

    return mongoose.model('Bond', bondSchema);
};
