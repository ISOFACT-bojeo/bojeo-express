/**
 * Created by ernestomr87@gmail.com on 16/07/14.
 */

module.exports = function (mongoose) {

    var Schema = mongoose.Schema;

    var timeLineSchema = new Schema({
        date: {type: Date, required: true},
        /*
         0 user activation
         1 reservation
         2 refunds
         3 buy bonds

         */
        type: {type: String, required: true},
        info: {type: String, required: true}

    }, {versionKey: false});

    return mongoose.model('TimeLine', timeLineSchema);
};
