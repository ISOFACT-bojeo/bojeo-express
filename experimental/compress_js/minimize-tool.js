/**
 * Created by jeandy.bryan@isofact.com on 12/2/2014.
 */

var uglifyjs = require("uglify-js");
var cssmin = require('cssmin');
var htmlminify = require('html-minifier').minify;
var path = require('path');
var findit = require('findit');
var fs = require('fs');

function minifyIt(type, options, content, callback) {
    if (typeof callback != 'function') {
        return;
    }
    switch (type) {
        case TYPE_JS:
            var opt = extend({fromString: true}, options);
            var result = content;
            try {
                result = uglifyjs.minify(content, opt).code;
            }
            catch (error) {
                console.log(error);
                console.log(content);
            }
            callback(result);
            break;
        case TYPE_CSS:
            var result = content;


            callback(result);
            break;
        case TYPE_HTML:
            var result = content;
            try {
                result = htmlminify(content,
                    {
                        "removeComments": true,
                        "removeCommentsFromCDATA": true,
                        "removeCDATASectionsFromCDATA": false,
                        "collapseWhitespace": true,
                        "conservativeCollapse": false,
                        "collapseBooleanAttributes": false,
                        "removeAttributeQuotes": false,
                        "removeRedundantAttributes": false,
                        "useShortDoctype": false,
                        "removeEmptyAttributes": false,
                        "removeOptionalTags": false,
                        "removeEmptyElements": false,
                        "lint": false,
                        "keepClosingSlash": false,
                        "caseSensitive": false,
                        "minifyJS": true,
                        "minifyCSS": true,
                        "ignoreCustomComments": [],
                        "processScripts": []
                    }
                )
            } catch (err) {

            }
            callback(result);
            break;

        default:
            callback(content);
            break;
    }
}

var finder = findit("c:/z_test/");


finder.on('directory', function (dir, stat, stop) {
    var base = path.basename(dir);
    if (base === 'lib') {
        console.log(dir + '/');
        stop()
    }

});

finder.on('file', function (file, stat) {
    var js_min = ".min.js";
    var ccs_min = ".min.css";
    var ccs_min_temp = ".min.css.temp";
    if (path.extname(file.toLowerCase()) == '.js' && !(file.length > js_min.length &&
        file.toLowerCase().substring(file.length - js_min.length) == js_min)) {
        console.log('JS for compress');
        console.log(file);
    }
    else if (path.extname(file.toLowerCase()) == '.css' && !(file.length > ccs_min.length &&
        file.toLowerCase().substring(file.length - ccs_min.length) == ccs_min) && !(file.length > ccs_min_temp.length &&
        file.toLowerCase().substring(file.length - ccs_min_temp.length) == ccs_min_temp)
        ) {
        var content = fs.readFileSync(file, 'utf-8');
        var result = cssmin(content);
        fs.writeFileSync(file, result);
    }

});



