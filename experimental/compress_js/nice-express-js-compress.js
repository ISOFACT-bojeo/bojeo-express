/**
 * Created by jeandy.bryan@isofact.com on 11/21/2014.
 * _no_compress ?= false
 */


var crypto = require('crypto');
var extend = require('util')._extend;

var onHeaders = require('on-headers');

var uglifyjs = require("uglify-js");
var cssmin = require('cssmin');
var htmlminify = require('html-minifier').minify;


var TYPE_TEXT = 0;
var TYPE_JS = 1;
var TYPE_CSS = 2;
var TYPE_HTML = 3;

var js_patern = /javascript/;
var css_patern = /css/;
var html_patern = /html/;

var memoryCache = {};

function cacheGetMem(hash, callback) {
    if (typeof callback != 'function') {
        return;
    }

    if (typeof memoryCache[hash] == 'undefined') {
        callback(new Error('miss'));
    } else {
        callback(null, memoryCache[hash]);
    }
}

function cachePutMem(hash, minized, callback) {
    memoryCache[hash] = minized;

    if (typeof callback == 'function') {
        callback(null);
    }
}

function minifyIt(type, options, content, callback) {
    if (typeof callback != 'function') {
        return;
    }
    switch (type) {
        case TYPE_JS:
            var opt = extend({fromString: true}, options);
            var result = content;
                try {
                    result = uglifyjs.minify(content, opt).code;
                }
            catch (error){
                console.log(error);
                console.log(content);
            }
            callback(result);
            break;
        case TYPE_CSS:
            var result = content;

                result = cssmin(content);

            callback(result);
            break;
        case TYPE_HTML:
            var result = content;
            try {
                result = htmlminify(content,
                    {
                        "removeComments": true,
                        "removeCommentsFromCDATA": true,
                        "removeCDATASectionsFromCDATA": false,
                        "collapseWhitespace": true,
                        "conservativeCollapse": false,
                        "collapseBooleanAttributes": false,
                        "removeAttributeQuotes": false,
                        "removeRedundantAttributes": false,
                        "useShortDoctype": false,
                        "removeEmptyAttributes": false,
                        "removeOptionalTags": false,
                        "removeEmptyElements": false,
                        "lint": false,
                        "keepClosingSlash": false,
                        "caseSensitive": false,
                        "minifyJS": true,
                        "minifyCSS": true,
                        "ignoreCustomComments": [],
                        "processScripts": []
                    }
                )
            } catch (err) {

            }
            callback(result);
            break;

        default:
            callback(content);
            break;
    }
}


exports.jsCompress = function (active) {

    var cache_get = cacheGetMem;
    var cache_put = cachePutMem;

    return function (req, res, next) {
        //if (!active) return next();

        var write = res.write;
        var end = res.end;

        var buf = null;
        var type = TYPE_TEXT;

        onHeaders(res, function () {
            if (req.method === 'HEAD') {
                return;
            }
            if (req._no_compress) {
                return;
            }
            var contentType = res.getHeader('Content-Type');

            if (contentType === undefined) {
                return;
            }

            if (js_patern.test(contentType)) {
                type = TYPE_JS
            }
            else if (css_patern.test(contentType)) {
                type = TYPE_CSS;
            }
            else if (html_patern.test(contentType)) {
                type = TYPE_HTML;
            }

            if (type == TYPE_TEXT) {
                return;
            }
            res.removeHeader('Content-Length');

            //clear buffer
            buf = [];

        });

        res.write = function (chunk, encoding) {
            if (!this._header) {
                this._implicitHeader();
            }

            if (buf === null || this._no_compress) {
                return write.call(this, chunk, encoding);
            }

            if (!this._hasBody) {
                return true;
            }

            if (typeof chunk !== 'string' && !Buffer.isBuffer(chunk)) {
                throw new TypeError('first argument must be a string or Buffer');
            }

            if (chunk.length === 0) return true;

            // no chunked_encoding here
            if (typeof chunk == 'string') {
                chunk = new Buffer(chunk, encoding);
            }

            buf.push(chunk);
        }

        res.end = function (data, encoding) {
            if (this.finished) {
                return false;
            }

            if (!this._header) {
                this._implicitHeader();
            }

            if (data && !this._hasBody) {
                data = false;
            }

            if (buf === null || this._no_compress) {
                return end.call(this, data, encoding);
            }

            if (data) {
                this.write(data, encoding);
            }

            var buffer = Buffer.concat(buf);

            var sha1 = crypto.createHash('sha1').update(buffer).digest('hex').toString();
            var _this = this;




            cache_get(sha1, function (err, minized) {
                if (err) {
                    switch (type) {
                        case TYPE_TEXT:
                            // impossible to reach here
                            throw new Error('[express-nice-minify] impossible to reach here. Please report the bug.');
                            break;
                        default:
                            var options = {};

                            // cache miss
                            minifyIt(type, options, buffer.toString(encoding), function (minized) {
                                if (_this._no_cache) {
                                    // do not save cache for this response
                                    write.call(_this, minized, 'utf8');
                                    end.call(_this);
                                } else {
                                    cache_put(sha1, minized, function () {
                                        write.call(_this, minized, 'utf8');
                                        end.call(_this);
                                    });
                                }
                            });
                            break;
                    }
                } else {
                    write.call(_this, minized, 'utf8');
                    end.call(_this);
                }
            });
        }

        next();
    }
};