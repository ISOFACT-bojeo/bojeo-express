global.passport = require('passport');
global.localStrategy = require('passport-local').Strategy;
global.facebookStrategy = require('passport-facebook').Strategy;
var controllerUser = require('../api/controllers/user');
var url = require('url');



exports.configPassport = function (app) {
    passport.use(new localStrategy({
            usernameField: 'email',
            passwordField: 'password'
        },
        function (username, password, done) {

            db.User.findOne({ email: username, available: true, remove: false}).exec(function (err, user) {
                if (err || !user) {
                    return done(null, false);
                }

                user.validPassword(password, function (err, isValid) {
                    if (err || !isValid)
                        return done(null, false);

                    var us = {
                        role: user.role,
                        _id: user._id.toString(),
                        username: user.userName,
                        lastname: user.lastName,
                        email: user.email,
                        complete: user.complete,
                        language: user.language

                    }
                    return done(null, us);
                })
            })

        }
    ));



    passport.use(new facebookStrategy({
            clientID: "305599916291450",
            clientSecret: "e12d5f58f49b0e00bea82218c12b79a3",
            callbackURL: url.resolve(global.bojeoServer, "login/facebook/callback"),

        },
        function (accessToken, refreshToken, profile, done) {
            db.User.findOne({ email: profile.emails ? profile.emails[0].value : null, remove: false}).exec(function (err, user) {
                if(user) {
                    var us = {
                        role: user.role,
                        _id: user._id.toString(),
                        username: user.userName,
                        lastname: user.lastName,
                        email: user.email,
                        complete: user.complete,
                        language: user.language

                    }
                    done(err, us);
                }
                else {
                    db.User.findOne({ socialId: profile.id, remove: false}).exec(function (err, user) {
                        if (err)
                            return done(err);
                        if (user) {
                            var us = {
                                role: user.role,
                                _id: user._id.toString(),
                                username: user.userName,
                                lastname: user.lastName,
                                email: user.email,
                                complete: user.complete,
                                language: user.language
                            }
                            done(err, us);
                        }
                        else {
                            controllerUser.createSocialUser(profile, function (err, user){
                                if(err){
                                    return done(err);
                                }else {
                                    var us = {
                                        role: user.role,
                                        _id: user._id.toString(),
                                        username: user.userName,
                                        lastname: user.lastName,
                                        email: user.email,
                                        complete: user.complete,
                                        language: user.language
                                    }
                                    done(err, us);
                                }
                            })

                        }
                    });
                }
            });



        }
    ));

    passport.serializeUser(function (user, done) {
        var id = user._id;
        delete user._is;
        done(null, id);
    });

    passport.deserializeUser(function (id, done) {
        db.User.findOne({ _id: id, remove: false}).exec(function (err, user) {
            var us = {
                role: user.role,
                _id: user._id.toString(),
                username: user.userName,
                lastname: user.lastName,
                email: user.email,
                complete: user.complete,
                language: user.language
            }
            done(err, us);
        });

    });


    app.use(passport.initialize());
    app.use(passport.session());
}