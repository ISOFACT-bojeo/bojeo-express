/**
 * Created by ernestomr87@gmail.com on 8/05/14.
 */

exports.configRoutes = function (app) {

    app.get('*', function (req, res, next) {

        if (req.headers.host != "www.alquilerdebarco.com") {
            res.redirect("http://www.alquilerdebarco.com" + req.url, 301);
            next();
        } else {
            next();
        }
    });

    require('../mongo_tool/express/routes').configRoutes(app);
    require('./routes/apisRest').configRoutes(app);
    require('./routes/backOffice').configRoutes(app);
    require('./routes/frontEnd').configRoutes(app);


    /******************************************************************************************************************/

    app.post('*', function (req, res) {
        res.status(404).json({error: 'Invalid POST request'})
    });
    app.delete('*', function (req, res) {
        res.status(404).json({error: 'Invalid DELETE request'})
    });
    app.put('*', function (req, res) {
        res.status(404).json({error: 'Invalid DELETE request'})
    });


}




