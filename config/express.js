/**
 * Created by ernestomr87@gmail.com on 8/05/14.
 */

var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var multer = require('multer');
var fs = require('fs-extra');
var prerender = require('prerender-node');

var conf = {
    secret: '076ee61d63aa10a125ea872411e433b9'
};



exports.configExpress = function (app, mongoose) {

    prerender.set('prerenderToken', 'JC12LJ8OA2U8rOUyZfFK');
    prerender.set('beforeRender', function (req, done) {
        done();
    });
    prerender.set('afterRender', function (req, prerender_res) {
        console.log('end =========================');
        console.log(req.url);
        console.log(prerender_res.body);
    });
    app.use(prerender);


    //app.set('view engine', 'html');
    //app.engine('html', require('ejs').renderFile);


    app.set('views', path.join(__dirname, '../views'));
    app.set('view engine', 'ejs');
    app.locals.pretty = true;

    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded());
    app.use(cookieParser()); // required before session.

    app.use(session({
        secret: conf.secret,
        store: new MongoStore({ mongooseConnection: mongoose.connection }),
        cookie: {maxAge: 1000 * 60 * 60 * 12}
    }));



    app.use(multer(
        {
            onFileUploadComplete: function (file) {
                console.log("nulter alert");
                console.log(file);
                var ext = ['txt', 'pdf', 'doc', 'docx', 'jpg', 'png', 'gif', 'bmp'];
                var tmp = false;
                for (var i = 0; i < ext.length; i++) {
                    if (file.extension.toLowerCase() == ext[i]) {

                        var a = new db.Media({});
                        fs.readFile(file.path, function (err, data) {
                            a.contentType = file.mimetype;
                            a.fieldName = file.name;
                            a.data = data;
                            a.save(function (err, a) {
                                if (err) console.log(err);
                                fs.unlink(file.path, function (err) {
                                    if (err)
                                        console.log(err);
                                });
                            });
                        });
                    }
                }
            }
        }));


    app.use(function (req, res, next) {
        if(/api\//.test(req.url) &&
            !/api\/media\//.test(req.url))
        {
            res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            res.setHeader("Pragma", "no-cache");
            res.setHeader("Expires", 0);
            return next();
        }
        var oneYear = 31556900000;
        if (!res.getHeader('Cache-Control')){
            res.setHeader('Cache-Control', 'public, max-age=' + (oneYear));
            res.setHeader('Expires', new Date(Date.now() + 31556900000));
        }
        next();
    });


    app.use(express.static(path.join(__dirname, '../public')));

    function logErrors(err, req, res, next) {
        console.error(err.stack);
        next(err);
    };

    function clientErrorHandler(err, req, res, next) {
        if (req.xhr) {
            res.send(500, {error: 'Something blew up!'});
        } else {
            next(err);
        }
    };

    function errorHandler(err, req, res, next) {
        res.status(500);
        res.render('error', {error: err});
    };

    if (app.get('env') === 'development') {
        app.use(function (err, req, res, next) {
            res.status(err.status || 500);
            res.end(err.message);
        });
    }

    app.use(function (err, req, res, next) {
        console.log(err.message);
        /*res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });*/
    });




// CORS support
    app.all('*', function (req, res, next) {
        if (!req.get('Origin')) return next();
        // use "*" here to accept any origin
        res.set('*', 'http://' + bojeoServer);
        res.set('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
        res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type');
        res.set('Access-Control-Allow-Credentials', 'true');
        if ('OPTIONS' == req.method) return res.send(200);
        next();
    })


}
