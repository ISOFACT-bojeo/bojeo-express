/**
 * Created by ernestomr87@gmail.com on 7/05/14.
 */


exports.connect = function (cb) {
    if (!global.hasOwnProperty('db')) {

        var mongoose = require('mongoose');
        global.conection = false;

        var mongoseServerConf = {
            auto_reconnect: true,
            poolSize: 50,
            socketOptions: {
                keepAlive: 1
            }
        }

        // the application is executed on the local machine ...
        var mongoseDB = mongoose.connection;

        mongoseDB.on('connecting', function () {
            console.log('connecting to MongoDB...');
        });
        mongoseDB.on('error', function (error) {
            console.error('Error in MongoDb connection: ' + error);
            global.conection = false;
            mongoose.disconnect();
        });
        mongoseDB.on('connected', function () {
            global.conection = true;
            console.log('MongoDB connected!');
            if(cb)
            cb(mongoose);
        });
        mongoseDB.once('open', function () {
            console.log('MongoDB connection opened!');
        });
        mongoseDB.on('reconnected', function () {
            console.log('MongoDB reconnected!');
        });
        mongoseDB.on('disconnected', function () {
            console.log('MongoDB disconnected!');
            mongoose.connect(mongodbServer, {server: mongoseServerConf});
        });
        mongoose.connect(mongodbServer, {server: mongoseServerConf});


        global.db = {
            mongoose: mongoose,
            //models
            User: require('./../api/models/userModel')(mongoose),
            News: require('./../api/models/newsModel')(mongoose),
            Ship: require('./../api/models/shipModel')(mongoose),
            Group: require('./../api/models/groupModel')(mongoose),
            Tour: require('./../api/models/toursModel')(mongoose),
            ReservationClient: require('./../api/models/reservationClientModel')(mongoose),
            ReservationTour: require('./../api/models/reservationTourModel')(mongoose),
            Port: require('./../api/models/portsModel')(mongoose),
            Bonds: require('./../api/models/bondsModel')(mongoose),
            GeneralInfo: require('./../api/models/generalInfoModel')(mongoose),
            Contact: require('./../api/models/contactsModel')(mongoose),
            Email: require('./../api/models/emailModel')(mongoose),
            Country: require('./../api/models/countryModel')(mongoose),
            Media: require('./../api/models/mediaModel')(mongoose),
            Service: require('./../api/models/serviceModel')(mongoose),
            Enterprise: require('./../api/models/enterpriseModel')(mongoose),
            Timeline: require('./../api/models/timeLine')(mongoose),
            ErrorLogs: require('./../api/models/errorLogs')(mongoose),
            FailedMails: require('./../api/models/failedMailsModel')(mongoose),
            Sale: require('./../api/models/saleModel')(mongoose),
            Pack: require('./../api/models/packModel')(mongoose),
            Tags: require('./../api/models/tagModel')(mongoose)
        };
    }
}



