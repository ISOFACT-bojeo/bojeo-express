/**
 * Created by ernestomr87@gmail.com on 10/05/14.
 */

var nodemailer = require("nodemailer");

/*
console.log(global.mailServer);
console.log(global.mailSMTPPort);
console.log(global.mailUser);
console.log(global.mailPass);*/


// create reusable transport method (opens pool of SMTP connections)
var smtpTransport = nodemailer.createTransport("SMTP", {
    host: global.mailServer, // hostname
    secureConnection: false, // not use SSL
    port: global.mailSMTPPort, // port for secure SMTP
    auth: {
        user: global.mailUser,
        pass: global.mailPass
    }
});

exports.notificationRegisterMail = function (req, res, uid) {
    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: "Bojeo <" + global.mailUser + ">", // sender address
        to: req.body.email, // list of receivers
        subject: "Confirmación de Registro", // Subject line
        text: "Para activar su cuenta de click en éste link : http://" + bojeoServer + "/login/" + uid // plaintext body
        //html: "<b>Hello world ✔</b>" // html body
    }

    // send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (err) {
        if (err) return res.json(err);
        smtpTransport.close()
        return res.json(true);

        ; // shut down the connection pool, no more messages
    });

}

exports.notificationPromotionalMail = function (req, res, uid) {
    for(var i=0; i< req.body.mails.length; i++){
        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: "Bojeo <" + global.mailUser + ">", // sender address
            to: req.body.mails[i], // list of receivers
            subject: req.body.subject, // Subject line
            text: req.body.body // plaintext body
        }

        // send mail with defined transport object
        smtpTransport.sendMail(mailOptions, function (err) {
            if (err) return res.json(err);
            smtpTransport.close()
            return res.json(true);

            ; // shut down the connection pool, no more messages
        });
    }

}


exports.templateActivationMail = function (req, res, uid) {
    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: "Bojeo <" + global.mailUser + ">", // sender address
        to: req.body.email, // list of receivers
        subject: "Confirmación de Registro", // Subject line
        text: "Para activar su cuenta de click en éste link : http://" + bojeoServer + "/login/" + uid // plaintext body
        //html: "<b>Hello world ✔</b>" // html body
    }

    // send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (err) {
        if (err) return res.json(err);
        smtpTransport.close()
        return res.json(true);

        ; // shut down the connection pool, no more messages
    });

}
exports.templateForgivePassword = function (req, res, uid) {
    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: "Bojeo <" + global.mailUser + ">", // sender address
        to: req.body.email, // list of receivers
        subject: "Cambio de contraseña", // Subject line
        text: "Para cambiar su contraseña haga click en este link : http://" + bojeoServer + "/changePass/" + uid // plaintext body
        //html: "<b>Hello world ✔</b>" // html body
    }

    // send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (err) {
        if (err) return res.json(err);
        smtpTransport.close()
        return res.json(true);

        ; // shut down the connection pool, no more messages
    });

}



