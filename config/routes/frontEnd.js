/**
 * Created by Nesto on 19/11/2014.
 */
var session = require('../../api/controllers/session');
var portsController = require('../../api/controllers/ports');
var toursController = require('../../api/controllers/tours');
var groupController = require('../../api/controllers/group');
var async = require('async');

var result = {
    BOJEO_COPYRIGHTS: null,
    NOTICECOOKIES: null,
    BOJEO_COUNTRIES: null
}

exports.configRoutes = function (app) {


    function startVars(req, res, cb) {
        async.series([
            function (callback) {
                db.GeneralInfo.findOne({active: true}).exec(function (err, success) {
                    if (err || !success) {
                        callback(true, null);
                    }
                    else {
                        result.BOJEO_COPYRIGHTS = success._doc.copyRights;
                        result.NOTICECOOKIES = success._doc.noticeCookies;
                        callback(false, true);
                    }
                })
            },
            function (callback) {
                initializeVars(function (err, countries) {
                    if (err || !countries) {
                        callback(true, null);
                    }
                    else {
                        result.BOJEO_COUNTRIES = countries;
                        callback(false, true);
                    }
                });
            }
        ], function (err) {
            if (err) {
                cb(false);
            } else {
                cb(true);
            }
        })
    };

    function initializeVars(cb) {
        db.Country.find(function (err, countries) {
            if (err || !countries) {
                cb(err, countries);
            }
            else {
                async.map(countries, function (country, callback) {
                        async.map(country.cities, function (city, callback1) {
                                db.Port.find({
                                    remove: false,
                                    available: true,
                                    'country.es': country._doc.name.es,
                                    'country.en': country._doc.name.en,
                                    'city.es': city.es,
                                    'city.en': city.en
                                }).exec(function (err, ports) {
                                    if (err || !ports) {
                                        callback1(err, ports);
                                    }
                                    else {
                                        async.map(ports, function (port, callback2) {
                                                db.Tour.find({
                                                    remove: false,
                                                    available: true,
                                                    port: port._doc._id
                                                }).select('photos data name group').exec(function (err, tours) {
                                                        if (err || !ports) {
                                                            callback2(err, ports);
                                                        }
                                                        else {
                                                            db.Group.find().select().exec(
                                                                function (err, groups) {
                                                                    if (err || !groups) {
                                                                        callback2(err, groups);
                                                                    }
                                                                    else {
                                                                        for (var i = 0; i < groups.length; i++) {
                                                                            groups[i]._doc.tours = [];
                                                                            for (var j = 0; j < tours.length; j++) {
                                                                                if (tours[j]._doc.group.toString() == groups[i]._doc._id.toString()) {
                                                                                    groups[i]._doc.tours.push(tours[j]);
                                                                                }
                                                                            }
                                                                        }
                                                                        var arrayGroups = [];
                                                                        for (var i = 0; i < groups.length; i++) {
                                                                            if (groups[i]._doc.tours.length) {
                                                                                arrayGroups.push(groups[i]);
                                                                            }
                                                                        }
                                                                        port._doc.groups = arrayGroups;
                                                                        callback2(err, port);
                                                                    }
                                                                }
                                                            );

                                                        }
                                                    }
                                                );
                                            }, function (err, result) {
                                                if (err || !result) {
                                                    city._doc.ports = [];
                                                    callback1(err, city);
                                                }
                                                else {
                                                    city._doc.ports = result;
                                                    callback1(err, city);
                                                }
                                            }
                                        )
                                    }
                                });
                            }, function (err, result) {
                                if (err || !result) {
                                    country._doc.cities = [];
                                }
                                else {
                                    country._doc.cities = result;
                                    callback(err, country);
                                }

                            }
                        )
                    }, function (err, result) {
                        if (err || !result) {
                            cb(err, result);
                        }
                        else {
                            var countries = [];
                            for (var i = 0; i < result.length; i++) {
                                if (result[i]._doc.cities.length) {
                                    var cities = [];
                                    for (var j = 0; j < result[i]._doc.cities.length; j++) {
                                        if (result[i]._doc.cities[j]._doc.ports.length) {
                                            var ports = [];
                                            for (var k = 0; k < result[i]._doc.cities[j]._doc.ports.length; k++) {
                                                if (result[i]._doc.cities[j]._doc.ports[k]._doc.groups.length) {
                                                    ports.push(result[i]._doc.cities[j]._doc.ports[k]);
                                                }
                                            }
                                            if (ports.length) {
                                                var aux = {
                                                    name: {
                                                        es: result[i]._doc.cities[j]._doc.es,
                                                        en: result[i]._doc.cities[j]._doc.en
                                                    },
                                                    ports: ports
                                                }
                                                cities.push(aux);
                                            }
                                        }
                                    }
                                    if (cities.length) {
                                        var aux = {
                                            name: {
                                                es: result[i]._doc.name.es,
                                                en: result[i]._doc.name.en
                                            },
                                            cities: cities
                                        }
                                        countries.push(aux);
                                    }
                                }
                            }
                            ;
                            cb(null, countries);
                        }

                    }
                )
            }
        });
    };


    app.get('/frontend/index', function (req, res) {
        res.render(('frontend/views/default'));
    });
    app.get('/frontend/return', function (req, res) {
        res.render(('frontend/views/return'));
    });
    app.get('/frontend/register', function (req, res) {
        res.render(('frontend/views/register'));
    });
    app.get('/frontend/receipt', function (req, res) {
        res.render(('frontend/views/receipt'));
    });
    app.get('/frontend/about', function (req, res) {
        res.render(('frontend/views/about'));
    });
    app.get('/frontend/where', function (req, res) {
        res.render(('frontend/views/where'));
    });
    app.get('/frontend/news', function (req, res) {
        res.render(('frontend/views/news'));
    });
    app.get('/frontend/new', function (req, res) {
        res.render(('frontend/views/new'));
    });
    app.get('/frontend/tour', function (req, res) {
        res.render(('frontend/views/tour'));
    });
    app.get('/frontend/toursEx', function (req, res) {
        res.render(('frontend/views/toursEx'));
    });
    app.get('/frontend/payForm', function (req, res) {
        res.render(('frontend/views/payForm'));
    });
    app.get('/frontend/searchResult', function (req, res) {
        res.render(('frontend/views/searchResult'));
    });
    app.get('/frontend/map', function (req, res) {
        res.render(('frontend/views/map'));
    });
    app.get('/frontend/buy', function (req, res) {
        res.render(('frontend/views/buy'));
    });
    app.get('/frontend/profile', session.nocache, function (req, res) {
        res.render(('frontend/views/profile'));
    });
    app.get('/frontend/login', function (req, res) {
        res.render(('frontend/views/login'));
    });
    app.get('/frontend/activatelogin', function (req, res) {
        res.render(('frontend/views/activatelogin'));
    });
    app.get('/frontend/useTerm', function (req, res) {
        res.render(('frontend/views/useTerm'));
    });
    app.get('/frontend/forgivepass', function (req, res) {
        res.render(('frontend/views/forgivepass'));
    });
    app.get('/frontend/changePass', function (req, res) {
        res.render(('frontend/views/changePass'));
    });
    app.get('/frontend/gallery', function (req, res) {
        res.render(('frontend/views/gallery'));
    });
    app.get('/frontend/bulletin', function (req, res) {
        res.render(('frontend/views/bulletin'));
    });
    app.get('/frontend/errorConection', function (req, res) {
        res.render(('frontend/views/errorConection'));
    });
    app.get('/frontend/occupiedBooking', function (req, res) {
        res.render(('frontend/views/occupiedBooking'));
    });
    app.get('/frontend/enterprise', function (req, res) {
        res.render(('frontend/views/enterprise'));
    });
    app.get('/frontend/cookies', function (req, res) {
        res.render(('frontend/views/cookies'));
    });
    app.get('/frontend/404', function (req, res) {
        res.render(('frontend/views/404'));
    });


    app.get('/:culture(es|en)/pay-form', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture/search/:text', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/map', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/buy', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/profile', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/profile/:buy', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/login', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/login/:token', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/terms-of-use', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/forgot-password', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/change-password/:tokenPass', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/gallery', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/bulletin', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/error-conection', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/occupied-booking', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/enterprise', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/cookies', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/refund', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/refund/:returnId', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/register', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/about', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/contact', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/news/:id', function (req, res) {
        goToIndex(req, res);
    });
    app.get('/:culture(es|en)/news', function (req, res) {
        goToIndex(req, res);
    });

    app.get('/:culture(es|en)/receipt', function (req, res) {
        goToIndex(req, res);
    });

    app.get('/:culture(es|en)/404', function (req, res) {
        goToIndex(req, res, "404");
    });

    app.get('/:culture(es|en)/:group/:slug', function (req, res) {
        db.Tour.findOne({$or : [{'slug.en': req.params.slug},{'slug.es': req.params.slug}]}).exec(function (err, success) {
            if (err || !success) {
                goToIndex(req, res, "404");
            }
            else {
                db.Group.findOne({$or : [{'slug.en': req.params.group},{'slug.es': req.params.group}]}).exec(function (err, success) {
                    if (err || !success) {
                        goToIndex(req, res, "404");
                    }
                    else {
                        goToIndex(req, res);

                    }
                })
            }
        })
    });
    app.get('/:culture(es|en)/:id', function (req, res) {
        db.Group.findOne({
            $or: [{'slug.es': req.params.id}, {'slug.en': req.params.id}]
        }).exec(function (err, success) {
            if (err || !success) {
                goToIndex(req, res, "404");
            }
            else {
                goToIndex(req, res);

            }
        })
    });


    app.get('/:culture(es|en)', function (req, res) {
        goToIndex(req, res);
    });

    app.get('/sitemap.xml', function (req, res) {
        async.parallel([
                toursController.getAviableTours,
                groupController.getGroupList
            ],
            function (err, results) {
                res.contentType("text/xml");
                res.render('frontend/views/sitemap',
                    {
                        tours: results[0],
                        groups: results[1]
                    });
            });


    });

    app.get('/', function (req, res) {
        if (req.user) {
            if (req.user.language == 'en') {
                res.redirect('/' + req.user.language, 301)
            }
            else {
                goToIndex(req, res);
            }
        }
        else {
            goToIndex(req, res);
        }
    });

    function goToIndex(req, res, status) {
        if (!status) status = "200";
        req.body.local = true;
        startVars(req, res, function (data) {
            if (data) {
                res.status(status).render('frontend/index', {
                    BOJEO_COUNTRIES: result.BOJEO_COUNTRIES,
                    BOJEO_COPYRIGHTS: result.BOJEO_COPYRIGHTS,
                    NOTICECOOKIES: result.NOTICECOOKIES
                });
            }
            else {
                res.status(status).render('frontend/index', {
                    BOJEO_COUNTRIES: new Array(),
                    BOJEO_COPYRIGHTS: result.BOJEO_COPYRIGHTS,
                    NOTICECOOKIES: result.NOTICECOOKIES
                });
            }
        })
    }

    app.post('/api/vars', function (req, res) {
        req.body.local = true;
        startVars(req, res, function (data) {
            if (data) {
                return res.json({
                    BOJEO_COUNTRIES: result.BOJEO_COUNTRIES,
                    BOJEO_COPYRIGHTS: result.BOJEO_COPYRIGHTS,
                    NOTICECOOKIES: result.NOTICECOOKIES
                });
            }
            else {
                return res.json({
                    BOJEO_COUNTRIES: new Array(),
                    BOJEO_COPYRIGHTS: result.BOJEO_COPYRIGHTS,
                    NOTICECOOKIES: result.NOTICECOOKIES
                });
            }

        })
    })

    app.get('*', function (req, res) {
        var status = "404";
        req.body.local = true;
        goToIndex(req, res, status);
    });
}


