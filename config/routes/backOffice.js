/**
 * Created by Nesto on 19/11/2014.
 */
var session = require('../../api/controllers/session');

exports.configRoutes = function (app) {
    /******************************************************************************************************************/
    /*Back Office*/
    app.get('/backoffice/login', function (req, res) {
        if (req.session.user) {
            if (req.session.user.role == 'admin')
                res.redirect('/backoffice');
            else res.redirect('/');
        }
        else res.render(('backoffice/login'));
    });
    app.get('/backoffice',session.nocache, session.admin, function (req, res) {
        res.render(("backoffice/Index"));
    });
    app.get('/backoffice/news', session.admin, function (req, res) {
        res.render(('backoffice/views/news'));
    });
    app.get('/backoffice/listNews', session.admin, function (req, res) {
        res.render(('backoffice/views/listNews'));
    });
    app.get('/backoffice/listUsers', session.admin, function (req, res) {
        res.render(('backoffice/views/listUsers'));
    });
    app.get('/backoffice/tours', session.admin, function (req, res) {
        res.render(('backoffice/views/tours'));
    });
    app.get('/backoffice/addTours', session.admin, function (req, res) {
        res.render(('backoffice/views/addTours'));
    });
    app.get('/backoffice/ships', session.admin, function (req, res) {
        res.render(('backoffice/views/ships'));
    });
    app.get('/backoffice/listShips', session.admin, function (req, res) {
        res.render(('backoffice/views/listShips'));
    });

    app.get('/backoffice/calendar', session.admin, function (req, res) {
        res.render(('backoffice/views/calendar'));
    });
    app.get('/backoffice/home', session.admin, function (req, res) {
        res.render(('backoffice/views/home'));
    });
    app.get('/backoffice/viewCalendar', session.admin, function (req, res) {
        res.render(('backoffice/views/viewCalendar'));
    });
    app.get('/backoffice/editCalendar', session.admin, function (req, res) {
        res.render(('backoffice/views/editCalendar'));
    });
    app.get('/backoffice/reservations', session.admin, function (req, res) {
        res.render(('backoffice/views/reservations'));
    });
    app.get('/backoffice/generalInfo', session.admin, function (req, res) {
        res.render(('backoffice/views/generalInfo'));
    });

    app.get('/backoffice/listGeneralInfo', session.admin, function (req, res) {
        res.render(('backoffice/views/listGeneralInfo'));
    });
    app.get('/backoffice/listPorts', session.admin, function (req, res) {
        res.render(('backoffice/views/listPorts'));
    });
    app.get('/backoffice/email', session.admin, function (req, res) {
        res.render(('backoffice/views/email'));
    });
    app.get('/backoffice/listContact', session.admin, function (req, res) {
        res.render(('backoffice/views/listContact'));
    });
    app.get('/backoffice/devolution', session.admin, function (req, res) {
        res.render(('backoffice/views/devolution'));
    });
    app.get('/backoffice/abono', session.admin, function (req, res) {
        res.render(('backoffice/views/abono'));
    });

    app.get('/backoffice/tags', session.admin, function (req, res) {
        res.render(('backoffice/views/tags'));
    });

    app.get('/backoffice/countries', session.admin, function (req, res) {
        res.render(('backoffice/views/countries'));
    });

    app.get('/backoffice/graphics', session.admin, function (req, res) {
        res.render(('backoffice/views/graphics'));
    });
    app.get('/backoffice/services', session.admin, function (req, res) {
        res.render(('backoffice/views/services'));
    });
    app.get('/backoffice/enterprise', session.admin, function (req, res) {
        res.render(('backoffice/views/enterprise'));
    });
    app.get('/backoffice/packages', session.admin, function (req, res) {
        res.render(('backoffice/views/packages'));
    });
    app.get('/backoffice/invoice', session.admin, function (req, res) {
        res.render(('backoffice/views/invoice'));
    });

}

