/**
 * Created by Nesto on 19/11/2014.
 */

var statistics = require('../../api/controllers/statistics');
var news = require('../../api/controllers/news');
var users = require('../../api/controllers/user');
var session = require('../../api/controllers/session');
var tours = require('../../api/controllers/tours');
var reservation = require('../../api/controllers/reservations');
var ships = require('../../api/controllers/ship');
var ports = require('../../api/controllers/ports');
var bonds = require('../../api/controllers/bonds');
var generalInformation = require('../../api/controllers/generalInfo');
var contacts = require('../../api/controllers/contacts');
var email = require('../../api/controllers/email');
var media = require('../../api/controllers/media');
var utils = require('../../api/utils');
var groups = require('../../api/controllers/group');
var country = require('../../api/controllers/country');
var service = require('../../api/controllers/service');
var enterprise = require('../../api/controllers/enterprise');
var packages = require('../../api/controllers/pack');
var timeline = require('../../api/controllers/timeLine');
var tags = require('../../api/controllers/tags');
exports.configRoutes = function (app) {


    app.get("/slug", function (req, res) {
        var async = require('async');
        var utils = require('./../../api/utils');
        async.parallel([
            function (cb) {
                db.Enterprise.find(function (err, enterprises) {
                    if (err || !enterprises) {
                        cb(null, null)
                    }
                    else {
                        async.map(enterprises, function (enterprise, callback) {
                            db.Enterprise.update({_id: enterprise._doc._id},
                                {
                                    $set: {
                                        slug: {
                                            en: utils.getSlug(enterprise.name.en),
                                            es: utils.getSlug(enterprise.name.es)
                                        }
                                    }
                                }).exec(function (err, succces) {
                                    if (err || !succces) {
                                        callback(null, null)
                                    }
                                    else {
                                        callback(err, succces)
                                    }
                                })
                        }, function (err, result) {
                            cb(err, result)
                        });
                    }
                })
            },
            function (cb) {
                db.Group.find(function (err, groups) {
                    if (err || !groups) {
                        cb(null, null)
                    }
                    else {
                        async.map(groups, function (group, callback) {
                            db.Group.update({_id: group._doc._id},
                                {
                                    $set: {
                                        slug: utils.getSlug(group.name.en)
                                    }
                                }).exec(function (err, succces) {
                                    if (err || !succces) {
                                        callback(null, null)
                                    }
                                    else {
                                        callback(err, succces)
                                    }
                                })
                        }, function (err, result) {
                            cb(err, result)
                        });
                    }
                })
            },
            function (cb) {
                db.News.find(function (err, news) {
                    if (err || !news) {
                        cb(null, null)
                    }
                    else {
                        async.map(news, function (nnew, callback) {
                            db.News.update({_id: nnew._doc._id},
                                {
                                    $set: {
                                        slug: {
                                            en: utils.getSlug(nnew.title.en),
                                            es: utils.getSlug(nnew.title.es)
                                        }
                                    }
                                }).exec(function (err, succces) {
                                    if (err || !succces) {
                                        callback(null, null)
                                    }
                                    else {
                                        callback(err, succces)
                                    }
                                })
                        }, function (err, result) {
                            cb(err, result)
                        });
                    }
                })
            },
            function (cb) {
                db.Port.find(function (err, ports) {
                    if (err || !ports) {
                        cb(null, null)
                    }
                    else {
                        async.map(ports, function (port, callback) {
                            db.Port.update({_id: port._doc._id},
                                {
                                    $set: {
                                        slug: {
                                            en: utils.getSlug(port.name.en),
                                            es: utils.getSlug(port.name.es)
                                        }
                                    }
                                }).exec(function (err, succces) {
                                    if (err || !succces) {
                                        callback(null, null)
                                    }
                                    else {
                                        callback(err, succces)
                                    }
                                })
                        }, function (err, result) {
                            cb(err, result)
                        });
                    }
                })
            },
            function (cb) {
                db.Service.find(function (err, services) {
                    if (err || !services) {
                        cb(null, null)
                    }
                    else {
                        async.map(services, function (service, callback) {
                            db.Service.update({_id: service._doc._id},
                                {
                                    $set: {
                                        slug: {
                                            en: utils.getSlug(service.name.en),
                                            es: utils.getSlug(service.name.es)
                                        }
                                    }
                                }).exec(function (err, succces) {
                                    if (err || !succces) {
                                        callback(null, null)
                                    }
                                    else {
                                        callback(err, succces)
                                    }
                                })
                        }, function (err, result) {
                            cb(err, result)
                        });
                    }
                })
            },
            function (cb) {
                db.Ship.find(function (err, ships) {
                    if (err || !ships) {
                        cb(null, null)
                    }
                    else {
                        async.map(ships, function (ship, callback) {
                            db.Ship.update({_id: ship._doc._id},
                                {
                                    $set: {
                                        slug: {
                                            en: utils.getSlug(ship.name),
                                            es: utils.getSlug(ship.name)
                                        }
                                    }
                                }).exec(function (err, succces) {
                                    if (err || !succces) {
                                        callback(null, null)
                                    }
                                    else {
                                        callback(err, succces)
                                    }
                                })
                        }, function (err, result) {
                            cb(err, result)
                        });
                    }
                })
            },
            function (cb) {
                db.Tour.find(function (err, tours) {
                    if (err || !tours) {
                        cb(null, null)
                    }
                    else {
                        async.map(tours, function (tour, callback) {
                            db.Tour.update({_id: tour._doc._id},
                                {
                                    $set: {
                                        slug: {
                                            en: utils.getSlug(tour.name.en),
                                            es: utils.getSlug(tour.name.es)
                                        }
                                    }
                                }).exec(function (err, succces) {
                                    if (err || !succces) {
                                        callback(null, null)
                                    }
                                    else {
                                        callback(err, succces)
                                    }
                                })
                        }, function (err, result) {
                            cb(err, result)
                        });
                    }
                })
            },
            function (cb) {
                db.User.find(function (err, users) {
                    if (err || !users) {
                        cb(null, null)
                    }
                    else {
                        async.map(users, function (user, callback) {
                            db.User.update({_id: user._doc._id},
                                {
                                    $set: {
                                        slug:  utils.getSlug(user.userName + user.lastName)
                                    }
                                }).exec(function (err, succces) {
                                    if (err || !succces) {
                                        callback(null, null)
                                    }
                                    else {
                                        callback(err, succces)
                                    }
                                })
                        }, function (err, result) {
                            cb(err, result)
                        });
                    }
                })
            },

        ], function (err, result) {
            if (err || !result) {
                return res.json("Error: " + err + "</br>" + "Result: " + result);
            }
            else {
                return res.json("Finish OK");
            }
        })
    })
    /******************************************************************************************************************/
    app.post('/post', function (req, res) {
        res.json({name: req.body.name, pass: req.body.pass});
    });
    app.post('/login', session.nocache,
        passport.authenticate('local', {
            successRedirect: '/',
            failureRedirect: '/es/login#error',
            failureFlash: false
        })
    );
    app.post('/loginB', session.nocache,
        passport.authenticate('local', {
            successRedirect: '/backoffice',
            failureRedirect: '/backoffice/login#error',
            failureFlash: false
        })
    );
    app.get('/logout', session.nocache, function (req, res) {
        req.logout();
        res.redirect('/');
    });
    //login facebook
    app.post('/login/facebook', passport.authenticate('facebook', {scope: 'email'}));
    app.get('/login/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect: '/',
            failureRedirect: '/es/login#error'
        })
    );

    app.get('/date', function (req, res) {
        var date = new Date();
        var y = date.getFullYear();
        var m = date.getMonth();
        var d = date.getDate();
        return res.json({y: y, m: m, d: d});
    });

    /*RESERVATIONS*/
    var PATH_RESERVATION = '/api/reservation';


    app.post(PATH_RESERVATION + '/save', session.auth, reservation.saveSession);
    app.get(PATH_RESERVATION + '/pay', session.auth, reservation.create);
    app.get(PATH_RESERVATION + '/cancel', session.auth, reservation.refundReservation);
    app.get(PATH_RESERVATION + '/return', session.auth,session.nocache, reservation.reservationReturn);
    app.get(PATH_RESERVATION + '/returnone', session.auth,session.nocache, reservation.reservationReturn);
    app.post(PATH_RESERVATION + '/return', session.auth, reservation.reservationReturn);
    app.get(PATH_RESERVATION + '/bonus/return', session.auth, reservation.reservationBonoReturn);
    app.get(PATH_RESERVATION + '/payCancel',session.nocache, reservation.reservationCancel);
    app.get(PATH_RESERVATION + '/:id', session.auth, reservation.get);
    app.post(PATH_RESERVATION + '/cancel', session.auth, reservation.refundReservation);
    app.post(PATH_RESERVATION, session.admin, reservation.list);
    app.post(PATH_RESERVATION + '/getRTbyShip', session.admin, reservation.getReservationTourByShip1);
    app.post(PATH_RESERVATION + '/saveBookInfo', session.auth, reservation.saveBookingInfo);
    app.post(PATH_RESERVATION + '/getBookingInfo', session.auth, reservation.getBookingInfo);


    //tpv
    app.post(PATH_RESERVATION + '/tpvForm', reservation.tpvForm);

    /*CONTACTS*/
    var PATH_CONTACTS = '/api/contacts';
    app.get(PATH_CONTACTS, session.admin, contacts.list);
    app.post(PATH_CONTACTS, contacts.create);
    app.delete(PATH_CONTACTS + "/:id", session.admin, contacts.remove);
    app.put(PATH_CONTACTS, session.admin, contacts.changeReading);

    /*BONDS*/
    var PATH_ABONOS = '/api/abonos';
    app.post(PATH_ABONOS, session.admin, bonds.create);
    app.put(PATH_ABONOS, session.admin, bonds.edit);
    app.get(PATH_ABONOS, bonds.list);
    app.post(PATH_ABONOS + '/back', session.admin, bonds.listBack);
    app.post(PATH_ABONOS + '/status', session.admin, bonds.changeStatus);
    app.post(PATH_ABONOS + '/remove', session.admin, bonds.remove);
    app.post(PATH_ABONOS + '/tour', session.admin, bonds.tourByBond);

    /*TAGS*/
    var PATH_TAGS = '/api/tags';
    app.post(PATH_TAGS, session.admin, tags.create);
    app.get(PATH_TAGS + "/:id", session.admin, tags.get);
    app.get(PATH_TAGS, session.admin, tags.list);
    app.put(PATH_TAGS, session.admin, tags.update);
    app.delete(PATH_TAGS + "/:id", session.admin, tags.remove);
    app.post(PATH_TAGS + '/status', session.admin, tags.changeStatus);

    /*GENERAL INFORMATIONS*/
    var PATH_INFO_GENERAL = '/api/generalInfo';
    app.post(PATH_INFO_GENERAL, session.admin, generalInformation.create);
    app.put(PATH_INFO_GENERAL, session.admin, generalInformation.update);
    app.delete(PATH_INFO_GENERAL + "/:id", session.admin, generalInformation.remove);
    app.get(PATH_INFO_GENERAL, session.admin, generalInformation.list);
    app.put(PATH_INFO_GENERAL + '/public', session.admin, generalInformation.publicated);
    app.post(PATH_INFO_GENERAL + '/getInfo', generalInformation.getInfo);
    app.post(PATH_INFO_GENERAL + "/updateImg", session.admin, generalInformation.updateImg);
    app.post(PATH_INFO_GENERAL + '/addAboutBojeoImgs', session.admin, generalInformation.addMedia);
    app.post(PATH_INFO_GENERAL + '/delAboutBojeoImg', session.admin, generalInformation.removeOnlyMedia);

    /*NEWS*/
    var PATH_NEWS = '/api/news';
    app.post(PATH_NEWS, session.admin, news.create);
    app.post(PATH_NEWS + '/listBack', session.admin, news.listBack);
    app.post(PATH_NEWS + '/delNewsImg', session.admin, news.removeOnlyImg);
    app.post(PATH_NEWS + '/delNewsDoc', session.admin, news.removeOnlyDoc);
    app.get(PATH_NEWS + "/:id", news.get);
    app.get(PATH_NEWS, news.list);
    app.put(PATH_NEWS, session.admin, news.update);
    app.put(PATH_NEWS + '/public', session.admin, news.publicated);
    app.delete(PATH_NEWS + "/:id", session.admin, news.remove);
    app.post(PATH_NEWS + "/updateImgDoc", session.admin, news.updateImgDoc);
    app.post(PATH_NEWS + "/updateDoc", session.admin, news.updateDoc);

    /*USERS*/
    var PATH_USERS = '/api/users';
    app.post(PATH_USERS, users.create);
    app.post(PATH_USERS + '/listBack', session.admin, users.list);
    app.post(PATH_USERS + '/createBack', session.admin, users.createBack);
    app.put(PATH_USERS, session.admin, users.update);
    app.post(PATH_USERS + "/remove", session.admin, users.remove);
    app.post(PATH_USERS + '/status', session.admin, users.status);
    app.get(PATH_USERS + '/myprofile', session.auth, users.profile);
    app.post(PATH_USERS + '/myprofile', session.auth, users.profileChangePass);
    app.put(PATH_USERS + '/myprofile', session.auth, users.profileUpdate);
    app.put('/changeSuscription', session.auth, users.updateSuscription);
    app.post(PATH_USERS + "/newpass", session.admin, users.changePassByAdmin);
    app.post('/forgivePass', users.forgivePass);
    app.post('/changePassByToken', users.changePassByToken);
    app.get(PATH_USERS + '/activate/:token', users.activate);
    app.post(PATH_USERS + '/activities', session.auth, users.getActivitiesByOwner);
    app.post(PATH_USERS + '/language', session.auth, users.changeLanguage);

    /*SESSIONS*/
    var PATH_SESSIONS = '/api/sessions';
    app.get(PATH_SESSIONS + '/ping', function (req, res) {
        if (!req.user) {
            return res.json(false)
        }
        return res.json(req.user);
    });

    /*STATISTICS*/
    var PATH_STATISTICS = '/api/statistics';
    app.get(PATH_STATISTICS, session.admin, statistics.statistics);
    app.get(PATH_STATISTICS + '/homeInfo', session.admin, statistics.getReservationTour);
    app.post(PATH_STATISTICS + '/reservations', session.admin, statistics.reservationsAndRefundsByYear);
    app.post(PATH_STATISTICS + '/timeLine', session.admin, timeline.listBack);
    app.post('/search', statistics.searchText);
    app.get('/api/gallery', statistics.showGalery);

    /*GROUPS*/
    var PATH_GROUPS = '/api/groups';
    app.get(PATH_GROUPS + "/pack", session.admin, groups.getGroupPack);
    app.get(PATH_GROUPS + "/:group", groups.getByGroup);
    app.get(PATH_GROUPS, groups.list);
    app.post(PATH_GROUPS, session.admin, groups.create);
    app.put(PATH_GROUPS, session.admin, groups.update);
    app.delete(PATH_GROUPS + "/:id", session.admin, groups.delete);
    app.post(PATH_GROUPS + "/upFront", session.admin, groups.upFront);
    app.post(PATH_GROUPS + "/downFront", session.admin, groups.downFront);
    app.post(PATH_GROUPS + '/listTours', session.admin, groups.listToursByGroup);
    app.post(PATH_GROUPS + "/updateImg", session.admin, groups.updateImg);

    /*TOURS*/
    var PATH_TOURS = '/api/tours';
    app.post(PATH_TOURS, session.admin, tours.create);
    app.get(PATH_TOURS, tours.list);
    app.post(PATH_TOURS + "/back", session.admin, tours.listBack);
    app.get(PATH_TOURS + "/available", tours.listAvailable);

    app.get(PATH_TOURS + "/:id", tours.get);

    app.post(PATH_TOURS + '/status', session.admin, tours.status);
    app.delete(PATH_TOURS + "/:id", session.admin, tours.remove);
    app.put(PATH_TOURS, session.admin, tours.update);
    app.post(PATH_TOURS + '/delMedia', session.admin, tours.removeOnlyMedia);
    app.post(PATH_TOURS + '/addMedia', session.admin, tours.addOnlyMedia);
    app.post(PATH_TOURS + '/ports', tours.listByPort);
    app.post(PATH_TOURS + '/groups', tours.listByGroup);
    app.post(PATH_TOURS + '/postGroup', session.admin, tours.listByPortGroup);
    app.post(PATH_TOURS + '/getTourReserve', tours.getTourForBooking);
    app.post(PATH_TOURS + '/disableDays', tours.getDisablesDay);
    app.post(PATH_TOURS + '/listBonds', tours.bonds);
    app.post(PATH_TOURS + '/recomend', tours.getRecomendTour);
    app.post(PATH_TOURS + "/updateImg", session.admin, tours.updateImg);
    app.post(PATH_TOURS + "/listPacks", session.admin, tours.listPacksByTour);
    app.post(PATH_TOURS + '/receipt', session.auth, reservation.getReceipt);

    /*SHIPS*/
    var PATH_SHIPS = '/api/ships';
    app.get(PATH_SHIPS, ships.list);
    app.get(PATH_SHIPS + "/owners", session.admin, users.listUsersOwners);
    app.get(PATH_SHIPS + "/patrons", session.admin, users.listUsersPatrons);
    app.get(PATH_SHIPS + "/:id", session.admin, ships.get);
    app.post(PATH_SHIPS, session.admin, ships.create);
    app.post(PATH_SHIPS + '/back', session.admin, ships.listBack);
    app.put(PATH_SHIPS, session.admin, ships.update);
    app.delete(PATH_SHIPS + "/:id", session.admin, ships.remove);
    app.post(PATH_SHIPS + '/delMedia', session.admin, ships.removeOnlyMedia);
    app.post(PATH_SHIPS + '/addMedia', session.admin, ships.addOnlyMedia);
    app.post(PATH_SHIPS + '/delTour', session.admin, ships.delTour);
    app.post(PATH_SHIPS + '/addTour', session.admin, ships.addTour);
    app.post(PATH_SHIPS + '/calendar', session.admin, ships.createCalendar);
    app.post(PATH_SHIPS + "/updateImg", session.admin, ships.updateImg);
    app.post(PATH_SHIPS + "/status", session.admin, ships.status);

    /*PORTS*/
    var PATH_PORT = '/api/ports';
    app.post(PATH_PORT, session.admin, ports.create);
    app.post(PATH_PORT + '/delMedia', session.admin, ports.removeOnlyMedia);
    app.post(PATH_PORT + '/addMedia', session.admin, ports.addOnlyMedia);
    app.post(PATH_PORT + '/back', session.admin, ports.listBack);
    app.get(PATH_PORT, ports.list);
    app.get(PATH_PORT + "/:id", session.admin, ports.get);
    app.put(PATH_PORT, session.admin, ports.update);
    app.delete(PATH_PORT + "/:id", session.admin, ports.remove);
    app.post(PATH_PORT + "/country", ports.filterByCountry);
    app.post(PATH_PORT + "/city", ports.filterByCity);
    app.post(PATH_PORT + "/updateImg", session.admin, ports.updateImg);
    app.post(PATH_PORT + "/available", ports.getAvailable);
    app.put(PATH_PORT + "/available", ports.setAvailable);
    app.post(PATH_PORT + "/listTours", session.admin, ports.listToursByPort);

    /*EMAIL*/
    var PATH_NOTY = '/api/noty';
    app.get(PATH_NOTY, session.admin, email.getConfigurationsServer);
    app.post(PATH_NOTY, session.admin, email.createConfigurationsEmailServer);
    app.put(PATH_NOTY, session.admin, email.saveConfigurationsEmailServer);
    app.post(PATH_NOTY + '/change', session.admin, email.changeNoty);
    app.post(PATH_NOTY + '/changeBody', session.admin, email.saveNotyBody);
    app.post(PATH_NOTY + '/send', email.testConection);
    app.post(PATH_NOTY + '/sendNoty', email.sendNotyEmail);

    /*IMAGES*/
    var PATH_MEDIA = '/api/media';
    app.get(PATH_MEDIA + '/:name', media.get)

    /*COUNTRIES*/
    var PATH_COUNTRIES = '/api/country';
    app.get(PATH_COUNTRIES + "/:id", country.get);
    app.get(PATH_COUNTRIES, country.list);
    app.post(PATH_COUNTRIES + '/front', ports.getFilterAvailable);
    app.post(PATH_COUNTRIES + '/back', country.listFront);
    app.post(PATH_COUNTRIES, session.admin, country.create);
    app.put(PATH_COUNTRIES, session.admin, country.update);
    app.delete(PATH_COUNTRIES + "/:id", session.admin, country.delete);
    app.post(PATH_COUNTRIES + '/addCity', session.admin, country.addCity);
    app.post(PATH_COUNTRIES + '/delCity', session.admin, country.delCity);
    app.post(PATH_COUNTRIES + '/updateCity', session.admin, country.updateCity);

    /*SERVICES*/
    var PATH_SERVICES = '/api/service';
    app.get(PATH_SERVICES + "/:id", service.get);
    app.get(PATH_SERVICES, service.list);
    app.delete(PATH_SERVICES + "/:id", session.admin, service.delete);
    app.post(PATH_SERVICES + '/back', session.admin, service.listBack);
    app.post(PATH_SERVICES, session.admin, service.create);
    app.post(PATH_SERVICES + '/status', session.admin, service.status);
    app.post(PATH_SERVICES + "/available", service.listAvailable);
    app.put(PATH_SERVICES, session.admin, service.update);
    app.post(PATH_SERVICES + "/updateImg", session.admin, service.updateImg);
    app.post(PATH_SERVICES + "/byPack", service.getServiceByPack);
    app.post(PATH_SERVICES + "/listPacks", session.admin, service.getPacksByService);

    /*ENTERPRISE*/
    var PATH_ENTERPRISE = '/api/enterprise';
    app.get(PATH_ENTERPRISE + "/:id", enterprise.get);
    app.get(PATH_ENTERPRISE, enterprise.list);
    app.post(PATH_ENTERPRISE + '/back', session.admin, enterprise.listBack);
    app.delete(PATH_ENTERPRISE + "/:id", session.admin, enterprise.delete);
    app.post(PATH_ENTERPRISE, session.admin, enterprise.create);
    app.put(PATH_ENTERPRISE, session.admin, enterprise.update);
    app.post(PATH_ENTERPRISE + "/updateImg", session.admin, enterprise.updateImg);
    app.post(PATH_ENTERPRISE + '/status', session.admin, enterprise.changeStatus);
    app.post(PATH_ENTERPRISE + "/listServices", session.admin, enterprise.listServicesByEnterprise);

    /*PACKAGE*/
    var PATH_PACKAGE = '/api/package';
    app.get(PATH_PACKAGE + "/:id", packages.get);
    app.get(PATH_PACKAGE, packages.list);
    app.delete(PATH_PACKAGE + "/:id", session.admin, packages.delete);
    app.post(PATH_PACKAGE + '/back', session.admin, packages.listBack);
    app.post(PATH_PACKAGE, session.admin, packages.create);
    app.post(PATH_PACKAGE + '/status', session.admin, packages.status);
    app.put(PATH_PACKAGE, session.admin, packages.update);

    //TPV
    var PATH_TPV = '/api/tpv';
    app.post(PATH_TPV + '/notification', function (req, res) {
        console.log("TPV ONLINE NOTIFICATION--------------------------------------");
        console.log(req.body);
        console.log(req.query);
        console.log("END TPV ONLINE NOTIFICATION--------------------------------------");
        if (req.body.Ds_TransactionType == '0') {
            db.Sale.findOne({signature: req.body.Ds_Order}, function (err, sale) {
                if (err || !sale) {
                    console.log('Notification Error', err);
                    console.log('Notification Error sale', sale);
                }
                else {
                    db.User.findOne({_id: sale._doc.user}, function (err, user) {
                        var us = {
                            role: user.role,
                            _id: user._id.toString(),
                            username: user.userName,
                            lastname: user.lastName,
                            email: user.email,
                            complete: user.complete,
                            language: user.language
                        };
                        req.user = us;
                        req.session.sale = sale._doc.bodySale;
                        reservation.reservationReturn(req, res);

                    })


                }
            })
        }
        else if (req.body.Ds_TransactionType == '3') {
            db.ReservationClient.findOne({'typePay.Ds_Order': req.body.Ds_Order}, function (err, success) {
                if (err || !success) {
                    console.log("ERROR:" + err);
                    console.log("RESPONSE:" + success);
                }
                else {
                    req.body.token = success._doc.token;
                    req.body.reason = "RAZON   BLA BLA BLA";
                    reservationController.refundReservation(req, res);
                }
            })
        }

    });

    var PATH_INVOICES = '/api/invoices';
    app.post(PATH_INVOICES + '/services', statistics.getInvoicesServicesList);
    app.post(PATH_INVOICES + '/ships', statistics.getInvoicesShipsList);
    app.post(PATH_INVOICES, statistics.getInvoice);

    app.post('/remove-images', function (req, res) {
        var async = require('async');
        db.Media.find(function (err, medias) {
            async.map(medias, function (media, callback0) {
                async.parallel([
                    function (cb) {
                        db.Enterprise.count({'photo.name': media._doc.fieldName}).exec(function (err, num) {
                            cb(null, num);
                        });
                    },
                    function (cb) {
                        db.Port.count({'photos.name': media.fieldName}).exec(function (err, num) {
                            cb(null, num);
                        });
                    },
                    function (cb) {
                        db.Ship.count({'photos.name': media.fieldName}).exec(function (err, num) {
                            cb(null, num);
                        });
                    },
                    function (cb) {
                        db.Service.count({'photos.name': media.fieldName}).exec(function (err, num) {
                            cb(null, num);
                        });
                    },
                    function (cb) {
                        db.Tour.count({'photos.name': media.fieldName}).exec(function (err, num) {
                            cb(null, num);
                        });
                    },
                    function (cb) {
                        db.News.count({'photo.name': media.fieldName}).exec(function (err, num) {
                            cb(null, num);
                        });
                    },
                    function (cb) {
                        db.GeneralInfo.count({'photos.name': media.fieldName}).exec(function (err, num) {
                            cb(null, num);
                        });
                    }
                ], function (err, results) {
                    var sem = false;
                    for (var i = 0; i < results.length; i++) {
                        if (results[i]) {
                            sem = true;
                            break;
                        }
                    }
                    if (!sem) {
                        db.Media.remove({_id: media._doc._id}, function (err, success) {
                            callback0(err, success)
                        })
                    }
                    else {
                        callback0(null, 1);
                    }
                });
            }, function (err, result) {
                return res.json('asd');
            })
        })
    })
};



