angular.module('bojeoApp.services', ['restful', 'ngResource'])

    .factory('Groups', ['$resource', function ($resource) {
        return $resource('/api/groups/:group', {}, {
            get: {method: 'GET'}
        });
    }])
    .factory('Tours', ['$resource', function ($resource) {
        return $resource('/api/tours/:id', {}, {
            get: {method: 'GET'}
        });
    }])
    .factory('Recomended', ['$resource', function ($resource) {
        return $resource('/api/tours/recomend', {}, {
            get: {method: 'POST'}
        });
    }])
    .factory('Service', ['$resource', function ($resource) {
        return $resource('/api/service/byPack', {}, {
            get: {method: 'POST'}
        });
    }])
    .factory('ToursReserv', ['$resource', function ($resource) {
        var resource = $resource('/api/tours/getTourReserve', {}, {
            get: {method: 'POST'}
        });

        resource.receipt = $resource('/api/tours/receipt', {}, {
            get: {method: 'POST'}
        });

        return resource;
    }])
    .factory('DisableDays', ['$resource', function ($resource) {
        return $resource('/api/tours/disableDays', {}, {
            disable: {method: 'POST'}
        });
    }])
    .factory('Abono', ['$restful', function ($restful) {
        return $restful('/api/abonos/:id', {params: {id: '@id'}});
    }])
    .factory('Abonos', ['$resource', function ($resource) {
        return $resource('/api/abonos/:id', {}, {
            get: {method: 'GET'}
        });
    }])
    .factory('Profile', ['$resource', function ($resource) {

        var resource = $resource('/api/users/myprofile', {}, {
            get: {method: 'GET'},
            changePass: {method: 'POST'},
            update: {method: 'PUT'}
        });

        resource.owner = $resource('/api/users/activities', {}, {
            query: {method: 'POST'}
        });
        return resource;
    }])


    .factory('Users', ['$resource', function ($resource) {
        return $resource('/api/users/:id', {}, {
            create: {method: 'POST'}
        });
    }])
    .factory('News', ['$resource', function ($resource) {
        return $resource('/api/news/:id', {}, {
            query: {method: 'GET'}
        });
    }])
    .factory('User', ['$restful', function ($restful) {
        return $restful('/api/users/:id', {params: {id: '@id'}});
    }])
    .factory('dailyTours', ['$restful', function ($restful) {
        return $restful('/api/dailyTour/:id', {params: {id: '@id'}});
    }])
    .factory('exAllDayTours', ['$restful', function ($restful) {
        return $restful('/api/exclusiveAllDayTour/:id', {params: {id: '@id'}});
    }])
    .factory('exDailyTours', ['$restful', function ($restful) {
        return $restful('/api/exclusiveDailyTour/:id', {params: {id: '@id'}});
    }])
    .factory('exHalfDayTours', ['$restful', function ($restful) {
        return $restful('/api/exclusiveHalfDayTour/:id', {params: {id: '@id'}});
    }])
    .factory('exManyDaysTours', ['$restful', function ($restful) {
        return $restful('/api/exclusiveManyDaysTour/:id', {params: {id: '@id'}});
    }])
    .factory('practDepartures', ['$restful', function ($restful) {
        return $restful('/api/practiceDepartures/:id', {params: {id: '@id'}});
    }])
    .factory('Ports', ['$restful', function ($restful) {
        return $restful('/api/ports/:id', {params: {id: '@id'}});
    }])

    .factory('Ships', ['$restful', function ($restful) {
        return $restful('/api/ships/:id', {params: {id: '@id'}});
    }])
    .factory('Reservation', ['$restful', function ($restful) {
        return $restful('/api/reservation/:id', {params: {id: '@id'}});
    }])
    .factory('Ping', ['$restful', function ($restful) {
        return $restful('/api/sessions/ping/:id', {params: {id: '@id'}});
    }])
    .factory('Statistics', ['$resource', function ($resource) {
        return $resource('/api/statistics', {}, {
            query: {method: 'GET'}
        });
    }])
    .factory('ToursGroup', ['$resource', function ($resource) {
        return $resource('/api/tours/groups', {}, {
            query: {method: 'POST'}
        });
    }])
    .factory('Enterprise', ['$resource', function ($resource) {
        return $resource('/api/enterprise/:id', {}, {
            get: {method: 'GET'}
        });
    }])
    .factory('DateServer', ['$resource', function ($resource) {
        return $resource('/date', {}, {
            query: {method: 'GET'}
        });
    }]);





  
 