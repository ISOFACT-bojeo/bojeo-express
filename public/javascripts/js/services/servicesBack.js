angular.module('bojeoApp.services', ['restful', 'ngResource'])

    .factory('News', ['$resource', function ($resource) {
        var resource = $resource('/api/news/:id', {}, {
            query: {method: 'GET'},
            save: {method: 'PUT'},
            create: {method: 'POST'},
            delete: {method: 'DELETE'}
        });

        resource.list = $resource('/api/news/listBack', {}, {
            query: {method: 'POST'}
        });

        return resource;
    }])

    .factory('Tags', ['$resource', function ($resource) {
        var resource = $resource('/api/tags/:id', {}, {
            query: {method: 'GET'},
            save: {method: 'PUT'},
            create: {method: 'POST'},
            delete: {method: 'DELETE'}
        });

        resource.status = $resource('/api/tags/status', {}, {
            query: {method: 'POST'}
        });

        return resource;
    }])
    .factory('Invoices', ['$resource', function ($resource) {
        var resource = $resource('/api/invoices/:id', {}, {
            query: {method: 'POST'}
        });
        resource.services = $resource('/api/invoices/services', {}, {
            query: {method: 'POST'}
        });
        resource.ships = $resource('/api/invoices/ships', {}, {
            query: {method: 'POST'}
        });

        return resource;
    }])

    .factory('Users', ['$resource', function ($resource) {

        var resource = $resource('/api/users/:id', {}, {
            save: {method: 'PUT'},
            create: {method: 'POST'},
            delete: {method: 'DELETE'}
        });

        resource.list = $resource('/api/users/listBack', {}, {
            query: {method: 'POST'}
        });

        resource.back = $resource('/api/users/createBack', {}, {
            create: {method: 'POST'}
        });
        resource.remove = $resource('/api/users/remove', {}, {
            query: {method: 'POST'}
        });

        return resource;
    }])
    .factory('Countries', ['$resource', function ($resource) {
        var resource = $resource('/api/country/:id', {}, {
            query: {method: 'GET'},
            save: {method: 'PUT'},
            create: {method: 'POST'},
            delete: {method: 'DELETE'}
        });
        resource.addCity = $resource('/api/country/addCity', {}, {
            create: {method: 'POST'}
        });
        resource.delCity = $resource('/api/country/delCity', {}, {
            delete: {method: 'POST'}
        });
        resource.saveCity = $resource('/api/country/updateCity', {}, {
            save: {method: 'POST'}
        });

        return resource;
    }])
    .factory('Email', ['$resource', function ($resource) {
        var resource = $resource('/api/noty/:id', {}, {
            query: {method: 'GET'},
            save: {method: 'PUT'},
            create: {method: 'POST'}
        });
        resource.changeNoty = $resource('/api/noty/change', {}, {
            update: {method: 'POST'}
        });
        resource.changeNotyBody = $resource('/api/noty/changeBody', {}, {
            update: {method: 'POST'}
        });
        resource.sendEmail = $resource('/api/noty/send', {}, {
            send: {method: 'POST'}
        });
        resource.sendNotyEmail = $resource('/api/noty/sendNoty', {}, {
            send: {method: 'POST'}
        });
        return resource;
    }])
    .factory('Reservation', ['$resource', function ($resource) {
        return $resource('/api/reservation/:id', {}, {
            query: {method: 'POST'}
        });
    }])
    .factory('Ports', ['$resource', function ($resource) {
        var resource = $resource('/api/ports/:id', {}, {
            get: {method: 'GET'},
            save: {method: 'PUT'},
            create: {method: 'POST'},
            delete: {method: 'DELETE'}
        });
        resource.list = $resource('/api/ports/back', {}, {
            query: {method: 'POST'}
        });

        return resource;

    }])
    .factory('GeneralInfos', ['$resource', function ($resource) {
        var resource = $resource('/api/generalInfo/:id', {}, {
            save: {method: 'PUT'}
        });
        return resource;
    }])
    .factory('GeneralInfo', ['$restful', function ($restful) {
        return $restful('/api/generalInfo/:id', {params: {id: '@id'}});
    }])

    .factory('Login', ['$restful', function ($restful) {
        return $restful('/login');
    }])
    .factory('Contact', ['$resource', function ($resource) {
        return $resource('api/Contacts/:id', {}, {
            create: {method: 'POST'},
            query: {method: 'GET'},
            save: {method: 'PUT'},
            remove: {method: 'DELETE'}
        });
    }])


    .factory('Statistics', ['$resource', function ($resource) {
        var resource = $resource('/api/statistics/:id', {}, {
            query: {method: 'GET'}
        });
        resource.list = $resource('/api/statistics/reservations', {}, {
            query: {method: 'POST'}
        });
        resource.reservations = $resource('/api/statistics/homeInfo', {}, {
            home: {method: 'GET'}
        });
        resource.timeLine = $resource('/api/statistics/timeLine', {}, {
            query: {method: 'POST'}
        });
        resource.memory = $resource('/api/statistics/memory', {}, {
            query: {method: 'GET'}
        });
        return resource;
    }])
    .factory('Ships', ['$resource', function ($resource) {
        var resource = $resource('/api/ships/:id', {}, {
            create: {method: 'POST'},
            get: {method: 'GET'},
            save: {method: 'PUT'},
            remove: {method: 'POST'}
        });
        resource.list = $resource('/api/ships/back', {}, {
            query: {method: 'POST'}
        });
        resource.status = $resource('/api/ships/status', {}, {
            change: {method: 'POST'}
        });
        return resource;
    }])
    .factory('ShipsOwners', ['$resource', function ($resource) {
        return $resource('/api/ships/owners', {}, {
            get: {method: 'GET'}
        });
    }])
    .factory('ShipsPatrons', ['$resource', function ($resource) {
        return $resource('/api/ships/patrons', {}, {
            get: {method: 'GET'}
        });
    }])
    .factory('Tour', ['$resource', function ($resource) {
        var resource = $resource('/api/tours/:id', {}, {
            create: {method: 'POST'},
            get: {method: 'GET'},
            save: {method: 'PUT'},
            remove: {method: 'DELETE'}
        });
        resource.list = $resource('/api/tours/back', {}, {
            query: {method: 'POST'}
        });
        return resource;
    }])
    .factory('Enterprise', ['$resource', function ($resource) {
        var resource = $resource('/api/enterprise/:id', {}, {
            create: {method: 'POST'},
            get: {method: 'GET'},
            save: {method: 'PUT'},
            remove: {method: 'DELETE'}
        });
        resource.list = $resource('/api/enterprise/back', {}, {
            query: {method: 'POST'}
        });
        return resource;
    }])
    .factory('Service', ['$resource', function ($resource) {
        var resource = $resource('/api/service/:id', {}, {
            create: {method: 'POST'},
            get: {method: 'GET'},
            save: {method: 'PUT'},
            remove: {method: 'DELETE'}
        });
        resource.list = $resource('/api/service/back', {}, {
            query: {method: 'POST'}
        });
        return resource;
    }])
    .factory('Package', ['$resource', function ($resource) {
        var resource = $resource('/api/package/:id', {}, {
            create: {method: 'POST'},
            get: {method: 'GET'},
            save: {method: 'PUT'},
            remove: {method: 'DELETE'}
        });
        resource.list = $resource('/api/package/back', {}, {
            query: {method: 'POST'}
        });
        return resource;
    }])
    .factory('Bond', ['$resource', function ($resource) {
        var resource = $resource('/api/abonos/:id', {}, {
            create: {method: 'POST'},
            get: {method: 'GET'},
            save: {method: 'PUT'},
            remove: {method: 'DELETE'}
        });

        resource.list = $resource('/api/abonos/back', {}, {
            query: {method: 'POST'}
        });
        return resource;
    }])
    .factory('Groups', ['$resource', function ($resource) {
        var resource = $resource('/api/groups/:id', {}, {
            create: {method: 'POST'},
            get: {method: 'GET'},
            save: {method: 'PUT'},
            remove: {method: 'DELETE'}
        });

        resource.pack = $resource('/api/groups/pack', {}, {
            query: {method: 'GET'}
        });
        return resource;
    }])
    .factory('ActivateNews', ['$resource', function ($resource) {
        return $resource('/api/news/public', {}, {
            query: {method: 'PUT'}
        });
    }]);


  
 