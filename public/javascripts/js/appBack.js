﻿'use strict';


// Declare app level module which depends on filters, and services
angular.module('bojeoApp', [
    'ngRoute',
    'ui.tinymce',
    'ngSanitize',
    'ui.calendar',
    'ui.bootstrap',
    'bojeoApp.NewsController',
    'bojeoApp.listNewsController',
    'bojeoApp.listUsersController',
    'bojeoApp.DataController',
    'bojeoApp.ToursController',
    'bojeoApp.addToursController',
    'bojeoApp.listShipsController',
    'bojeoApp.calendarController',
    'bojeoApp.services',
    'bojeoApp.MyCtrl',
    'bojeoApp.HomeController',
    'bojeoApp.viewCalendarController',
    'bojeoApp.editCalendarController',
    'bojeoApp.abonoController',
    'bojeoApp.reservationController',
    'bojeoApp.generalInfoController',
    'bojeoApp.listGeneralInfoController',
    'bojeoApp.graphicsController',
    'bojeoApp.emailController',
    'bojeoApp.listContactController',
    'bojeoApp.devolutionController',
    'bojeoApp.listPortsController',
    'bojeoApp.countriesController',
    'bojeoApp.servicesController',
    'bojeoApp.enterpriseController',
    'bojeoApp.packagesController',
    'bojeoApp.invoiceController',
    'bojeoApp.ValidFocusDirective',
    'bojeoApp.durationHour',
    'bojeoApp.horary',
    'bojeoApp.tagsController'

]).
    config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/news', { templateUrl: 'backoffice/news', controller: 'NewsController' });
        $routeProvider.when('/listNews', { templateUrl: 'backoffice/listNews', controller: 'listNewsController' });
        $routeProvider.when('/users', { templateUrl: 'backoffice/listUsers', controller: 'listUsersController' });
        $routeProvider.when('/users/:id', { templateUrl: 'backoffice/listUsers', controller: 'listUsersController' });
        $routeProvider.when('/groups', { templateUrl: 'backoffice/tours', controller: 'ToursController' });
        $routeProvider.when('/tours', { templateUrl: 'backoffice/addTours', controller: 'addToursController' });
        $routeProvider.when('/tours/:id', { templateUrl: 'backoffice/addTours', controller: 'addToursController' });
        $routeProvider.when('/ships', { templateUrl: 'backoffice/listShips', controller: 'listShipsController' });
        $routeProvider.when('/ships/:id', { templateUrl: 'backoffice/listShips', controller: 'listShipsController' });
        $routeProvider.when('/calendar/:shipId', { templateUrl: 'backoffice/calendar', controller: 'calendarController' });
        $routeProvider.when('/viewCalendar/:shipId', { templateUrl: 'backoffice/viewCalendar', controller: 'viewCalendarController' });
        $routeProvider.when('/editCalendar/:shipId', { templateUrl: 'backoffice/editCalendar', controller: 'editCalendarController' });
        $routeProvider.when('/home', { templateUrl: 'backoffice/home', controller: 'HomeController' });
        $routeProvider.when('/bonds', { templateUrl: 'backoffice/abono', controller: 'abonoController' });
        $routeProvider.when('/reservations', { templateUrl: 'backoffice/reservations', controller: 'reservationController' });
        $routeProvider.when('/reservations/:ship', { templateUrl: 'backoffice/reservations', controller: 'reservationController' });
        $routeProvider.when('/generalInfo', { templateUrl: 'backoffice/generalInfo', controller: 'generalInfoController' });
        $routeProvider.when('/listGeneralInfo', { templateUrl: 'backoffice/listGeneralInfo', controller: 'listGeneralInfoController' });
        $routeProvider.when('/ports', { templateUrl: 'backoffice/listPorts', controller: 'listPortsController' });
        $routeProvider.when('/ports/:text', { templateUrl: 'backoffice/listPorts', controller: 'listPortsController' });
        $routeProvider.when('/email', { templateUrl: 'backoffice/email', controller: 'emailController' });
        $routeProvider.when('/contacts', { templateUrl: 'backoffice/listContact', controller: 'listContactController' });
        $routeProvider.when('/refunds', { templateUrl: 'backoffice/devolution', controller: 'devolutionController' });
        $routeProvider.when('/countries', { templateUrl: 'backoffice/countries', controller: 'countriesController' });
        $routeProvider.when('/services', { templateUrl: 'backoffice/services', controller: 'servicesController' });
        $routeProvider.when('/services/:id', { templateUrl: 'backoffice/services', controller: 'servicesController' });
        $routeProvider.when('/enterprises', { templateUrl: 'backoffice/enterprise', controller: 'enterpriseController' });
        $routeProvider.when('/packages', { templateUrl: 'backoffice/packages', controller: 'packagesController' });
        $routeProvider.when('/packages/:id', { templateUrl: 'backoffice/packages', controller: 'packagesController' });
        $routeProvider.when('/graphics', { templateUrl: 'backoffice/graphics', controller: 'graphicsController' });
        $routeProvider.when('/invoice', { templateUrl: 'backoffice/invoice', controller: 'invoiceController' });
        $routeProvider.when('/tags', { templateUrl: 'backoffice/tags', controller: 'tagsController' });


        $routeProvider.otherwise({ redirectTo: '/home' });
    }])
    .run(['$rootScope', '$restful', '$http', function ($rootScope, $restful, $http) {
        $restful.$baseURL = '/';

        setInterval(function () {
            $http.get('/api/sessions/ping').success(function (data) {
                if (data == "false") {
                    window.location = '/backoffice/login';
                }
            });
        }, 5 * 60 * 1000);
    }]);


