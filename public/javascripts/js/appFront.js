/**
 * Created by ernestomr87@gmail.com on 06/06/2014.
 */

'use strict';

// Declare app level module which depends on filters, and services
angular.module('bojeoApp', [
    'ngRoute',
    'ngSanitize',
    'ui.bootstrap',
    /*DIRECTIVES*/
    'bojeoApp.PoilerToursDirective',
    'bojeoApp.perfectParallax',
    /*SERVICES*/
    'bojeoApp.services',
    /*CONTTROLLERS*/
    'bojeoApp.IndexController',
    'bojeoApp.GalleryController',
    'bojeoApp.DataController',
    'bojeoApp.ReturnController',
    'bojeoApp.RegisterController',
    'bojeoApp.AboutController',
    'bojeoApp.BulletinController',
    'bojeoApp.WhereController',
    'bojeoApp.NewsController',
    'bojeoApp.NewController',
    'bojeoApp.ToursController',
    'bojeoApp.TourController',
    'bojeoApp.payFormController',
    'bojeoApp.searchResultController',
    'bojeoApp.MapController',
    'bojeoApp.BuyController',
    'bojeoApp.ProfileController',
    'bojeoApp.LoginController',
    'bojeoApp.PoilerToursController',
    'bojeoApp.CookiesController',
    'bojeoApp.EnterpriseController',
    'bojeoApp.RouteController',
    'bojeoApp.useTermController',
    'bojeoApp.forgivePassController',
    'bojeoApp.changePassController',
    'bojeoApp.receiptController',
    'bojeoApp.ValidFocusDirective',
    'bojeoApp.durationHour',
    'bojeoApp.horary',

    'ngCookies',
    'ezfb',
    'ngKookies'
    , 'angular-google-analytics'
/*    ,'google-maps'*/
]).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/', {templateUrl: '/frontend/index', controller: 'IndexController'});
        $routeProvider.when('/:culture', {templateUrl: '/frontend/index', controller: 'IndexController'});
        $routeProvider.when('/:culture/index', {templateUrl: '/frontend/index', controller: 'IndexController'});
        $routeProvider.when('/:culture/receipt', {templateUrl: '/frontend/receipt', controller: 'receiptController'});
        $routeProvider.when('/:culture/gallery', {templateUrl: '/frontend/gallery', controller: 'GalleryController'});
        $routeProvider.when('/:culture/news', {templateUrl: '/frontend/news', controller: 'NewsController'});
        $routeProvider.when('/:culture/news/:id', {templateUrl: '/frontend/new', controller: 'NewController'});
        $routeProvider.when('/:culture/refund/:returnId', {templateUrl: '/frontend/return',controller: 'ReturnController'});
        $routeProvider.when('/:culture/refund', {templateUrl: '/frontend/return', controller: 'ReturnController'});
        $routeProvider.when('/:culture/register', {templateUrl: '/frontend/register',controller: 'RegisterController'});
        $routeProvider.when('/:culture/about', {templateUrl: '/frontend/about', controller: 'AboutController'});
        $routeProvider.when('/:culture/bulletin', {templateUrl: '/frontend/bulletin',controller: 'BulletinController'});
        $routeProvider.when('/:culture/contact', {templateUrl: '/frontend/where', controller: 'WhereController'});
        $routeProvider.when('/:culture/pay-form', {templateUrl: '/frontend/payForm', controller: 'payFormController'});
        $routeProvider.when('/:culture/search', {templateUrl: '/frontend/searchResult',controller: 'searchResultController'});
        $routeProvider.when('/:culture/search/:text', {templateUrl: '/frontend/searchResult',controller: 'searchResultController'});
        $routeProvider.when('/:culture/map', {templateUrl: '/frontend/map', controller: 'MapController'});
        $routeProvider.when('/:culture/buy/', {templateUrl: '/frontend/buy', controller: 'BuyController'});
        $routeProvider.when('/:culture/profile/:buy', {templateUrl: '/frontend/profile',controller: 'ProfileController'});
        $routeProvider.when('/:culture/profile/', {templateUrl: '/frontend/profile', controller: 'ProfileController'});
        $routeProvider.when('/:culture/login', {templateUrl: '/frontend/login', controller: 'LoginController'});
        $routeProvider.when('/:culture/login/:token', {templateUrl: '/frontend/login', controller: 'LoginController'});
        $routeProvider.when('/:culture/terms-of-use', {templateUrl: '/frontend/useTerm',controller: 'useTermController'});
        $routeProvider.when('/:culture/forgot-password', {templateUrl: '/frontend/forgivepass',controller: 'forgivePassController'});
        $routeProvider.when('/:culture/change-password/:tokenPass', {templateUrl: '/frontend/changePass',controller: 'changePassController'});
        $routeProvider.when('/:culture/error-conection', {templateUrl: '/frontend/errorConection',controller: 'IndexController'});
        $routeProvider.when('/:culture/occupied-booking', {templateUrl: '/frontend/occupiedBooking',controller: 'IndexController'});
        $routeProvider.when('/:culture/404', {templateUrl: '/frontend/404', controller: 'IndexController'});
        $routeProvider.when('/:culture/cookies', {templateUrl: '/frontend/cookies', controller: 'CookiesController'});
        $routeProvider.when('/:culture/enterprise', {templateUrl: '/frontend/enterprise',controller: 'EnterpriseController'});
        $routeProvider.when('/:culture/:group/:slug', {templateUrl: '/frontend/tour', controller: 'TourController'});
        $routeProvider.when('/:culture/:group', {templateUrl: '/frontend/toursEx',controller: 'ToursController'});

        $routeProvider.otherwise({templateUrl: '/frontend/404', controller: 'IndexController'});
    }])


    .config(['$locationProvider', function ($locationProvider) {
        $locationProvider.html5Mode(true);
    }])
    .config(['$logProvider', function ($logProvider) {
        $logProvider.debugEnabled(true);
    }])
    .config(['ezfbProvider', function (ezfbProvider) {
        ezfbProvider.setInitParams({
            appId: '305599916291450'
        });
    }])
    .config(['$sceProvider', function ($sceProvider) {
        $sceProvider.enabled(false);
    }])
    .config(['$kookiesProvider', function ($kookiesProvider) {
            $kookiesProvider.config.raw = true;
    }])
    .config(['AnalyticsProvider', function (AnalyticsProvider) {
        AnalyticsProvider.setAccount('UA-59768547-1');
        AnalyticsProvider.trackPages(true);
        AnalyticsProvider.useECommerce(true, false);
    }])
    .run(['$rootScope', '$restful', '$location', '$route', '$cookies','Analytics',
        function ($rootScope, $restful, $location, $route, $cookies,Analytics) {
            $rootScope.countries = BOJEO_COUNTRIES || new Array();
            $rootScope.noticeCookies = NOTICECOOKIES;
            $rootScope.copyRights = BOJEO_COPYRIGHTS;
            $rootScope.first_Time = true;


            $restful.$baseURL = "/";
            $rootScope.$on('$viewContentLoaded', function (current, previous, rejection) {
                if ($rootScope.PageSelected != 'index') {
                    $('#-').attr("tabIndex", -1).focus();
                }

                if ($route.current.params.culture == 'es' || $route.current.params.culture == 'en') {
                    if ($cookies.languageSelected != $route.current.params.culture) {
                        $rootScope.changeLanguage($route.current.params.culture)
                    }
                }
                else {
                    var aux = $cookies.languageSelected || 'es';
                    $location.path('/' + aux  )
                }

            })
        }]);


