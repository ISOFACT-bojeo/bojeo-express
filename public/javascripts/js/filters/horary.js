/**
 * Created by Juan Javier on 16/02/2015.
 */
angular.module('bojeoApp.horary', [])
    .filter('horary', function () {
        return function (input) {
            // input will be the string we pass in
            if (input) {
                var hour = parseInt(input);
                var minute = Math.round((input - hour) * 60);
                if (minute < 10) {
                    minute = '0' + minute;
                }
                return hour + ":" + minute;
            }
        }
    });
