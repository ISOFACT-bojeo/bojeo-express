/**
 * Created by ernestomr87@gmail.com on 21/10/2014.
 */

angular.module('bojeoApp.PoilerToursDirective', [])
    .controller('Controller', ['$scope', function ($scope) {
        $scope.naomi = { name: 'Naomi', address: '1600 Amphitheatre' };
        $scope.igor = { name: 'Igor', address: '123 Somewhere' };
    }])
    .directive('poilerTours', function () {
        return {
            restrict: 'E',
            controller: 'PoilerToursController',
            scope: {
                customerInfo: '=info'
            },
            templateUrl: '/javascripts/js/partials/poilerTours.html'
        };

    })