/**
 * Created by ernestomr87@gmail.com on 25/07/2014.
 */

function htmlToPlaintext(text) {
    return String(text).replace(/<[^>]+>/gm, '');
}

function getNameDay(index) {
    var nameDays = {
        0: {
            es: "Domingo",
            en: "Sunday"
        },
        1: {
            es: "Lunes",
            en: "Monday"
        },
        2: {
            es: "Martes",
            en: "Tuesday"
        },
        3: {
            es: "Miércoles",
            en: "Wednesday"
        },
        4: {
            es: "Jueves",
            en: "Thursday"
        },
        5: {
            es: "Viernes",
            en: "Friday"
        },
        6: {
            es: "Sábado",
            en: "Saturday"
        }
    }
    return nameDays[index];
}

/*
var production = true;
var urlServer = "";
if (production) {
    urlServer = "http://bojeo.azurewebsites.net/api/reservation/pay/";
} else {
    urlServer = "http://localhost:2000/api/reservation/pay/";
}
*/
