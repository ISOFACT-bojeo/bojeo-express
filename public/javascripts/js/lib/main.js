$().ready(function () {
	var map = L.map('map',{
		zoomControl : true
	}).setView([51.505, -0.09], 13);

	userMarker = L.marker([51.505, -0.09]);
	userMarker.addTo(map);
	userMarker.bindPopup('<p>You are there! Your ID is ' + 123 + '</p>').openPopup();
});