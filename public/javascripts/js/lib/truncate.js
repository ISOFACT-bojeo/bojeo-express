/**
 * Created by ernestomr87@gmail.com on 20/05/14.
 */

$.fn.truncate = function(options) {
    options = $.extend({maxLength:20}, options);

    this.each(function() {
        var $this = $(this);

        var html = $this.text();

        if (html.length <= options.maxLength) return;

        html = html.substring(0, options.maxLength);
       // html = html.replace(/\w+$/, '');

        /*html += '<a href="#" ' +
         'onclick="this.parentNode.innerHTML=' +
         'unescape(\'' + escape($this.html()) + '\');return false;">' +
         '...<\/a>';*/

        $this.text(html);
    });
}
