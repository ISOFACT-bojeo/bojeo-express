/**
 * Created by Home on 04/06/2015.
 */

angular.module('bojeoApp.tagsController', [])
    .controller('tagsController', ['$scope', '$rootScope', '$http', 'Tags',
        function ($scope, $rootScope, $http, Tags) {

            $rootScope.PageSelected = 'tags';
            $rootScope.loadStatistics();


            $scope.openClose = true;
            $scope.editTag = false;
            $scope.initializeVars = function () {
                $scope.tag = {
                    name: {
                        es: '',
                        en: ''
                    }
                }
            };

            $scope.initializeVars();

            $scope.showForm = function () {
                $scope.openClose = false;
                $scope.initializeVars();
                $('#form-bond').show();
            };
            $scope.hideForm = function () {
                $scope.openClose = true;
                $('#form-bond').hide();
            };
            $scope.edit = function (tag) {
                $scope.editTag = true;
                $scope.selectTag(tag);
                $('#form-bond').slideDown(500);
            };
            $scope.selectTag = function (tag) {
                $scope.tag = angular.copy(tag);
            };


            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;
            $scope.totalItems = 0;
            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.loadTags();
            };
            $scope.loadTags = function () {
                Tags.query({limit: $scope.maxSize, skip: $scope.skip}, function (data) {
                    if (data.count) {
                        $scope.totalItems = data.cont;
                        $scope.tags = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.tags.length > 0) {
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadTags();
                            }
                        }
                    } else {
                        $scope.tags = new Array();
                        $scope.empty = true;
                    }
                });
            };
            $scope.loadTags();


            $scope.create = function () {
                Tags.create({name: $scope.tag.name}, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Categoría creada satisfactoriamenete', 0);
                        $scope.loadTags();
                        $rootScope.loadStatistics();
                        $scope.hideForm();
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido crear la Categoría', 2);
                    }
                    $scope.formTags.$setPristine(true);
                });
            };
            $scope.delete = function () {
                if(!$scope.tag.dependencies.length){
                    Tags.delete({id: $scope.tag._id}, function (data) {
                        if (data.res) {
                            $rootScope.Notifications('Categoía eliminada satisfactoriamenete', 0);
                            $rootScope.loadStatistics();
                            $scope.loadTags();
                        }
                        else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido eliminar la Categoía', 2);
                        }
                    });
                }

            };

            $scope.save = function () {
                $('#myModal').modal('hide');
                Tags.save({id: $scope.tag._id, name: $scope.tag.name}, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                        $scope.hideForm();
                        $scope.loadTags();
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                    }
                });
                //$http({
                //    method: 'PUT', url: '/api/abonos',
                //    data: {
                //        id: $scope.id,
                //        hours: $scope.hours,
                //        price: $scope.price
                //    }
                //}).
                //    success(function (data) {
                //        if (data.res) {
                //            $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                //            $scope.hideForm();
                //            $scope.loadBonds();
                //        }
                //        else {
                //            $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                //        }
                //    });
            };

            $scope.changeStatus = function (tag) {
                Tags.status.query({id: tag._id}, function (data) {
                    if (data.res) {
                        $scope.loadTags();
                    } else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                    }
                });
            };
        }]);
