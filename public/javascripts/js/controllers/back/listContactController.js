/**
 * Created by sandoval on 8/21/2014.
 */

angular.module('bojeoApp.listContactController', [])
    .controller('listContactController', ['Contact', 'Statistics', '$scope', '$rootScope', '$upload', '$http',
        function (Contact, Statistics, $scope, $rootScope, $upload, $http) {
            $rootScope.PageSelected = 'contacts';
            $rootScope.loadStatistics();
            $scope.allContact = null;

            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;
            $scope.totalItems = 0;
            $scope.text = "";

            $scope.loadContact = function () {
                Contact.query({limit: $scope.maxSize, skip: $scope.skip, text: $scope.text}, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.cont;
                        $scope.allContact = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.allContact.length > 0) {
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadContact();
                            }
                        }
                    } else {
                        $scope.allContact = new Array();
                        $scope.empty = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener los contactos.', 2);
                    }
                });
            };
            $scope.loadContact();
            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.loadContact();
            };
            $scope.delContact = null;
            $scope.vContact = null;
            $scope.deleteContact = function (data) {
                $scope.delContact = data;
            };
            $scope.viewContact = function (contact) {
                $scope.vContact = contact;
                if (contact.reading) {
                    $('#div-contacto').show();
                } else {
                    $http({
                        method: 'PUT', url: '/api/contacts',
                        data: {id: contact._id, reading: true}
                    }).
                        success(function (data) {
                            if (data.res) {
                                $scope.loadContact();
                                $('#div-contacto').show();
                            } else {
                                $rootScope.Notifications('Ha ocurrido un error, no se ha podido mostrar el mensaje', 2);
                            }
                        });
                }
            };
            $scope.hideForm = function () {
                $('#div-contacto').hide();
            };
            $scope.dropContact = function () {
                Contact.remove({id: $scope.delContact._id}, function (data) {
                    if (data) {
                        $scope.cantContact = $scope.cantContact - 1;
                        $rootScope.total_contact = $rootScope.total_contact - 1;
                        $rootScope.Notifications('Contacto eliminado con éxito.', 0);
                        $scope.loadContact();
                        //$rootScope.loadStatistics();

                    }
                });
            };
            $scope.showFilter = function () {
                $('#filterModal').modal('show');
            }
        }])
;
