/**
 * Created by ernestomr87@gmail.com on 26/05/14.
 */

angular.module('bojeoApp.listUsersController', [])
    .controller('listUsersController', ['$scope', '$rootScope', '$routeParams', 'Users', 'Statistics', '$cacheFactory', '$http',
        function ($scope, $rootScope, $routeParams, Users, Statistics, $cacheFactory, $http) {

            $rootScope.PageSelected = 'users';
            $rootScope.loadStatistics();
            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.loadUsers();
            };

            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;
            $scope.openClose = true;
            $scope.semaphore = true;
            $scope.passEdit = true;
            $scope.order = 'userName';
            $scope.changeOrder = function (order) {
                $scope.order = order;
            };
            $scope.month = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
            $scope.initializeUser = function () {
                $scope.passSuccessShow = false;
                var date = new Date();
                $scope.userName = "";
                $scope.lastName = "";
                $scope.sex = "male";
                $scope.birthday = date.getTime();
                $scope.email = "";
                $scope.phoneNumber = "";
                $scope.enterprise = "";
                $scope.address = "";
                $scope.city = "";
                $scope.postalCode = "";
                $scope.country = "";
                $scope.role = "user";
                $scope.subscription = "";
                $scope.password = "";
                $scope.password1 = "";

                $scope.years = [];
                for (var i = 1900; i < date.getFullYear(); i++) {
                    $scope.years.push(i);
                }
                $scope.year = date.getFullYear() - 30;
                $scope.month = 0;
                $scope.dt = new Date($scope.birthday);
                contructDays(31);
            };
            $scope.initializeUser();

            $scope.changeDate = function () {
                var bisiesto = ($scope.year % 4 == 0) && (($scope.year % 100 != 0) || ($scope.year % 400 == 0));
                if ($scope.month == 0 || $scope.month == 2 || $scope.month == 4 || $scope.month == 6 || $scope.month == 7 || $scope.month == 9 || $scope.month == 11) {
                    contructDays(31);
                }
                if ($scope.month == 3 || $scope.month == 5 || $scope.month == 8 || $scope.month == 10) {
                    contructDays(30);
                }
                if ($scope.month == 1) {
                    if (bisiesto) {
                        contructDays(29);
                    } else {
                        contructDays(28);
                    }
                }
            };

            function contructDays(cant) {
                $scope.days = [];
                for (var i = 1; i <= cant; i++) {
                    $scope.days.push(i);
                }
                if ($scope.day == null) {
                    $scope.day = 1;
                } else {
                    if ($scope.day > cant) {
                        $scope.day = 1;
                    }
                }
            }

            $scope.showCalendar = function () {
                $('.dropdown-calendar').slideDown(200);
            };
            $scope.fadeCalendar = function () {
                $('.dropdown-calendar').slideUp(200);
            };
            $rootScope.SelectedDay = function () {
                $scope.dt = new Date($scope.dt.getFullYear(), $scope.dt.getMonth(), $scope.dt.getDate());
                $scope.fadeCalendar();
            };
            $scope.showForm = function () {
                $scope.openClose = false;
                $('#form-user').show();
            };
            $scope.hideForm = function () {
                $('#form-user').hide();
                $scope.openClose = true;
                $scope.semaphore = true;
                $scope.initializeUser();
            };
            $scope.emailValidator = function () {
                if (!(/\w{1,}[@][\w\-]{1,}([.]([\w\-]{1,})){1,3}$/.test($scope.email))) {
                    return false;
                }
                else {
                    return true;
                }
            };
            $scope.phoneValidator = function () {
                if (!(/^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/.test($scope.phoneNumber))) {
                    return false;
                }
                else {
                    return true;
                }
            };
            /*$scope.postaCodeValidator = function () {
             if (!(/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/.test($scope.postalCode))) {
             return false;
             }
             else {
             return true;
             }
             }*/
            $scope.edit = function (user) {
                $scope.editUser = angular.copy(user);
                $scope.semaphore = false;
                $scope.id_user = $scope.editUser._id;
                $scope.userName = $scope.editUser.userName;
                $scope.lastName = $scope.editUser.lastName;
                $scope.sex = $scope.editUser.sex;
                $scope.dt = new Date($scope.editUser.birthday);
                $scope.email = $scope.editUser.email;
                $scope.phoneNumber = $scope.editUser.phoneNumber;
                $scope.enterprise = $scope.editUser.enterprise;
                $scope.address = $scope.editUser.address;
                $scope.city = $scope.editUser.city;
                $scope.country = $scope.editUser.country;
                $scope.role = $scope.editUser.role;
                $scope.postalCode = $scope.editUser.postalCode;
                $scope.password = "";
                $scope.day = $scope.dt.getDate();
                $scope.month = $scope.dt.getMonth();
                $scope.year = $scope.dt.getFullYear();
                $scope.showForm();
            };

            $scope.text = "";
            $scope.reload = false;
            $scope.user_id = "";
            if ($routeParams.id) {
                $scope.user_id = $routeParams.id;
                $scope.reload = true;
            }
            $scope.loadUsers = function () {
                Users.list.query({limit: $scope.maxSize, skip: $scope.skip, id: $scope.user_id, word: $scope.text}, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.cont;
                        $scope.user = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.user.length > 0) {
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadUsers();
                            }
                        }
                    } else {
                        $scope.user = new Array();
                        $scope.empty = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener los usuario.', 2);
                    }
                });
            };
            $scope.loadUsers();
            $scope.select = function (user) {
                $scope.deleteObject = user;
                Users.remove.query({id: user._id, action: 0}, function (data) {
                    if (data.res) {
                        $scope.showStatusModal(user, data.res, data.role, 1);
                    } else {
                        if (data.role == 'admin') {
                            $rootScope.Notifications('Lo sentimos, solo existe un usuario administrador', 2);
                        }
                        else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado.', 2);
                        }
                    }
                })
            };
            $scope.create = function () {
                var birthday = new Date($scope.year, $scope.month, $scope.day);
                if ($scope.validateCreateForm()) {
                    Users.back.create({
                        userName: $scope.userName,
                        lastName: $scope.lastName,
                        email: $scope.email,
                        sex: $scope.sex,
                        birthday: birthday.getTime(),
                        phoneNumber: $scope.phoneNumber,
                        enterprise: $scope.enterprise,
                        address: $scope.address,
                        city: $scope.city,
                        postalCode: $scope.postalCode,
                        country: $scope.country,
                        role: $scope.role,
                        password: $scope.password
                    }, function (data) {
                        if (data.res) {
                            $scope.registerForm.$setPristine();
                            $rootScope.Notifications('Usuario creado satisfactoriamente', 0);
                            $rootScope.loadStatistics();
                            $scope.user_id = "";
                            $scope.loadUsers();
                            $rootScope.loadStatistics();
                            $scope.hideForm();
                        }
                        else {
                            if (data.error == 0) {
                                $rootScope.Notifications('Debe completar todos los campos requeridos para el registro.', 2);
                            } else {
                                if (data.error == 1) {
                                    $rootScope.Notifications('El email registrado por Ud. ya está siendo usado. Por favor, elija otro email.', 2);
                                } else {
                                    $rootScope.Notifications('Ha ocurrido un error, el usuario no se ha podido crear', 2);
                                }
                            }
                        }
                    })
                }
            };
            $scope.save = function () {
                var birthday = new Date($scope.year, $scope.month, $scope.day);
                if ($scope.validateCreateEdit()) {
                    Users.save({
                        id: $scope.id_user,
                        userName: $scope.userName,
                        lastName: $scope.lastName,
                        email: $scope.email,
                        sex: $scope.sex,
                        birthday: birthday.getTime(),
                        phoneNumber: $scope.phoneNumber,
                        enterprise: $scope.enterprise,
                        address: $scope.address,
                        city: $scope.city,
                        postalCode: $scope.postalCode,
                        country: $scope.country,
                        role: $scope.role
                    }, function (data) {
                        if (data.res) {
                            $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                            $scope.user_id = "";
                            $scope.loadUsers();
                            $rootScope.loadStatistics();
                            $scope.hideForm();
                        }
                        else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                        }
                    })
                }
            };
            $scope.delete = function () {
                if (!$scope.deleteObject.length) {
                    Users.remove.query({id: $scope.deleteObject._id, action: 1}, function (data) {
                        if (data.res) {
                            if (data.complete) {
                                $scope.deleteObject = "";
                                $scope.loadUsers();
                                $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                            }

                        } else {
                            if (data.role == 'admin') {
                                $rootScope.Notifications('Lo sentimos, solo existe un usuario administrador', 2);
                            }
                            else {
                                $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado.', 2);
                            }
                        }
                    })
                }

            };
            $scope.changeStatus = function (user, action) {
                $scope.selectedUser = user;
                $http({
                    method: 'POST', url: '/api/users/status',
                    data: {
                        id: user._id,
                        action: action
                    }
                }).
                    success(function (data) {
                        if (data.res) {
                            if (data.complete) {
                                $scope.user_id = "";
                                $scope.loadUsers();
                            } else {
                                $scope.showStatusModal(user, data.res, data.role, 0);
                            }
                        } else {
                            if (data.role == 'admin') {
                                $rootScope.Notifications('Lo sentimos, solo existe un usuario administrador', 2);
                            }
                            else {
                                $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado.', 2);
                            }
                        }
                    });
            };

            $scope.showStatusModal = function (data, result, role, modal) {
                $scope.changeStatusItem = data;
                $scope.role = role;
                $scope.ships = null;
                $scope.services = null;
                $scope.packs = null;
                $scope.ship = null;
                if (role == 'patron') {
                    $scope.ship = result
                }
                else if (role == 'owner') {
                    $scope.ships = result[0];
                    $scope.services = result[1];
                    $scope.packs = result[2];
                }
                if (modal) {
                    $('#deleteModal').modal('show');
                }
                else {
                    $('#statusModal').modal('show');
                }

            };

            $scope.changePass = function () {
                $http({
                    method: 'POST', url: '/api/users/newpass',
                    data: {
                        id: $scope.id_user,
                        password: $scope.password
                    }
                }).
                    success(function (data) {
                        if (data.res) {
                            $rootScope.Notifications('Se ha cambiado satisfactoriamente la contraseña', 0);
                            $scope.passSuccessShow = true;
                        }
                        else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar la contraseña', 2);
                        }
                    });
            };

            $scope.validateCreateForm = function () {
                var aux = (
                $scope.password == $scope.password1 &&
                $scope.password.length >= 6 &&
                $scope.password1.length >= 6 &&
                $scope.userName.length > 0 &&
                $scope.lastName.length > 0 &&
                $scope.emailValidator() &&
                $scope.address.length > 0 &&
                $scope.city.length > 0 &&
                $scope.country.length > 0
                );
                return aux;
            }
            $scope.validateCreateEdit = function () {
                var aux = (
                $scope.userName.length > 0 &&
                $scope.lastName.length > 0 &&
                $scope.emailValidator() &&
                $scope.address.length > 0 &&
                $scope.city.length > 0 &&
                $scope.country.length > 0
                );
                return aux;
            }


        }]);