/**
 * Created by ernestomr87@gmail.com on 17/07/14.
 */

angular.module('bojeoApp.countriesController', [])
    .controller('countriesController', ['$scope', '$rootScope', 'Countries',
        function ($scope, $rootScope, Countries) {

            $rootScope.PageSelected = 'countries';
            $rootScope.loadStatistics();
            $scope.pos = 0;
            $scope.initializeCountry = function () {
                $scope.iso = "";
                $scope.name = {
                    es: "",
                    en: ""
                };
                $scope.cities = new Array();
            };
            $scope.initializeCity = function () {
                $scope.ccity = {
                    es: "",
                    en: ""
                };
                $scope.cities = new Array();
            };
            $scope.initializeCountry();
            $scope.initializeCity();
            $scope.showFormCoutry = false;
            $scope.showFormCity = false;

            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;

            $scope.semaphoreEditCountry = false;
            $scope.semaphoreEditCity = false;
            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.loadCountries();
            };
            $scope.changeShowFormCountries = function (bool) {
                if (bool == true) {
                    $scope.initializeCountry();
                    $scope.semaphoreEditCountry = false;
                }
                $scope.showFormCoutry = bool;
            };
            $scope.changeFormCities = function (bool) {
                if (bool == true) {
                    $scope.initializeCity();
                    $scope.semaphoreEditCity = false;
                }
                $scope.showFormCity = bool;
            }
            $scope.selectedCountry = function (index) {
                $scope.countrySelected = $scope.countries[index];
                $scope.pos = index;
            }
            $scope.edit = function (country) {
                $scope.id = country._id;
                $scope.iso = country.iso;
                $scope.name = {
                    es: country.name.es,
                    en: country.name.en
                };
                $scope.semaphoreEdit = true;
                $scope.showFormCoutry = true;
            };
            $scope.text = "";
            $scope.loadCountries = function () {
                $scope.countrySelected=new Array();
                Countries.query({limit: $scope.maxSize, skip: $scope.skip, text: $scope.text}, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.cont;
                        $scope.countries = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.countries.length > 0) {
                            $scope.countrySelected = $scope.countries[$scope.pos];
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadCountries();
                            }
                        }
                    } else {
                        $scope.allContact = new Array();
                        $scope.empty = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener los contactos.', 2);
                    }
                });
            };
            $scope.loadCountries();
            $scope.create = function () {
                Countries.create({
                    iso: $scope.iso,
                    name: $scope.name,
                    cities: $scope.cities
                }, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('País creado satisfactoriamente',0);
                        $scope.loadCountries();
                        $rootScope.loadStatistics();
                        $scope.changeShowFormCountries(false);
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido crear el País',2);
                    }
                })
            };
            $scope.update = function () {
                Countries.save({
                    id: $scope.id,
                    iso: $scope.iso,
                    name: $scope.name,
                    cities: $scope.cities
                }, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Cambios guardados satisfactoriamente',0);
                        $scope.loadCountries();
                        $rootScope.loadStatistics();
                        $scope.changeShowFormCountries(false)();
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios',2);
                    }
                })
            };
            $scope.delete = function (country) {
                Countries.delete({
                    id: country._id
                }, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('País eliminado satisfactoriamente',0);
                        $scope.pos = 0;
                        $scope.loadCountries();
                        $rootScope.loadStatistics();
                        $scope.changeFormCities(false);

                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido eliminar el País',2);
                    }
                })
            }

            $scope.updateCity = function () {
                Countries.saveCity.save({
                    id: $scope.countrySelected._id,
                    cityOld: $scope.oldCity,
                    city: $scope.ccity
                }, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Cambios guardados satisfactoriamente',0);
                        $scope.loadCountries();
                        $scope.changeFormCities(false);
                    }
                    else {
                        if(data.code){
                            $rootScope.Notifications('La ciudad ya se encuentra registrada',2);
                        }else{
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios',2);
                        }
                    }
                })
            }
            $scope.addCity = function () {
                Countries.addCity.create({
                    id: $scope.countrySelected._id,
                    city: $scope.ccity
                }, function (data) {
                    if (data.res) {
                        $scope.loadCountries();
                        $rootScope.loadStatistics();
                        $scope.changeFormCities(false);
                        $rootScope.Notifications('Ciudad creada satisfactoriamente',0);
                    }
                    else {
                        if(data.code){
                            $rootScope.Notifications('La ciudad ya se encuentra registrada',2);
                        }else{
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido crear la Ciudad',2);
                        }

                    }
                })
            }
            $scope.editCity = function (city) {
                $scope.oldCity = city;
                $scope.ccity = {
                    es: city.es,
                    en: city.en
                }
                $scope.semaphoreEditCity = true;
                $scope.showFormCity = true;
            }
            $scope.delCity = function (city) {
                Countries.delCity.delete({
                    id: $scope.countrySelected._id,
                    city: city
                }, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Ciudad eliminada satisfactoriamente',0);
                        $scope.loadCountries();
                        $rootScope.loadStatistics();
                        $scope.changeFormCities(false);
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido eliminar la Ciudad',2);
                    }
                })
            }
            /*$scope.update = function () {
             Countries.save({
             id: $scope.id,
             iso: $scope.iso,
             name: $scope.name,
             cities: $scope.cities
             }, function (data) {
             if (data.res) {
             $scope.loadCountries();
             $rootScope.loadStatistics();
             $scope.changehideFormCountries();
             }
             else {
             alert('error');
             }
             })
             }*/

            /* $scope.hours;
             $scope.price;
             $scope.deleteItem;
             $scope.editBono = false;
             $scope.id;
             $scope.changeDeleteItem = function (bond) {
             $scope.deleteItem = bond;
             $('#myModal').modal('show');
             };
             $scope.showForm = function () {
             $('#form-bond').slideDown(500);
             };
             $scope.hideForm = function () {

             $('#form-bond').slideUp(500);
             $scope.editBono = false;
             $scope.hours = '';
             $scope.price = '';

             };
             $scope.edit = function (bond) {
             $scope.editBono = true;
             $scope.hours = bond.hours;
             $scope.price = bond.price;
             $scope.id = bond._id;
             $('#form-bond').slideDown(500);
             };
             $scope.loadBonds = function () {
             $http({method: 'GET', url: '/api/abonos'}).
             success(function (data) {
             if (data.res) {
             $scope.bonds = data.res;
             }
             });
             };
             $scope.loadBonds();
             $scope.create = function () {
             $http({method: 'POST', url: '/api/abonos',
             data: {
             hours: $scope.hours,
             price: $scope.price
             }}).
             success(function (data) {
             if (data.res) {
             $scope.loadBonds();
             $rootScope.loadStatistics();
             }
             });
             };
             $scope.remove = function () {
             $('#myModal').modal('hide');
             $http({method: 'POST', url: '/api/abonos/remove',
             data: {
             id: $scope.deleteItem._id
             }}).
             success(function (data) {
             if (data.res) {
             $rootScope.loadStatistics();
             $scope.loadBonds();
             }
             });
             };
             $scope.save = function () {
             $('#myModal').modal('hide');
             $http({method: 'PUT', url: '/api/abonos',
             data: {
             id: $scope.id,
             hours: $scope.hours,
             price: $scope.price
             }}).
             success(function (data) {
             if (data.res) {
             $scope.hideForm();
             $scope.loadBonds();
             }
             });
             };

             $scope.changeStatus = function (bond) {
             $http({method: 'POST', url: '/api/abonos/status',
             data: {
             id: bond._id,
             available: bond.available
             }}).
             success(function (data) {
             if (data.res) {
             $scope.loadBonds();
             }
             });
             }*/


        }]);
