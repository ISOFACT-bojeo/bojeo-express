/**
 * Created by Juan Javier on 19/11/2014.
 */

angular.module('bojeoApp.enterpriseController', [])
    .controller('enterpriseController', ['$scope', '$rootScope', 'Enterprise', '$http', '$upload',
        function ($scope, $rootScope, Enterprise, $http, $upload) {

            $rootScope.PageSelected = 'enterprise';
            $rootScope.loadStatistics();
            $scope.semaphore = true;
            $scope.openClose = true;


            $scope.name = {
                es: "",
                en: ""
            };
            $scope.description = {
                es: "",
                en: ""
            };

            $scope.url = "";

            $scope.tabsShowEs = {
                description: true
            };

            $scope.image = null;
            $scope.files = [];
            var img = false;

            $scope.cantImage = 1;
            $scope.dimensionImage = "300x220px"
            $scope.formatImage = "[ jpg, png ]";
            $scope.resolucionImage = "72dpi";

            $scope.validCantImg = 0;
            $scope.cantidadImg = 0;
            $scope.erroresNameImage = [];

            $scope.text = "";
            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;
            $scope.loadEnterprise = function () {
                Enterprise.list.query({limit: $scope.maxSize, skip: $scope.skip, word: $scope.text}, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.cont;
                        $scope.enterprise = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.enterprise.length > 0) {
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadEnterprise();
                            }
                        }
                    } else {
                        $scope.enterprise = new Array();
                        $scope.empty = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener las empresas.', 2);
                    }
                });
            };

            $scope.loadEnterprise();

            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.loadEnterprise();
            };

            $scope.showForm = function () {
                $scope.openClose = false;
                $scope.tabsShowEs = {
                    description: true
                };
                $('#form-enterprise').show();
            };
            $scope.hideForm = function () {
                $scope.openClose = true;
                $('#form-enterprise').hide();
                $scope.clearField();
            };

            $scope.clearField = function () {
                $scope.name = {
                    es: "",
                    en: ""
                };
                $scope.description = {
                    es: "",
                    en: ""
                };
                $scope.url = "";
                $scope.files = [];
                $scope.image = null;
                img = false;
                $scope.validCantImg = 0;
                $scope.cantidadImg = 0;
                $scope.erroresNameImage = [];
                $('#imgFile').val(null);
            };

            /**
             * @method Evento que se ejecuta cuando el archivo es seleccionado
             * @param $files
             */
            $scope.onImgSelect = function ($files) {
                img = false;
                $scope.erroresNameImage = [];
                $scope.validCantImg = 0;
                var typeArray = ["image/jpeg", "image/png"];
                if ($rootScope.validateFile($files[0], typeArray)) {
                    $scope.files[0] = $files[0];
                    $scope.cantidadImg++;
                    img = true;
                } else {
                    $scope.error = "errorType";
                    $scope.erroresNameImage.push($files[0].name);
                    $('#imgFile').val(null);
                }
            };

            $scope.createEnterprise = function () {
                var bool = true;
                $scope.errorNewsImg = true;
                if ($scope.name.es == "" || $scope.name.es == null) {
                    bool = false;
                    $scope.errorNombre = "Campo vacío";
                }
                if ($scope.name.en == "" || $scope.name.en == null) {
                    bool = false;
                    $scope.errorNombreEn = "Campo vacío";
                }
                if ($scope.description.es == "" || $scope.description.es == null) {
                    bool = false;
                    $scope.errorDescripcion = "Campo vacío";
                }
                if ($scope.description.en == "" || $scope.description.en == null) {
                    bool = false;
                    $scope.errorDescripcion_en = "Campo vacío";
                }
                if (!img) {
                    bool = false;
                    $scope.errorImgUpload = "Campo vacío";
                }
                if (bool) {
                    $upload.upload({
                        url: '/api/enterprise', //upload.php script, node.js route, or servlet url
                        method: 'POST',
                        headers: {'header-key': 'multipart/form-data'},
                        data: { name_es: $scope.name.es, name_en: $scope.name.en, description_es: $scope.description.es, description_en: $scope.description.en, url: $scope.url},
                        file: $scope.files
                    }).success(function (data, status) {
                        if (data.res) {
                            $rootScope.Notifications('Empresa creada satisfactoriamente.', 0);
                            $scope.loadEnterprise();
                            $scope.hideForm();
                            $rootScope.loadStatistics();
                        } else {
                            $rootScope.Notifications('Ha ocurrido un error, la empresa no se ha podido crear.', 2);
                        }

                    }).error(function (status) {
                        $rootScope.Notifications('Ha ocurrido un error, la empresa no se ha podido crear.', 2);
                    });
                } else {
                    return;
                }
            };

            $scope.showEditEnterprise = function (data) {
                $scope.idEnterpriseEdit = data._id;
                $scope.name = data.name;
                $scope.description = data.description;
                $scope.url = data.url;
                $scope.image = data.photo;
                $('#imgFile').val(null);
                $scope.files[0] = $scope.image;
                img = true;
                $scope.semaphore = false;
                $scope.nameImgDel = null;
                $scope.showForm();
            };

            $scope.deleteEnterpriseImg = function (name) {
                $scope.image = null;
                $scope.files[0] = null;
                $scope.nameImgDel = name;
            };

            $scope.saveEnterprise = function () {
                var bool = true;
                if ($scope.name.es == "" || $scope.name.es == null) {
                    bool = false;
                    $scope.errorNombre = "Campo vacío";
                }
                if ($scope.name.en == "" || $scope.name.en == null) {
                    bool = false;
                    $scope.errorNombreEn = "Campo vacío";
                }
                if ($scope.description.es == "" || $scope.description.es == null) {
                    bool = false;
                    $scope.errorDescripcion = "Campo vacío";
                }
                if ($scope.description.en == "" || $scope.description.en == null) {
                    bool = false;
                    $scope.errorDescripcion_en = "Campo vacío";
                }
                if (!img) {
                    bool = false;
                    $scope.errorImgUpload = "Campo vacío";
                }
                if (bool) {
                    Enterprise.save({
                        id: $scope.idEnterpriseEdit,
                        name: $scope.name,
                        description: $scope.description,
                        url: $scope.url
                    }, function (data) {
                        if (data.res) {
                            if ($scope.nameImgDel != null) {
                                $upload.upload({
                                    url: "/api/enterprise/updateImg", //upload.php script, node.js route, or servlet url
                                    method: 'POST',
                                    headers: {'header-key': 'multipart/form-data'},
                                    data: { id: $scope.idEnterpriseEdit, nameImgDel: $scope.nameImgDel},
                                    file: $scope.files
                                }).success(function (data) {
                                    if (data.result) {
                                        $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                                        $scope.loadEnterprise();
                                        $scope.hideForm();
                                        $rootScope.loadStatistics();
                                    } else {
                                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                                    }
                                }).error(function (status) {
                                    $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                                });
                            } else {
                                $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                                $scope.loadEnterprise();
                                $scope.hideForm();
                                $rootScope.loadStatistics();
                            }
                        } else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                        }
                    })
                } else {
                    return;
                }
            };

            $scope.changeStatus = function(enterprise) {
                //action
                //0 consulta
                //1 confirmacion
                //2 active
                if (enterprise.available) {
                    $http({
                        method: 'POST', url: '/api/enterprise/status',
                        data: {id: enterprise._id, action: 0}
                    }).success(function (data) {
                        if (data.res) {
                            if (!data.complete) {
                                $scope.showChangeStatusModal(enterprise, data.res);
                            } else {
                                $scope.loadEnterprise();
                            }
                        } else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                        }
                    })
                } else {
                    $http({
                        method: 'POST', url: '/api/enterprise/status',
                        data: {id: enterprise._id, action: 2}
                    }).success(function (data) {
                        if (data.res) {
                            $scope.loadEnterprise();
                        } else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                        }
                    });
                }
            };

            $scope.showChangeStatusModal = function (data, services) {
                $scope.changeStatusItem = data;
                $scope.associationService = services;
                $('#changeStatusModal').modal('show');
            };

            $scope.changeStatusNow = function (enterprise) {
                $http({
                    method: 'POST', url: '/api/enterprise/status',
                    data: {id: enterprise._id, action: 1}
                }).success(function (data) {
                    if (data.res) {
                        $scope.loadEnterprise();
                    } else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                    }
                });
            };

            $scope.deleteEnterprise = function (data) {
                $scope.delEnterprise = data;
                $http({
                    method: 'POST', url: '/api/enterprise/listServices',
                    data: {
                        id: data._id
                    }
                }).success(function (data) {
                    if (data.res) {
                        $scope.deleteServices = data.res;
                    }else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido obtener los datos', 2);
                    }
                });
            };

            $scope.dropEnterprise = function () {
                Enterprise.remove({id: $scope.delEnterprise._id}, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Empresa eliminada satisfactoriamente', 0);
                        $scope.loadEnterprise();
                        $rootScope.loadStatistics();
                    } else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido eliminar la empresa', 2);
                    }
                });
            };
        }]);