/**
 * Created by ernestomr87@gmail.com on 22/07/14.
 */

angular.module('bojeoApp.reservationController', [])
    .controller('reservationController', ['$scope', '$rootScope', '$routeParams', 'Reservation', '$http',
        function ($scope, $rootScope, $routeParams, Reservation, $http) {

            $rootScope.PageSelected = 'reservations';
            $rootScope.loadStatistics();


            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;
            $scope.totalItems = 0;
            $scope.text = "";

            $scope.reload = false;
            $scope.ship_id = "";
            if ($routeParams.ship) {
                $scope.ship_id = $routeParams.ship;
                $scope.reload = true;
            }

            $scope.loadReservations = function () {
                Reservation.query({
                    limit: $scope.maxSize,
                    skip: $scope.skip,
                    text: $scope.text,
                    id: $scope.ship_id
                }, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.cont;
                        $scope.reservations = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.reservations.length > 0) {
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadReservations();
                            }
                        }
                    } else {
                        $scope.reservations = new Array();
                        $scope.empty = true;

                    }
                });
            };
            $scope.loadReservations();
            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.loadReservations();
            };
            $scope.saveReservation = function (reservation) {
                $scope.resTmp = reservation;
            }
            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.loadReservations();
            };
            $scope.reason = "";
            $scope.executeDevolution = function () {
                var data = {
                    token: $scope.resTmp.payment.token,
                    reason: $scope.reason
                }
                $http({method: 'POST', url: '/api/reservation/cancel', data: data}).success(
                    function (data) {
                        $rootScope.loadStatistics()
                        $scope.reason = "";
                        if (data.res) {
                            $scope.loadReservations();
                            $rootScope.Notifications('Devolución satisfactoria.', 0);
                        }
                        else {
                            $rootScope.Notifications('No se ha podido realizar la devolución.', 2);
                        }
                    });
            }

        }]);
