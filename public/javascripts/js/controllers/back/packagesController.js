/**
 * Created by Juan Javier on 27/11/2014.
 */
angular.module('bojeoApp.packagesController', [])
    .controller('packagesController', ['$scope', '$rootScope', '$routeParams', 'Package', '$http', 'Groups',
        function ($scope, $rootScope, $routeParams, Package, $http, Groups) {

            $rootScope.PageSelected = 'packages';
            $rootScope.loadStatistics();
            $scope.packages = null;
            $scope.semaphore = true;
            $scope.openClose = true;
            $scope.editPackage = false;

            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;

            $scope.showSelectPort = true;
            $scope.tourDescription = '';
            $scope.arrayServices = [];
            $scope.text = "";

            $scope.colors = ['#00FF00', '#3FB3E9', '#00FFFF', '#FF7DA3', '#FFFF00', '#F279DA'];
            $scope.color = $scope.colors[0];
            $scope.changeColor = function (index) {
                $scope.color = $scope.colors[index];
            };

            $scope.styleColor = function (color) {
                return {'background_color': color};
            };

            $scope.reload = false;
            $scope.idService = "";
            if ($routeParams.id) {
                $scope.idService = $routeParams.id;
                $scope.reload = true;
            }

            $scope.listPackages = function () {
                Package.list.query({
                    limit: $scope.maxSize,
                    skip: $scope.skip,
                    text: $scope.text,
                    id: $scope.idService
                }, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.cont;
                        $scope.allPackages = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.allPackages.length > 0) {
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.listPackages();
                            }
                        }
                    } else {
                        $scope.allPackages = new Array();
                        $scope.empty = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener los contactos.', 2);
                    }
                });
            };
            $scope.listPackages();
            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.listPackages();
            };

            $scope.startProcess = function (cb) {
                $http({method: 'POST', url: '/api/country/front'}).
                    success(function (ddata) {
                        if (!ddata.res) {
                            window.location = "/";
                        }
                        else {
                            if (ddata.res.length) {
                                $scope.countries = ddata.res;
                                var auxCountries = [];
                                $http({method: 'POST', url: '/api/ports/available'}).
                                    success(function (data) {
                                        if (data.res) {
                                            $scope.allExistsPorts = data.res;
                                            for (var i = 0; i < $scope.countries.length; i++) {
                                                var exist_country = false;
                                                for (var j = 0; j < $scope.allExistsPorts.length; j++) {
                                                    if ($scope.countries[i].name.es == $scope.allExistsPorts[j].country.es) {
                                                        for (var k = 0; k < $scope.countries[i].cities.length; k++) {
                                                            if ($scope.countries[i].cities[k].es == $scope.allExistsPorts[j].city.es) {
                                                                exist_country = true;
                                                                break;
                                                            }
                                                        }
                                                        if (exist_country)break;
                                                    }
                                                }
                                                if (exist_country) {
                                                    auxCountries.push($scope.countries[i]);
                                                }
                                            }
                                            $scope.countries = auxCountries;
                                            $scope.selectedCountry = $scope.countries[0];
                                            $scope.cities = $scope.selectedCountry.cities;
                                            $scope.country = $scope.selectedCountry.name.es;


                                            var auxCities = [];
                                            for (var i = 0; i < $scope.cities.length; i++) {
                                                var exist_city = false;
                                                for (var j = 0; j < $scope.allExistsPorts.length; j++) {
                                                    if ($scope.cities[i].es == $scope.allExistsPorts[j].city.es) {
                                                        exist_city = true;
                                                        break;
                                                    }
                                                }
                                                if (exist_city) {
                                                    auxCities.push($scope.cities[i]);
                                                }
                                            }
                                            $scope.cities = auxCities;
                                            $scope.selectedCity = $scope.cities[0];
                                            $scope.city = $scope.selectedCity.es;
                                            $scope.filterByCity();

                                            if (cb) cb(true);
                                        }
                                        else {
                                            alert("Error");
                                        }

                                    });
                            }

                        }

                    });
                Groups.pack.query(function (data) {
                    $scope.groups = data.res;
                    $scope.groupSelected = $scope.groups[0];
                })
            };
            $scope.filterByCity = function () {
                $http({
                    method: 'POST', url: '/api/ports/city', data: {
                        country: $scope.selectedCountry.name.en,
                        city: $scope.selectedCity.en
                    }
                }).
                    success(function (data) {
                        if (!data.res) {
                            $scope.allPorts = null;
                        }
                        else {
                            $scope.allPorts = data.res;
                            $scope.selectedPort = $scope.allPorts[0];
                            $scope.namePort = $scope.selectedPort.name.es;
                            $scope.id_port = $scope.allPorts[0]._id;
                        }
                    });
            };
            $scope.changeCountry = function () {
                for (var i = 0; i < $scope.countries.length; i++) {
                    if ($scope.countries[i].name.es == $scope.country || $scope.countries[i].name.en == $scope.country) {
                        $scope.selectedCountry = $scope.countries[i];


                        $scope.cities = $scope.selectedCountry.cities;
                        $scope.country = $scope.selectedCountry.name.es;

                        var auxCities = [];
                        for (var i = 0; i < $scope.cities.length; i++) {
                            var exist_city = false;
                            for (var j = 0; j < $scope.allExistsPorts.length; j++) {
                                if ($scope.cities[i].es == $scope.allExistsPorts[j].city.es) {
                                    exist_city = true;
                                    break;
                                }
                            }
                            if (exist_city) {
                                auxCities.push($scope.cities[i]);
                            }
                        }
                        $scope.cities = auxCities;
                        for (var i = 0; i < $scope.cities.length; i++) {
                            var exist_city = false;
                            for (var j = 0; j < $scope.allExistsPorts.length; j++) {
                                if ($scope.cities[i].es == $scope.allExistsPorts[j].city.es) {
                                    exist_city = true;
                                    break;
                                }
                            }
                            if (!exist_city) {
                                $scope.cities.splice(i, 1);
                            }
                        }
                        $scope.selectedCity = $scope.cities[0];
                        $scope.city = $scope.selectedCity.es;
                        $scope.filterByCity();
                        $scope.tourDescription = '';
                        $scope.arrayServices = [];
                        break;
                    }
                }

            };
            $scope.changeCity = function () {
                for (var i = 0; i < $scope.selectedCountry.cities.length; i++) {
                    if ($scope.selectedCountry.cities[i].es == $scope.city) {
                        $scope.selectedCity = $scope.selectedCountry.cities[i];
                        $scope.city = $scope.selectedCity.es;
                        $scope.filterByCity();
                        break;
                    }
                }
                $scope.tourDescription = '';
                $scope.arrayServices = [];
            };
            $scope.startProcess();

            $scope.changeStatus = function (pack, action) {
                $scope.selectedPack = pack;
                $http({
                    method: 'POST', url: '/api/package/status',
                    data: {id: pack._id, action: action}
                }).success(function (data) {
                    if (data.res) {
                        if (data.complete) {
                            $scope.idService = "";
                            $scope.listPackages();
                            //$rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                        } else {
                            $scope.showStatusModal(pack, data.res);
                        }

                    } else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado.', 2);
                    }
                });


            };

            $scope.showStatusModal = function (data, result) {
                $scope.changeStatusItem = data;
                $scope.services = result.services;
                $scope.tour = result.cantTour;
                $scope.port = result.ports;
                $scope.users = result.users;
                $scope.enterprises = result.enterprises;

                $('#statusModal').modal('show');
            };

            $scope.changePort = function () {
                $scope.tourDescription = '';
                $scope.arrayServices = [];
            };

            $scope.showForm = function () {
                $scope.openClose = false;
                //$scope.loadTours();
                $('#form-package').show();
            };
            $scope.hideForm = function () {
                $scope.openClose = true;
                $('#form-package').hide();
                $scope.clearField();
            };

            $scope.clearField = function () {
                $scope.tourDescription = '';
                $scope.arrayServices = [];
                $scope.idTour = null;
                $scope.groupSelected = $scope.groups[0];
                $scope.editPackage = false;
                $scope.semaphore = true;
                $scope.idService = "";
            };

            $scope.currentPageT = 1;
            $scope.maxSizeT = 10;
            $scope.skipT = 0;
            $scope.wordSearchT = "";
            $scope.listTours = function () {
                var pos = -1;
                for (var i = 0; i < $scope.allExistsPorts.length; i++) {
                    if ($scope.allExistsPorts[i].name.es == $scope.namePort && $scope.allExistsPorts[i].city.es == $scope.city && $scope.allExistsPorts[i].country.es == $scope.country) {
                        pos = i;
                        break;
                    }
                }
                if (pos != -1) {
                    $scope.port = $scope.allExistsPorts[pos];
                    $http({
                        method: 'POST', url: '/api/tours/postGroup', data: {
                            idPort: $scope.port._id,
                            idGroup: $scope.groupSelected._id,
                            limit: $scope.maxSizeT,
                            skip: $scope.skipT,
                            text: $scope.wordSearchT
                        }
                    }).success(function (data) {
                        if (data.res) {
                            $scope.totalItemsT = data.cont;
                            $scope.tours = data.res;
                            if (data.cont == 0) {
                                $scope.emptyTour = true;
                            }
                            if ($scope.tours.length > 0) {
                                $scope.emptyTour = false;
                            } else {
                                if ($scope.currentPageT > 1) {
                                    $scope.currentPageT--;
                                    $scope.skipT = $scope.skipT - $scope.maxSizeT;
                                    $scope.listTours();
                                }
                            }
                        } else {
                            $scope.tours = new Array();
                            $scope.emptyTour = true;
                            $rootScope.Notifications('Ha ocurrido un error. No se han podido obtener los Tours.', 2);
                        }
                    });
                }
            };
            $scope.pageChangedT = function () {
                $scope.skipT = ($scope.currentPageT - 1) * $scope.maxSizeT;
                $scope.listTours();
            };

            $scope.currentPageS = 1;
            $scope.maxSizeS = 10;
            $scope.skipS = 0;
            $scope.wordSearchS = "";
            $scope.loadServicesAvailable = function (services) {
                $http({
                    method: 'POST', url: '/api/service/available', data: {
                        limit: $scope.maxSizeS,
                        skip: $scope.skipS,
                        text: $scope.wordSearchS
                    }
                }).success(function (data) {
                    if (data.res) {
                        $scope.totalItemsS = data.cont;
                        var result = data.res;
                        if (data.cont == 0) {
                            $scope.emptyService = true;
                        }
                        if (result.length > 0) {
                            $scope.emptyService = false;
                            if (services == null) {
                                $scope.services = data.res;
                            } else {
                                for (var i = 0; i < services.length; i++) {
                                    for (var j = 0; j < result.length; j++) {
                                        if (services[i].id == result[j]._id) {
                                            result[j].active = true;
                                            break;
                                        }
                                    }
                                }
                                $scope.services = result;
                            }
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadServicesAvailable();
                            }
                        }
                    } else {
                        $scope.services = new Array();
                        $scope.emptyService = true;
                        $rootScope.Notifications('Ha ocurrido un error. No se han podido obtener los Servicios.', 2);
                    }
                });
            };

            $scope.pageChangedS = function () {
                $scope.skipS = ($scope.currentPageS - 1) * $scope.maxSizeS;
                $scope.loadServicesAvailable($scope.arrayServices);
            };

            $scope.addTour = function (tour) {
                if (tour._id != $scope.idTour) {
                    $scope.idTour = tour._id;
                    $scope.tourDescription = tour.name.es;
                    $scope.arrayServices = [];
                    $scope.loadServicesAvailable(null);
                }
            };

            $scope.selectedGroup = function () {
                $scope.tourDescription = '';
                $scope.arrayServices = [];
            };

            $scope.addServ = function (service) {
                service.active = true;
                var service = {id: service._id};
                $scope.arrayServices.push(service);
            };

            $scope.delServ = function (service) {
                service.active = false;
                for (var i = 0; i < $scope.arrayServices.length; i++) {
                    if ($scope.arrayServices[i].id == service._id) {
                        $scope.arrayServices.splice(i, 1);
                        break;
                    }
                }
            };

            $scope.createPackage = function () {
                var bool = true;
                if ($scope.tourDescription == "" || $scope.tourDescription == null) {
                    bool = false;
                    $scope.errorTourDescription = "Campo vacío";
                }
                if (bool) {
                    Package.create({
                        tour: $scope.idTour,
                        services: $scope.arrayServices,
                        color: $scope.color
                    }, function (data) {
                        if (data.res) {
                            $rootScope.Notifications('Paquete creado satisfactoriamente.', 0);
                            $scope.idService = "";
                            $scope.listPackages();
                            $scope.hideForm();
                            $rootScope.loadStatistics();
                        } else {
                            if (data.code == "1") {
                                $rootScope.Notifications('Ya existe un paquete creado con ese Tour y Servicio.', 2);
                            } else {
                                $rootScope.Notifications('Ha ocurrido un error, el paquete no se ha podido crear.', 2);
                            }
                        }
                    })
                } else {
                    return;
                }
            };

            $scope.deletePackage = function (data) {
                $scope.delPackage = data;
            };

            $scope.dropPackage = function () {
                Package.remove({id: $scope.delPackage._id}, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Paquete eliminado satisfactoriamente', 0);
                        $scope.idService = "";
                        $scope.listPackages();
                        $rootScope.loadStatistics();
                    } else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido eliminar el paquete', 2);
                    }
                });
            };

            $scope.showEditPackage = function (data) {
                $scope.editPackage = true;
                $scope.semaphore = false;
                $scope.idPackageEdit = data._id;
                $scope.idTour = data.tour.id;
                $scope.inputCategoriaTour = data.tour.nameGroup.es;
                $scope.tourDescription = data.tour.name.es;
                $scope.color = data.color,
                    $scope.arrayServices = [];
                for (var i = 0; i < data.services.length; i++) {
                    var service = {id: data.services[i].id};
                    $scope.arrayServices.push(service);
                }
                $scope.loadServicesAvailable(data.services);
                $scope.showForm();
            };

            $scope.savePackage = function () {
                Package.save({
                    id: $scope.idPackageEdit,
                    tour: $scope.idTour,
                    services: $scope.arrayServices,
                    color: $scope.color
                }, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                        $scope.listPackages();
                        $scope.hideForm();
                        $rootScope.loadStatistics();
                    } else {
                        if (data.code == "1") {
                            $rootScope.Notifications('Ya existe un paquete creado con ese Tour y Servicio.', 2);
                        } else {
                            $rootScope.Notifications('Ha ocurrido un error, el paquete no se ha podido salvar .', 2);
                        }
                    }
                })
            };

        }]);