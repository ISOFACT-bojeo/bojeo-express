/**
 * Created by ernestomr87@gmail.com on 15/05/14.
 */

angular.module('bojeoApp.DateController', [])
.controller('DateController', function($scope) {
    $scope.var1 = '15-05-2014';
});


angular.directive('datetimez', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            element.datetimepicker({
                dateFormat:'dd-MM-yyyy',
                language: 'en',
                pickTime: false,
                startDate: '15-05-2014',      // set a minimum date
                endDate: '01-11-2030'          // set a maximum date
            }).on('changeDate', function(e) {
                    ngModelCtrl.$setViewValue(e.date);
                    scope.$apply();
                });
        }
    };
});
