/**
 * Created by ernestomr87@gmail.com on 17/07/14.
 */

angular.module('bojeoApp.abonoController', [])
    .controller('abonoController', ['$scope', '$rootScope', '$http', 'Bond',
        function ($scope, $rootScope, $http, Bond) {

            $rootScope.PageSelected = 'bonds';
            $rootScope.loadStatistics();

            $scope.hours;
            $scope.price;
            $scope.deleteItem;
            $scope.editBono = false;
            $scope.id;

            $scope.changeDeleteItem = function (bond) {
                if (!bond.occupied.tour.hasReservation && !bond.available && !bond.occupied.user) {
                    $scope.deleteItem = bond;
                    $http({
                        method: 'POST', url: '/api/abonos/tour',
                        data: {
                            id: bond._id
                        }
                    }).success(function (data) {
                        if (data.res) {
                            $scope.deleteTours = data.res;
                        }else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido obtener los datos', 2);
                        }
                    });
                }
            };

            $scope.openClose = true;
            $scope.showForm = function () {
                $scope.openClose = false;
                $('#form-bond').show();
            };
            $scope.hideForm = function () {
                $scope.openClose = true;
                $('#form-bond').hide();
                $scope.hours = '';
                $scope.price = '';
            };

            $scope.edit = function (bond) {
                $scope.editBono = true;
                $scope.hours = bond.hours;
                $scope.price = bond.price;
                $scope.id = bond._id;
                $('#form-bond').slideDown(500);
            };


            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;
            $scope.totalItems = 0;
            $scope.loadBonds = function () {
                Bond.list.query({limit: $scope.maxSize, skip: $scope.skip}, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.cont;
                        $scope.bonds = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.bonds.length > 0) {
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadBonds();
                            }
                        }
                    } else {
                        $scope.allContact = new Array();
                        $scope.empty = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener los contactos.', 2);
                    }
                });
            };
            $scope.loadBonds();

            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.loadBonds();
            };

            $scope.create = function () {
                $http({
                    method: 'POST', url: '/api/abonos',
                    data: {
                        hours: $scope.hours,
                        price: $scope.price
                    }
                }).
                    success(function (data) {
                        if (data.res) {
                            $rootScope.Notifications('Bono creado satisfactoriamenete', 0);
                            $scope.loadBonds();
                            $rootScope.loadStatistics();
                            $scope.hideForm();
                        }
                        else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido crear el Bono', 2);
                        }
                    });
            };

            $scope.delete = function () {
                $http({
                    method: 'POST', url: '/api/abonos/remove',
                    data: {
                        id: $scope.deleteItem._id
                    }
                }).success(function (data) {
                        if (data.res) {
                            $rootScope.Notifications('Bono eliminado satisfactoriamenete', 0);
                            $rootScope.loadStatistics();
                            $scope.loadBonds();
                        }
                        else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido eliminar el Tour', 2);
                        }
                    });
            };
            $scope.save = function () {
                $('#myModal').modal('hide');
                $http({
                    method: 'PUT', url: '/api/abonos',
                    data: {
                        id: $scope.id,
                        hours: $scope.hours,
                        price: $scope.price
                    }
                }).
                    success(function (data) {
                        if (data.res) {
                            $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                            $scope.hideForm();
                            $scope.loadBonds();
                        }
                        else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                        }
                    });
            };

            $scope.changeStatus = function (bond) {
                //action
                //0 consulta
                //1 confirmacion
                //2 active
                if (!bond.available) {
                    $http({
                        method: 'POST', url: '/api/abonos/status',
                        data: {
                            id: bond._id,
                            action: 0,
                            available: bond.available
                        }
                    }).
                        success(function (data) {
                            if (!data.complete) {
                                $scope.showChangeStatusModal(bond, data.res);
                            } else {
                                $scope.loadBonds();
                            }
                        });
                } else {
                    $http({
                        method: 'POST', url: '/api/abonos/status',
                        data: {
                            id: bond._id,
                            action: 2,
                            available: bond.available
                        }
                    }).
                        success(function (data) {
                            if (data.res) {
                                $scope.loadBonds();
                            } else {
                                $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                            }
                        });
                }
            };

            $scope.showChangeStatusModal = function (data, tours) {
                $scope.changeStatusItem = data;
                $scope.associationTours = tours;
                $('#changeStatusModal').modal('show');
            };

            $scope.changeStatusNow = function (bond) {
                $http({
                    method: 'POST', url: '/api/abonos/status',
                    data: {
                        id: bond._id,
                        action: 1,
                        available: bond.available
                    }
                }).success(function (data) {
                    if (data.res) {
                        $scope.loadBonds();
                    }else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                    }
                });
            };
        }]);
