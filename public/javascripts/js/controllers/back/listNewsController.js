/**
 * Created by ernestomr87@gmail.com on 18/05/14.
 */
angular.module('bojeoApp.listNewsController', [])
    .controller('listNewsController', ['$scope', '$rootScope', 'News', 'Statistics', '$http', '$upload',
        function ($scope, $rootScope, News, Statistics, $http, $upload) {
            $rootScope.PageSelected = 'listNews';
            $rootScope.loadStatistics();
            $scope.semaphore = true;
            $scope.doc = null;
            $scope.image = null;

            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;
            $scope.text = "";

            $scope.openClose = true;

            $scope.files = [];
            var img = false;
            var doc = false;

            $scope.cantImage = 1;
            $scope.dimensionImage = "300x220px"
            $scope.formatImage = "[ jpg, png ]";
            $scope.resolucionImage = "72dpi";

            $scope.cantDoc = 1;
            $scope.formatDoc = "[ pdf, doc, docx ]";

            $scope.validCantImg = 0;
            $scope.cantidadImg = 0;

            $scope.validCantDoc = 0;
            $scope.cantidadDoc = 0;

            $scope.erroresNameImage = [];
            $scope.erroresNameDoc = [];

            $scope.loadNews = function () {
                News.list.query({limit: $scope.maxSize, skip: $scope.skip, text: $scope.text}, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.cont;
                        $scope.news = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.news.length > 0) {
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadNews();
                            }
                        }
                    } else {
                        $scope.news = new Array();
                        $scope.empty = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener los contactos.', 2);
                    }
                });
            };
            $scope.loadNews();
            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.loadNews();
            };
            $scope.dismissErrorDiv = function () {
                $scope.errorTitle = "";
                $scope.errorIntroduction = "";
                $scope.errorBody = "";
            };
            $scope.showForm = function () {
                $scope.openClose = false;
                $('#form-new').show();
            };
            $scope.hideForm = function () {
                $scope.semaphore = true;
                $scope.openClose = true;
                $('#form-new').hide();
                $scope.clearField();
            };
            $scope.tabsShowEs = {
                description: true
            };
            $scope.tabsShowBodyEs = {
                description: true
            };
            $scope.changeStatus = function (id, status) {
                $http({
                    method: 'PUT', url: '/api/news/public',
                    data: {id: id, status: status}
                }).
                    success(function (data) {
                        if (data.res) {
                            $scope.loadNews();
                        }
                    });
            };
            $scope.closeModal = function () {
                $('#imgFile').val(null);
                $('#docFile').val(null);
                $('#editModal').modal('hide');
                $scope.files = [];
                $scope.loadNews();
            };
            $scope.createNew = function () {
                var bool = true;
                $scope.errorNewsImg = true;
                $scope.errorNewsDocs = true;

                if ($scope.title_es == "" || $scope.title_es == null) {
                    bool = false;
                    $scope.errorTitle = "Campo vacío";
                }
                if ($scope.title_en == "" || $scope.title_en == null) {
                    bool = false;
                    $scope.errorTitle = "Campo vacío";
                }
                if ($scope.introduction_es == "" || $scope.introduction_es == null) {
                    bool = false;
                    $scope.errorIntroduction = "Campo vacío";
                }
                if ($scope.introduction_en == "" || $scope.introduction_en == null) {
                    bool = false;
                    $scope.errorIntroduction_en = "Campo vacío";
                }
                if ($scope.body_es == "" || $scope.body_es == null) {
                    bool = false;
                    $scope.errorBody = "Campo vacío";
                }
                if ($scope.body_en == "" || $scope.body_en == null) {
                    bool = false;
                    $scope.errorBody_en = "Campo vacío";
                }
                if (!img) {
                    bool = false;
                    $scope.errorImgUpload = "Campo vacío";
                }
                if (bool) {
                    $upload.upload({
                        url: '/api/news', //upload.php script, node.js route, or servlet url
                        method: 'POST',
                        headers: {'header-key': 'multipart/form-data'},
                        data: {
                            title_es: $scope.title_es,
                            title_en: $scope.title_en,
                            introduction_es: $scope.introduction_es,
                            introduction_en: $scope.introduction_en,
                            body_es: $scope.body_es,
                            body_en: $scope.body_en,
                            video: $scope.video,
                            originalAutor: $scope.originalAutor,
                            imageFooter_es: $scope.imageFooter_es,
                            imageFooter_en: $scope.imageFooter_en
                        },
                        file: $scope.files
                    }).success(function (data, status) {
                        if (data.res) {
                            $rootScope.Notifications('Noticia creada satisfactoriamente.', 0);
                            $rootScope.loadStatistics();
                            $scope.loadNews();
                            $rootScope.loadStatistics();
                            $scope.hideForm();
                        } else {
                            $rootScope.Notifications('Ha ocurrido un error, la noticia no se ha podido crear.', 2);
                        }

                    }).error(function (status) {
                        $rootScope.Notifications('Ha ocurrido un error, la noticia no se ha podido crear.', 2);
                    });
                } else {
                    return;
                }
            };
            $scope.changeLimit = function () {
                $scope.loadNews();
            };
            $scope.editNews = null;
            $scope.delNews = null;
            $scope.vNews = null;
            $scope.attach = null;
            $scope.showEditNews = function (data) {
                $scope.idNewEdit = data._id;
                $scope.title_es = data.title.es;
                $scope.title_en = data.title.en;
                $scope.introduction_es = data.introduction.es;
                $scope.introduction_en = data.introduction.en;
                $scope.body_es = data.body.es;
                $scope.body_en = data.body.en;
                $scope.video = data.video;
                $scope.imageFooter_es = data.imageFooter.es;
                $scope.imageFooter_en = data.imageFooter.en;
                $scope.originalAutor = data.originalAutor;
                $scope.image = data.photo;
                $('#imgFile').val(null);
                $('#docFile').val(null);
                $scope.files[0] = $scope.image;
                img = true;
                if (data.document.name == "") {
                    $scope.doc = null;
                } else {
                    $scope.doc = data.document;
                    $scope.files[1] = $scope.doc;
                }
                $scope.semaphore = false;
                $scope.nameImgDel = null;
                $scope.nameDocDel = null;
                $scope.showForm();
            };
            $scope.deleteNews = function (data) {
                $scope.delNews = data;
            };
            $scope.sendNews = function () {
                var bool = true;
                if ($scope.title_es == "" || $scope.title_es == null) {
                    bool = false;
                    $scope.errorTitle = "Campo vacío";
                }
                if ($scope.title_en == "" || $scope.title_en == null) {
                    bool = false;
                    $scope.errorTitle = "Campo vacío";
                }
                if ($scope.introduction_es == "" || $scope.introduction_es == null) {
                    bool = false;
                    $scope.errorIntroduction = "Campo vacío";
                }
                if ($scope.introduction_en == "" || $scope.introduction_en == null) {
                    bool = false;
                    $scope.errorIntroduction_en = "Campo vacío";
                }
                if ($scope.body_es == "" || $scope.body_es == null) {
                    bool = false;
                    $scope.errorBody = "Campo vacío";
                }
                if ($scope.body_en == "" || $scope.body_en == null) {
                    bool = false;
                    $scope.errorBody_en = "Campo vacío";
                }
                if (!img) {
                    bool = false;
                    $scope.errorImgUpload = "Campo vacío";
                }
                if (bool) {
                    News.save({
                        ids: $scope.idNewEdit,
                        title_es: $scope.title_es,
                        title_en: $scope.title_en,
                        introduction_es: $scope.introduction_es,
                        introduction_en: $scope.introduction_en,
                        body_es: $scope.body_es,
                        body_en: $scope.body_en,
                        video: $scope.video,
                        originalAutor: $scope.originalAutor,
                        imageFooter_es: $scope.imageFooter_es,
                        imageFooter_en: $scope.imageFooter_en
                    }, function (data) {
                        if (data.res) {
                            var arrayFiles = [];
                            if ($scope.nameImgDel != null) {
                                arrayFiles[0] = $scope.files[0];
                                if ($scope.nameDocDel != null) {
                                    if ($scope.files[1] != null) {
                                        arrayFiles[1] = $scope.files[1];
                                    }
                                } else if ($scope.files[1] != null) {
                                    arrayFiles[1] = $scope.files[1];
                                }
                                $upload.upload({
                                    url: "/api/news/updateImgDoc", //upload.php script, node.js route, or servlet url
                                    method: 'POST',
                                    headers: {'header-key': 'multipart/form-data'},
                                    data: {
                                        id: $scope.idNewEdit,
                                        nameImgDel: $scope.nameImgDel,
                                        nameDocDel: $scope.nameDocDel
                                    },
                                    file: arrayFiles
                                }).success(function (data) {
                                    if (data.res) {
                                        $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                                        $scope.loadNews();
                                        $rootScope.loadStatistics();
                                        $scope.hideForm();
                                    } else {
                                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                                    }
                                }).error(function (status) {
                                    $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                                });
                            } else {
                                var upload = false;
                                var deleteDoc = false;
                                if ($scope.nameDocDel != null) {
                                    if ($scope.files[1] != null) {
                                        arrayFiles[0] = $scope.files[1];
                                        upload = true;
                                    } else {
                                        deleteDoc = true;
                                    }
                                } else if ($scope.files[1] != null && $scope.files[1] instanceof File) {
                                    arrayFiles[0] = $scope.files[1];
                                    upload = true;
                                }
                                if (upload) {
                                    $upload.upload({
                                        url: "/api/news/updateDoc", //upload.php script, node.js route, or servlet url
                                        method: 'POST',
                                        headers: {'header-key': 'multipart/form-data'},
                                        data: {id: $scope.idNewEdit, nameDocDel: $scope.nameDocDel},
                                        file: arrayFiles
                                    }).success(function (data) {
                                        if (data.res) {
                                            $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                                            $scope.loadNews();
                                            $rootScope.loadStatistics();
                                            $scope.hideForm();
                                        } else {
                                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                                        }
                                    }).error(function (status) {
                                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                                    });
                                } else {
                                    if (deleteDoc) {
                                        $http({
                                            method: 'POST',
                                            url: 'api/news/delNewsDoc',
                                            data: {id: $scope.idNewEdit, name: $scope.nameDocDel}
                                        }).
                                            success(function (data) {
                                                if (data.res) {
                                                    $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                                                    $scope.loadNews();
                                                    $rootScope.loadStatistics();
                                                    $scope.hideForm();
                                                } else {
                                                    $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                                                }
                                            }).
                                            error(function (data, status, headers, config) {
                                                $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                                            });
                                    } else {
                                        $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                                        $scope.loadNews();
                                        $rootScope.loadStatistics();
                                        $scope.hideForm();
                                    }
                                }
                            }
                        } else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                        }
                    });
                }
                else {
                    return;
                }
            };
            $scope.clearField = function () {
                $scope.title_es = "";
                $scope.title_en = "";
                $scope.introduction_es = "";
                $scope.introduction_en = "";
                $scope.body_es = "";
                $scope.body_en = "";
                $scope.video = "";
                $scope.imageFooter_es = "";
                $scope.imageFooter_en = "";
                $scope.originalAutor = "";
                $scope.files = [];
                $scope.doc = null;
                $scope.image = null;
                img = false;
                doc = false;
                $scope.validCantImg = 0;
                $scope.cantidadImg = 0;
                $scope.validCantDoc = 0;
                $scope.cantidadDoc = 0;
                $scope.erroresNameImage = [];
                $scope.erroresNameDoc = [];
                $('#imgFile').val(null);
                $('#docFile').val(null);
                $scope.tabsShowEs = {
                    description: true
                };
                $scope.tabsShowBodyEs = {
                    description: true
                };
            };
            $scope.dropNews = function () {
                News.delete({id: $scope.delNews._id}, function (data) {
                    if (data.res) {
                        $scope.loadNews();
                        $rootScope.loadStatistics();
                        $scope.hideForm();
                        $rootScope.Notifications('Noticia eliminada satisfactoriamente', 0);
                    } else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido eliminar la noticia', 2);
                    }
                });
            };
            $scope.deleteNewsImg = function (name) {
                $scope.image = null;
                $scope.files[0] = null;
                $scope.nameImgDel = name;
            }
            $scope.deleteNewsDoc = function (name) {
                $scope.doc = null;
                $scope.files[1] = null;
                $scope.nameDocDel = name;
            }

            /**
             * @method Evento que se ejecuta cuando el archivo es seleccionado
             * @param $files
             */
            $scope.onImgSelect = function ($files) {
                img = false;
                $scope.erroresNameImage = [];
                $scope.validCantImg = 0;
                var typeArray = ["image/jpeg", "image/png"];
                if ($rootScope.validateFile($files[0], typeArray)) {
                    $scope.files[0] = $files[0];
                    $scope.cantidadImg++;
                    img = true;
                } else {
                    $scope.error = "errorType";
                    $scope.erroresNameImage.push($files[0].name);
                    $('#imgFile').val(null);
                }
            };

            /**
             * @method Evento que se ejecuta cuando el documento es seleccionado
             * @param $files
             */
            $scope.onDocSelect = function ($files) {
                doc = false;
                $scope.erroresNameDoc = [];
                $scope.validCantDoc = 0;
                var extDocs = new Array("application/pdf", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                if ($rootScope.validateFile($files[0], extDocs)) {
                    $scope.files[1] = $files[0];
                    $scope.cantidadDoc++;
                    doc = true;
                } else {
                    $scope.error = "errorTypeDoc";
                    $scope.erroresNameDoc.push($files[0].name);
                    $('#docFile').val(null);
                }
            }
        }]);
