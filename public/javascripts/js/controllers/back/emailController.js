/**
 * Created by ernestomr87@gmail.com on 17/07/14.
 */

angular.module('bojeoApp.emailController', [])
    .controller('emailController', ['$scope', '$rootScope', 'Email', '$location','$anchorScroll',
        function ($scope, $rootScope, Email, $location,$anchorScroll) {

            $rootScope.PageSelected = 'email';
            $rootScope.loadStatistics();
            $scope.server = "";
            $scope.port = null;
            $scope.user = "";
            $scope.password = "";
            $scope.password1 = "";

            $scope.loadData = true;
            $scope.showEditPass = false;

            $scope.tabsShowEs = {
                description: true
            };

            $scope.tabsShowBoletinEs = {
                description: true
            };

            $scope.tabsShowRegistroEs = {
                description: true
            };
            $scope.tabsShowBookingEs = {
                description: true
            };
            $scope.tabsShowRefundEs = {
                description: true
            };
            $scope.tabsShowSignEs = {
                description: true
            };

            $scope.loadAllData = function () {
                Email.query(function (data) {
                    if (data.res) {
                        $scope.testConection = false;
                        $scope.id = data.res._id;
                        $scope.server = data.res.mailServer;
                        $scope.port = data.res.mailSMTPPort;
                        $scope.user = data.res.mailUser;
                        $scope.password = data.res.mailPass;
                        $scope.password1 = data.res.mailPass;

                        $scope.notyReserved = data.res.notificationReserved;
                        $scope.notyChangePass = data.res.notificationChangePass;
                        $scope.devolution = data.res.devolution;
                        $scope.sign = data.res.sign;

                        $scope.notyRegister = data.res.notificationRegister;
                        $scope.bulletin = data.res.bulletin;
                        $scope.promo = data.res.promo;


                        $scope.loadData = false;
                    }
                })
            };
            $scope.loadAllData();
            $scope.CreateConfiguration = function () {
                Email.create(function (data) {
                    if (data.res) {
                        $scope.loadAllData();
                        $scope.loadData = false;
                    }
                    else {
                        $scope.loadData = true;
                    }
                });
            };
            $scope.saveConfigurationServer = function () {
                Email.save({
                    id: $scope.id,
                    mailServer: $scope.server,
                    mailSMTPPort: $scope.port,
                    mailUser: $scope.user,
                    mailPass: $scope.password
                }, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Cambios satisfactorios', 0);
                    }
                    else {
                        $rootScope.Notifications('Error, no se ha podido enviar el email', 2);
                    }
                });
            };
            $scope.changeNoty = function (type, active) {
                Email.changeNoty.update({
                    id: $scope.id,
                    type: type,
                    active: active
                }, function (data) {
                    if (!data.res) {
                        $rootScope.Notifications('Error, no se ha podido enviar el email', 2);
                    }
                });
            };
            $scope.changeNotyBody = function (type, noty) {
                Email.changeNotyBody.update({
                    id: $scope.id,
                    type: type,
                    noty: noty
                }, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Cambios satisfactorios', 0);
                    }
                    else {
                        $rootScope.Notifications('Los cambios no se han realizado satifactoriamente', 2);
                    }
                });
            };
            $scope.sendEmail = function () {
                $scope.testConection = false;
                Email.sendEmail.send(function (data) {
                    if (data.res) {
                        $scope.testConection = true;
                        $rootScope.Notifications('Se ha establecido la conexión', 0);
                    }
                    else {
                        $rootScope.Notifications('Error, no se ha podido establecer conexión', 2);
                    }
                })
            }
            $scope.sendNotyEmail = function (type) {
                Email.sendNotyEmail.send({
                    type: type
                }, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('El envío ha sido satisfactotio', 0);
                    }
                    else {
                        $rootScope.Notifications('Error, no se ha podido enviar el email', 2);
                    }
                })
            }
            $scope.movePage = function (id) {
                $location.hash(id);
                $anchorScroll();
                $location.hash('');
            }

        }
    ])
;
