/**
 * Created by ernestomr87@gmail.com on 24/04/14.
 */
angular.module('bojeoApp.DataController', [])
    .controller('DataController', ['$scope', '$rootScope', 'Statistics', '$http', '$timeout',
        function ($scope, $rootScope, Statistics, $http, $timeout) {
            $rootScope.loadingImg = false;
            $rootScope.changePage = function (page) {
                $rootScope.PageSelected = page;
            };


            $http({method: 'GET', url: '/api/sessions/ping'}).
                success(function (data) {
                    if (data == "false") {
                        window.location = "/";
                    }
                    else {
                        $rootScope.user = data;
                        if ($rootScope.user.role != 'admin') {
                            window.location = "/";
                        }
                    }
                });
            function bytesToSize(bytes) {
                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                if (bytes == 0) return '0 Bytes';
                var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
            };


            $rootScope.loadStatistics = function (cb) {
                Statistics.query(function (data) {
                    if (data.res) {
                        $rootScope.statistics = data.res;
                        $rootScope.cpu = data.cpu;
                        $rootScope.memoryPC = bytesToSize(data.memory);
                        if (cb) cb(true);
                    }
                    else {
                        if (cb) cb(true);
                    }
                });
            }
            $rootScope.loadStatistics();
            /**
             * @method Validar un archivo por su extension
             * @param $file
             * @param extArray
             * @returns {boolean}
             */
            $rootScope.validateFile = function ($file, typeArray) {
                var flag = false;
                if ($file.type != "" || $file.type != null) {
                    for (var j = 0; j < typeArray.length; j++) {
                        if (typeArray[j] == $file.type) {
                            flag = true;
                        }
                    }
                }
                return flag;
            };
            $rootScope.Notifications = function (msg, type, action) {
                if (action == 'create') {
                    if (type == 0) {
                        $rootScope.notyBojeo = {
                            msg: 'Elemento creado satisfactoriamente',
                            type: type
                        };
                    }
                    else if (type == 2) {
                        $rootScope.notyBojeo = {
                            msg: 'Ha ocurrido un error, por favor compruebe los datos',
                            type: type
                        };
                    }

                }
                else if (action == 'delete') {
                    if (type == 0) {
                        $rootScope.notyBojeo = {
                            msg: 'Elemento eliminado satisfactoriamente',
                            type: type
                        };
                    }
                    else if (type == 2) {
                        $rootScope.notyBojeo = {
                            msg: 'Ha ocurrido un error, por favor compruebe los datos',
                            type: type
                        };
                    }
                }
                else if (action == 'save') {
                    if (type == 0) {
                        $rootScope.notyBojeo = {
                            msg: 'Cambios salvados satisfactoriamente',
                            type: type
                        };
                    }
                    else if (type == 2) {
                        $rootScope.notyBojeo = {
                            msg: 'Ha ocurrido un error, por favor compruebe los datos',
                            type: type
                        };
                    }
                }
                else {
                    $rootScope.notyBojeo = {
                        msg: msg,
                        type: type
                    };
                }
                angular.element('#noty_information').fadeIn();

                $timeout(function () {
                    angular.element('#noty_information').fadeTo(200, 'toggle');

                }, 3000);

            };

            $rootScope.tinymceOptions = {
                theme: "modern",
                plugins: [
                    "advlist autolink lists link  charmap print preview",
                    "searchreplace wordcount   code fullscreen",
                    " media image save table contextmenu directionality",
                    "  paste textcolor colorpicker textpattern"
                ],
                toolbar1: " undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                toolbar2: "print preview media | forecolor backcolor "
            };


        }]);

