/**
 * Created by ernestomr87@gmail.com on 02/06/2014.
 */


angular.module('bojeoApp.MyCtrl', ['angularFileUpload'])
    .controller('MyCtrl', ['$scope', '$upload',
        function ($scope, $upload) {
            $scope.onFileSelect = function ($files) {
                //$files: an array of files selected, each file has name, size, and type.
                for (var i = 0; i < $files.length; i++) {
                    var file = $files[i];
                    $scope.upload = $upload.upload({
                        url: '/api/news/upload', //upload.php script, node.js route, or servlet url
                        // method: 'POST' or 'PUT',
                        method: 'POST',
                        // headers: {'header-key': 'header-value'},
                        headers: {'Content-Type': 'multipart/form-data'},
                        // withCredentials: true,
                        data: {myObj: $scope.myModelObj},
                        file: file
                    }).progress(function (evt) {

                    }).success(function (data, status, headers, config) {


                    });

                }

            };
        }]);