/**
 * Created by ernestomr87@gmail.com on 8/06/14.
 */

/**
 * calendarDemoApp - 0.8.0
 */


angular.module('bojeoApp.calendarController', [])
    .controller('calendarController', ['$scope', '$rootScope', 'Ships', '$upload', '$routeParams', '$http',
        function ($scope, $rootScope, Ships, $upload, $routeParams, $http) {
            $scope.matchCalendarBlock = function (a, b, c, d) {
                if (c < a && a < d) {
                    return true;
                }
                if (c < b && b < d) {
                    return true;
                }
                if (a < c && c < b) {
                    return true;
                }
                if (a < d && d < b) {
                    return true;
                }
                if (a.getTime() == c.getTime() && b.getTime() == d.getTime()) {
                    return true;
                }
                return false;
            }

            $scope.constM = 60000;
            $scope.constD = 86400000;

            $rootScope.PageSelected = 'calendar';
            $rootScope.loadStatistics();
            $scope.shipId = $routeParams.shipId;
            $scope.events = [];
            $scope.calendarDays = new Array();
            $scope.popupEvent = false;
            $scope.showModalEvent = function (show) {
                $scope.popupEvent = show;
            }
            $scope.saveNameEvent = function () {
                for (var i = 0; i < $scope.caledarShip.block.length; i++) {
                    if ($scope.caledarShip.block[i]._id == $scope.eventSelected._id) {
                        $scope.caledarShip.block[i].title = $scope.eventName;
                        $scope.caledarShip.block[i].description = $scope.eventDescription;
                        break;
                    }
                }


                $scope.eventSelected.title = $scope.eventName;
                $scope.eventSelected.description = $scope.eventDescription;
                $scope.showModalEvent(false);
                $('#calendar').fullCalendar('removeEvents', $scope.eventSelected._id);

                var aux = {
                    backgroundColor: $scope.eventSelected.backgroundColor,
                    textColor: $scope.eventSelected.textColor,
                    title: $scope.eventSelected.title,
                    description: $scope.eventSelected.description,
                    start: $scope.eventSelected.start,
                    end: $scope.eventSelected.end,
                    editable: true,
                    allDay: $scope.eventSelected.allDay
                };

                $('#calendar').fullCalendar('renderEvent', aux, true);
            }
            $scope.removeEvent = function () {
                for (var i = 0; i < $scope.caledarShip.block.length; i++) {
                    if ($scope.caledarShip.block[i]._id == $scope.eventSelected._id) {
                        $scope.caledarShip.block.splice(i, 1);
                        break;
                    }
                }

                for (var i = 0; i < $scope.events.length; i++) {
                    if ($scope.events[i]._id == $scope.eventSelected._id) {
                        $scope.events.splice(i, 1);
                        break;
                    }
                }
                $('#calendar').fullCalendar('removeEvents', $scope.eventSelected._id);
                $scope.showModalEvent(false);
            }

            $scope.loadShip = function () {
                Ships.get({id: $scope.shipId}, function (ship) {
                    if (ship.res) {
                        $scope.ship = ship.res;
                        $scope.caledarShip = {
                            start: {},
                            end: {},
                            block: new Array()
                        };
                        filterOccupiedDay($scope.ship.calendar.block);
                        if ($scope.ship.calendar.start) {
                            var startShip = $scope.ship.calendar.start;
                            var aux = {
                                backgroundColor: '#669533',
                                borderColor: '#669533',
                                title: 'Día de inicio',
                                start: new Date(startShip),
                                editable: false,
                                allDay: true
                            };
                            $scope.caledarShip.start = aux;
                            $scope.events.push(aux);
                            $('#start').hide();
                        }
                        if ($scope.ship.calendar.end) {
                            var endShip = $scope.ship.calendar.end;
                            var aux = {
                                backgroundColor: '#DD5600',
                                borderColor: '#DD5600',
                                title: 'Día de fin',
                                start: new Date(endShip),
                                editable: false,
                                allDay: true
                            };
                            $scope.caledarShip.end = aux;
                            $scope.events.push(aux);
                            $('#end').hide();
                        }
                        $('#external-events div.external-event').each(function () {
                            // it doesn't need to have a start or end
                            var eventObject = {
                                title: $.trim($(this).text()) // use the element's text as the event title
                            };

                            // store the Event Object in the DOM element so we can get to it later
                            $(this).data('eventObject', eventObject);

                            // make the event draggable using jQuery UI
                            $(this).draggable({
                                zIndex: 999,
                                revert: true,      // will cause the event to go back to its
                                revertDuration: 0  //  original position after the drag
                            });

                        });
                        $('#calendar').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,agendaWeek,agendaDay'
                            },

                            events: $scope.events,
                            editable: true,
                            droppable: true, // this allows things to be dropped onto the calendar !!!
                            eventDrop: function (event, dayDelta, minuteDelta) {
                                for (var i = 0; i < $scope.events.length; i++) {
                                    if ($scope.events[i]._id == event._id) {
                                        $scope.events = _.without($scope.events, $scope.events[i]);
                                        break;
                                    }
                                }
                                $('#calendar').fullCalendar('removeEvents', event._id);
                                var day = 0, min = 0;
                                if (dayDelta != 0) {
                                    day = (dayDelta * $scope.constD) * -1;
                                }
                                if (minuteDelta != 0) {
                                    min = (minuteDelta * $scope.constM) * -1;
                                }
                                var start = new Date(event.start.getTime() + day + min);
                                var end = event.end;

                                if (end) {
                                    end = new Date(end.getTime() + day + min);

                                }
                                else if (!event.allDay && !dayDelta && !minuteDelta) {
                                    end = new Date(start.getTime() + (60 * $scope.constM * -1))
                                }
                                var oldEvent = {
                                    _id: event._id,
                                    backgroundColor: event.backgroundColor,
                                    textColor: event.textColor,
                                    title: event.title,
                                    description: event.description,
                                    start: start,
                                    end: end,
                                    editable: event.editable,
                                    allDay: event.allDay
                                };
                                if (!$scope.validateAddEvent($scope.events, event)) {
                                    $('#calendar').fullCalendar('renderEvent', oldEvent, true);
                                    $scope.events.push(oldEvent);
                                }
                                else {
                                    var end = event.end;
                                    if (!event.allDay && !dayDelta && !end) {
                                        end = new Date(event.start.getTime() + (120 * $scope.constM))
                                    }
                                    var nowEvent = {
                                        _id: event._id,
                                        backgroundColor: event.backgroundColor,
                                        textColor: event.textColor,
                                        title: event.title,
                                        description: event.description,
                                        start: event.start,
                                        end: end,
                                        editable: event.editable,
                                        allDay: event.allDay
                                    };
                                    $scope.events.push(nowEvent)
                                    $('#calendar').fullCalendar('renderEvent', nowEvent, true);
                                }
                                //$scope.apply()
                            },
                            eventClick: function (event, dayDelta) {
                                if (!event.day) {
                                    if (event.title == 'Día de inicio') {
                                        $('#calendar').fullCalendar('removeEvents', event._id);
                                        $(this).remove();
                                        $scope.caledarShip.start = null;
                                        $('#start').fadeIn();
                                    }
                                    else if (event.title == 'Día de fin') {
                                        $('#calendar').fullCalendar('removeEvents', event._id);
                                        $(this).remove();
                                        $scope.caledarShip.end = null;
                                        $('#end').fadeIn();
                                    }
                                    else if (event.editable) {
                                        $scope.showModalEvent(true);
                                        $scope.eventSelected = event;
                                        $scope.eventName = event.title;
                                        $scope.eventDescription = event.description;
                                        $scope.editableEvent = event.editable;
                                        $scope.dayDeltaE = dayDelta;
                                        $scope.$apply();
                                    }

                                }


                            },
                            drop: function (date, allDay) { // this function is called when something is dropped

                                // retrieve the dropped element's stored Event Object
                                var originalEventObject = $(this).data('eventObject');

                                // we need to copy it, so that multiple events don't have a reference to the same object
                                var copiedEventObject = $.extend({}, originalEventObject);

                                // assign it the date that was reported
                                copiedEventObject.start = date;
                                copiedEventObject.allDay = allDay;

                                var semV = true;
                                for (var i = 0; i < $scope.defaultEvents.length; i++) {
                                    if ($scope.defaultEvents[i].title == copiedEventObject.title) {
                                        copiedEventObject.backgroundColor = $scope.defaultEvents[i].color;
                                        copiedEventObject.borderColor = $scope.defaultEvents[i].color;
                                        copiedEventObject.editable = $scope.defaultEvents[i].editable;


                                        if ($scope.defaultEvents[i].id == 'start') {
                                            $scope.caledarShip.start = copiedEventObject;
                                            copiedEventObject._id = 'start';
                                            $('#start').hide();
                                        }
                                        else if ($scope.defaultEvents[i].id == 'end') {
                                            $scope.caledarShip.end = copiedEventObject;
                                            copiedEventObject._id = 'end';
                                            $('#end').hide();
                                        }
                                        else {

                                            if ($scope.validateAddEvent($scope.events, copiedEventObject)) {
                                                $scope.caledarShip.block.push(copiedEventObject);
                                                if (!copiedEventObject.allDay) {
                                                    var date = copiedEventObject.start.getTime() + (2 * 3600000);
                                                    copiedEventObject.end = new Date(date);
                                                }
                                                $scope.events.push(copiedEventObject);
                                            }
                                            else {
                                                semV = false;
                                            }

                                        }
                                        break;
                                    }
                                }
                                $scope.$apply();
                                if (semV) {
                                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                                }


                            },
                            eventResize: function (event, dayDelta, minuteDelta) {
                                for (var i = 0; i < $scope.events.length; i++) {
                                    if ($scope.events[i]._id == event._id) {
                                        $scope.events = _.without($scope.events, $scope.events[i]);
                                        break;
                                    }
                                }
                                $('#calendar').fullCalendar('removeEvents', event._id);
                                var day = 0, min = 0;
                                if (dayDelta != 0) {
                                    day = (dayDelta * $scope.constD) * -1;
                                }
                                if (minuteDelta != 0) {
                                    min = (minuteDelta * $scope.constM) * -1;
                                }

                                var start = event.start;

                                var end = event.end;
                                if (end) {
                                    end = new Date(event.end.getTime() + day + min)
                                }

                                var oldEvent = {
                                    _id: event._id,
                                    backgroundColor: event.backgroundColor,
                                    textColor: event.textColor,
                                    title: event.title,
                                    description: event.description,
                                    start: start,
                                    end: end,
                                    editable: event.editable,
                                    allDay: event.allDay
                                };
                                if (!$scope.validateAddEvent($scope.events, event)) {
                                    $('#calendar').fullCalendar('renderEvent', oldEvent, true);
                                    $scope.events.push(oldEvent);
                                }
                                else {
                                    var end = event.end;
                                    if (!event.allDay && !dayDelta && !end) {
                                        end = new Date(event.start.getTime() + (120 * $scope.constM))
                                    }
                                    var nowEvent = {
                                        _id: event._id,
                                        backgroundColor: event.backgroundColor,
                                        textColor: event.textColor,
                                        title: event.title,
                                        description: event.description,
                                        start: event.start,
                                        end: end,
                                        editable: event.editable,
                                        allDay: event.allDay
                                    };
                                    $scope.events.push(nowEvent)
                                    $('#calendar').fullCalendar('renderEvent', nowEvent, true);
                                }
                            }
                        });
                    }
                })
            };
            $scope.loadShip();
            $scope.defaultEvents = [
                {id: 'start', title: 'Día de inicio', color: '#669533', allDay: true, editable: false},
                {id: 'end', title: 'Día de fin', color: '#DD5600', allDay: true, editable: false},
                {id: 'block', title: 'Bloqueado', color: '#BD4247', allDay: true, editable: true},
            ];

            $scope.saveCalendar = function () {
                var calendar = {
                    start: $scope.caledarShip.start._start.getTime(),
                    end: $scope.caledarShip.end._start.getTime(),
                    block: new Array()
                };
                for (var i = 0; i < $scope.caledarShip.block.length; i++) {
                    if ($scope.caledarShip.block[i].tag != 'booked') {
                        if ($scope.caledarShip.block[i].allDay) {
                            if ($scope.caledarShip.block[i].end) {
                                var endb = $scope.caledarShip.block[i].end.getTime();
                            }
                            else {
                                var endb = null;
                            }
                            var aux = {
                                idRest: $scope.caledarShip.block[i].idRest || null,
                                allDay: true,
                                start: $scope.caledarShip.block[i].start.getTime(),
                                end: endb,
                                title: $scope.caledarShip.block[i].title,
                                description: $scope.caledarShip.block[i].description || ""
                            }
                            calendar.block.push(aux);
                        }
                        else {
                            if ($scope.caledarShip.block[i].end) {
                                var endb = $scope.caledarShip.block[i].end.getTime();
                            }
                            else {
                                var endb = $scope.caledarShip.block[i].start.getTime() + 3600000;
                            }
                            var aux = {
                                idRest: $scope.caledarShip.block[i].idRest || null,
                                allDay: false,
                                start: $scope.caledarShip.block[i].start.getTime(),
                                end: endb,
                                title: $scope.caledarShip.block[i].title,
                                description: $scope.caledarShip.block[i].description || ""
                            }

                            calendar.block.push(aux);
                        }
                    }
                }
                ;

                $http({method: 'POST', url: '/api/ships/calendar', data: {id: $scope.shipId, calendar: calendar}}).
                    success(function (data) {
                        if (data.res) {
                            $rootScope.Notifications('Cambios guardados', 0);

                        }
                        else {
                            $rootScope.Notifications('Error, no se han podido guardar los cambios', 2);
                        }
                    });
            };
            function filterOccupiedDay(eventsShip) {
                for (var i = 0; i < eventsShip.length; i++) {

                    var editable = false;
                    if (eventsShip[i].tour) {
                        var color = eventsShip[i].tour.group.color;
                        var title = eventsShip[i].tour.name.es;
                        var description = null;
                    }
                    else {
                        var color = '#BD4247';
                        var title = eventsShip[i].title || "Bloqueado";
                        var description = eventsShip[i].description || "";
                        var editable = true;
                    }

                    var aux = {
                        backgroundColor: color,
                        textColor: '#000',
                        title: title,
                        start: new Date(eventsShip[i].start),
                        end: new Date(eventsShip[i].end),
                        allDay: false,
                        editable: editable,
                        tag: eventsShip[i].tag,
                        idRest: eventsShip[i]._id,
                        description:description
                    };
                    $scope.events.push(aux);
                    $scope.caledarShip.block.push(aux);
                }
            };
            $scope.splitHours = function (hours) {
                var array = new Array();
                var aux = new Array();
                var sem = true;

                if (hours.length > 0) {
                    var cont = 0;
                    aux.push(hours[cont]);
                    for (var i = 1; i < hours.length; i++) {
                        if ((aux[cont] + 1) == hours[i]) {
                            aux.push(hours[i]);
                            cont++;
                        }
                        else {
                            array.push(aux);
                            aux = new Array();
                            var cont = 0;
                            aux.push(hours[i]);
                        }
                    }
                    array.push(aux);
                    return array;
                }
                else {
                    return array;
                }
            };
            $scope.createBlockRes = function (reservation) {
                var array = new Array();
                if (reservation.day == 1) {
                    var start = reservation.start.getHours();
                    var end = (reservation.end ? reservation.end.getHours() : 24);
                    if (start == 0) {
                        start++;
                    }
                    if (end == 23) {
                        end++;
                    }
                    for (var i = start; i <= end; i++) {
                        array.push(i);
                    }
                }

                return array;
            };
            $scope.validateAddEvent = function (array, event) {

                for (var i = 0; i < array.length; i++) {
                    var start = array[i].start;
                    var end = array[i].end;
                    if (array[i].end == null) {
                        end = start;
                    }
                    if (array[i].allDay) {
                        end = new Date(end.getFullYear(), end.getMonth(), end.getDate());
                        end = new Date(end.getTime() + 86399999);
                    }

                    var eventEnd = event.end;
                    if (event.end == null) {
                        eventEnd = event.start;
                    }
                    if (event.allDay) {
                        eventEnd = new Date(eventEnd.getFullYear(), eventEnd.getMonth(), eventEnd.getDate());
                        eventEnd = new Date(eventEnd.getTime() + 86399999);
                    }

                    if ($scope.matchCalendarBlock(start, end, event.start, eventEnd)) {
                        return false;
                    }
                }
                return true;

            };
            $scope.validateDropEvent = function (event) {

                var end = event.end;

                for (var i = 0; i < $scope.events.length; i++) {
                    if ($scope.events[i].title == "Día de fin") {
                        if ($scope.events[i].start <= end) {
                            return false;
                        }
                    }
                    else if ($scope.events[i].title == "Día de inicio") {
                        if ($scope.events[i].start >= end) {
                            return false;
                        }
                    }
                    else if (event._id != $scope.events[i]._id) {
                        if ($scope.events[i].allDay) {
                            if ($scope.events[i].start.toLocaleDateString() == end.toLocaleDateString()) {
                                return false;
                            }
                            if (event.start <= $scope.events[i].start && $scope.events[i].start <= event.end) {
                                return false;
                            }
                            if (event.start <= $scope.events[i].end && $scope.events[i].end <= event.start.end) {
                                return false;
                            }
                        }
                        else if (event.allDay) {
                            if (!$scope.events[i].allDay && $scope.events[i].start.toLocaleDateString() == end.toLocaleDateString()) {
                                return false;
                            }
                            if (event.start <= $scope.events[i].start && $scope.events[i].start <= event.end) {
                                return false;
                            }
                            if (event.start <= $scope.events[i].end && $scope.events[i].end <= event.start.end) {
                                return false;
                            }

                            else {
                                if ($scope.events[i].start.getTime() <= end.getTime() && $scope.events[i].end.getTime() >= end.getTime()) {
                                    return false;
                                }
                                var date = end.getTime() + (2 * 3600000);
                                if ($scope.events[i].start.getTime() <= date && $scope.events[i].end.getTime() >= date) {
                                    return false;
                                }
                            }
                        }
                    }
                    ;

                }
                return true;
            };
            $scope.goToDay = function (day) {
                var date = new Date(day);
                $("#calendar").fullCalendar('gotoDate', date);
            }
        }
    ])
;




