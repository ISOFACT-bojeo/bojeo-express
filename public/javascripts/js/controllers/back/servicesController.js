/**
 * Created by ernestomr87@gmail.com on 15/05/14.
 */

angular.module('bojeoApp.servicesController', [])
    .controller('servicesController', ['$scope', '$rootScope', '$routeParams', 'Service', 'Enterprise', 'ShipsOwners', '$http', '$upload',
        function ($scope, $rootScope, $routeParams, Service, Enterprise, ShipsOwners, $http, $upload) {

            $rootScope.PageSelected = 'services';
            $rootScope.loadStatistics();
            $scope.semaphore = true;
            $scope.openClose = true;
            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;

            $scope.name = {
                es: "",
                en: ""
            };
            $scope.description = {
                es: "",
                en: ""
            };
            $scope.type = "tourist";
            $scope.price = 0;
            $scope.nameEnterprise = "";
            $scope.owner = "";
            $scope.commission = 0;
            $scope.url = "";

            $scope.tabsShowEs = {
                description: true
            };

            $scope.files = [];
            var img = false;
            $scope.imgfileUpload = null;
            $scope.cantImage = 4;
            $scope.dimensionImage = "300x400px"
            $scope.formatImage = "[ jpg, png ]";
            $scope.resolucionImage = "72dpi";

            $scope.erroresNameImage = [];
            $scope.erroresExistImage = [];
            $scope.validCantImg = 0;
            $scope.cantImg = 0;
            $scope.files.length;

            $scope.reload = false;
            $scope.idEnterprise = "";
            if ($routeParams.id) {
                $scope.idEnterprise = $routeParams.id;
                $scope.reload = true;
            }

            $scope.loadService = function () {
                Service.list.query({
                    limit: $scope.maxSize,
                    skip: $scope.skip,
                    id: $scope.idEnterprise
                }, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.cont;
                        $scope.services = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.services.length > 0) {
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadService();
                            }
                        }
                    } else {
                        $scope.services = new Array();
                        $scope.empty = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener los servicios.', 2);
                    }
                });
            };

            $scope.loadService();

            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.loadService();
            };

            $scope.showForm = function () {
                $scope.openClose = false;
                $('#form-service').show();
            };

            $scope.hideForm = function () {
                $scope.openClose = true;
                $('#form-service').hide();
                $scope.semaphore = true;
                $scope.clearField();
            };

            $scope.clearField = function () {
                $scope.name = {
                    es: "",
                    en: ""
                };
                $scope.description = {
                    es: "",
                    en: ""
                };
                $scope.type = "tourist";
                $scope.price = 0;
                $scope.nameEnterprise = "";
                $scope.owner = "";
                $scope.commission = 0;
                $scope.url = "";
                $scope.files = [];
                img = false;
                $scope.erroresNameImage = [];
                $scope.erroresExistImage = [];
                $scope.validCantImg = 0;
                $scope.cantImg = 0;
                $scope.nameImgDel = [];
                $('#imgFile').val(null);
                $scope.tabsShowEs = {
                    description: true
                };
            };

            $scope.changeStatus = function (service, status) {
                //action
                //0 consulta
                //1 confirmacion
                //2 active
                if (!status) {
                    $http({
                        method: 'POST', url: '/api/service/status',
                        data: {id: service._id, action: 0, status: status}
                    }).success(function (data) {
                        if (data.res) {
                            if (!data.complete) {
                                $scope.showChangeStatusModal(service, data.res, false);
                            } else {
                                $scope.idEnterprise = "";
                                $scope.loadService();
                            }
                        } else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                        }
                    })
                } else {
                    $http({
                        method: 'POST', url: '/api/service/status',
                        data: {id: service._id, action: 2, status: status}
                    }).success(function (data) {
                        if (data.res) {
                            if (data.complete) {
                                $scope.idEnterprise = "";
                                $scope.loadService();
                            } else {
                                $scope.showChangeStatusModal(service, new Array(), data.res.user, data.res.enterprise);
                            }
                        } else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                        }
                    });
                }
            };

            $scope.showChangeStatusModal = function (data, packs, user, enterprise) {
                $scope.changeStatusItem = data;
                $scope.associationPacks = packs;
                $scope.associationUser = user;
                $scope.associationEnterprise = enterprise;
                $('#changeStatusModal').modal('show');
            };

            $scope.changeStatusNow = function (service) {
                if (service.status) {
                    $http({
                        method: 'POST', url: '/api/service/status',
                        data: {
                            id: service._id,
                            action: 1,
                            status: false
                        }
                    }).success(function (data) {
                        if (data.res) {
                            $scope.idEnterprise = "";
                            $scope.loadService();
                        } else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                        }
                    });
                } else {
                    $http({
                        method: 'POST', url: '/api/service/status',
                        data: {
                            id: service._id,
                            action: 3,
                            status: true
                        }
                    }).success(function (data) {
                        if (data.res) {
                            $scope.idEnterprise = "";
                            $scope.loadService();
                        } else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                        }
                    });
                }
            };

            $scope.deleteService = function (data) {
                $scope.delService = data;
                $http({
                    method: 'POST', url: '/api/service/listPacks',
                    data: {
                        id: data._id
                    }
                }).success(function (data) {
                    if (data.res) {
                        $scope.deletePacks = data.res;
                    } else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido obtener los datos', 2);
                    }
                });
            };

            $scope.dropService = function () {
                Service.remove({id: $scope.delService._id}, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Servicio eliminado satisfactoriamente', 0);
                        $scope.loadService();
                        $rootScope.loadStatistics();
                    } else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido eliminar el Servicio', 2);
                    }
                });
            };

            $scope.showEditService = function (data) {
                $scope.idServiceEdit = data._id;
                $scope.name = data.name;
                $scope.description = data.description;
                $scope.type = data.type;
                $scope.price = data.price;
                $scope.nameEnterprise = data.enterprise.name;
                $scope.idEnterprise = data.enterprise.id;
                $scope.owner = data.owner.email;
                $scope.commission = data.commission;
                $scope.url = data.url;
                $scope.idOwner = data.owner.id;
                $('#imgFile').val(null);
                $scope.files = [];
                for (var i = 0; i < data.photos.length; i++) {
                    $scope.files[i] = data.photos[i];
                }
                $scope.cantImg = $scope.files.length;
                img = true;
                $scope.semaphore = false;
                $scope.nameImgDel = [];
                $scope.showForm();
            };

            $scope.currentPageE = 1;
            $scope.maxSizeE = 10;
            $scope.skipE = 0;
            $scope.wordSearchE = "";
            $scope.listEnterprises = function () {
                Enterprise.get({limit: $scope.maxSizeE, skip: $scope.skipE, word: $scope.wordSearchE}, function (data) {
                    if (data.res) {
                        $scope.totalItemsE = data.cont;
                        $scope.enterprises = data.enterprises;
                        if (data.cont == 0) {
                            $scope.emptyEnterprise = true;
                        }
                        if ($scope.enterprises.length > 0) {
                            $scope.emptyEnterprise = false;
                        } else {
                            if ($scope.currentPageE > 1) {
                                $scope.currentPageE--;
                                $scope.skipE = $scope.skipE - $scope.maxSizeE;
                                $scope.listEnterprises();
                            }
                        }
                    }
                    else {
                        $scope.enterprises = new Array();
                        $scope.emptyEnterprise = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener las empresas.', 2);
                    }
                })
            };

            $scope.pageChangedE = function () {
                $scope.skipE = ($scope.currentPageE - 1) * $scope.maxSizeE;
                $scope.listEnterprises();
            };

            $scope.searchEnterprises = function () {
                $scope.listEnterprises();
            };

            $scope.addEnterprise = function (enterprise) {
                $scope.idEnterprise = enterprise._id;
                $scope.nameEnterprise = enterprise.name.es;
            };

            $scope.currentPageO = 1;
            $scope.maxSizeO = 10;
            $scope.skipO = 0;
            $scope.wordSearchO = "";
            $scope.listOwners = function () {
                ShipsOwners.get({
                    limit: $scope.maxSizeO,
                    skip: $scope.skipO,
                    word: $scope.wordSearchO
                }, function (data) {
                    if (data.response) {
                        $scope.users = data.response;
                        $scope.totalItemsO = data.count;
                        if ($scope.totalItemsO == 0) {
                            $scope.emptyOwner = true;
                        }
                        if ($scope.users.length > 0) {
                            $scope.emptyOwner = false;
                        } else {
                            if ($scope.currentPageO > 1) {
                                $scope.currentPageO--;
                                $scope.skipO = $scope.skipO - $scope.maxSizeO;
                                $scope.listOwners();
                            }
                        }
                    }
                    else {
                        $scope.users = new Array();
                        $scope.emptyOwner = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener los proveedores.', 2);
                    }
                })
            };

            $scope.pageChangedO = function () {
                $scope.skipO = ($scope.currentPageO - 1) * $scope.maxSizeO;
                $scope.listOwners();
            };

            $scope.searchOwners = function () {
                $scope.listOwners();
            };

            $scope.addOwner = function (user) {
                $scope.idOwner = user._id;
                $scope.owner = user.email;
            };

            /**
             * @method Evento que se ejecuta cuando el archivo es seleccionado
             * @param $files
             */
            $scope.onImgSelect = function (files) {
                img = false;
                $scope.erroresNameImage = [];
                $scope.erroresExistImage = [];
                $scope.validCantImg = 0;
                recursivo(files, 0, function (data) {
                    $scope.$apply();
                });
            };

            function recursivo(files, pos, cb) {
                if (files.length == pos) {
                    $scope.$apply();
                    if (cb) cb(true)
                } else {
                    var typeArray = ["image/jpeg", "image/png"];
                    if ($rootScope.validateFile(files[pos], typeArray)) {
                        if ($scope.files.length > 0) {
                            if ($scope.verificarExiste(files[pos])) {
                                $scope.error = "errorExist";
                                $scope.erroresExistImage.push(files[pos].name);
                            } else {
                                if ($scope.cantImg < $scope.cantImage) {
                                    $scope.cantImg++;
                                    var reader = new FileReader();
                                    reader.readAsDataURL(files[pos]);
                                    reader.onload = function () {
                                        files[pos].image = reader.result;
                                        $scope.files.push(files[pos]);
                                        img = true;
                                        pos++;
                                        if (cb) cb(recursivo(files, pos, cb));
                                    };
                                } else {
                                    $('#imgFile').val(null);
                                    $scope.validCantImg = 1;
                                }
                            }
                        } else {
                            $scope.cantImg++;
                            var reader = new FileReader();
                            reader.readAsDataURL(files[pos]);
                            reader.onload = function () {
                                files[pos].image = reader.result;
                                $scope.files.push(files[pos]);
                                img = true;
                                pos++;
                                if (cb) cb(recursivo(files, pos, cb));
                            };
                        }
                    } else {
                        $scope.error = "errorType";
                        $scope.erroresNameImage.push(files[pos].name);
                        pos++;
                        if (cb) cb(recursivo(files, pos, cb));
                    }
                }
            }

            $scope.verificarExiste = function ($file) {
                var flag = false;
                var cont = 0;
                while (!flag && $scope.files.length > cont) {
                    if ($file.name == $scope.files[cont].name && $file.size == $scope.files[cont].size) {
                        flag = true;
                    }
                    cont = cont + 1;
                }
                return flag;
            };

            $scope.deleteServiceImg = function (file) {
                var pos = 0;
                var cantidad = $scope.files.length;
                var flag = false;
                while (!flag && pos < cantidad) {
                    if (file.name == $scope.files[pos].name) {
                        $scope.files.splice(pos, 1);
                        flag = true;
                    }
                    pos++;
                }
                $scope.cantImg--;
                $scope.validCantImg = 0;
                if ($scope.files.length == 0) {
                    img = false;
                }
                if (file.path) {
                    $scope.nameImgDel.push(file.name);
                }
                $('#imgFile').val(null);
            };

            $scope.createService = function () {
                var bool = true;
                $scope.errorNewsImg = true;
                if ($scope.name.es == "" || $scope.name.es == null) {
                    bool = false;
                    $scope.errorNombre = "Campo vacío";
                }
                if ($scope.name.en == "" || $scope.name.en == null) {
                    bool = false;
                    $scope.errorNombreEn = "Campo vacío";
                }
                if ($scope.description.es == "" || $scope.description.es == null) {
                    bool = false;
                    $scope.errorDescripcion = "Campo vacío";
                }
                if ($scope.description.en == "" || $scope.description.en == null) {
                    bool = false;
                    $scope.errorDescripcion_en = "Campo vacío";
                }
                if ($scope.type == "" || $scope.type == null) {
                    bool = false;
                    $scope.errorTipo = "Campo vacío";
                }
                if ($scope.price == "" || $scope.price == null) {
                    bool = false;
                    $scope.errorPrice = "Campo vacío";
                }
                if ($scope.nameEnterprise == "" || $scope.nameEnterprise == null) {
                    bool = false;
                    $scope.errorEnterprise = "Campo vacío";
                }
                if ($scope.owner == "" || $scope.owner == null) {
                    bool = false;
                    $scope.errorOwner = "Campo vacío";
                }
                if ($scope.commission == null) {
                    bool = false;
                    $scope.errorCommission = "Campo vacío";
                }
                if (!img) {
                    bool = false;
                    $scope.errorImgUpload = "Campo vacío";
                }
                if (bool) {
                    $rootScope.loadingImg = true;

                    $upload.upload({
                        url: '/api/service', //upload.php script, node.js route, or servlet url
                        method: 'POST',
                        headers: {'header-key': 'multipart/form-data'},
                        data: {
                            name_es: $scope.name.es,
                            name_en: $scope.name.en,
                            description_es: $scope.description.es,
                            description_en: $scope.description.en,
                            type: $scope.type, price: $scope.price,
                            enterprise: $scope.idEnterprise,
                            owner: $scope.idOwner,
                            commission: $scope.commission,
                            url: $scope.url
                        },
                        file: $scope.files
                    }).success(function (data) {
                        if (data.res) {
                            $rootScope.loadingImg = false;
                            $rootScope.Notifications('Servicio creado satisfactoriamente.', 0);
                            $scope.idEnterprise = "";
                            $scope.loadService();
                            $scope.hideForm();
                            $rootScope.loadStatistics();
                        } else {
                            $rootScope.loadingImg = false;
                            $rootScope.Notifications('Ha ocurrido un error, el servicio no se ha podido crear.', 2);
                        }

                    });

                } else {
                    return;
                }
            };

            $scope.saveService = function () {
                var bool = true;
                if ($scope.name.es == "" || $scope.name.es == null) {
                    bool = false;
                    $scope.errorNombre = "Campo vacío";
                }
                if ($scope.name.en == "" || $scope.name.en == null) {
                    bool = false;
                    $scope.errorNombreEn = "Campo vacío";
                }
                if ($scope.description.es == "" || $scope.description.es == null) {
                    bool = false;
                    $scope.errorDescripcion = "Campo vacío";
                }
                if ($scope.description.en == "" || $scope.description.en == null) {
                    bool = false;
                    $scope.errorDescripcion_en = "Campo vacío";
                }
                if ($scope.type == "" || $scope.type == null) {
                    bool = false;
                    $scope.errorTipo = "Campo vacío";
                }
                if ($scope.price == "" || $scope.price == null) {
                    bool = false;
                    $scope.errorPrice = "Campo vacío";
                }
                if ($scope.nameEnterprise == "" || $scope.nameEnterprise == null) {
                    bool = false;
                    $scope.errorEnterprise = "Campo vacío";
                }
                if ($scope.owner == "" || $scope.owner == null) {
                    bool = false;
                    $scope.errorOwner = "Campo vacío";
                }
                if ($scope.commission == null) {
                    bool = false;
                    $scope.errorCommission = "Campo vacío";
                }
                if (!img) {
                    bool = false;
                    $scope.errorImgUpload = "Campo vacío";
                }
                if (bool) {


                    Service.save({
                        id: $scope.idServiceEdit,
                        name: $scope.name,
                        description: $scope.description,
                        type: $scope.type,
                        price: $scope.price,
                        commission: $scope.commission,
                        enterprise: $scope.idEnterprise,
                        owner: $scope.idOwner,
                        url: $scope.url
                    }, function (data) {
                        if (data.res) {
                            var files = preparedFiles($scope.files);
                            if ($scope.nameImgDel.length != 0 || files.length != 0) {
                                $rootScope.loadingImg = true;
                                $upload.upload({
                                    url: "/api/service/updateImg", //upload.php script, node.js route, or servlet url
                                    method: 'POST',
                                    headers: {'header-key': 'multipart/form-data'},
                                    data: {id: $scope.idServiceEdit, nameImgDel: $scope.nameImgDel},
                                    file: files
                                }).success(function (data) {
                                    if (data.result) {
                                        $rootScope.loadingImg = false;
                                        $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                                        $scope.idEnterprise = "";
                                        $scope.loadService();
                                        $scope.hideForm();
                                        $rootScope.loadStatistics();
                                    } else {
                                        $rootScope.loadingImg = false;
                                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                                    }
                                }).error(function (status) {
                                    $rootScope.loadingImg = false;
                                    $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                                });
                            } else {
                                $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                                $scope.idEnterprise = "";
                                $scope.loadService();
                                $scope.hideForm();
                                $rootScope.loadStatistics();
                            }
                        } else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                        }
                    })
                } else {
                    return;
                }
            };

            function preparedFiles(files) {
                var arrayTemp = [];
                for (var i = 0; i < files.length; i++) {
                    if (files[i].image) {
                        arrayTemp.push(files[i]);
                    }
                }
                return arrayTemp;
            }
        }]);


