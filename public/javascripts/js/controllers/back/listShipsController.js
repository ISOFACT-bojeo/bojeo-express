/**
 * Created by ernestomr87@gmail.com on 6/06/14.
 */

angular.module('bojeoApp.listShipsController', [])
    .controller('listShipsController', ['$scope', '$rootScope', '$routeParams', 'Ships', '$upload', '$http', 'Tour', 'ShipsOwners', 'ShipsPatrons', 'Tags',
        function ($scope, $rootScope, $routeParams, Ships, $upload, $http, Tour, ShipsOwners, ShipsPatrons, Tags) {
            $rootScope.PageSelected = 'ships';
            $rootScope.loadStatistics();

            $scope.users = null;

            //Pagination Ships
            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;
            //Pagination Owners
            $scope.currentPageO = 1;
            $scope.maxSizeO = 10;
            $scope.skipO = 0;
            $scope.wordSearchO = "";
            //Pagination Patrons
            $scope.currentPageP = 1;
            $scope.maxSizeP = 10;
            $scope.skipP = 0;
            $scope.wordSearchP = "";


            $scope.name = "";
            $scope.commission = 0;
            $scope.description = {
                es: "",
                en: ""
            };
            $scope.tabsShowEs = {
                description: true
            };
            $scope.patron = "";
            $scope.owner = {
                id: "",
                comitions: 0
            };
            $scope.totalItems = 0;
            $scope.text = "";
            $scope.tag = {
                name: {
                    _id: null,
                    es: '',
                    en: ''
                }
            }

            $scope.imgfileUpload = null;
            $scope.files = [];
            var img = false;
            $scope.cantImage = 1;
            $scope.dimensionImage = "130x130px"
            $scope.formatImage = "[ jpg, png ]";
            $scope.resolucionImage = "72dpi";

            $scope.erroresNameImage = [];
            $scope.erroresExistImage = [];
            $scope.validCantImg = 0;
            $scope.cantImg = 0;

            $scope.addOwner = function (user) {
                $scope.owner.id = user._id;
                $scope.nameUserOwner = user.email;
            };
            $scope.addPatron = function (user) {
                $scope.patron = user._id;
                $scope.nameUserPatron = user.email;
            };
            $scope.create = function () {
                if ($scope.validateForm()) {
                    $upload.upload({
                        url: '/api/ships', //upload.php script, node.js route, or servlet url
                        method: 'POST',
                        headers: {'header-key': 'multipart/form-data'},
                        data: {
                            name: $scope.name,
                            description_es: $scope.description.es,
                            description_en: $scope.description.en,
                            patron: $scope.patron,
                            owner_id: $scope.owner.id,
                            owner_comitions: $scope.owner.comitions,
                            tag: $scope.tag
                        },
                        file: $scope.files
                    }).success(function (data, status) {
                        $scope.formship.$setPristine(true);
                        if (data.res) {
                            $rootScope.Notifications('Barco creado satisfactoriamente.', 0);
                            $scope.loadShips();
                            $scope.hideForm();
                            $rootScope.loadStatistics();
                        } else {
                            $rootScope.Notifications('Ha ocurrido un error, el barco no se ha podido crear.', 2);
                        }
                    }).error(function (status) {
                        $rootScope.Notifications('Ha ocurrido un error, el barco no se ha podido crear.', 2);
                    });
                }
            };

            $scope.reload = false;
            $scope.tour_id = "";
            if ($routeParams.id) {
                $scope.tour_id = $routeParams.id;
                $scope.reload = true;
            }

            $scope.startProcess = function (cb) {
                $http({method: 'POST', url: '/api/country/front'}).
                    success(function (ddata) {
                        if (!ddata.res) {
                            window.location = "/";
                        } else {
                            if (ddata.res.length) {
                                $scope.countries = ddata.res;
                                var auxCountries = [];
                                $http({method: 'POST', url: '/api/ports/available'}).
                                    success(function (data) {
                                        if (data.res) {
                                            $scope.allExistsPorts = data.res;
                                            for (var i = 0; i < $scope.countries.length; i++) {
                                                var exist_country = false;
                                                for (var j = 0; j < $scope.allExistsPorts.length; j++) {
                                                    if ($scope.countries[i].name.es == $scope.allExistsPorts[j].country.es) {
                                                        for (var k = 0; k < $scope.countries[i].cities.length; k++) {
                                                            if ($scope.countries[i].cities[k].es == $scope.allExistsPorts[j].city.es) {
                                                                exist_country = true;
                                                                break;
                                                            }
                                                        }
                                                        if (exist_country)break;
                                                    }
                                                }
                                                if (exist_country) {
                                                    auxCountries.push($scope.countries[i]);
                                                }
                                            }
                                            $scope.countries = auxCountries;
                                            $scope.selectedCountry = $scope.countries[0];
                                            $scope.cities = $scope.selectedCountry.cities;
                                            $scope.country = $scope.selectedCountry.name.es;


                                            var auxCities = [];
                                            for (var i = 0; i < $scope.cities.length; i++) {
                                                var exist_city = false;
                                                for (var j = 0; j < $scope.allExistsPorts.length; j++) {
                                                    if ($scope.cities[i].es == $scope.allExistsPorts[j].city.es) {
                                                        exist_city = true;
                                                        break;
                                                    }
                                                }
                                                if (exist_city) {
                                                    auxCities.push($scope.cities[i]);
                                                }
                                            }
                                            $scope.cities = auxCities;
                                            $scope.selectedCity = $scope.cities[0];
                                            $scope.city = $scope.selectedCity.es;
                                            $scope.filterByCity();

                                            if (cb) cb(true);
                                        }
                                        else {
                                            alert("Error");
                                        }

                                    });
                            }

                        }

                    });
            };
            $scope.filterByCity = function () {
                $http({
                    method: 'POST', url: '/api/ports/city', data: {
                        country: $scope.selectedCountry.name.en,
                        city: $scope.selectedCity.en
                    }
                }).
                    success(function (data) {
                        if (!data.res) {
                            $scope.allPorts = null;
                        }
                        else {
                            $scope.allPorts = data.res;
                            $scope.selectedPort = $scope.allPorts[0];
                            $scope.namePort = $scope.selectedPort.name.es;
                            $scope.id_port = $scope.allPorts[0]._id;
                            $scope.loadTours($scope.ship);
                        }
                    });
            };
            $scope.changeCountry = function () {
                for (var i = 0; i < $scope.countries.length; i++) {
                    if ($scope.countries[i].name.es == $scope.country || $scope.countries[i].name.en == $scope.country) {
                        $scope.selectedCountry = $scope.countries[i];


                        $scope.cities = $scope.selectedCountry.cities;
                        $scope.country = $scope.selectedCountry.name.es;

                        var auxCities = [];
                        for (var i = 0; i < $scope.cities.length; i++) {
                            var exist_city = false;
                            for (var j = 0; j < $scope.allExistsPorts.length; j++) {
                                if ($scope.cities[i].es == $scope.allExistsPorts[j].city.es) {
                                    exist_city = true;
                                    break;
                                }
                            }
                            if (exist_city) {
                                auxCities.push($scope.cities[i]);
                            }
                        }
                        $scope.cities = auxCities;
                        for (var i = 0; i < $scope.cities.length; i++) {
                            var exist_city = false;
                            for (var j = 0; j < $scope.allExistsPorts.length; j++) {
                                if ($scope.cities[i].es == $scope.allExistsPorts[j].city.es) {
                                    exist_city = true;
                                    break;
                                }
                            }
                            if (!exist_city) {
                                $scope.cities.splice(i, 1);
                            }
                        }
                        $scope.selectedCity = $scope.cities[0];
                        $scope.city = $scope.selectedCity.es;
                        $scope.filterByCity();
                        $scope.tourDescription = '';
                        $scope.arrayServices = [];
                        break;
                    }
                }

            };
            $scope.changeCity = function () {
                for (var i = 0; i < $scope.selectedCountry.cities.length; i++) {
                    if ($scope.selectedCountry.cities[i].es == $scope.city) {
                        $scope.selectedCity = $scope.selectedCountry.cities[i];
                        $scope.city = $scope.selectedCity.es;
                        $scope.filterByCity();
                        break;
                    }
                }
                $scope.tourDescription = '';
                $scope.arrayServices = [];
            };

            $scope.changePort = function () {
                $scope.loadTours($scope.ship);
            };

            $scope.startProcess();

            $scope.loadShips = function () {
                Ships.list.query({
                    limit: $scope.maxSize,
                    skip: $scope.skip,
                    text: $scope.text,
                    id: $scope.tour_id
                }, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.cont;
                        $scope.ships = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.ships.length > 0) {
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadShips();
                            }
                        }
                    } else {
                        $scope.allContact = new Array();
                        $scope.empty = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener los barcos.', 2);
                    }
                });
            };
            $scope.loadShips();

            $scope.showImg = function (ship) {
                $scope.hideImg();
                $scope.objImg = ship;
                $scope.images = ship.photos;
                $scope.openClose = false;
                $('#imagesObject').show();
            };

            $scope.hideImg = function () {
                $('#imagesObject').hide();
            };

            /**
             * @method Evento que se ejecuta cuando el archivo es seleccionado
             * @param $files
             */
            $scope.onImgSelect = function (files) {
                img = false;
                $scope.erroresNameImage = [];
                $scope.erroresExistImage = [];
                $scope.validCantImg = 0;
                recursivo(files, 0, function (data) {
                    $scope.$apply();
                });
            };

            function recursivo(files, pos, cb) {
                if (files.length == pos) {
                    $scope.$apply();
                    if (cb) cb(true)
                } else {
                    var typeArray = ["image/jpeg", "image/png"];
                    if ($rootScope.validateFile(files[pos], typeArray)) {
                        if ($scope.files.length > 0) {
                            if ($scope.verificarExiste(files[pos])) {
                                $scope.error = "errorExist";
                                $scope.erroresExistImage.push(files[pos].name);
                            } else {
                                if ($scope.cantImg < $scope.cantImage) {
                                    $scope.cantImg++;
                                    var reader = new FileReader();
                                    reader.readAsDataURL(files[pos]);
                                    reader.onload = function () {
                                        files[pos].image = reader.result;
                                        $scope.files.push(files[pos]);
                                        img = true;
                                        pos++;
                                        if (cb) cb(recursivo(files, pos, cb));
                                    };
                                } else {
                                    $('#imgFile').val(null);
                                    $scope.validCantImg = 1;
                                }
                            }
                        } else {
                            $scope.cantImg++;
                            var reader = new FileReader();
                            reader.readAsDataURL(files[pos]);
                            reader.onload = function () {
                                files[pos].image = reader.result;
                                $scope.files.push(files[pos]);
                                img = true;
                                pos++;
                                if (cb) cb(recursivo(files, pos, cb));
                            };
                        }
                    } else {
                        $scope.error = "errorType";
                        $scope.erroresNameImage.push(files[pos].name);
                        pos++;
                        if (cb) cb(recursivo(files, pos, cb));
                    }
                }
            }

            $scope.verificarExiste = function ($file) {
                var flag = false;
                var cont = 0;
                while (!flag && $scope.files.length > cont) {
                    if ($file.name == $scope.files[cont].name && $file.size == $scope.files[cont].size) {
                        flag = true;
                    }
                    cont = cont + 1;
                }
                return flag;
            };

            $scope.deleteShipImg = function (file) {
                var pos = 0;
                var cantidad = $scope.files.length;
                var flag = false;
                while (!flag && pos < cantidad) {
                    if (file.name == $scope.files[pos].name) {
                        $scope.files.splice(pos, 1);
                        flag = true;
                    }
                    pos++;
                }
                $scope.cantImg--;
                $scope.validCantImg = 0;
                if ($scope.files.length == 0) {
                    img = false;
                }
                if (file.path) {
                    $scope.nameImgDel.push(file.name);
                }
                $('#imgFile').val(null);
            };

            $scope.addMedia = function () {
                $upload.upload({
                    url: "/api/ships/addMedia", //upload.php script, node.js route, or servlet url
                    method: 'POST',
                    headers: {'header-key': 'multipart/form-data'},
                    data: {id: $scope.objImg._id},
                    file: $scope.files
                }).success(function (data) {
                    if (data.res) {
                        $scope.images.push(data.res);
                        $scope.files = new Array();
                        $("#imgFile").val(null);
                    }
                });
            };

            $scope.delMedia = function (photo) {
                $http({method: 'POST', url: '/api/ships/delMedia', data: {id: $scope.objImg._id, photo: photo}}).
                    success(function (data) {
                        if (data.res) {
                            for (var i = 0; i < $scope.images.length; i++) {
                                if (photo.name == $scope.images[i].name) {
                                    $scope.images.splice(i, 1);
                                }
                            }
                        }
                    }).
                    error(function (data, status, headers, config) {

                    });
            };

            $scope.validateFile = function ($files, extArray) {
                var flag = false;
                for (var i = 0; i < $files.length; i++) {
                    while ($files[i].name.indexOf("\\") != -1)
                        $files[i].name = $files[i].name.slice($files[i].name.indexOf("\\") + 1);
                    var ext = $files[i].name.slice($files[i].name.indexOf(".")).toLowerCase();
                    for (var j = 0; j < extArray.length; j++) {
                        if (extArray[j] == ext) {
                            //$scope.files.push($files[i]);
                            flag = true;
                        }
                    }
                }
                if (!flag) {
                    alert("Archivo invalido...");
                    $('#imgFile').val(null);
                }
                return flag;
            };

            $scope.edit = function (ship) {
                $scope.ship_id = ship._id;
                $scope.name = ship.name;
                $scope.description = ship.description;
                $scope.patron = ship.patron.id;
                $scope.owner = {
                    id: ship.owner.id,
                    comitions: ship.owner.comitions
                };
                $scope.tag = ship.tag;
                $scope.nameUserOwner = ship.owner.email;
                $scope.nameUserPatron = ship.patron.email;
                $('#imgFile').val(null);
                $scope.files = [];
                for (var i = 0; i < ship.photos.length; i++) {
                    $scope.files[i] = ship.photos[i];
                }
                $scope.cantImg = $scope.files.length;
                img = true;
                $scope.nameImgDel = [];
                $scope.editG = true;
                $scope.showForm();
            };

            $scope.crudTour = function (ship) {
                $scope.ship = ship;
                $scope.name = ship.name;

                $scope.loadTours(ship);
                $scope.showTours();
            };

            $scope.showTours = function () {
                $('#toursShip').slideDown();
            };

            $scope.hideTours = function () {
                $('#toursShip').slideUp();

            };

            $scope.listOwners = function () {
                ShipsOwners.get({
                    limit: $scope.maxSizeO,
                    skip: $scope.skipO,
                    word: $scope.wordSearchO
                }, function (data) {
                    if (data.response) {
                        $scope.users = data.response;
                        $scope.totalItemsOwners = data.count;
                        if ($scope.totalItemsOwners == 0) {
                            $scope.emptyOwner = true;
                        }
                        if ($scope.users.length > 0) {
                            $scope.emptyOwner = false;
                        } else {
                            if ($scope.currentPageO > 1) {
                                $scope.currentPageO--;
                                $scope.skipO = $scope.skipO - $scope.maxSizeO;
                                $scope.listOwners();
                            }
                        }
                    }
                    else {
                        $scope.users = [];
                        $scope.emptyOwner = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener los proveedores.', 2);
                    }
                })
            };

            $scope.searchOwners = function () {
                $scope.listOwners();
            };

            $scope.listPatrons = function () {
                ShipsPatrons.get({
                    limit: $scope.maxSizeP,
                    skip: $scope.skipP,
                    word: $scope.wordSearchP
                }, function (data) {
                    if (data.response) {
                        $scope.users = data.response;
                        $scope.totalItemsPatrons = data.count;
                        if ($scope.totalItemsPatrons == 0) {
                            $scope.emptyPatron = true;
                        }
                        if ($scope.users.length > 0) {
                            $scope.emptyPatron = false;
                        } else {
                            if ($scope.currentPageP > 1) {
                                $scope.currentPageP--;
                                $scope.skipP = $scope.skipP - $scope.maxSizeP;
                                $scope.listPatrons();
                            }
                        }
                    }
                    else {
                        $scope.users = [];
                        $scope.emptyPatron = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener los patrones.', 2);
                    }
                })
            };

            $scope.searchPatrons = function () {
                $scope.listPatrons();
            };

            $scope.pageChangedP = function () {
                $scope.skipP = ($scope.currentPageP - 1) * $scope.maxSizeP;
                $scope.listPatrons();
            };

            $scope.pageChangedO = function () {
                $scope.skipO = ($scope.currentPageO - 1) * $scope.maxSizeO;
                $scope.listOwners();
            };

            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.loadShips();
            };

            $scope.save = function () {
                Ships.save({
                    id: $scope.ship_id,
                    name: $scope.name,
                    description: $scope.description,
                    patron: $scope.patron,
                    owner: $scope.owner,
                    tag: $scope.tag
                }, function (data) {
                    $scope.formship.$setPristine(true);
                    if (data.res) {
                        var files = preparedFiles($scope.files);
                        if ($scope.nameImgDel.length != 0 || files.length != 0) {
                            $upload.upload({
                                url: "/api/ships/updateImg", //upload.php script, node.js route, or servlet url
                                method: 'POST',
                                headers: {'header-key': 'multipart/form-data'},
                                data: {id: $scope.ship_id, nameImgDel: $scope.nameImgDel},
                                file: files
                            }).success(function (data) {
                                if (data.result) {
                                    $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                                    $scope.loadShips();
                                    $scope.hideForm();
                                } else {
                                    $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                                }
                            }).error(function (status) {
                                $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                            });
                        } else {
                            $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                            $scope.loadShips();
                            $scope.hideForm();
                        }
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                    }
                })
            };

            $scope.delete = function () {
                Ships.delete({id: $scope.deleteItem._id}, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Barco eliminado satisfactoriamente', 0);
                        $scope.loadShips();
                        $rootScope.loadStatistics();
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido eliminar el Barco', 2);
                    }
                })
            };

            $scope.showDeleteModal = function (ship) {
                if (!ship.hasRes) {
                    $scope.deleteItem = ship;
                }
            };

            $scope.openClose = true;
            $scope.showForm = function () {
                $scope.openClose = false;
                $('#form-ship').show();
            };

            $scope.hideForm = function () {
                //$scope.hideImg();
                $scope.editG = false;
                $scope.openClose = true;
                $('#form-ship').hide();
                $scope.clearField();
                $scope.formship.$setPristine(true);
            };

            $scope.clearField = function () {
                $scope.name = "";
                $scope.description = {
                    es: "",
                    en: ""
                };
                $scope.owner.comitions = 0;
                $scope.nameUserPatron = "";
                $scope.nameUserOwner = "";
                $scope.files = [];
                img = false;
                $scope.erroresNameImage = [];
                $scope.erroresExistImage = [];
                $scope.validCantImg = 0;
                $scope.cantImg = 0;
                $scope.nameImgDel = [];
                $('#imgFile').val(null);
                $scope.tabsShowEs = {
                    description: true
                };
                $scope.tag = {
                    name: {
                        _id: null,
                        es: '',
                        en: ''
                    }
                }
            };

            $scope.loadTours = function (ship) {
                if (ship != null) {
                    var pos = -1;
                    for (var i = 0; i < $scope.allExistsPorts.length; i++) {
                        if ($scope.allExistsPorts[i].name.es == $scope.namePort && $scope.allExistsPorts[i].city.es == $scope.city && $scope.allExistsPorts[i].country.es == $scope.country) {
                            pos = i;
                            break;
                        }
                    }
                    if (pos != -1) {
                        $scope.port = $scope.allExistsPorts[pos];
                        $http({
                            method: 'POST', url: '/api/tours/ports', data: {
                                port: $scope.port._id
                            }
                        }).success(function (data) {
                            var tours = data.res;
                            $scope.tours = [];
                            for (var i = 0; i < tours.length; i++) {
                                var tmp = false;
                                for (var j = 0; j < ship.tours.length; j++) {
                                    if (tours[i]._id == ship.tours[j]) {
                                        tmp = true;
                                        break;
                                    }
                                }
                                var aux = {
                                    id: tours[i]._id,
                                    name: tours[i].name.es,
                                    group: tours[i].group,
                                    active: tmp
                                };
                                $scope.tours.push(aux);
                            }
                        })
                    }
                }
//                Tour.get(function (data) {
//                    if (data.res) {
//                        var tours = data.res;
//                        $scope.tours = [];
//                        for (var i = 0; i < tours.length; i++) {
//                            var tmp = false;
//                            for (var j = 0; j < ship.tours.length; j++) {
//                                if (tours[i]._id == ship.tours[j]) {
//                                    tmp = true;
//                                    break;
//                                }
//                            }
//                            var aux = {
//                                id: tours[i]._id,
//                                name: tours[i].name.es,
//                                group: tours[i].group,
//                                active: tmp
//                            };
//                            $scope.tours.push(aux);
//                        }
//                    }
//                })
            };

            $scope.addTour = function (tour) {
                $http({method: 'POST', url: '/api/ships/addTour', data: {id: $scope.ship._id, tour_id: tour.id}}).
                    success(function (data) {
                        if (data.res) {
                            $scope.ship.tours.push(tour.id);
                            $scope.loadTours($scope.ship);
                        }
                    });
            };

            $scope.delTour = function (tour) {
                $http({method: 'POST', url: '/api/ships/delTour', data: {id: $scope.ship._id, tour_id: tour.id}}).
                    success(function (data) {
                        if (data.res) {
                            for (var i = 0; i < $scope.ship.tours.length; i++) {
                                if ($scope.ship.tours[i] == tour.id) {
                                    $scope.ship.tours.splice(i, 1);
                                    break;
                                }
                            }

                            $scope.loadTours($scope.ship);
                        }
                    });
            };

            $scope.validateForm = function () {
                return ($scope.name.length >= 1
                && $scope.description.es.length >= 1
                && $scope.description.en.length >= 1
                && $scope.nameUserPatron.length >= 1
                && $scope.nameUserOwner.length >= 1
                && $scope.tag.name.es.length >= 1
                && $scope.owner.comitions > 0
                );
            };

            function preparedFiles(files) {
                var arrayTemp = [];
                for (var i = 0; i < files.length; i++) {
                    if (files[i].image) {
                        arrayTemp.push(files[i]);
                    }
                }
                return arrayTemp;
            }

            /******************************************************************/
            /*CALENDAR*/
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            $scope.events = [];
            $http({
                method: 'POST', url: '/api/reservation/getRTbyShip', data: {
                    ship: "5463a0be7a393a543c945e55"
                }
            }).success(function (data) {
                if (data.res) {
                    for (var i = 0; i < data.res.length; i++) {
                        var aux = createEvent(data.res[i]);
                        $scope.events.push(aux);
                    }
                }
            });

            /* event source that calls a function on every view switch */
            $scope.eventsF = function (start, end, callback) {
                var s = new Date(start).getTime() / 1000;
                var e = new Date(end).getTime() / 1000;
                var m = new Date(start).getMonth();
                var events = [
                    {
                        title: 'Feed Me ' + m,
                        start: s + (50000),
                        end: s + (100000),
                        allDay: false,
                        className: ['customFeed']
                    }
                ];
                callback(events);
            };

            $scope.calEventsExt = {
                color: '#f00',
                textColor: 'yellow',
                events: [
                    {
                        type: 'party',
                        title: 'Lunch',
                        start: new Date(y, m, d, 12, 0),
                        end: new Date(y, m, d, 14, 0),
                        allDay: false
                    },
                    {
                        type: 'party',
                        title: 'Lunch 2',
                        start: new Date(y, m, d, 12, 0),
                        end: new Date(y, m, d, 14, 0),
                        allDay: false
                    },
                    {
                        type: 'party',
                        title: 'Click for Google',
                        start: new Date(y, m, 28),
                        end: new Date(y, m, 29),
                        url: 'http://google.com/'
                    }
                ]
            };
            /* alert on eventClick */
            $scope.alertEventOnClick = function (date, allDay, jsEvent, view) {
                $scope.alertMessage = ('Day Clicked ' + date);
            };
            /* alert on Drop */
            $scope.alertOnDrop = function (event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
                $scope.alertMessage = ('Event Droped to make dayDelta ' + dayDelta);
            };
            /* alert on Resize */
            $scope.alertOnResize = function (event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
                $scope.alertMessage = ('Event Resized to make dayDelta ' + minuteDelta);
            };
            /* add and removes an event source of choice */
            $scope.addRemoveEventSource = function (sources, source) {
                var canAdd = 0;
                angular.forEach(sources, function (value, key) {
                    if (sources[key] === source) {
                        sources.splice(key, 1);
                        canAdd = 1;
                    }
                });
                if (canAdd === 0) {
                    sources.push(source);
                }
            };
            /* add custom event*/
            $scope.addEvent = function () {
                $scope.events.push({
                    title: 'Open Sesame',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    className: ['openSesame']
                });
            };
            /* remove event */
            $scope.remove = function (index) {
                $scope.events.splice(index, 1);
            };
            /* Change View */
            $scope.changeView = function (view, calendar) {
                calendar.fullCalendar('changeView', view);
            };
            /* Change View */
            $scope.renderCalender = function (calendar) {
                calendar.fullCalendar('render');
            };
            /* config object */

            $scope.uiConfig = {
                calendar: {
                    height: 450,
                    editable: true,
                    header: {
                        left: 'today prev,next',
                        center: 'title',
                        right: 'month agendaWeek'
                    },
                    editable: true,
                    droppable: true,
                    dayClick: $scope.alertEventOnClick,
                    eventDrop: $scope.alertOnDrop,
                    eventResize: $scope.alertOnResize

                }
            };
            /* event sources array*/
            $scope.eventSources = [$scope.events];
            /* $scope.events = [
             {title: 'All Day Event',start: new Date(y, m, 1)},
             {title: 'Long Event',start: new Date(y, m, d - 5),end: new Date(y, m, d - 2)},
             {id: 999,title: 'Repeating Event',start: new Date(y, m, d - 3, 16, 0),allDay: false},
             {id: 999,title: 'Repeating Event',start: new Date(y, m, d + 4, 16, 0),allDay: false},
             {title: 'Birthday Party',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
             {title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
             ];*/

            function createEvent(rest) {
                if (rest.type == 'day') {
                    var start = new Date(rest.date),
                        end = new Date(86400000 * (rest.days - 1) + rest.date);
                    var aux = {
                        //color: '#f00',
                        // textColor: 'yellow',
                        title: rest.tour.name.es,
                        start: start,
                        end: end,
                        editable: false,
                        allDay: true
                    };
                }
                else if (rest.type == 'agree') {
                    var start = new Date(rest.date);
                    var aux = {
                        //color: '#f00',
                        // textColor: 'yellow',
                        title: rest.tour.name.es,
                        start: start,
                        allDay: true,
                        editable: false
                    };
                }
                else {
                    var tmp = new Date(rest.date);
                    var start = new Date(tmp.getFullYear(), tmp.getMonth(), tmp.getDate(), rest.departure);
                    var end = new Date(tmp.getFullYear(), tmp.getMonth(), tmp.getDate(), (rest.departure + Math.ceil(rest.tour.hours)));
                    var aux = {
                        //color: '#f00',
                        // textColor: 'yellow',
                        title: rest.tour.name.es,
                        start: start,
                        end: end,
                        allDay: false,
                        editable: false
                    };
                }
                return aux;
            };

            $scope.changeStatus = function (ship) {
                $scope.ship = angular.copy(ship);
                //action
                //0 consulta
                //1 confirmacion
                //2 active
                /*if (ship.available) {
                 Ships.status.change({id: ship._id, action: 2}, function (data) {
                 if (data.res) {
                 $scope.loadShips();
                 }
                 else {
                 $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                 }
                 })
                 } else {
                 Ships.status.change({id: ship._id, action: 0}, function (data) {
                 if (data.res) {
                 if (!data.complete) {
                 $scope.showChangeStatusModal(ship, data.res);
                 } else {
                 $scope.loadShips();
                 }
                 }
                 else {
                 $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                 }
                 })
                 }*/
                Ships.status.change({id: $scope.ship._id}, function (data) {
                    if (data.res) {
                        $scope.loadShips();
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                    }
                })
            };

            $scope.showChangeStatusModal = function (data, users) {
                $scope.changeStatusItem = data;
                $scope.associationUsers = users;
                $('#changeStatusModal').modal('show');
            };

            $scope.changeStatusNow = function (ship) {
                Ships.status.change({id: ship._id, action: 1}, function (data) {
                    if (data.res) {
                        $scope.loadShips();
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                    }
                })
            };


            /*****TAGS****/
            $scope.currentPageTags = 1;
            $scope.maxSizeTags = 10;
            $scope.skipTags = 0;
            $scope.totalItemsTags = 0;
            $scope.pageChangedTags = function () {
                $scope.skipTags = ($scope.currentPageTags - 1) * $scope.maxSizeTags;
                $scope.loadTags();
            };
            $scope.loadTags = function () {
                Tags.query({limit: $scope.maxSizeTags, skip: $scope.skipTags}, function (data) {
                    if (data.count) {
                        $scope.totalItemsTags = data.cont;
                        $scope.tags = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.tags.length > 0) {
                            $scope.emptyTags = false;
                        } else {
                            if ($scope.currentPageTags > 1) {
                                $scope.currentPageTags--;
                                $scope.skipTags = $scope.skipTags - $scope.maxSizeTags;
                                $scope.loadTags();
                            }
                        }
                    } else {
                        $scope.tags = new Array();
                        $scope.emptyTags = true;
                    }
                });
            };

            $scope.SelectTag = function (tag) {
                $scope.tag = angular.copy(tag);
            }

        }]);

