/**
 * Created by ernestomr87@gmail.com on 4/07/14.
 */

angular.module('bojeoApp.editCalendarController', [])
    .controller('editCalendarController', ['$scope', '$rootScope', '$upload', '$routeParams', '$http',
        function ($scope, $rootScope, $upload, $routeParams, $http) {

            $rootScope.PageSelected = 'calendar';
            $rootScope.loadStatistics();
            $scope.shipId = $routeParams.shipId;

            $scope.eventStart = null;
            $scope.eventEnd = null;
            $scope.blockEvents = [];
            $scope.calendar = false;
            $scope.shipName = null;
            var count = 0;
            $scope.availableEvents = [];

            $http({method: 'get', url: '/api/ships/'+$scope.shipId}).
                success(function (status, headers, config) {
                    $scope.shipName = status.name;
                    if(status.calendar){
                        $scope.calendar = true;
                        $scope.eventStart = status.calendar.start;
                        $scope.eventEnd = status.calendar.end;
                        for(var i = 0; i < status.calendar.block.length; i++){
                            if(status.calendar.block[i].available == false)
                                $scope.blockEvents.push(status.calendar.block[i].day);
                        }
                    }

                }).
                error(function (data, status, headers, config) {

                });


            function saveEventStart(start){
                $scope.eventStart = start;
            }
            function saveEventEnd(end){
                $scope.eventEnd = end;
            }
            function saveBlockEvents(date){
                $scope.blockEvents.push(date);
            }

            $scope.compStartEndDates = function(event){

                var a = event.start.getTime();
                $scope.flag = true;
                $scope.flagBlock = true;

                if(event.title == "Inicio"){
                    if($scope.eventEnd != null){
                        if(a < $scope.eventEnd){
                            var f = $scope.compBlocksWithStart(event);
                            if(f) saveEventStart(a);
                            else $scope.flagBlock = false;
                        }else {
                            $scope.flag = false;
                        }
                    }else{
                        var f = $scope.compBlocksWithStart(event);
                        if(f) saveEventStart(a);
                        else $scope.flagBlock = false;
                    }
                }
                if(event.title == "Fin"){
                    if($scope.eventStart != null){
                        if(a > $scope.eventStart){
                            var f = $scope.compBlocksWithEnd(event);
                            if(f) saveEventEnd(a);
                            else $scope.flagBlock = false;
                        }else {
                            $scope.flag = false;
                        }
                    }else{
                        var f = $scope.compBlocksWithEnd(event);
                        if(f) saveEventEnd(a);
                        else $scope.flagBlock = false;
                    }
                }
                if(event.title == "Bloqueado"){
                    if($scope.eventStart != null){
                            var f = $scope.compBlocksWithDates(event);
                            if(f){
                                $scope.saveBlocksEvents();
                            }
                            else {
                                $scope.flagBlock = false;
                            }
                        }else {
                            $scope.flag = false;
                        }
                    if($scope.eventEnd != null){
                            var f = $scope.compBlocksWithDates(event);
                            if(f){
                                $scope.saveBlocksEvents();
                            }
                            else {
                                $scope.flagBlock = false;
                            }
                    }else {
                            $scope.flag = false;
                        }
                }
            }
            $scope.compBlocksWithStart = function(event){
                var a = event.start.getTime();
                var blockFlag = true;

                if($scope.blockEvents.length != 0){
                    for(var i = 0; i < $scope.blockEvents.length; i++){
                        if(a > $scope.blockEvents[i]){
                            blockFlag = false;
                        }
                    }
                }
                return blockFlag;
            }
            $scope.compBlocksWithEnd = function(event){
                var a = event.start.getTime();
                var blockFlag = true;

                if($scope.blockEvents.length != 0){
                    for(var i = 0; i < $scope.blockEvents.length; i++){
                        if(a < $scope.blockEvents[i]){
                            blockFlag = false;
                        }
                    }
                }
                return blockFlag;
            }
            $scope.compBlocksWithDates = function(event){
                var a = event.start.getTime();
                var blockFlag = true;
                if(a < $scope.eventStart){
                    blockFlag = false;
                }
                if(a > $scope.eventEnd){
                    blockFlag = false;
                }
                return blockFlag;
            }

            $scope.saveBlocksEvents = function(){
                $scope.blockEvents = [];
                for(var i = 0; i < $scope.allEvents.length; i++){
                    if($scope.allEvents[i].title == 'Bloqueado')
                        $scope.blockEvents.push($scope.allEvents[i].start.getTime());
                }
            }

            function saveUnblockEventsDb(day){
                $http({method: 'PUT', url: '/api/ships/unblock', data: {id: $scope.shipId, start: $scope.eventStart, end: $scope.eventEnd, block: day}}).
                    success(function (status, headers, config) {
                    }).
                    error(function (data, status, headers, config) {

                    });
            }

            function saveBlockEventsDb(day){
                $http({method: 'PUT', url: '/api/ships/block', data: {id: $scope.shipId, start: $scope.eventStart, end: $scope.eventEnd, block: day}}).
                    success(function (data, headers, config) {
                        if(data.result){
                            var d = new Date(day);
                            $scope.getBookingByDate(d, function(data){
                                if (data != false) {
                                    for(var i=0; i<data.length; i++){
                                        var aux = {
                                            _id: data[i].client,
                                            token: data[i].token,
                                            reason: "Bloqueado el día por el administrador"
                                        }
                                        $http({method: 'POST', url: '/api/reservation/cancel', data: aux}).
                                            success(function (data, status, headers, config) {
                                                if(data.result){
                                                    $rootScope.total_devolutions = $rootScope.total_devolutions + 1;
                                                    $rootScope.total_reserves = $rootScope.total_reserves - 1;
                                                }else{
                                                    return false;
                                                }
                                            })
                                    }
                                }else{
                                    return;
                                }
                            });
                        }
                    }).
                    error(function (data, status, headers, config) {

                    });
            }

            $scope.getBookingByDate = function(day, cb){
                $http({method: 'POST', url: '/api/reservation/byDate', data: {date: day.getTime()}}).
                    success(function (data, headers, config) {
                        if(data.result) {
                            if(cb) cb(data.result)
                        }else{
                            if(cb) cb(false)
                        }
                    }).
                    error(function (data, status, headers, config) {

                    });
            }

            $('#calendar').fullCalendar({
                eventClick: function(calEvent, jsEvent, view) {
                    if(calEvent.title == "Disponible") {
                        $scope.getBookingByDate(calEvent.start, function(data){
                            if (data != false) {
                                if (confirm("Existen reservas para este día. ¿Desea bloquearlo?")) {

                                    var date = calEvent.start;
                                    var eve = ({
                                        title: 'Bloqueado',
                                        start: date,
                                        color: 'red'
                                    })
                                    $('#calendar').fullCalendar('renderEvent', eve);
                                    saveBlockEvents(date.getTime());
                                    saveBlockEventsDb(date.getTime());
                                    $('#calendar').fullCalendar('removeEvents', calEvent._id);

                                }
                            } else {
                                var date = calEvent.start;
                                var eve = ({
                                    title: 'Bloqueado',
                                    start: date,
                                    color: 'red'
                                })
                                $('#calendar').fullCalendar('renderEvent', eve);
                                saveBlockEvents(date.getTime());
                                saveBlockEventsDb(date.getTime());
                                $('#calendar').fullCalendar('removeEvents', calEvent._id);
                            }
                        })
                    }

                    if(calEvent.title == "Bloqueado"){
                        if(confirm("Esta acción habilitará nuevamente el día. ¿Desea continuar?")){
                            $('#calendar').fullCalendar('renderEvent',
                                {
                                    title: 'Disponible',
                                    start: calEvent.start,
                                    color: '#DDDDDD'
                                }
                            );
                            $('#calendar').fullCalendar('removeEvents', calEvent._id);
                            for(var i = 0; i < $scope.blockEvents.length; i++){
                                if(calEvent.start.getTime() == $scope.blockEvents[i]){
                                    $scope.blockEvents.splice(i,1);
                                    saveUnblockEventsDb(calEvent.start.getTime());
                                }
                            }
                        }
                    }
                },
                eventDrop: function(event){

                    $scope.compStartEndDates(event);
                    if(!$scope.flag || !$scope.flagBlock){
                        if(event.title == "Inicio"){
                            alert("El día de incio debe ser menor que las demás fechas");
                            event.start = new Date($scope.eventStart);
                        }if(event.title == "Fin"){
                            alert("El día final debe ser mayor que las demás fechas");
                            event.start = new Date($scope.eventEnd);
                        }if(event.title == "Bloqueado"){
                            for(var i = 0; i < $scope.allEvents.length; i++){
                                if(event._id == $scope.allEvents[i]._id){
                                    alert("Las fechas de bloqueo deben estar entre la de inicio y la de fin");
                                    event.start = new Date($scope.blockEvents[i]);
                                }
                            }
                        }
                    }else{
                        var pos = null;
                        var flag = true;
                        for(var i=0; i<$scope.availableEvents.length; i++){
                            if(event.start.getTime() == $scope.availableEvents[i].start.getTime()){
                                $('#calendar').fullCalendar('removeEvents', $scope.availableEvents[i]._id);
                                var pos = i;
                            }else{
                               flag = false;
                            }
                        }
                        if(event.title == "Fin"){
                            if(pos != null){
                                for(var i=pos; i<$scope.availableEvents.length; i++){

                                    $('#calendar').fullCalendar('removeEvents', $scope.availableEvents[i]._id);

                                }

                            }
                        }
                        if(event.title == "Inicio"){
                            if(pos != null){
                                for(var i=0; i<pos; i++){
                                    $('#calendar').fullCalendar('removeEvents', $scope.availableEvents[i]._id);
                                    //$scope.availableEvents.splice(i, 1);
                                }
                            }
                        }
                        saveEvents();
                    }
                },
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar !!!

                events: function(start, end, callback) {
                    $.ajax({
                        url: '/api/ships/'+$scope.shipId,
                        type: 'get',
                        success: function(doc) {
                            var events = [];
                            var eventStart = new Date(doc.calendar.start);
                            var eventEnd = new Date(doc.calendar.end);
                            if(doc.calendar){
                                if(doc.calendar.block.length != 0)
                                    var cantBlock = doc.calendar.block.length;
                                events.push({
                                    title: 'Inicio',
                                    start: eventStart, // will be parsed
                                    color: 'green'
                                });
                                events.push({
                                    title: 'Fin',
                                    start: eventEnd, // will be parsed
                                    color: 'blue'
                                });
                                var allDates = [];
                                var pos = 0;
                                for(var j = doc.calendar.start+86400000; j < doc.calendar.end; j = j+86400000){
                                    allDates[pos] = j;
                                    pos++;
                                }
                                for(var i = 0; i < cantBlock; i++){
                                    if(doc.calendar.block[i].badmin == true){
                                        var eventBlock = new Date(doc.calendar.block[i].day);
                                        events.push({
                                            title: 'Bloqueado',
                                            start: eventBlock,
                                            color: 'red'
                                        })
                                        for(var j=0; j<allDates.length; j++){
                                            if(doc.calendar.block[i].day == allDates[j]){
                                                allDates.splice(j, 1);
                                            }
                                        }
                                    }
                                }
                                for(var i = 0; i < cantBlock; i++){
                                    if(doc.calendar.block[i].available == false){
                                        var eventBlock = new Date(doc.calendar.block[i].day);
                                        events.push({
                                            title: 'Ocupado',
                                            start: eventBlock,
                                            color: 'pink'
                                        })
                                        for(var j=0; j<allDates.length; j++){
                                            if(doc.calendar.block[i].day == allDates[j]){
                                                allDates.splice(j, 1);
                                            }
                                        }
                                    }
                                }
                                for(var j=0; j<allDates.length; j++){
                                    var ev = ({
                                     title: 'Disponible',
                                     start: new Date(allDates[j]),
                                     color: '#DDDDDD'
                                     })
                                     $('#calendar').fullCalendar('renderEvent', ev);
                                     $scope.availableEvents.push(ev);
                                }

                                $scope.allEvents = events;
                                callback(events);
                            }
                        }
                    });
                }
            });
        }]);
