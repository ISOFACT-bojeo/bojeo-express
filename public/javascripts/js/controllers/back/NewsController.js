angular.module('bojeoApp.NewsController', [])
    .controller('NewsController', ['$scope', '$rootScope', '$upload',
        function ($scope, $rootScope, $upload) {
            $rootScope.PageSelected = 'news';
            $rootScope.loadStatistics();

            $scope.title = null;
            $scope.introduction = null;
            $scope.body = null;
            $scope.video = null;
            $scope.imageFooter_es = null;
            $scope.imageFooter_en = null;
            $scope.originalAutor = null;
            $scope.files = [];
            var img = false;
            var doc = false;

            $scope.errorTitle = "";
            $scope.errorIntroduction = "";
            $scope.errorBody = "";

            $scope.dismissErrorDiv = function(){
                $scope.errorTitle = "";
                $scope.errorIntroduction = "";
                $scope.errorIntroduction_en = "";
                $scope.errorBody = "";
                $scope.errorDocUpload  = "";
                $scope.errorImgUpload  = "";
                $scope.errorBody_en  = "";
            }
            $scope.validateFile = function($files, Array, elem){
                var flag = false;
                for (var i = 0; i < $files.length; i++) {
                    while ($files[i].name.indexOf("\\") != -1)
                        $files[i].name = $files[i].name.slice($files[i].name.indexOf("\\") + 1);
                    ext = $files[i].name.slice($files[i].name.indexOf(".")).toLowerCase();
                    for (var j = 0; j < Array.length; j++) {
                        if (Array[j] == ext) {
                            $scope.files.push($files[i]);
                            flag = true;
                        }
                    }
                }
                if(!flag){
                    alert("Archivo invalido...");
                    $('#'+elem).val(null);
                }
                return flag;
            }
            $scope.onImgSelect = function ($files) {
                img = false;
                extArray = new Array(".jpg", ".png", ".gif", ".bmp");
                var flag = $scope.validateFile($files, extArray, 'imgFile');
                if(flag){
                    $scope.files[0] = $files[0];
                    if($scope.files[0] != null)
                        img = true;
                }
            }
            $scope.onDocSelect = function ($files) {
                doc = false;
                extDocs = new Array('.txt', '.pdf', '.doc', '.docx');
                var flag = $scope.validateFile($files, extDocs, 'docFile');
                if(flag){
                    $scope.files[1] = $files[0];
                    if($scope.files[1] != null)
                        doc = true;
                }
            }
            $scope.createNew = function () {
                var bool = true;
                $scope.errorNewsImg = true;
                $scope.errorNewsDocs = true;

                if($scope.title_es == "" || $scope.title_es == null){
                    bool = false;
                    $scope.errorTitle = "Campo vacío";
                }
                if($scope.title_en == "" || $scope.title_en == null){
                    bool = false;
                    $scope.errorTitle = "Campo vacío";
                }
                if($scope.introduction_es == "" || $scope.introduction_es == null){
                    bool = false;
                    $scope.errorIntroduction = "Campo vacío";
                }
                if($scope.introduction_en == "" || $scope.introduction_en == null){
                    bool = false;
                    $scope.errorIntroduction_en = "Campo vacío";
                }
                if($scope.body_es == "" || $scope.body_es == null){
                    bool = false;
                    $scope.errorBody = "Campo vacío";
                }
                if($scope.body_en == "" || $scope.body_en == null){
                    bool = false;
                    $scope.errorBody_en = "Campo vacío";
                }
                if(!img){
                    bool = false;
                    $scope.errorImgUpload = "Campo vacío";
                }
                    if(bool){
                        $upload.upload({
                            url: '/api/news', //upload.php script, node.js route, or servlet url
                            method: 'POST',
                            headers: {'header-key': 'multipart/form-data'},
                            // withCredentials: true,
                            data: { title_es: $scope.title_es, title_en: $scope.title_en, introduction_es: $scope.introduction_es, introduction_en: $scope.introduction_en,
                                body_es: $scope.body_es, body_en: $scope.body_en, video: $scope.video, originalAutor: $scope.originalAutor,
                                imageFooter_es: $scope.imageFooter_es, imageFooter_en: $scope.imageFooter_en},
                            file: $scope.files
                        }).success(function (data, status) {

                            if(data.result){
                                $scope.clearField();
                                $rootScope.total_news += 1;
                                showActionStatus(true);
                                setTimeout(function() {
                                    window.location = '/backoffice#/listNews';
                                },2000);
                            }else{
                                showActionStatus(false);
                            }

                        }).error(function (status) {
                            showActionStatus(false);
                        });
                    }else{
                        return;
                    }
            }
            $scope.clearField = function(){
                $scope.title_es = "";
                $scope.title_en = "";
                $scope.introduction_es = "";
                $scope.introduction_en = "";
                $scope.body_es = "";
                $scope.body_en = "";
                $scope.video = "";
                $scope.imageFooter_es = "";
                $scope.imageFooter_en = "";
                $scope.originalAutor = "";
                $scope.files = [];
                /*$('#imgFile').val(null);
                $('#docFile').val(null);*/
                img = false;
                $scope.dismissErrorDiv();
            }
            $scope.closeModal = function(){
                $scope.dismissErrorDiv();
                $scope.clearField();
                window.location = '/backoffice#/listNews';
            }
            function showActionStatus(flag){
                if(flag == true){
                    var html = ' <div class="alertSuccess alert-success" id="success-login" > <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Noticia creada con éxito</strong> </div>';
                    $('#success-create-alert').html(html).fadeIn(1000);
                    setTimeout(function() {
                        $("#success-create-alert").fadeOut(1500);
                    },2000);
                }else{
                    var html = ' <div class="alertError alert-error" id="error-login" > <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Lo siento, ocurrió un error al guardar los datos</strong> </div>';
                    $('#success-create-alert').html(html).fadeIn(1000);
                    setTimeout(function() {
                        $("#success-create-alert").fadeOut(1500);
                    },2000);
                }
            }
        }]);

