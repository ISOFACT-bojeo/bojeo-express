/**
 * Created by ernestomr87@gmail.com on 3/06/14.
 */

angular.module('bojeoApp.ToursController', new Array())
    .controller('ToursController', ['$scope', '$rootScope', 'Groups', '$http', '$upload',
        function ($scope, $rootScope, Groups, $http, $upload) {

            $rootScope.PageSelected = 'groups';
            $rootScope.loadStatistics();


            /***************************IMAGENES******************************/
            $scope.imgfileUpload = null;
            $scope.files = new Array();
            var img = false;
            $scope.cantImage = 1;
            $scope.dimensionImage = "672x340px"
            $scope.formatImage = "[ jpg, png ]";
            $scope.resolucionImage = "72dpi";

            $scope.erroresNameImage = new Array();
            $scope.erroresExistImage = new Array();
            $scope.validCantImg = 0;
            $scope.cantImg = 0;
            /***************************END IMAGENES******************************/


            $scope.name_es = "";
            $scope.name_en = "";
            $scope.description_es = "";
            $scope.description_en = "";
            $scope.tabsShowEs = {
                description: true
            };

            $scope.payform = 'currencies';

            $scope.colors = ['#00FF00', '#3FB3E9', '#00FFFF', '#FF7DA3', '#FFFF00', '#F279DA'];
            $scope.color = $scope.colors[0];
            $scope.changeColor = function (index) {
                $scope.color = $scope.colors[index];
            };

            $scope.styleColor = function (color) {
                return {'background_color': color};
            };

            $scope.editG = false;

            $scope.validFormGroup = function () {
                if ($scope.name_es.length < 7) {
                    return false;
                }
                if ($scope.name_en.length < 7) {
                    return false;
                }
                return true;
            };

            $scope.initializeTours = function () {
                $scope.name_es = "";
                $scope.name_en = "";
                $scope.description_es = "";
                $scope.description_en = "";
                $scope.payform = 'currencies';
                $scope.nameImgDel = new Array();
                $scope.files = new Array();
                img = false;
                $scope.erroresNameImage = new Array();
                $scope.erroresExistImage = new Array();
                $scope.validCantImg = 0;
                $scope.cantImg = 0;
                $scope.nameImgDel = new Array();
                $scope.seo = {
                    title: {
                        es: '',
                        en: ''
                    },
                    description: {
                        es: '',
                        en: ''
                    }
                };
                $('#imgFile').val(null);

            };

            $scope.editGroup = function (group) {
                $scope.initializeTours();
                $scope.editG = true;
                $scope.name_es = group.name.es;
                $scope.name_en = group.name.en;
                $scope.description_es = group.description.es;
                $scope.description_en = group.description.en;
                $scope.payform = group.payform;
                $scope.color = group.color;
                $scope.id = group._id;
                $scope.seo = group.seo;
                $('#imgFile').val(null);
                $scope.files = new Array();
                for (var i = 0; i < group.photos.length; i++) {
                    $scope.files[i] = group.photos[i];
                }
                $scope.cantImg = $scope.files.length;
                img = true;
                $scope.nameImgDel = new Array();

                $scope.showForm();
            };
            $scope.loadGroups = function () {
                Groups.get(function (data) {
                    $scope.groups = data.res;
                    if ($scope.groups.length > 0) {
                        $scope.empty = false;
                    } else {
                        $scope.empty = true;
                    }
                })
            };

            $scope.deleteGroup = function () {
                Groups.remove({id: $scope.deleteItem._id}, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Grupo eliminado satisfactoriamente', 0);
                        $rootScope.loadStatistics();
                        $scope.loadGroups();
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido eliminar el grupo', 2);
                    }
                })

            };

            $scope.openClose = true;
            $scope.showForm = function () {
                $scope.openClose = false;
                $('#form-bond').show();
            };
            $scope.hideForm = function () {
                $scope.openClose = true;
                $('#form-bond').hide();
                $scope.editBono = false;
                $scope.editG = false;
                $scope.initializeTours();
            };
            $scope.showDeleteModal = function (group) {
                $scope.deleteItem = group;
                $http({
                    method: 'POST', url: '/api/groups/listTours',
                    data: {
                        id: group._id
                    }
                }).success(function (data) {
                    if (data.res) {
                        $scope.cantReservation = data.cantReservation;
                        $scope.deleteTours = data.res;
                    } else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido obtener los datos', 2);
                    }
                });
            };

            $scope.loadGroups();

            $scope.upFront = function (group) {
                $http({method: 'POST', url: '/api/groups/upFront', data: {id: group._id, front: group.front}}).
                    success(function (data) {
                        if (data.res) {
                            $scope.loadGroups();
                        }
                    });
            };
            $scope.downFront = function (group) {
                $http({method: 'POST', url: '/api/groups/downFront', data: {id: group._id, front: group.front}}).
                    success(function (data) {
                        if (data.res) {
                            $scope.loadGroups();
                        }
                    });
            };


            $scope.onImgSelect = function (files) {
                img = false;
                $scope.erroresNameImage = new Array();
                $scope.erroresExistImage = new Array();
                $scope.validCantImg = 0;
                $scope.files = new Array();
                recursivo(files, 0, function () {
                    $scope.$apply();
                });
            };

            function recursivo(files, pos, cb) {
                $scope.cantImg++;
                var reader = new FileReader();
                reader.readAsDataURL(files[0]);
                reader.onload = function () {
                    files[pos].image = reader.result;
                    $scope.files[0] = files[0];
                    img = true;
                    pos++;
                    $scope.$apply();
                    cb(true);
                };


                //if (files.length == pos) {
                //    $scope.$apply();
                //    if (cb) cb(true)
                //} else {
                //    var typeArray = ["image/jpeg", "image/png"];
                //    if ($rootScope.validateFile(files[pos], typeArray)) {
                //        if ($scope.files.length > 0) {
                //            if ($scope.verificarExiste(files[pos])) {
                //                $scope.error = "errorExist";
                //                $scope.erroresExistImage.push(files[pos].name);
                //            } else {
                //                if ($scope.cantImg < $scope.cantImage) {
                //                    $scope.cantImg++;
                //                    var reader = new FileReader();
                //                    reader.readAsDataURL(files[pos]);
                //                    reader.onload = function () {
                //                        files[pos].image = reader.result;
                //                        $scope.files.push(files[pos]);
                //                        img = true;
                //                        pos++;
                //                        if (cb) cb(recursivo(files, pos, cb));
                //                    };
                //                } else {
                //                    $('#imgFile').val(null);
                //                    $scope.validCantImg = 1;
                //                }
                //            }
                //        } else {
                //            $scope.cantImg++;
                //            var reader = new FileReader();
                //            reader.readAsDataURL(files[pos]);
                //            reader.onload = function () {
                //                files[pos].image = reader.result;
                //                $scope.files.push(files[pos]);
                //                img = true;
                //                pos++;
                //                if (cb) cb(recursivo(files, pos, cb));
                //            };
                //        }
                //    } else {
                //        $scope.error = "errorType";
                //        $scope.erroresNameImage.push(files[pos].name);
                //        pos++;
                //        if (cb) cb(recursivo(files, pos, cb));
                //    }
                //}
            }

            $scope.verificarExiste = function ($file) {
                var flag = false;
                var cont = 0;
                while (!flag && $scope.files.length > cont) {
                    if ($file.name == $scope.files[cont].name && $file.size == $scope.files[cont].size) {
                        flag = true;
                    }
                    cont = cont + 1;
                }
                return flag;
            };

            $scope.deleteShipImg = function (file) {
                var pos = 0;
                //var cantidad = $scope.files.length;
                //var flag = false;
                //while (!flag && pos < cantidad) {
                //    if (file.name == $scope.files[pos].name) {
                //        $scope.files.splice(pos, 1);
                //        flag = true;
                //    }
                //    pos++;
                //}
                $scope.files = new Array();
                $scope.cantImg--;
                $scope.validCantImg = 0;
                if ($scope.files.length == 0) {
                    img = false;
                }
                if (file.path) {
                    $scope.nameImgDel.push(file.name);
                }
                $('#imgFile').val(null);
            };

            $scope.createGroup = function () {
                $scope.errorMsg = false;
                $scope.successMsg = false;

                $upload.upload({
                    url: '/api/groups', //upload.php script, node.js route, or servlet url
                    method: 'POST',
                    headers: {'header-key': 'multipart/form-data'},
                    data: {
                        es: $scope.name_es,
                        en: $scope.name_en,
                        descriptions_es: $scope.description_es,
                        descriptions_en: $scope.description_en,
                        payform: $scope.payform,
                        color: $scope.color,
                        front: $scope.groups.length + 1,
                        seo:$scope.seo
                    },
                    file: $scope.files
                }).success(function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Grupo creado satisfactoriamente', 0);
                        $rootScope.loadStatistics();
                        $scope.successMsg = true;
                        $scope.name_es = "";
                        $scope.name_en = "";
                        $scope.hideForm();
                        $scope.loadGroups();
                    }
                    else {
                        $scope.errorMsg = true;
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido crear el grupo', 2);
                    }
                    $scope.initializeTours();
                });
            };

            $scope.saveGroup = function () {
                $('#myModal').modal('hide');

                Groups.save({
                    id: $scope.id,
                    es: $scope.name_es,
                    en: $scope.name_en,
                    descriptions_es: $scope.description_es,
                    descriptions_en: $scope.description_en,
                    color: $scope.color,
                    payform: $scope.payform,
                    seo:$scope.seo
                }, function (data) {
                    if (data.res) {

                        var files = preparedFiles($scope.files);
                        if ($scope.nameImgDel.length != 0 || files.length != 0) {
                            $upload.upload({
                                url: "/api/groups/updateImg", //upload.php script, node.js route, or servlet url
                                method: 'POST',
                                headers: {'header-key': 'multipart/form-data'},
                                data: {id: $scope.id, nameImgDel: $scope.nameImgDel},
                                file: files
                            }).success(function (data) {
                                if (data.result) {
                                    $rootScope.Notifications('Los cambios se han guardado satisfactoriamente', 0);
                                    $scope.successMsg = true;
                                    $scope.name_es = "";
                                    $scope.name_en = "";
                                    $scope.hideForm();
                                    $scope.loadGroups();
                                } else {
                                    $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                                }
                            });
                        }
                        else {
                            $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                            $scope.loadGroups();
                            $scope.hideForm();
                        }

                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                        $scope.errorMsg = true;
                    }
                    $scope.initializeTours();
                    $scope.editG = false;
                })
            };
            function preparedFiles(files) {
                var arrayTemp = new Array();
                for (var i = 0; i < files.length; i++) {
                    if (files[i].image) {
                        arrayTemp.push(files[i]);
                    }
                }
                return arrayTemp;
            }


        }]);