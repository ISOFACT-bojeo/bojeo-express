/**
 * Created by ernestomr87@gmail.com on 1/08/14.
 */

angular.module('bojeoApp.graphicsController', [])
    .controller('graphicsController', ['$scope', '$rootScope', 'Statistics',
        function ($scope, $rootScope, Statistics) {
            $rootScope.PageSelected = 'graphics';
            $rootScope.loadStatistics();

            $scope.year = new Date().getFullYear();
            $scope.changeYear = function (year) {
                $scope.year = $scope.year + year;
                $scope.loadReservations();
            };
            $scope.loadReservations = function () {
                Statistics.list.query({year: $scope.year}, function (data) {
                    $scope.allReservation = data.res,
                        $scope.reservations = new Array(),
                        $scope.refunds = new Array(),
                        $scope.resSeries = new Array(),
                        $scope.refSeries = new Array();
                    for (var i = 0; i < $scope.allReservation.length; i++) {
                        if ($scope.allReservation[i].client.cancel) {
                            $scope.reservations.push($scope.allReservation[i]);
                        }
                        else {
                            $scope.refunds.push($scope.allReservation[i]);
                        }
                    }
                    ;
                    $(function () {
                        $('#container').highcharts({
                            chart: {
                                type: 'line'
                            },
                            title: {
                                text: 'Reservas y Devoluciones'
                            },
                            subtitle: {
                                text: 'Año ' + $scope.year
                            },
                            xAxis: {
                                categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
                            },
                            yAxis: {
                                title: {
                                    text: ''
                                }
                            },
                            tooltip: {
                                enabled: false
                            },
                            plotOptions: {
                                line: {
                                    dataLabels: {
                                        enabled: true,
                                        style: {
                                            textShadow: '0 0 3px white, 0 0 3px white'
                                        }
                                    },
                                    enableMouseTracking: true
                                }
                            },
                            series: [{
                                name: 'Reservaciones',
                                data: createSerie($scope.reservations)
                            }, {
                                name: 'Devoluciones',
                                data: createSerie($scope.refunds)
                            }]
                        });
                    });
                })
            };
            $scope.loadReservations();
            function createSerie(array) {
                var result = new Array();
                for (var i = 0; i < 12; i++) {

                    var cont = 0;
                    var start = new Date($scope.year, i, 1);
                    var end = new Date($scope.year, i + 1, 1);
                    for (var j = 0; j < array.length; j++) {
                        if (new Date(array[j].res.date) >= start && new Date(array[j].res.date) < end) {
                            cont++;
                        }
                    }
                    result.push(cont);
                    if (i == 10) {
                        var cont = 0;
                        var start = new Date($scope.year, i + 1, 1);
                        var end = new Date($scope.year + 1, 0, 1);

                        for (var j = 0; j < array.length; j++) {
                            if (new Date(array[j].res.date)>= start && new Date(array[j].res.date) < end) {
                                cont++;
                            }
                        }
                        result.push(cont);
                        return result;
                    }
                }
                return result;
            }
        }]);
