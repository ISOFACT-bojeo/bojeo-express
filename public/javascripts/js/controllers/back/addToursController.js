/**
 * Created by ernestomr87@gmail.com on 4/06/14.
 */

angular.module('bojeoApp.addToursController', [])
    .controller('addToursController', ['$scope', '$rootScope', '$routeParams', 'Groups', 'Ports', '$http', 'Tour', '$upload', 'Bond',
        function ($scope, $rootScope, $routeParams, Groups, Ports, $http, Tour, $upload, Bond) {
            $rootScope.PageSelected = 'tours';
            $rootScope.loadStatistics();
            $scope.cont = 0;
            $scope.imgfileUpload = null;
            $scope.files = [];
            $scope.loaders = true;
            $scope.showSelectPort = true;
            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;
            $scope.text = "";
            $scope.empty = false;
            $scope.departureValue = '00:00';
            $scope.addDep = false;
            $scope.openClose = true;
            $scope.reload = false;
            $scope.tour_id = "";
            $scope.editPort = false;
            $scope.percentArray = [];
            for (var i = 0; i <= 100; i++) {
                $scope.percentArray.push(i);
            }
            ;

            $scope.order = 'port.name.es';
            $scope.changeOrder = function (order) {
                $scope.order = order;
            };
            $scope.initializeVars = function () {
                $scope.tour = {
                    group: null,
                    port: null,
                    name: {
                        es: "",
                        en: ""
                    },
                    description: {
                        es: "",
                        en: ""
                    },
                    description: {
                        es: "",
                        en: ""
                    },
                    daysWeek: [1, 1, 1, 1, 1, 1, 1],
                    daysTxt: {
                        es: "",
                        en: ""
                    },
                    recommendations: {
                        es: "",
                        en: ""
                    },
                    priceTxt: {
                        es: "",
                        en: ""
                    },
                    departureTxt: {
                        es: "",
                        en: ""
                    },
                    forReserve: {
                        es: "",
                        en: ""
                    },
                    transport: {
                        es: "",
                        en: ""
                    },
                    conditions: {
                        es: "",
                        en: ""
                    },
                    language: {
                        es: "",
                        en: ""
                    },
                    extras: {
                        es: "",
                        en: ""
                    },
                    noIncludes: {
                        es: "",
                        en: ""
                    },
                    data: {
                        capacity: {
                            maxPerson: 1,
                            type: 'person'
                        },
                        price: {
                            value: [],
                            type: 'day',
                            toPay: 10
                        },
                        payForm: null,
                        departures: []
                    },
                    seo: {
                        title: {
                            es: '',
                            en: ''
                        },
                        description: {
                            es: '',
                            en: ''
                        }
                    }
                };
            };
            $scope.startProcess = function (cb) {
                $http({method: 'POST', url: '/api/country/back'}).
                    success(function (ddata) {
                        if (!ddata.res) {
                            window.location = "/";
                        }
                        else {
                            $scope.countries = ddata.res;
                            var auxCountries = [];
                            $http({method: 'POST', url: '/api/ports/available'}).
                                success(function (data) {
                                    if (data.res) {
                                        $scope.allExistsPorts = data.res;
                                        for (var i = 0; i < $scope.countries.length; i++) {
                                            var exist_country = false;
                                            for (var j = 0; j < $scope.allExistsPorts.length; j++) {
                                                if ($scope.countries[i].name.es == $scope.allExistsPorts[j].country.es) {

                                                    for (var k = 0; k < $scope.countries[i].cities.length; k++) {
                                                        if ($scope.countries[i].cities[k].es == $scope.allExistsPorts[j].city.es) {
                                                            exist_country = true;
                                                            break;
                                                        }
                                                    }
                                                    if (exist_country)break;
                                                }
                                            }
                                            if (exist_country) {
                                                auxCountries.push($scope.countries[i]);
                                            }
                                        }
                                        $scope.countries = auxCountries;
                                        $scope.selectedCountry = $scope.countries[0];
                                        $scope.cities = $scope.selectedCountry.cities;
                                        $scope.country = $scope.selectedCountry.name.es;
                                        var auxCities = [];
                                        for (var i = 0; i < $scope.cities.length; i++) {
                                            var exist_city = false;
                                            for (var j = 0; j < $scope.allExistsPorts.length; j++) {
                                                if ($scope.cities[i].es == $scope.allExistsPorts[j].city.es) {
                                                    exist_city = true;
                                                    break;
                                                }
                                            }
                                            if (exist_city) {
                                                auxCities.push($scope.cities[i]);
                                            }
                                        }
                                        $scope.cities = auxCities;
                                        $scope.selectedCity = $scope.cities[0];
                                        $scope.city = $scope.selectedCity.es;
                                        $scope.filterByCity();
                                        if (cb) cb(true);
                                    }
                                    else {
                                        alert("Error");
                                    }

                                });
                        }
                    });
            }
            $scope.startProcess();


            $scope.filterByCity = function () {
                $http({
                    method: 'POST', url: '/api/ports/city', data: {
                        country: $scope.selectedCountry.name.en,
                        city: $scope.selectedCity.en
                    }
                }).
                    success(function (data) {
                        if (!data.res) {
                            $scope.allPorts = null;

                        }
                        else {
                            $scope.allPorts = data.res;
                            $scope.selectedPort = $scope.allPorts[0];
                            $scope.namePort = $scope.selectedPort.name.es;
                            $scope.tour.port = $scope.allPorts[0]._id;
                        }


                    });
            }
            $scope.changeCountry = function () {
                for (var i = 0; i < $scope.countries.length; i++) {
                    if ($scope.countries[i].name.es == $scope.country || $scope.countries[i].name.en == $scope.country) {
                        $scope.selectedCountry = $scope.countries[i];
                        $scope.cities = $scope.selectedCountry.cities;
                        $scope.country = $scope.selectedCountry.name.es;
                        var auxCities = [];
                        for (var i = 0; i < $scope.cities.length; i++) {
                            var exist_city = false;
                            for (var j = 0; j < $scope.allExistsPorts.length; j++) {
                                if ($scope.cities[i].es == $scope.allExistsPorts[j].city.es) {
                                    exist_city = true;
                                    break;
                                }
                            }
                            if (exist_city) {
                                auxCities.push($scope.cities[i]);
                            }
                        }
                        $scope.cities = auxCities;
                        for (var i = 0; i < $scope.cities.length; i++) {
                            var exist_city = false;
                            for (var j = 0; j < $scope.allExistsPorts.length; j++) {
                                if ($scope.cities[i].es == $scope.allExistsPorts[j].city.es) {
                                    exist_city = true;
                                    break;
                                }
                            }
                            if (!exist_city) {
                                $scope.cities.splice(i, 1);
                            }
                        }
                        $scope.selectedCity = $scope.cities[0];
                        $scope.city = $scope.selectedCity.es;
                        $scope.filterByCity();
                        break;
                    }
                }

            };
            $scope.changeCity = function () {
                for (var i = 0; i < $scope.selectedCountry.cities.length; i++) {
                    if ($scope.selectedCountry.cities[i].es == $scope.city) {
                        $scope.selectedCity = $scope.selectedCountry.cities[i];
                        $scope.city = $scope.selectedCity.es;
                        $scope.filterByCity();
                        break;
                    }
                }
            };
            $scope.changePort = function () {
                for (var i = 0; i < $scope.allPorts.length; i++) {
                    if ($scope.allPorts[i].name.es == $scope.namePort) {
                        $scope.selectedPort = $scope.allPorts[i];
                        $scope.namePort = $scope.selectedPort.name.es;
                        $scope.tour.port = $scope.allPorts[i]._id;
                        break;
                    }
                }

            }
            $scope.start = function () {
                Groups.get(function (data) {
                    $scope.groups = data.res;
                    $scope.groupSelected = null;
                    $scope.maxPerson = 0;
                    $scope.daysWeek = [true, true, true, true, true, true, true];
                    $scope.initializeVars();
                    $scope.selectedGroup();
                })
            }
            $scope.start();


            $scope.tabsShowEs = {
                description: true
            };
            $scope.tabsShowDaysTxtEs = {
                description: true
            };
            $scope.tabsShowRecomendEs = {
                description: true
            };
            $scope.tabsShowForReserveEs = {
                description: true
            };
            $scope.tabsShowTransporteEs = {
                description: true
            };
            $scope.tabsShowCondicionesEs = {
                description: true
            };
            $scope.tabsShowLanguageEs = {
                description: true
            };
            $scope.tabsShowExtrasEs = {
                description: true
            };
            $scope.tabsShowNoIncludesEs = {
                description: true
            };
            $scope.tabsShowDepartureTxtEs = {
                description: true
            };
            $scope.tabsShowPriceTxtEs = {
                description: true
            };

            $scope.changeTypeDuration = function () {
                $scope.hourArray = [];
                for (var i = 0; i < 24; i++) {
                    var aux = {
                        hour: i,
                        occupied: false
                    }
                    $scope.hourArray.push(aux);
                }
                $scope.selectedhour = $scope.hourArray[0].hour;

                if ($scope.tour.data.price.type == 'hours') {
                    $scope.pairDurationPrice = {
                        duration: '01:00',
                        //duration: 3600000,
                        price: 0
                    };
                }
                else {
                    $scope.pairDurationPrice = {
                        duration: 1,
                        //duration: 86400000,
                        price: 0
                    };
                }
            }
            $scope.addPriceValue = function () {
                var price = parseFloat($scope.pairDurationPrice.price);
                var aux = $scope.convertTimeStampNumber($scope.pairDurationPrice.duration);

                var flag = true;
                for (var i = 0; i < $scope.tour.data.price.value.length; i++) {
                    if ($scope.tour.data.price.value[i].price == $scope.pairDurationPrice.price &&
                        $scope.tour.data.price.value[i].duration == $scope.pairDurationPrice.duration) {
                        flag = false;
                        break;
                    }
                }

                if (flag) {
                    if (aux.value && !isNaN(price)) {
                        $scope.pairDurationPrice.duration = aux.format;
                        $scope.pairDurationPrice.price = price;
                        $scope.tour.data.price.value.push({
                            price: $scope.pairDurationPrice.price,
                            duration: $scope.pairDurationPrice.duration
                        });
                    }
                    else {
                        $rootScope.Notifications('Formato incorrecto', 2);
                    }
                }
                else {
                    $rootScope.Notifications('Lo sentimos, ya existe el elemento', 2);
                }

            };
            $scope.removePriceValue = function (index) {
                var array = [];
                for (var i = 0; i < $scope.tour.data.price.value.length; i++) {
                    if (i != index) {
                        array.push($scope.tour.data.price.value[i]);
                    }
                }
                $scope.tour.data.price.value = array;
            };
            $scope.convertTimeStampNumber = function (time) {
                if ($scope.addDep) {
                    $scope.addDep = false;
                    var array = time.split(':');
                    if (array.length == 2) {
                        var hour = parseInt(array[0]);
                        var min = parseInt(array[1]);

                        if (isNaN(hour) || isNaN(min)) {
                            var aux = {
                                value: false,
                                format: null
                            }
                            return aux;
                        }
                        else {
                            if (hour >= 0 && hour < 24 && min >= 0 && min < 60) {
                                hour = hour < 10 ? '0' + hour : hour;
                                min = min < 10 ? '0' + min : min;
                                var aux = {
                                    value: (hour * 60 + min) * 60000,
                                    format: hour + ':' + min
                                }
                                return aux;
                            }
                            else {
                                var aux = {
                                    value: false,
                                    format: null
                                }
                                return aux;
                            }

                        }
                    }
                    else {
                        var aux = {
                            value: false,
                            format: null
                        }
                        return aux;
                    }


                }
                else {
                    if ($scope.tour.data.price.type == 'day') {
                        var day = parseInt(time);
                        if (isNaN(day)) {
                            var aux = {
                                value: false,
                                format: null
                            }
                            return aux;
                        }
                        else {
                            var aux = {
                                value: day * 86400000,
                                format: day
                            }
                            return aux;
                        }
                    }
                    else if ($scope.tour.data.price.type == 'hours') {
                        var array = time.split(':');
                        if (array.length == 2) {
                            var hour = parseInt(array[0]);
                            var min = parseInt(array[1]);

                            if (isNaN(hour) || isNaN(min)) {
                                var aux = {
                                    value: false,
                                    format: null
                                }
                                return aux;
                            }
                            else {
                                hour = hour < 10 ? '0' + hour : hour;
                                min = min < 10 ? '0' + min : min;
                                var aux = {
                                    value: (hour * 60 + min) * 60000,
                                    format: hour + ':' + min
                                }
                                return aux;
                            }
                        }
                        else {
                            var aux = {
                                value: false,
                                format: null
                            }
                            return aux;
                        }
                    }
                }


            };
            $scope.showForm = function () {
                $scope.initializeVars();
                $scope.startProcess(function(data){
                    $scope.openClose = false;
                    $('#imagesObject').hide();
                    $('#form-bond').show();
                })
            };

            $scope.validateForm = function () {
                if (!validateNonEmpty($scope.tour.name.es)) {
                    return false;
                }
                if (!validateNonEmpty($scope.tour.name.en)) {
                    return false;
                }
                /***********************************************/
                if (isNaN($scope.tour.maxPerson) || $scope.tour.maxPerson <= 0) {
                    return false;
                }
                /***********************************************/
                if (!validateNonEmpty($scope.tour.description.es)) {
                    return false;
                }
                if (!validateNonEmpty($scope.tour.description.en)) {
                    return false;
                }
                /***********************************************/
                if (!validateNonEmpty($scope.tour.daysTxt.es)) {
                    return false;
                }
                if (!validateNonEmpty($scope.tour.daysTxt.en)) {
                    return false;
                }
                /***********************************************/
                if (!validateNonEmpty($scope.tour.recommendations.es)) {
                    return false;
                }
                if (!validateNonEmpty($scope.tour.recommendations.en)) {
                    return false;
                }
                /***********************************************/
                if (!validateNonEmpty($scope.tour.forReserve.es)) {
                    return false;
                }
                if (!validateNonEmpty($scope.tour.forReserve.en)) {
                    return false;
                }
                /***********************************************/
                if (!validateNonEmpty($scope.tour.transport.es)) {
                    return false;
                }
                if (!validateNonEmpty($scope.tour.transport.en)) {
                    return false;
                }
                /***********************************************/
                if (!validateNonEmpty($scope.tour.conditions.es)) {
                    return false;
                }
                if (!validateNonEmpty($scope.tour.conditions.en)) {
                    return false;
                }
                /***********************************************/
                if (!validateNonEmpty($scope.tour.language.es)) {
                    return false;
                }
                if (!validateNonEmpty($scope.tour.language.en)) {
                    return false;
                }
                /***********************************************/
                if (!validateNonEmpty($scope.tour.extras.es)) {
                    return false;
                }
                if (!validateNonEmpty($scope.tour.extras.en)) {
                    return false;
                }
                /***********************************************/
                if (!validateNonEmpty($scope.tour.noincludes.es)) {
                    return false;
                }
                if (!validateNonEmpty($scope.tour.noincludes.en)) {
                    return false;
                }
                /***********************************************/
                if ($scope.groupSelected == null) {
                    return false;
                }
                /***********************************************/
                /*if ($scope.price.payForm == 'bonds') {
                 if (isNaN($scope.price.maxHour) || $scope.price.maxHour <= 0) {
                 return false;
                 }
                 if (isNaN($scope.price.minHour) || $scope.price.minHour <= 0) {
                 return false;
                 }
                 if ($scope.price.maxHour < $scope.price.minHour) {
                 return false;
                 }
                 }
                 else {
                 if (!validateNonEmpty($scope.price.priceTxt.es)) {
                 return false;
                 }
                 if (!validateNonEmpty($scope.price.priceTxt.en)) {
                 return false;
                 }
                 if ($scope.typeDuration == 'agree') {
                 if (isNaN($scope.price.price) || $scope.price.price < 0) {
                 return false;
                 }
                 }
                 if ($scope.typeDuration == 'hours') {
                 if (isNaN($scope.hours.duration) || $scope.hours.duration < 1) {
                 return false;
                 }
                 if (!validateArray($scope.hours.departures)) {
                 return false;
                 }
                 if (isNaN($scope.price.price) || $scope.price.price < 0) {
                 return false;
                 }
                 if (!validateNonEmpty($scope.hours.departureTxt.es)) {
                 return false;
                 }
                 if (!validateNonEmpty($scope.hours.departureTxt.en)) {
                 return false;
                 }

                 }
                 if ($scope.typeDuration == 'day') {
                 if (!validateArray($scope.hours.departures)) {
                 return false;
                 }
                 if (!validateNonEmpty($scope.hours.departureTxt.es)) {
                 return false;
                 }
                 if (!validateNonEmpty($scope.hours.departureTxt.en)) {
                 return false;
                 }
                 if (!validateArray($scope.price.price)) {
                 return false;
                 }


                 }

                 }*/

                return true;


            }
            $scope.hideForm = function () {
                $scope.initializeVars();
                $('#form-bond').hide();
                $('#imagesObject').hide();
                $scope.openClose = true;
                $scope.start();

                $scope.nameGroup = false;
                $scope.editG = false;
                $scope.editPort = false;
                $scope.showSelectPort = true;

            };
            $scope.showImg = function (tour) {
                $('#imagesObject').hide();
                $scope.objImg = tour;
                $scope.images = tour.photos;
                $scope.objActive = tour.available;
                $('#form-bond').hide();
                $('#imagesObject').show();
                $scope.openClose = false;
            };
            $scope.showDeleteButton = function () {
                var aux = ( $scope.objActive && $scope.images.length > 4) || !$scope.objActive;
                return aux;
            }
            $scope.showDeleteModal = function (tour) {
                if (!tour.available && !tour.hasShip) {
                    $scope.deleteItem = tour;
                    $('#myModal').modal('show');
                }
            };
            $scope.addDepartures = function () {
                $scope.addDep = true;
                if ($scope.departureValue.length) {
                    var aux = $scope.convertTimeStampNumber($scope.departureValue);
                    var flag = true;
                    for (var i = 0; i < $scope.tour.data.departures.length; i++) {
                        if ($scope.tour.data.departures[i] == $scope.departureValue) {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {
                        if (aux.value >= 0) {
                            $scope.departureValue = aux.format;
                            $scope.tour.data.departures.push($scope.departureValue);
                        }
                        else {
                            $rootScope.Notifications('Formato incorrecto', 2);
                        }
                    }
                    else {
                        $rootScope.Notifications('Lo sentimos, ya existe el elemento', 2);
                    }
                }

            };
            $scope.delDepartures = function (index) {
                var array = [];
                for (var i = 0; i < $scope.tour.data.departures.length; i++) {
                    if (i != index) {
                        array.push($scope.tour.data.departures[i]);
                    }
                }
                $scope.tour.data.departures = array;
            };
            $scope.selectedGroup = function () {
                $scope.noBonds = true;
                if ($scope.tour.group != null) {
                    for (var i = 0; i < $scope.groups.length; i++) {
                        if ($scope.groups[i]._id == $scope.tour.group._id) {
                            $scope.tour.data.payForm = $scope.groups[i].payform;
                            break;
                        }
                    }

                    if ($scope.tour.data.payForm == 'currencies') {
                        $scope.tour.data.price = {
                            value: [],
                            type: 'day',
                            toPay: 0
                        };
                    }
                    else {
                        $scope.noBonds = true;
                        Bond.get(function (data) {
                            if (data.res) {
                                $scope.bonds = data.res;
                                for (var i = $scope.bonds.length - 1; i >= 0; i--) {
                                    var ocupied = $scope.bonds[i].occupied.tour || $scope.bonds[i].occupied.tour;
                                    if (ocupied || !$scope.bonds[i].available) {
                                        $scope.bonds.splice(i, 1);
                                    }
                                }
                                if ($scope.bonds.length > 0) {

                                    $scope.noBonds = false;
                                    $scope.tour.data.price = {
                                        value: [],
                                        type: 'day',
                                        toPay: 0
                                    };
                                    $scope.tour.data.payForm = 'bonds';
                                    $scope.tour.data.bond = {
                                        bond: $scope.bonds[0]._id,
                                        minHour: 1,
                                        maxHour: 2
                                    }
                                }
                                else {
                                    $scope.noBonds = false;
                                    $scope.tour.data.price = {
                                        value: [],
                                        type: 'day',
                                        toPay: 0
                                    };
                                    $scope.tour.data.payForm = 'bonds';
                                    $scope.tour.data.bond = {
                                        bond: null,
                                        minHour: 1,
                                        maxHour: 2
                                    }
                                }
                            }
                            else {
                                $scope.noBonds = false;
                                $scope.tour.data.price = {
                                    value: [],
                                    type: 'day',
                                    toPay: 0
                                };
                                $scope.tour.data.payForm = 'bonds';
                                $scope.tour.data.bond = {
                                    bond: null,
                                    minHour: 1,
                                    maxHour: 2
                                }
                            }
                        });
                    }
                    $scope.changeTypeDuration();
                }
                else {
                    $scope.tour.data.price = {
                        value: [],
                        type: 'day',
                        toPay: 0
                    };
                }

            };

            if ($routeParams.id) {
                $scope.tour_id = $routeParams.id;
                $scope.reload = true;
            }
            $scope.loadTours = function () {

                Tour.list.query({
                    limit: $scope.maxSize,
                    skip: $scope.skip,
                    text: $scope.text,
                    id: $scope.tour_id
                }, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.cont;
                        $scope.tours = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.tours.length > 0) {
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadTours();
                            }
                        }
                    } else {
                        $scope.news = new Array();
                        $scope.empty = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener los tours.', 2);
                    }
                });
            }
            $scope.loadTours();
            $scope.changeStatus = function (tour) {
                $scope.loaders = true;
                $http({
                    method: 'POST', url: '/api/tours/status',
                    data: {
                        id: tour._id,
                        available: tour.available
                    }
                }).
                    success(function (data) {
                        if (data.res) {
                            $scope.tour_id = "";
                            $scope.loadTours();
                        }
                    });
            };
            $scope.delete = function () {
                $scope.loaders = true;
                Tour.delete({id: $scope.deleteItem._id}, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Tour eliminado satisfactoriamenete', 0);
                        $scope.loadTours();
                        $rootScope.loadStatistics();
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido eliminar el Tour', 2);
                    }
                })
            };
            $scope.edit = function (tour) {
                $scope.noBonds = true;
                if (tour.data.payForm == 'bonds') {
                    $scope.noBonds = true;
                    Bond.get(function (data) {
                        if (data.res) {
                            $scope.bonds = data.res;

                            for (var i = $scope.bonds.length - 1; i >= 0; i--) {
                                if ($scope.bonds[i].occupied && $scope.bonds[i].occupied.id != tour._id || !$scope.bonds[i].available) {
                                    $scope.bonds.splice(i, 1);
                                }
                            }
                            if ($scope.bonds.length == 0) {
                                $scope.noBonds = false;
                            }
                        }
                        else {
                            $scope.bonds = new Array();
                            $scope.noBonds = false;
                        }

                    })
                }

                $scope.tour = angular.copy(tour);
                $scope.tour.group = $scope.tour.group._id;
                $scope.tour.port = $scope.tour.port._id;
                $scope.groupSelected = tour.group._id;
                $scope.nameGroup = tour.group.name.es;
                $scope.namePort = tour.port.name;
                $scope.old_port = tour.port._id;
                $scope.editG = true;
                $scope.editPort = true;
                $scope.showSelectPort = false;
                $scope.openClose = false;
                $('#imagesObject').hide();
                $('#form-bond').show();
                //$scope.showForm();
            };
            $scope.nameGroup = false;
            $scope.changeShowSelectPort = function () {
                $scope.showSelectPort = true;
            };
            $scope.create = function () {
                $scope.loaders = true;
                Tour.create({
                    group: $scope.tour.group._id,
                    name: $scope.tour.name,
                    description: $scope.tour.description,
                    port: $scope.tour.port,
                    daysWeek: $scope.tour.daysWeek,
                    daysTxt: $scope.tour.daysTxt,
                    recommendations: $scope.tour.recommendations,
                    forReserve: $scope.tour.forReserve,
                    transport: $scope.tour.transport,
                    conditions: $scope.tour.conditions,
                    language: $scope.tour.language,
                    extras: $scope.tour.extras,
                    noIncludes: $scope.tour.noIncludes,
                    priceTxt: $scope.tour.priceTxt,
                    departureTxt: $scope.tour.departureTxt,
                    data: $scope.tour.data,
                    seo: $scope.tour.seo
                }, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Tour creado satisfactoriamente', 0);
                        $rootScope.loadStatistics();
                        $scope.loadTours();
                        $scope.hideForm();
                        $scope.start();
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido crear el Tour', 2);
                    }
                })
            };
            $scope.save = function () {
                $scope.loaders = true;
                Tour.save({
                    id: $scope.tour._id,
                    group: $scope.tour.group,
                    name: $scope.tour.name,
                    description: $scope.tour.description,
                    port: $scope.tour.port,
                    daysWeek: $scope.tour.daysWeek,
                    daysTxt: $scope.tour.daysTxt,
                    recommendations: $scope.tour.recommendations,
                    forReserve: $scope.tour.forReserve,
                    transport: $scope.tour.transport,
                    conditions: $scope.tour.conditions,
                    language: $scope.tour.language,
                    extras: $scope.tour.extras,
                    noIncludes: $scope.tour.noIncludes,
                    priceTxt: $scope.tour.priceTxt,
                    departureTxt: $scope.tour.departureTxt,
                    data: $scope.tour.data,
                    seo: $scope.tour.seo
                }, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                        $scope.loadTours();
                        $scope.hideForm();
                        $scope.start();
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                    }
                })
            };
            $scope.onImgSelect = function ($files) {
                var img = false;
                var extArray = new Array(".jpg", ".png", ".gif", ".bmp");
                var flag = $scope.validateFile($files, extArray, 'imgFile');
                if (flag) {
                    $scope.files[0] = $files[0];
                    if ($scope.files[0] != null)
                        img = true;
                }

            };
            $scope.addMedia = function () {
                $upload.upload({
                    url: "/api/tours/addMedia", //upload.php script, node.js route, or servlet url
                    method: 'POST',
                    headers: {'header-key': 'multipart/form-data'},
                    data: {id: $scope.objImg._id},
                    file: $scope.files
                }).success(function (data) {
                    if (data.res) {
                        $scope.images.push(data.res);
                        $("#imgFile").val(null);
                    }
                });
            };
            $scope.delMedia = function (photo) {
                $http({method: 'POST', url: '/api/tours/delMedia', data: {id: $scope.objImg._id, photo: photo}}).
                    success(function (data) {
                        if (data.res) {
                            for (var i = 0; i < $scope.images.length; i++) {
                                if (photo.name == $scope.images[i].name) {
                                    $scope.images.splice(i, 1);
                                }
                            }
                        }
                    });
            };
            $scope.validateFile = function ($files, extArray) {
                var flag = false;
                for (var i = 0; i < $files.length; i++) {
                    while ($files[i].name.indexOf("\\") != -1)
                        $files[i].name = $files[i].name.slice($files[i].name.indexOf("\\") + 1);
                    var ext = $files[i].name.slice($files[i].name.indexOf(".")).toLowerCase();
                    for (var j = 0; j < extArray.length; j++) {
                        if (extArray[j] == ext) {
                            //$scope.files.push($files[i]);
                            flag = true;
                        }
                    }
                }
                if (!flag) {
                    alert("Archivo invalido...");
                    $('#imgFile').val(null);
                }
                return flag;
            };
            function validateNonEmpty(field) {
                if (field.length == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }

            function choiceGroup(id) {
                for (var i = 0; i < $scope.groups.length; i++) {
                    if ($scope.groups[i]._id == id) {
                        return $scope.groups[i];
                    }
                }

            }

            function validateArray(array) {
                if (array.length > 0) {
                    for (var i = 0; i < array.length; i++) {
                        if (isNaN(array[i])) {
                            return false;
                        }
                        if (array[i] == null) {
                            return false;
                        }
                    }
                    return true;
                }
                else {
                    return false;
                }
            }

            function inicializeDepartures(departures) {
                $scope.hourArray = [];
                var pos = null;
                for (var i = 0; i < 24; i++) {
                    var occupied = false;
                    for (var j = 0; j < departures.length; j++) {
                        if (i == departures[j]) {
                            var occupied = true;
                            break;
                        }
                    }
                    var aux = {
                        hour: i,
                        occupied: occupied
                    }
                    if ((pos == null) && !occupied) {
                        pos = i;
                    }
                    $scope.hourArray.push(aux);
                }
                $scope.selectedhour = $scope.hourArray[pos].hour;
            }

        }
    ])
;

