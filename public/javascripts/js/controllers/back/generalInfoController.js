/**
 * Created by ernestomr87@gmail.com on 26/07/14.
 */

angular.module('bojeoApp.generalInfoController', [])
    .controller('generalInfoController', ['$scope', '$rootScope', '$http', '$upload', 'GeneralInfo',
        function ($scope, $rootScope, $http, $upload, GeneralInfo) {

            $rootScope.PageSelected = 'generalInfo';
            $rootScope.loadStatistics();

            $scope.createGeneralInfo = function(){
                $scope.dismissErrorDiv();
                var bool = true;

                if($scope.aboutBojeo_es == null || $scope.aboutBojeo_es == ""){
                    bool = false
                    $scope.errorAboutBojeo_es = "Campo vacío";
                }
                if($scope.aboutBojeo_en == null || $scope.aboutBojeo_en == ""){
                    bool = false
                    $scope.errorAboutBojeo_en = "Campo vacío";
                }
                if($scope.payForms_es == null || $scope.payForms_es == ""){
                    bool = false
                    $scope.errorPayForms_es = "Campo vacío";
                }
                if($scope.payForms_en == null || $scope.payForms_en == ""){
                    bool = false
                    $scope.errorPayForms_en = "Campo vacío";
                }
                if($scope.devolutions_es == null || $scope.devolutions_es == ""){
                    bool = false
                    $scope.errorDevolutions_es = "Campo vacío";
                }
                if($scope.devolutions_en == null || $scope.devolutions_en == ""){
                    bool = false
                    $scope.errorDevolutions_en = "Campo vacío";
                }
                if($scope.useTerms_es == null || $scope.useTerms_es == ""){
                    bool = false
                    $scope.errorUseTerms_es = "Campo vacío";
                }
                if($scope.useTerms_en == null || $scope.useTerms_en == ""){
                    bool = false
                    $scope.errorUseTerms_en = "Campo vacío";
                }
                if (bool) {
                    var info = new GeneralInfo({aboutBojeo_es: $scope.aboutBojeo_es, aboutBojeo_en: $scope.aboutBojeo_en,
                        payForms_es: $scope.payForms_es, payForms_en: $scope.payForms_en, devolutions_es: $scope.devolutions_es,
                        devolutions_en: $scope.devolutions_en, useTerms_es: $scope.useTerms_es, useTerms_en: $scope.useTerms_en
                    });
                    info.$create(function (data) {
                        if (data.result) {
                            $scope.clearField();
                            showActionStatus(true);
                            setTimeout(function () {
                                window.location = '/backoffice#/listGeneralInfo';
                            }, 2000);
                        }else {
                            showActionStatus(false)
                        }
                    })
                } else {
                    //showActionStatus(false);
                }
            }

            $scope.clearField = function(){
                $scope.aboutBojeo_es = null;
                $scope.aboutBojeo_en = null;
                $scope.payForms_es = null;
                $scope.payForms_en = null;
                $scope.devolutions_es = null;
                $scope.devolutions_en = null;
                $scope.useTerms_es = null;
                $scope.useTerms_en = null;
                $scope.dismissErrorDiv();
            }

            $scope.dismissErrorDiv = function(){
                $scope.errorAboutBojeo_es = "";
                $scope.errorAboutBojeo_en = "";
                $scope.errorPayForms_es = "";
                $scope.errorPayForms_en = "";
                $scope.errorDevolutions_es = "";
                $scope.errorDevolutions_en = "";
                $scope.errorUseTerms_es = "";
                $scope.errorUseTerms_en = "";
            }

            $scope.closeModal = function(){
                $scope.dismissErrorDiv();
                $scope.clearField();
                window.location = '/backoffice#/listGeneralInfo';
            }

            function showActionStatus(flag){
                if(flag == true){
                    var html = ' <div class="alertSuccess alert-success" id="success-login" > <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Información creada con éxito</strong> </div>';
                    $('#success-create-alert').html(html).fadeIn(1000);
                    setTimeout(function() {
                        $("#success-create-alert").fadeOut(1500);
                    },2000);
                }else{
                    var html = ' <div class="alertError alert-error" id="error-login" > <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Lo siento, ocurrió un error al guardar los datos</strong> </div>';
                    $('#success-create-alert').html(html).fadeIn(1000);
                    setTimeout(function() {
                        $("#success-create-alert").fadeOut(1500);
                    },2000);
                }
            }

            function showPassError(){
                var html = ' <div class="alertErrorPass alert-error" id="error-pass-confirm" ><strong>Contraseñas no coinciden</strong> </div>';
                $('#error-pass-confirm').html(html).fadeIn(1000);
                setTimeout(function() {
                    $("#error-pass-confirm").fadeOut(1500);
                },2000);
            }

        }]);
