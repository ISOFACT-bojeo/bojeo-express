/**
 * Created by ernestomr87@gmail.com on 26/07/14.
 */

angular.module('bojeoApp.listGeneralInfoController', [])
    .controller('listGeneralInfoController', ['$scope', '$rootScope', '$http', '$upload', 'GeneralInfo', 'GeneralInfos', '$window',
        function ($scope, $rootScope, $http, $upload, GeneralInfo, GeneralInfos, $window) {

            $rootScope.PageSelected = 'listGeneralInfo';

            $scope.delInfo = null;
            $scope.editInfo = null;
            $scope.vInfo = null;

            $scope.tabsShowEs = {
                description: true
            };

            $scope.tabsShowPayFormsEs = {
                description: true
            };

            $scope.tabsShowDevolutionsEs = {
                description: true
            };

            $scope.tabsShowUseTermsEs = {
                description: true
            };

            $scope.tabsShowPrivacyPolicyEs = {
                description: true
            };

            $scope.tabsShowPoliticCookiesEs = {
                description: true
            };

            $scope.tabsShowCopyRightsEs = {
                description: true
            };

            $scope.tabsShowNoticeCookiesEs = {
                description: true
            };

            var img = false;
            $scope.files = [];

            $scope.maxImag = 4;
            $scope.imgfileUpload = null;
            $scope.nameImgDel = [];

            $scope.cantImage = 4;
            $scope.dimensionImage = "300x400px";
            $scope.formatImage = "[ jpg, png ]";
            $scope.resolucionImage = "72dpi";

            $scope.erroresNameImage = [];
            $scope.erroresExistImage = [];
            $scope.validCantImg = 0;
            $scope.cantImg = 0;

            /**
             * @method Cargando los datos de la informacion general
             */
            $scope.loadInfo = function () {
                GeneralInfo.query(function (data) {
                    $scope.gInfo = data[0];
                    $scope.files = [];
                    for (var i = 0; i < data[0].photos.length; i++) {
                        $scope.files[i] = data[0].photos[i];
                    }
                    $scope.cantImg = $scope.files.length;
                    img = true;
                    $scope.nameImgDel = [];
                })
            };

            // ejecutando la carga inicial de los datos
            $scope.loadInfo();

            /**
             * @method Evento que se ejecuta cuando el archivo es seleccionado
             * @param $files
             */
            $scope.onImgSelect = function (files) {
                img = false;
                $scope.erroresNameImage = [];
                $scope.erroresExistImage = [];
                $scope.validCantImg = 0;
                recursivo(files, 0, function (data) {
                    $scope.$apply();
                });
            };

            function recursivo(files, pos, cb) {
                if (files.length == pos) {
                    $scope.$apply();
                    if (cb) cb(true)
                } else {
                    var typeArray = ["image/jpeg", "image/png"];
                    if ($rootScope.validateFile(files[pos], typeArray)) {
                        if ($scope.files.length > 0) {
                            if ($scope.verificarExiste(files[pos])) {
                                $scope.error = "errorExist";
                                $scope.erroresExistImage.push(files[pos].name);
                            } else {
                                if ($scope.cantImg < $scope.cantImage) {
                                    $scope.cantImg++;
                                    var reader = new FileReader();
                                    reader.readAsDataURL(files[pos]);
                                    reader.onload = function () {
                                        files[pos].image = reader.result;
                                        $scope.files.push(files[pos]);
                                        img = true;
                                        pos++;
                                        if (cb) cb(recursivo(files, pos, cb));
                                    };
                                } else {
                                    $('#imgFile').val(null);
                                    $scope.validCantImg = 1;
                                }
                            }
                        } else {
                            $scope.cantImg++;
                            var reader = new FileReader();
                            reader.readAsDataURL(files[pos]);
                            reader.onload = function () {
                                files[pos].image = reader.result;
                                $scope.files.push(files[pos]);
                                img = true;
                                pos++;
                                if (cb) cb(recursivo(files, pos, cb));
                            };
                        }
                    } else {
                        $scope.error = "errorType";
                        $scope.erroresNameImage.push(files[pos].name);
                        pos++;
                        if (cb) cb(recursivo(files, pos, cb));
                    }
                }
            }

            $scope.verificarExiste = function ($file) {
                var flag = false;
                var cont = 0;
                while (!flag && $scope.files.length > cont) {
                    if ($file.name == $scope.files[cont].name && $file.size == $scope.files[cont].size) {
                        flag = true;
                    }
                    cont = cont + 1;
                }
                return flag;
            };

            /**
             * @method Adicionando imagen en base de datos
             */
            $scope.addMedia = function () {
                $upload.upload({
                    url: "/api/generalInfo/addAboutBojeoImgs", //upload.php script, node.js route, or servlet url
                    method: 'POST',
                    headers: {'header-key': 'multipart/form-data'},
                    data: { id: $scope.gInfo._id},
                    file: $scope.files
                }).success(function (data, status, headers, config) {
                    if (data.result) {
                        for(var i = 0; i < data.result.length; i++){
                            $scope.images.push(data.result[i]);
                        }
                        $scope.cantidadImg = $scope.images.length;
                        $("#imgFile").val(null);
                        $scope.valor = false;
                        $scope.files = [];
                    }
                }).error(function (status) {

                });
            };

            /**
             * @method Eliminar la Imagen
             * @param name
             */
            $scope.deleteSobreBojeoImg = function (file) {
                var pos = 0;
                var cantidad = $scope.files.length;
                var flag = false;
                while (!flag && pos < cantidad) {
                    if (file.name == $scope.files[pos].name) {
                        $scope.files.splice(pos, 1);
                        flag = true;
                    }
                    pos++;
                }
                $scope.cantImg--;
                $scope.validCantImg = 0;
                if ($scope.files.length == 0) {
                    img = false;
                }
                if (file.path) {
                    $scope.nameImgDel.push(file.name);
                }
                $('#imgFile').val(null);
            };

            /**
             * Viesualizador de imagen
             * @param path
             */
            $scope.viewImage = function (path) {
                $window.location = path;
            };

            $scope.saveSobreBojeo = function(){
                GeneralInfos.save({
                    ids: $scope.gInfo._id,
                    aboutBojeo_es: $scope.gInfo.aboutBojeo.es,
                    aboutBojeo_en: $scope.gInfo.aboutBojeo.en
                }, function (data) {
                    if (data) {
                        var files = preparedFiles($scope.files);
                        if ($scope.nameImgDel.length != 0 || files.length != 0) {
                            $rootScope.loadingImg = true;
                            $upload.upload({
                                url: "/api/generalInfo/updateImg", //upload.php script, node.js route, or servlet url
                                method: 'POST',
                                headers: {'header-key': 'multipart/form-data'},
                                data: {id: $scope.gInfo._id, nameImgDel: $scope.nameImgDel},
                                file: files
                            }).success(function (data) {
                                if (data.result) {
                                    $rootScope.loadingImg = false;
                                    $rootScope.Notifications('Información sobre Bojeo guardada con éxito', 0);
                                    $scope.loadInfo();
                                } else {
                                    $rootScope.loadingImg = false;
                                    $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                                }
                            }).error(function (status) {
                                $rootScope.loadingImg = false;
                                $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                            });
                        } else {
                            $rootScope.Notifications('Información sobre Bojeo guardada con éxito', 0);
                            $scope.loadInfo();
                        }
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                    }
                })
            };

            $scope.saveFormaPago = function(){
                var generalinfo = new GeneralInfo({ids: $scope.gInfo._id, payForms_es: $scope.gInfo.payForms.es, payForms_en: $scope.gInfo.payForms.en});
                generalinfo.$update(function (data) {
                    if(data){
                        $scope.loadInfo();
                        $rootScope.Notifications('Información de formas de pago guardada con éxito', 0);
                    }else{
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                    }
                });
            };

            $scope.saveDevolucion = function(){
                var generalinfo = new GeneralInfo({ids: $scope.gInfo._id, devolutions_es: $scope.gInfo.devolutions.es, devolutions_en: $scope.gInfo.devolutions.en});
                generalinfo.$update(function (data) {
                    if(data){
                        $scope.loadInfo();
                        $rootScope.Notifications('Información de devoluciones guardada con éxito', 0);
                    }else{
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                    }
                });
            };

            $scope.saveTerminoUso = function(){
                var generalinfo = new GeneralInfo({ids: $scope.gInfo._id, useTerms_es: $scope.gInfo.useTerms.es, useTerms_en: $scope.gInfo.useTerms.en});
                generalinfo.$update(function (data) {
                    if(data){
                        $scope.loadInfo();
                        $rootScope.Notifications('Información de término de uso guardada con éxito', 0);
                    }else{
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                    }
                });
            };

            $scope.savePoliticaPrivacidad = function(){
                var generalinfo = new GeneralInfo({ids: $scope.gInfo._id, privacyPolicy_es: $scope.gInfo.privacyPolicy.es, privacyPolicy_en: $scope.gInfo.privacyPolicy.en});
                generalinfo.$update(function (data) {
                    if(data){
                        $scope.loadInfo();
                        $rootScope.Notifications('Información de política de privacidad guardada con éxito', 0);
                    }else{
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                    }
                });
            };

            $scope.savePoliticaCookies = function(){
                var generalinfo = new GeneralInfo({ids: $scope.gInfo._id, politicCookies_es: $scope.gInfo.politicCookies.es, politicCookies_en: $scope.gInfo.politicCookies.en});
                generalinfo.$update(function (data) {
                    if(data){
                        $scope.loadInfo();
                        $rootScope.Notifications('Información de política de cookies guardada con éxito', 0);
                    }else{
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                    }
                });
            };

            $scope.saveAvisoCookies = function(){
                var generalinfo = new GeneralInfo({ids: $scope.gInfo._id, noticeCookies_es: $scope.gInfo.noticeCookies.es, noticeCookies_en: $scope.gInfo.noticeCookies.en});
                generalinfo.$update(function (data) {
                    if(data){
                        $scope.loadInfo();
                        $rootScope.Notifications('Información de aviso de cookies guardada con éxito', 0);
                    }else{
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                    }
                });
            };

            $scope.saveCopyRights = function(){
                var generalinfo = new GeneralInfo({ids: $scope.gInfo._id, copyRights_es: $scope.gInfo.copyRights.es, copyRights_en: $scope.gInfo.copyRights.en});
                generalinfo.$update(function (data) {
                    if(data){
                        $scope.loadInfo();
                        $rootScope.Notifications('Información de Copy Rights guardada con éxito', 0);
                    }else{
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                    }
                });
            };

            function showActionStatus(flag, message) {
                if (flag == true) {
                    var html = ' <div class="alertSuccess alert-success" id="success-login" > <button type="button" class="close" data-dismiss="alert">&times;</button><strong>' + message + '</strong> </div>';
                    $('#success-edit-alert').html(html).fadeIn(1000);
                    setTimeout(function () {
                        $("#success-edit-alert").fadeOut(1500);
                    }, 2000);
                }
            }

            function showErrorStatus() {
                var html = ' <div class="alertError alert-error" id="error-login" > <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Lo siento, ocurrió un error al guardar los datos</strong> </div>';
                $('#success-edit-alert').html(html).fadeIn(1000);
                setTimeout(function () {
                    $("#success-edit-alert").fadeOut(1500);
                }, 2000);
            }

            $scope.sendInfo = function () {
                var bool = true;
                if ($scope.editInfo.aboutBojeo.es == null || $scope.editInfo.aboutBojeo.es == "") {
                    bool = false
                }
                if ($scope.editInfo.aboutBojeo.en == null || $scope.editInfo.aboutBojeo.en == "") {
                    bool = false
                }
                if ($scope.editInfo.payForms.es == null || $scope.editInfo.payForms.es == "") {
                    bool = false
                }
                if ($scope.editInfo.payForms.en == null || $scope.editInfo.payForms.en == "") {
                    bool = false
                }
                if ($scope.editInfo.devolutions.es == null || $scope.editInfo.devolutions.es == "") {
                    bool = false
                }
                if ($scope.editInfo.devolutions.en == null || $scope.editInfo.devolutions.en == "") {
                    bool = false
                }
                if ($scope.editInfo.useTerms.es == null || $scope.editInfo.useTerms.es == "") {
                    bool = false
                }
                if ($scope.editInfo.useTerms.en == null || $scope.editInfo.useTerms.en == "") {
                    bool = false
                }

                if (bool) {
                    $scope.sendGeneralInfoMod();
                } else {
                    showActionStatus(false);
                }
            };

            $scope.sendGeneralInfoMod = function () {
                var generalinfo = new GeneralInfo({ids: $scope.editInfo._id, aboutBojeo_es: $scope.editInfo.aboutBojeo.es, aboutBojeo_en: $scope.editInfo.aboutBojeo.en,
                    payForms_es: $scope.editInfo.payForms.es, payForms_en: $scope.editInfo.payForms.en, devolutions_es: $scope.editInfo.devolutions.es,
                    devolutions_en: $scope.editInfo.devolutions.en, useTerms_es: $scope.editInfo.useTerms.es, useTerms_en: $scope.editInfo.useTerms.en
                });
                generalinfo.$update(function (data) {
                    $('#editModal').modal('hide')
                    if(data.result){
                        $scope.loadInfo();
                        showActionStatus(true, 'Información modificada con éxito');
                    }else{
                        alert("Aqui...");
                        showErrorStatus();
                    }
                });
            };

            $scope.smallDescription = function (description) {
                var small = "";
                if (description.length > 100) {
                    for (var i = 0; i < 100; i++) {
                        small = small + description[i];
                    }
                    return small;
                }
                return description;
            };

            $scope.changeStatus = function (id, status) {
                var user_active = true;
                if (status) {
                    user_active = false;
                }
                $http({method: 'PUT', url: '/api/generalInfo/public', data: {id: id, active: user_active}}).
                    success(function (status, headers, config) {
                        $scope.loadInfo();
                    }).
                    error(function (data, status, headers, config) {
                    });
            };

            $scope.editInfo = function (data) {
                $scope.editInfo = angular.copy(data);
            };

            $scope.viewInfo = function (data) {
                $scope.vInfo = angular.copy(data);
            };

            $scope.deleteInfo = function (data) {
                $scope.delInfo = data;
            };

            $scope.dropInfo = function () {
                var user = new GeneralInfo({id: $scope.delInfo._id});
                user.$destroy(function (data){
                   if(data.result){
                       $scope.loadInfo();
                       showActionStatus(true, "Información Eliminada con éxito");
                   } else{
                       showErrorStatus();
                   }
                });
            };

            function preparedFiles(files) {
                var arrayTemp = [];
                for (var i = 0; i < files.length; i++) {
                    if (files[i].image) {
                        arrayTemp.push(files[i]);
                    }
                }
                return arrayTemp;
            }
        }]);
