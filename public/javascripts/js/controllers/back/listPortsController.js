/*
 angular.module('bojeoApp.listPortsController', [])
 .controller('listPortsController', ['$scope', '$rootScope', 'Statistics', '$filter', 'Ports', '$upload', '$http', '$window',
 function ($scope, $rootScope, Statistics, $filter, Ports, $upload, $http, $window) {*/
/**
 * Created by ernestomr87@gmail.com on 6/06/14.
 */

angular.module('bojeoApp.listPortsController', [])
    .controller('listPortsController', ['$scope', '$rootScope', '$routeParams', 'Ports', '$upload', '$http',
        function ($scope, $rootScope, $routeParams, Ports, $upload, $http) {
            $rootScope.PageSelected = 'ports';
            $rootScope.loadStatistics();

            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;
            $scope.totalItems = 0;
            $scope.reload = false;
            $scope.text = "";
            if ($routeParams.text) {
                $scope.text = $routeParams.text;
                $scope.reload = true;
            }

            $scope.semaphore = true;
            $scope.tabsShowEs = {
                address: true
            };
            $scope.maxImag = 6;

            $scope.imgfileUpload = null;
            $scope.files = [];
            var img = false;
            $scope.cantImage = 6;
            $scope.dimensionImage = "1449x600px"
            $scope.formatImage = "[ jpg, png ]";
            $scope.resolucionImage = "72dpi";

            $scope.erroresNameImage = [];
            $scope.erroresExistImage = [];
            $scope.validCantImg = 0;
            $scope.cantImg = 0;

            $scope.initialize = function () {
                $http({method: 'POST', url: '/api/country/back'}).
                    success(function (data) {
                        if (!data.res) {
                            window.location = "/";
                        }
                        else {
                            $scope.countries = data.res;
                            $scope.selectedCountry = $scope.countries[0];
                            $scope.country = $scope.selectedCountry.name.es;
                            $scope.selectedCity = $scope.selectedCountry.cities[0];
                            $scope.city = $scope.selectedCity.es;
                        }
                    });
                $scope.changeCountry = function () {
                    for (var i = 0; i < $scope.countries.length; i++) {
                        if ($scope.countries[i].name.es == $scope.country) {
                            $scope.selectedCountry = $scope.countries[i];
                            $scope.country = $scope.selectedCountry.name.es;
                            $scope.selectedCity = $scope.selectedCountry.cities[0];
                            $scope.city = $scope.selectedCity.es;
                        }
                    }
                };
                $scope.changeCity = function () {
                    for (var i = 0; i < $scope.selectedCountry.cities.length; i++) {
                        if ($scope.selectedCountry.cities[i].es == $scope.city) {
                            $scope.selectedCity = $scope.selectedCountry.cities[i];
                        }
                    }
                };

                $scope.name = {
                    es: "",
                    en: ""
                };
                $scope.address = {
                    es: "",
                    en: ""
                };
                $scope.location = {
                    x: 0,
                    y: 0
                };
                $scope.files = [];
                img = false;
                $scope.erroresNameImage = [];
                $scope.erroresExistImage = [];
                $scope.validCantImg = 0;
                $scope.cantImg = 0;
                $scope.nameImgDel = [];
                $('#imgFile').val(null);
            };
            $scope.initialize();
//            $scope.setPage = function (pageNo) {
//                $scope.currentPage = pageNo;
//            };

            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.loadPorts();
            };

            $scope.loadPorts = function () {
                Ports.list.query({limit: $scope.maxSize, skip: $scope.skip, text: $scope.text}, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.cont;
                        $scope.ports = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.ports.length > 0) {
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadPorts();
                            }
                        }
                    } else {
                        $scope.ports = new Array();
                        $scope.empty = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener los Puertos.', 2);
                    }
                });
            };
            $scope.loadPorts();

            $scope.showDeleteButton = function () {
                var aux = ( $scope.objActive && $scope.images.length > $scope.maxImag) || !$scope.objActive;
                return aux;
            };
            $scope.hideImg = function () {
                $('#imagesObject').hide();
            };

            /**
             * @method Evento que se ejecuta cuando el archivo es seleccionado
             * @param $files
             */
            $scope.onImgSelect = function (files) {
                img = false;
                $scope.erroresNameImage = [];
                $scope.erroresExistImage = [];
                $scope.validCantImg = 0;
                recursivo(files, 0, function (data) {
                    $scope.$apply();
                });
            };

            function recursivo(files, pos, cb) {
                if (files.length == pos) {
                    $scope.$apply();
                    if (cb) cb(true)
                } else {
                    var typeArray = ["image/jpeg", "image/png"];
                    if ($rootScope.validateFile(files[pos], typeArray)) {
                        if ($scope.files.length > 0) {
                            if ($scope.verificarExiste(files[pos])) {
                                $scope.error = "errorExist";
                                $scope.erroresExistImage.push(files[pos].name);
                            } else {
                                if ($scope.cantImg < $scope.cantImage) {
                                    $scope.cantImg++;
                                    var reader = new FileReader();
                                    reader.readAsDataURL(files[pos]);
                                    reader.onload = function () {
                                        files[pos].image = reader.result;
                                        $scope.files.push(files[pos]);
                                        img = true;
                                        pos++;
                                        if (cb) cb(recursivo(files, pos, cb));
                                    };
                                } else {
                                    $('#imgFile').val(null);
                                    $scope.validCantImg = 1;
                                }
                            }
                        } else {
                            $scope.cantImg++;
                            var reader = new FileReader();
                            reader.readAsDataURL(files[pos]);
                            reader.onload = function () {
                                files[pos].image = reader.result;
                                $scope.files.push(files[pos]);
                                img = true;
                                pos++;
                                if (cb) cb(recursivo(files, pos, cb));
                            };
                        }
                    } else {
                        $scope.error = "errorType";
                        $scope.erroresNameImage.push(files[pos].name);
                        pos++;
                        if (cb) cb(recursivo(files, pos, cb));
                    }
                }
            }

            $scope.verificarExiste = function ($file) {
                var flag = false;
                var cont = 0;
                while (!flag && $scope.files.length > cont) {
                    if ($file.name == $scope.files[cont].name && $file.size == $scope.files[cont].size) {
                        flag = true;
                    }
                    cont = cont + 1;
                }
                return flag;
            };

            $scope.deletePortImg = function (file) {
                var pos = 0;
                var cantidad = $scope.files.length;
                var flag = false;
                while (!flag && pos < cantidad) {
                    if (file.name == $scope.files[pos].name) {
                        $scope.files.splice(pos, 1);
                        flag = true;
                    }
                    pos++;
                }
                $scope.cantImg--;
                $scope.validCantImg = 0;
                if ($scope.files.length == 0) {
                    img = false;
                }
                if (file.path) {
                    $scope.nameImgDel.push(file.name);
                }
                $('#imgFile').val(null);
            };

            /**
             * Viesualizador de imagen
             * @param path
             */
            $scope.viewImage = function (path) {
                $window.location = path;
            };

            $scope.addMedia = function () {
                $upload.upload({
                    url: "/api/ports/addMedia", //upload.php script, node.js route, or servlet url
                    method: 'POST',
                    headers: {'header-key': 'multipart/form-data'},
                    data: {id: $scope.objImg._id},
                    file: $scope.files
                }).success(function (data) {
                    $scope.files = new Array();
                    if (data.res) {
                        $scope.images.push(data.res);
                        $("#imgFile").val(null);
                    }
                });
            };

            $scope.delMedia = function (photo) {
                $http({method: 'POST', url: '/api/ports/delMedia', data: {id: $scope.objImg._id, photo: photo}}).
                    success(function (data) {
                        if (data.res) {
                            for (var i = 0; i < $scope.images.length; i++) {
                                if (photo.name == $scope.images[i].name) {
                                    $scope.images.splice(i, 1);
                                    if ($scope.images.length < $scope.maxImag) {
                                        var port = {
                                            id: $scope.objImg._id,
                                            available: true
                                        };
                                        $scope.changeStatus(port);
                                    }

                                }
                            }
                        }
                    });
            };
            $scope.validateFile = function ($files, extArray) {
                var flag = false;
                for (var i = 0; i < $files.length; i++) {
                    while ($files[i].name.indexOf("\\") != -1)
                        $files[i].name = $files[i].name.slice($files[i].name.indexOf("\\") + 1);
                    var ext = $files[i].name.slice($files[i].name.indexOf(".")).toLowerCase();
                    for (var j = 0; j < extArray.length; j++) {
                        if (extArray[j] == ext) {
                            //$scope.files.push($files[i]);
                            flag = true;
                        }
                    }
                }
                if (!flag) {
                    alert("Archivo invalido...");
                    $('#imgFile').val(null);
                }
                return flag;
            };
            $scope.edit = function (port) {
                for (var i = 0; i < $scope.countries.length; i++) {
                    if ($scope.countries[i].name.es == port.country.es) {
                        $scope.selectedCountry = $scope.countries[i];
                        for (var j = 0; j < $scope.countries[i].cities.length; j++) {
                            if ($scope.countries[i].cities[j].es == port.city.es) {
                                $scope.selectedCity = $scope.countries[i].cities[j];
                                break;
                            }
                        }
                        break;
                    }
                }
                $scope.id = port._id;
                $scope.name = port.name;
                $scope.address = port.address;
                $scope.location = port.location;
                $scope.city = port.city.es;
                $scope.country = port.country.es;
                $('#imgFile').val(null);
                $scope.files = [];
                for (var i = 0; i < port.photos.length; i++) {
                    $scope.files[i] = port.photos[i];
                }
                $scope.cantImg = $scope.files.length;
                img = true;
                $scope.nameImgDel = [];
                $scope.semaphore = false;
                $scope.showForm();

            };
            $scope.changeStatus = function (port) {
                if (port.available) {
                    if (port.tour.length == 0) {
                        $http({
                            method: 'PUT', url: '/api/ports/available',
                            data: {
                                id: port._id,
                                action: 2,
                                available: port.available
                            }
                        }).success(function (data) {
                            if (data.res) {
                                $scope.loadPorts();
                            } else {
                                $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                            }
                        });
                    }
                    else {
                        $http({
                            method: 'PUT', url: '/api/ports/available',
                            data: {
                                id: port._id,
                                tours: port.tour,
                                action: 0,
                                available: port.available
                            }
                        }).success(function (data) {
                            if (data.res) {
                                if (!data.complete) {
                                    $scope.showChangeStatusModal(port, data.res);
                                } else {
                                    $scope.loadPorts();
                                }
                            } else {
                                $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                            }
                        });
                    }
                } else {
                    $http({
                        method: 'PUT', url: '/api/ports/available',
                        data: {
                            id: port._id,
                            action: 2,
                            available: port.available
                        }
                    }).success(function (data) {
                        if (data.res) {
                            $scope.loadPorts();
                        } else {
                            $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                        }
                    });
                }
            };

            $scope.showChangeStatusModal = function (data, tours) {
                $scope.changeStatusItem = data;
                $scope.associationTours = tours;
                $('#changeStatusModal').modal('show');
            };

            $scope.changeStatusNow = function (port) {
                $http({
                    method: 'PUT', url: '/api/ports/available',
                    data: {
                        id: port._id,
                        action: 1,
                        tours: $scope.associationTours,
                        available: port.available
                    }
                }).success(function (data) {
                    if (data.res) {
                        $scope.loadPorts();
                    } else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido cambiar el estado', 2);
                    }
                });
            };

            $scope.save = function () {
                Ports.save({
                    id: $scope.id,
                    name: $scope.name,
                    address: $scope.address,
                    location: $scope.location,
                    city: $scope.selectedCity,
                    country: $scope.selectedCountry.name
                }, function (data) {
                    if (data.res) {
                        var files = preparedFiles($scope.files);
                        if ($scope.nameImgDel.length != 0 || files.length != 0) {
                            $rootScope.loadingImg = true;
                            $upload.upload({
                                url: "/api/ports/updateImg", //upload.php script, node.js route, or servlet url
                                method: 'POST',
                                headers: {'header-key': 'multipart/form-data'},
                                data: {id: $scope.id, nameImgDel: $scope.nameImgDel},
                                file: files
                            }).success(function (data) {
                                if (data.result) {
                                    $rootScope.loadingImg = false;
                                    $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                                    $scope.semaphore = true;
                                    $scope.initialize();
                                    $scope.loadPorts();
                                    $scope.hideForm();
                                } else {
                                    $rootScope.loadingImg = false;
                                    $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                                }
                            }).error(function (status) {
                                $rootScope.loadingImg = false;
                                $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                            });
                        } else {
                            $rootScope.Notifications('Cambios guardados satisfactoriamente', 0);
                            $scope.semaphore = true;
                            $scope.initialize();
                            $scope.loadPorts();
                            $scope.hideForm();
                        }
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido guardar los cambios', 2);
                    }
                })
            };
            $scope.create = function () {
                if ($scope.validateForm()) {
                    $rootScope.loadingImg = true;
                    $upload.upload({
                        url: '/api/ports', //upload.php script, node.js route, or servlet url
                        method: 'POST',
                        headers: {'header-key': 'multipart/form-data'},
                        data: {
                            name_es: $scope.name.es,
                            name_en: $scope.name.en,
                            address_es: $scope.address.es,
                            address_en: $scope.address.en,
                            location_x: $scope.location.x,
                            location_y: $scope.location.y,
                            city_es: $scope.selectedCity.es,
                            city_en: $scope.selectedCity.en,
                            country_es: $scope.selectedCountry.name.es,
                            country_en: $scope.selectedCountry.name.en,
                            country_id: $scope.selectedCountry._id
                        },
                        file: $scope.files
                    }).success(function (data, status) {
                        if (data.res) {
                            $rootScope.loadingImg = false;
                            $rootScope.Notifications('Puerto creado satisfactoriamente.', 0);
                            $scope.loadPorts();
                            $scope.hideForm();
                            $rootScope.loadStatistics();
                        } else {
                            $rootScope.loadingImg = false;
                            $rootScope.Notifications('Ha ocurrido un error, el puerto no se ha podido crear.', 2);
                        }
                    });
                }
            };

            $scope.delete = function () {
                Ports.delete({id: $scope.deleteItem._id}, function (data) {
                    if (data.res) {
                        $rootScope.Notifications('Puerto eliminado satisfactoriamente', 0);
                        $scope.loadPorts();
                        $rootScope.loadStatistics();
                    }
                    else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido eliminar el Puerto', 2);
                    }
                })
            };

            $scope.showDeleteModal = function (port) {
                $scope.deleteItem = port;
                $http({
                    method: 'POST', url: '/api/ports/listTours',
                    data: {
                        id: port._id
                    }
                }).success(function (data) {
                    if (data.res) {
                        $scope.deleteTours = data.res;
                    } else {
                        $rootScope.Notifications('Ha ocurrido un error, no se ha podido obtener los datos', 2);
                    }
                });
            };

            $scope.openClose = true;
            $scope.showForm = function () {
                $scope.openClose = false;
                $('#imagesObject').hide();
                $('#form-ship').show();
            };
            $scope.hideForm = function () {
                $scope.semaphore = true;
                $('#form-ship').hide();
                $('#imagesObject').hide();
                $scope.openClose = true;
                $scope.initialize();

            };
            $scope.validateNumber = function (num) {
                var FLOAT_REGEXP = /^\-?\d+((\.)\d+)?$/;
                return FLOAT_REGEXP.test(num);
            };
            $scope.validateForm = function () {

                return ($scope.name.es.length >= 1
                && $scope.name.en.length >= 1
                && $scope.address.es.length >= 1
                && $scope.address.en.length >= 1
                );
            };

            function preparedFiles(files) {
                var arrayTemp = [];
                for (var i = 0; i < files.length; i++) {
                    if (files[i].image) {
                        arrayTemp.push(files[i]);
                    }
                }
                return arrayTemp;
            }
        }]);

