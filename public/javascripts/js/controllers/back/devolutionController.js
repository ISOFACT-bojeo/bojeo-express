/**
 * Created by ernestomr87@gmail.com on 22/07/14.
 */

angular.module('bojeoApp.devolutionController', [])
    .controller('devolutionController', ['$scope', '$rootScope', 'Reservation',
        function ($scope, $rootScope, Reservation) {

            $rootScope.PageSelected = 'refunds';
            $rootScope.loadStatistics();
            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;
            $scope.totalItems = 0;
            $scope.text = "";

            $scope.loadReservations = function () {
                Reservation.query({
                    limit: $scope.maxSize,
                    skip: $scope.skip,
                    text: $scope.text,
                    id: '',
                    type: 'refund'
                }, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.cont;
                        $scope.reservations = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.reservations.length > 0) {
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadReservations();
                            }
                        }
                    } else {
                        $scope.reservations = new Array();
                        $scope.empty = true;
                    }
                });
            };
            $scope.loadReservations();
            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.loadReservations();
            };
            //$scope.currentPage = 1;
            //$scope.maxSize = 10;
            //$scope.skip = 0;
            //$scope.email = '';
            //
            //
            //$scope.loadReservations = function () {
            //    $scope.empty = true;
            //    Reservation.get({
            //        limit: $scope.maxSize,
            //        skip: ($scope.currentPage - 1) * 10,
            //        email: $scope.email
            //    }, function (data) {
            //        if (data.res) {
            //            $scope.reservations = data.res;
            //            for (var i = $scope.reservations.length - 1; i >= 0; i--) {
            //                if ($scope.reservations[i].cancel) {
            //                    $scope.reservations.splice(i, 1);
            //                }
            //            }
            //            if ($scope.reservations.length > 0) {
            //                $scope.empty = false;
            //            }
            //        }
            //    })
            //};
            //$scope.loadReservations();
            $scope.showReason = function (res) {
                $scope.reason = res.reason;
            }
        }]);
