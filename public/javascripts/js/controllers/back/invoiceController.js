/**
 * Created by ernestomr87@gmail.com on 17/07/14.
 */

angular.module('bojeoApp.invoiceController', [])
    .controller('invoiceController', ['$scope', '$rootScope', 'Invoices', '$http',
        function ($scope, $rootScope, Invoices, $http) {

            $rootScope.PageSelected = 'invoice';

            $scope.pos = 0;
            $scope.pos1 = 0;
            $scope.tabSelected = 'tab0';
            $scope.changeTab = function (tab) {
                if (tab == 'tab0') {
                    $scope.loadServices();
                }
                else {
                    $scope.loadShips();
                }
                $scope.tabSelected = tab;
            };

            $http({
                method: 'GET', url: '/date'
            }).success(function (data) {
                $scope.year = data.y;
                $scope.month = data.m;
                $scope.monthName = getMonthName($scope.month);
            });

            function getMonthName(month){
                switch (month){
                    case 0:
                        return "Enero";
                    case 1:
                        return "Febrero";
                    case 2:
                        return "Marzo";
                    case 3:
                        return "Abril";
                    case 4:
                        return "Mayo";
                    case 5:
                        return "Junio";
                    case 6:
                        return "Julio";
                    case 7:
                        return "Agosto";
                    case 8:
                        return "Septiembre";
                    case 9:
                        return "Octubre"
                    case 10:
                        return "Noviembre"
                    default:
                        return "Diciembre"
                }
            }

            $scope.changeMonthYear = function (value) {
                if(value < 0){
                    if($scope.month == 0){
                        $scope.month = 11;
                        $scope.year = $scope.year + value;
                    }else{
                        $scope.month = $scope.month + value;
                    }
                }else{
                    if($scope.month == 11){
                        $scope.month = 0;
                        $scope.year = $scope.year + value;
                    }else{
                        $scope.month = $scope.month + value;
                    }
                }
                $scope.monthName = getMonthName($scope.month);
                if($scope.tabSelected == 'tab0'){
                    $scope.Invoice($scope.serviceSelected.id, 'service', $scope.year, $scope.month);
                }else{
                    $scope.Invoice($scope.shipSelected.id, 'ship', $scope.year, $scope.month);
                }
            };

            /*Services*/
            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;
            $scope.totalItems = 0;

            $scope.loadServices = function () {
                $scope.serviceSelected = new Array();
                Invoices.services.query({limit: $scope.maxSize, skip: $scope.skip}, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.count;
                        $scope.services = data.res;
                        $scope.serviceSelected = $scope.services[$scope.pos];
                        $scope.Invoice($scope.serviceSelected.id, 'service', $scope.year, $scope.month);
                    }else{
                        $rootScope.Notifications('Ha ocurrido un error al obtener los servicios', 2);
                    }
                })
            };

            $scope.selectedService = function (index) {
                $scope.serviceSelected = $scope.services[index];
                $scope.pos = index;
                $scope.Invoice($scope.serviceSelected.id, 'service', $scope.year, $scope.month);
            };

            /*Ships*/
            $scope.currentPageShip = 1;
            $scope.maxSizeShip = 10;
            $scope.skipShip = 0;
            $scope.totalItemsShip = 0;

            $scope.loadShips = function () {
                Invoices.ships.query({limit: $scope.maxSizeShip, skip: $scope.skipShip}, function (data) {
                    if (data.res) {
                        $scope.totalItemsShip = data.count;
                        $scope.ships = data.res;
                        $scope.shipSelected = $scope.ships[$scope.pos1];
                        $scope.Invoice($scope.shipSelected.id, 'ship', null, null);
                    }else{
                        $rootScope.Notifications('Ha ocurrido un error al obtener los barcos', 2);
                    }
                })
            };
            $scope.loadServices();

            $scope.selectedShip = function (index) {
                $scope.shipSelected = $scope.ships[index];
                $scope.pos1 = index;
                $scope.Invoice($scope.shipSelected.id, 'ship', null, null);
            };

            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.loadServices();
            };
            $scope.pageChangedShip = function () {
                $scope.skipShip = ($scope.currentPageShip - 1) * $scope.maxSizeShip;
                $scope.loadShips();
            };

            $scope.Invoice = function (id, type, year, month) {
                Invoices.query({id: id, type: type, year: year, month: month}, function (data) {
                    if(data.res){
                        if(type == 'service'){
                            $scope.serviceData = data.res;
                        }else{
                            $scope.shipData = {
                                tours: [],
                                total: 0
                            };
                            $scope.shipData1 = {
                                tours: []
                            };
                            for(var i = 0; i < data.res.tours.length; i++){
                                if(data.res.tours[i].payform == 'bonds'){
                                    $scope.shipData1.tours.push(data.res.tours[i]);
                                }else{
                                    $scope.shipData.tours.push(data.res.tours[i])
                                }
                            }
                            $scope.shipData.total = data.res.total;
                        }
                    }else{
                        $rootScope.Notifications('Ha ocurrido un error al obtener los datos', 2);
                    }
                })
            }
        }]);
