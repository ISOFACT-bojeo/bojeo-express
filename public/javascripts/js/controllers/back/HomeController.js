/**
 * Created by ernestomr87@gmail.com on 24/04/14.
 */
angular.module('bojeoApp.HomeController', [])
    .controller('HomeController', ['$scope', '$rootScope', 'Statistics',
        function ($scope, $rootScope, Statistics) {
            $rootScope.PageSelected = 'home';
            $rootScope.loadStatistics();
            $rootScope.changePage = function (page) {
                $rootScope.PageSelected = page;
            };


            Statistics.timeLine.query(function (data) {
                if (data.res) {
                    for (object in data.res) {
                        data.res[object].date = (new Date(data.res[object].date)).getTime();
                        for (evento in data.res[object].event) {
                            data.res[object].event[evento].info = JSON.parse(data.res[object].event[evento].info);
                        }
                    }
                    $scope.timeLine = data.res;
                }
                else {
                    $scope.timeLine = new Array();
                }
            });


        }]);

