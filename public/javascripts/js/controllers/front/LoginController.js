/**
 * Created by Byran on 01/08/2014.
 */
angular.module('bojeoApp.LoginController', [])
    .controller('LoginController', ['$scope', '$rootScope', '$location', '$anchorScroll', '$routeParams', '$http',
        function ($scope, $rootScope, $location, $anchorScroll, $routeParams, $http) {

            if ($location.path() == '/login' && $rootScope.user != null) {
                $location.path('/' + $cookies.languageSelected + '/index');
            }

            angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_login);
            $rootScope.seo_description = $rootScope.texts.seo_page_description_login;

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });

            /*Activate User*/
            $scope.token = $routeParams.token;
            if ($scope.token == null) {
                $scope.token = null;
            }
            else {
                $http({method: 'GET', url: '/api/users/activate/' + $scope.token}).
                    success(function (data) {
                        $scope.token = data.res;
                    });
            }

            if ($location.hash() == 'error') {
                $rootScope.login_error = true;
                $anchorScroll();
            } else {
                $rootScope.login_error = false;
                $location.hash('page');
                $anchorScroll();
                $location.hash("");
            }
            ;

            //update page pos

        }]);