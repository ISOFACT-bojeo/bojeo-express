angular.module('bojeoApp.NewController', [])
    .controller('NewController', ['$scope', '$rootScope', '$routeParams', '$http','$location','$anchorScroll',
        function ($scope, $rootScope, $routeParams, $http, $location, $anchorScroll) {
            $rootScope.PageSelected = 'new';

            $scope.id = $routeParams.id;

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });

            $http({method: 'GET', url: '/api/news/' + $scope.id}).
                success(function (data) {
                    if(data.cont == 0){
                        $location.path('/' + $cookies.languageSelected + '/index');
                    }else{
                        $scope.new = data;
                    }
                });



            function htmlToPlaintext(text) {
                return String(text).replace(/<[^>]+>/gm, '');
            }
            //update page pos



            $scope.newUrl = 'http://www.bojeo.com/#'+$location.path();
        }]);
