/**
 * Created by ernestomr87@gmail.com on 19/06/14.
 */

angular.module('bojeoApp.payFormController', [])
    .controller('payFormController', ['$scope', '$rootScope', '$http', '$parse','$location', '$anchorScroll',
        function ($scope, $rootScope, $http, $parse, $location, $anchorScroll) {
            $rootScope.PageSelected = 'pay-form';

            angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_payment_methods);
            $rootScope.seo_description = $rootScope.texts.seo_page_description_payment_methods;

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });


            if (!$rootScope.devolutions) {
                $rootScope.getInfo('payForms',function(data){
                    $rootScope.payForms = data;
                });
            }

            //update page pos


        }]);
