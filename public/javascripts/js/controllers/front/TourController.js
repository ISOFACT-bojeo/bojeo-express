/**
 * Created by ernestomr87@gmail.com on 18/06/14.
 */

angular.module('bojeoApp.TourController', [])
    .controller('TourController', ['$scope', '$rootScope', '$routeParams', '$location', 'ToursReserv', 'Abonos', 'Profile', 'DisableDays', 'Tours', '$anchorScroll', '$timeout', 'Recomended', '$http', '$cookies',
        function ($scope, $rootScope, $routeParams, $location, ToursReserv, Abonos, Profile, DisableDays, Tours, $anchorScroll, $timeout, Recomended, $http, $cookies) {

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
                $rootScope.updatePage("/"+$rootScope.languageSelected.min+"/"+$scope.tour.group.slug[$rootScope.languageSelected.min]+"/"+$scope.tour.slug[$rootScope.languageSelected.min]);
            });

            $scope.slug = $routeParams.slug;
            $scope.shipsOc = true;
            $scope.hidePreactice = false;
            $scope.hideDesireHour = true;
            $scope.initTour = false;
            $scope.animateImg = true;
            $scope.viewForm = false;
            $scope.day = 1;
            $scope.firstMinDate = null;

            $scope.myScroll = new IScroll('#wrapper', {
                scrollbars: true,
                mouseWheel: true,
                interactiveScrollbars: true,
                shrinkScrollbars: 'scale',
                fadeScrollbars: true
            });

            Tours.get({id: $scope.slug}, function (data) {
                if (data.res) {
                    $scope.tour = data.res;

                    var cleanUrl =  $location.absUrl().replace($location.url(),'');
                    $rootScope.myLocation = {
                        es:  cleanUrl + "/es/" + $scope.tour.group.slug.es + "/" + $scope.tour.slug.es,
                        en:  cleanUrl + "/en/" + $scope.tour.group.slug.en + "/" + $scope.tour.slug.en
                    }

                    if ($scope.tour.group.slug[$rootScope.languageSelected.min] != $routeParams.group){
                        $location.path('/' + $cookies.languageSelected + '/404');
                        return;
                    }
                    Recomended.get({port: $scope.tour.port.id, tour: $scope.tour._id}, function (recomend) {
                        $scope.recommendationsTours = recomend.res;
                    });

                    if ($scope.tour.seo) {
                        angular.element("#bojeo_title").text($scope.tour.seo.title[$rootScope.languageSelected.min]);
                        $rootScope.seo_description = $scope.tour.seo.description[$rootScope.languageSelected.min];

                        $rootScope.$on('changeLanguage', function () {
                            angular.element("#bojeo_title").text($scope.tour.seo.title[$rootScope.languageSelected.min]);
                            $rootScope.seo_description = $scope.tour.seo.description[$rootScope.languageSelected.min];
                        });
                    }
                    else {
                        angular.element("#bojeo_title").text('Undefine');
                        $rootScope.seo_description = 'Undefine';


                    }


                    $scope.initTour = true;
                    $scope.photo = $scope.tour.photos[0].path;
                    $scope.show = true;
                    $scope.loadTour = function (dt, ship) {
                        ToursReserv.get({slug: $scope.slug, dt: dt, ship: ship}, function (data) {
                            if (data.res) {
                                if (data.occupied) {
                                    $scope.shipsOc = data.ships;
                                    $scope.occupied = data.occupied;
                                    var date = new Date(data.res.dt.y, data.res.dt.m, data.res.dt.d);
                                    $scope.today = new Date(data.today.y, data.today.m, data.today.d);
                                    $scope.dt = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                                }
                                else {
                                    $scope.services = new Array();
                                    if (data.pack) {
                                        $scope.services = data.pack.services;
                                        $scope.packColor = data.pack.color;
                                        $scope.pack = data.pack;
//                                        for (var i = 0; i < $scope.services.length; i++) {
//                                            $scope.tour.photos[3 - i].path = $scope.services[i].photos[0].path;
//                                        }
                                    }


                                    $scope.occupied = data.occupied;
                                    var date = new Date(data.res.dt.y, data.res.dt.m, data.res.dt.d);
                                    $scope.today = new Date(data.today.y, data.today.m, data.today.d);
                                    $scope.dt = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                                    $rootScope.getWeader($scope.dt);

                                    $scope.ships = data.res.ship;

                                    $scope.ship = $scope.ships[0];
                                    /*SHIPS*/
                                    $scope.startProcess = function () {
                                        $scope.daysDisables = $scope.ship.disables;
                                        $scope.shipName = $scope.ship.name;
                                        $scope.capacity = $scope.ship.capacity;


                                        if ($scope.ship.calendar.start < $scope.today.getTime()) {
                                            $scope.minDate = new Date($scope.today.getTime() + 86400000);
                                        }
                                        else {
                                            $scope.minDate = new Date($scope.ship.calendar.start);
                                        }
                                        //if (!$scope.firstMinDate) {
                                        //    $scope.firstMinDate = $scope.dt;
                                        //    $scope.minDate = $scope.firstMinDate;
                                        //}

                                        $scope.maxDate = new Date($scope.ship.calendar.end);

                                        if ($scope.tour.data.payForm != 'bonds') {

                                            $scope.ship.departures = sortDepartures($scope.ship.departures);

                                            $scope.departureSelected = $scope.ship.departures[0];
                                            $scope.durationSelected = $scope.departureSelected.duration[0];
                                            $scope.duration = $scope.durationSelected.duration;
                                            $scope.price = $scope.departureSelected.duration[0].price;
                                            $scope.departure = $scope.departureSelected.departure;

                                            if ($scope.capacity.priceBy == 'boat') {
                                                $scope.cantPerson = $scope.durationSelected.maxPerson;
                                                $scope.changeDaysAndPerson();
                                            }
                                            else {
                                                $scope.arrayPerson = [];
                                                for (var i = 1; i <= $scope.durationSelected.maxPerson; i++) {
                                                    $scope.arrayPerson.push(i);
                                                }
                                                $scope.cantPerson = $scope.arrayPerson[0];
                                                $scope.changeDaysAndPerson();
                                            }
                                        }
                                        else {
                                            if ($rootScope.user == null) {
                                                $scope.hidePreactice = true;
                                            }
                                            else {
                                                $scope.cantPerson = $scope.capacity.maxPerson;
                                            }


                                            Profile.get(function (profile) {
                                                if (profile.res) {
                                                    var sem = true;
                                                    $scope.profile = profile.res;
                                                    for (var i = 0; i < $scope.profile.abonos.length; i++) {
                                                        if ($scope.profile.abonos[i].id == $scope.tour.data.bond.bond) {
                                                            $scope.bond = $scope.profile.abonos[i];
                                                            sem = false;
                                                            break;
                                                        }
                                                    }

                                                    if (sem) {
                                                        $scope.bond = {
                                                            id: null,
                                                            cant: 0
                                                        }
                                                    }

                                                    $scope.desireHours = [];
                                                    if ($scope.bond.cant < $scope.tour.data.bond.maxHour) {
                                                        var max = $scope.bond.cant;
                                                    }
                                                    else {
                                                        var max = $scope.tour.data.bond.maxHour
                                                    }
                                                    for (var i = $scope.tour.data.bond.minHour; i <= max; i++) {
                                                        $scope.desireHours.push(i + 'h')
                                                    }
                                                    $scope.selecteddesireHours = $scope.desireHours[0];
                                                }
                                                else {
                                                    $scope.bond = {
                                                        id: null,
                                                        cant: 0
                                                    }
                                                    $scope.desireHours = [];
                                                    if ($scope.bond.cant < $scope.tour.data.bond.maxHour) {
                                                        var max = $scope.bond.cant;
                                                    }
                                                    else {
                                                        var max = $scope.tour.data.bond.maxHour
                                                    }
                                                    for (var i = $scope.tour.data.bond.minHour; i <= max; i++) {
                                                        $scope.desireHours.push(i + 'h')
                                                    }
                                                    $scope.selecteddesireHours = $scope.desireHours[0];
                                                }

                                            });
                                            Abonos.get(function (data) {
                                                if (data.res) {
                                                    $scope.abonosList = data.res;
                                                    for (var i = 0; i < $scope.abonosList.length; i++) {
                                                        if ($scope.abonosList[i]._id == $scope.tour.data.bond.bond) {
                                                            $scope.showbond = $scope.abonosList[i];
                                                            break;
                                                        }
                                                    }
                                                }
                                            });

                                        }
                                    }
                                    $scope.startProcess();


                                }

                                $scope.disabled = function (date, mode) {
                                    var res = (
                                    (date.getDay() === 0 && $scope.tour.daysWeek[0] == false) ||
                                    (date.getDay() === 1 && $scope.tour.daysWeek[1] == false) ||
                                    (date.getDay() === 2 && $scope.tour.daysWeek[2] == false) ||
                                    (date.getDay() === 3 && $scope.tour.daysWeek[3] == false) ||
                                    (date.getDay() === 4 && $scope.tour.daysWeek[4] == false) ||
                                    (date.getDay() === 5 && $scope.tour.daysWeek[5] == false) ||
                                    (date.getDay() === 6 && $scope.tour.daysWeek[6] == false)
                                    );

                                    var dis = false;
                                    if (!$scope.occupied) {
                                        var auxDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                                        for (var i = 0; i < $scope.daysDisables.length; i++) {
                                            if ($scope.daysDisables[i] == auxDate.getTime()) {
                                                dis = true;
                                                break;
                                            }
                                        }
                                    }
                                    return ( mode === 'day' && res || mode === 'day' && dis);
                                };
                            }
                        })
                    }
                    $scope.loadTour(null, null);

                }
                else {
                    $location.path('/' + $cookies.languageSelected + '/404');
                }
            })

            $scope.changeShip = function (ship) {
                for (var i = 0; i < $scope.ships.length; i++) {
                    if ($scope.ships[i] == ship) {
                        var aux = $scope.ships[0];
                        $scope.ships[0] = ship;
                        $scope.ships[i] = aux;
                        $scope.ship = ship;
                        break;
                    }
                }
                $scope.fadeShip();
                $scope.startProcess();

            }
            $scope.changePhoto = function (path, tour) {

                angular.element('#animateImg').fadeTo(200, 'toggle');

                $timeout(function () {
                    $scope.photo = path;
                    angular.element('#animateImg').fadeIn();
                }, 200);

                $scope.photoDescription = tour || null;
            };
            $scope.tabSelected = 0;
            $scope.changeTab = function (tab) {
                $scope.tabSelected = tab;
            };
            $scope.nameDays = function () {
                var array = [];
                for (var i = 0; i < 7; i++) {
                    array.push(getNameDay(i));
                }
                return array;
            };

            $scope.showCalendar = function () {
                $('.dropdown-calendar').slideDown(200);
            };
            $scope.fadeCalendar = function () {
                $('.dropdown-calendar').slideUp(200);
            };

            $scope.showShip = function () {
                $('#wrapper').slideDown();
                setTimeout(function () {
                    $scope.myScroll.refresh();
                }, 500);

            };
            $scope.refresh = function () {
                $scope.myScroll.refresh();
            }

            $scope.fadeShip = function () {
                $('#wrapper').slideUp(200);
            };


            // Disable weekend selection

            $rootScope.SelectedDay = function () {
                $scope.dt = new Date($scope.dt.getFullYear(), $scope.dt.getMonth(), $scope.dt.getDate());
                $scope.fadeCalendar();
                var dt = {
                    d: $scope.dt.getDate(),
                    m: $scope.dt.getMonth(),
                    y: $scope.dt.getFullYear()
                }

                var shipId = $scope.ship ? $scope.ship.id : null;
                $scope.loadTour(dt, shipId);


            };
            $scope.changeDeparture = function () {
                var pos = -1;
                for (var i = 0; i < $scope.ship.departures.length; i++) {
                    if ($scope.departure == $scope.ship.departures[i].departure) {
                        pos = i;
                        break;
                    }
                }
                if (pos >= 0) {
                    $scope.departureSelected = $scope.ship.departures[pos];
                    $scope.durationSelected = $scope.departureSelected.duration[0];
                    $scope.duration = $scope.durationSelected.duration;
                    $scope.price = $scope.departureSelected.duration[0].price;
                    $scope.departure = $scope.departureSelected.departure;

                    if ($scope.capacity.priceBy == 'boat') {
                        $scope.cantPerson = $scope.durationSelected.maxPerson;
                        $scope.changeDaysAndPerson();
                    }
                    else {
                        $scope.arrayPerson = [];
                        for (var i = 1; i <= $scope.durationSelected.maxPerson; i++) {
                            $scope.arrayPerson.push(i);
                        }
                        $scope.cantPerson = $scope.arrayPerson[0];
                        $scope.changeDaysAndPerson();
                    }
                }


            };

            $scope.changeDuration = function () {
                var pos = -1;
                for (var i = 0; i < $scope.departureSelected.duration.length; i++) {
                    if ($scope.duration == $scope.departureSelected.duration[i].duration) {
                        pos = i;
                        if ($scope.tour.data.price.type == 'day') {
                            $scope.day = parseInt($scope.duration);
                        }
                        break;
                    }
                }
                if (pos >= 0) {
                    $scope.departure = $scope.departureSelected.departure;
                    $scope.price = $scope.departureSelected.duration[pos].price;
                    $scope.durationSelected = $scope.departureSelected.duration[pos];
                    $scope.duration = $scope.durationSelected.duration;
                    if ($scope.capacity.priceBy == 'boat') {
                        $scope.cantPerson = $scope.durationSelected.maxPerson;
                        $scope.changeDaysAndPerson();
                    }
                    else {
                        $scope.arrayPerson = [];
                        for (var i = 1; i <= $scope.durationSelected.maxPerson; i++) {
                            $scope.arrayPerson.push(i);
                        }
                        $scope.cantPerson = $scope.arrayPerson[0];
                        $scope.changeDaysAndPerson();
                    }
                }
            }

            $scope.changeDaysAndPerson = function () {
                var aux = 0;
                if ($scope.services.length) {
                    for (var i = 0; i < $scope.services.length; i++) {
                        aux = aux + $scope.services[i].price * $scope.cantPerson * $scope.day;
                    }
                }
                if ($scope.capacity.priceBy == 'boat') {
                    $scope.tourMonto = $scope.price;
                    $scope.monto = $scope.price + aux;
                }
                else {
                    $scope.tourMonto = $scope.price * $scope.cantPerson;
                    $scope.monto = ($scope.price * $scope.cantPerson) + aux;
                }

            };

            $scope.buyHours = function () {
                if ($rootScope.user) {
                    $location.path('/' + $cookies.languageSelected + '/profile/bonu');
                } else {
                    $location.path('/' + $cookies.languageSelected + '/profile/bonu');
                }
            };
            $scope.Reserved = function (method) {

                var pack = $scope.slug;
                if (!($scope.services.length > 0)) {
                    pack = null;
                }
                var bond = null;
                if ($scope.tour.data.payForm == 'bonds') {
                    bond = $scope.tour.data.bond.bond;
                }
                var tour = {
                    id: $scope.tour._id,
                    slug: $scope.slug,
                    monto: $scope.monto,
                    tourMonto: $scope.tourMonto,
                    departure: $scope.departure,
                    duration: $scope.duration,
                    cantPerson: $scope.cantPerson,
                    date: $scope.dt.getTime(),
                    ship: {
                        id: $scope.ship.id,
                        name: $scope.ship.name
                    },
                    payForm: $scope.tour.data.payForm,
                    bond: bond,
                    hours: $scope.selecteddesireHours,
                    days: $scope.day,
                    pack: pack,
                    color: $scope.packColor ? $scope.packColor : null,
                    method: method
                }


                //var tour = JSON.stringify(tour);

                if ($rootScope.user == null) {
                    $rootScope.nextStep = {
                        msg: $rootScope.texts.page_account_login_to_reserve,
                        path: '/buy'
                    }
                    $location.path('/' + $cookies.languageSelected + '/login');
                }
                else {
                    if ($rootScope.user.complete) {
                        $http({
                            method: 'POST', url: '/api/reservation/saveBookInfo',
                            data: {info: tour}
                        }).
                            success(function (data) {
                                if (data.res) {
                                    var url = '/' + $cookies.languageSelected + '/buy';
                                    $location.path(url);
                                }

                            });


                    }
                    else {
                        $location.path('/' + $cookies.languageSelected + '/profile/complete');
                    }

                }
            };
            function sortDepartures(departures) {
                for (var i = 0; i < departures.length - 1; i++) {
                    for (var j = i + 1; j < departures.length; j++) {
                        var first = departureTime(departures[i].departure);
                        var second = departureTime(departures[j].departure);

                        if (first > second) {
                            var aux = departures[i];
                            departures[i] = departures[j];
                            departures[j] = aux;
                        }
                    }
                }
                return departures;
            };

            $scope.reduceText = function (text) {
                //var myString:String = "<p><b>bold</b> <i>italic</i> <a href='#'>link</a> <br>linebreak</p>";
                //trace(myString)
                var removeHtmlRegExp = new RegExp("<[^<]+?>", "gi");
                text = text.replace(removeHtmlRegExp, "");
                text = NormalizeString(text);
                var lengthText = 250;
                if (text.length <= lengthText) {
                    return text;
                }
                else {
                    var aux = "", newText = "";
                    for (var i = 0; i < lengthText; i++) {
                        aux = aux + text[i];
                    }

                    var last = aux.lastIndexOf(' ');

                    for (var i = 0; i < last; i++) {
                        newText = newText + aux[i];
                    }
                    newText = newText + "...";


                    return newText;
                }
            };
            function NormalizeString(r) {
                r = r.replace(new RegExp("&aacute;"), "á");
                r = r.replace(new RegExp("&eacute;"), "é");
                r = r.replace(new RegExp("&iacute;"), "í");
                r = r.replace(new RegExp("&oacute;"), "ó");
                r = r.replace(new RegExp("&uacute;"), "ú");
                /*******************************************/
                r = r.replace(new RegExp("&aacute;"), "á");
                r = r.replace(new RegExp("&eacute;"), "é");
                r = r.replace(new RegExp("&iacute;"), "í");
                r = r.replace(new RegExp("&oacute;"), "ó");
                r = r.replace(new RegExp("&uacute;"), "ú");
                return r;
            }

            $scope.showServiceDescription = function (serviceIndex) {
                $scope.tabSelected = 0;

                $timeout(function () {
                    $location.hash('service-' + serviceIndex);
                    $anchorScroll();
                    $location.hash("");
                }, 500);

            };
            $location.hash('page');
            $anchorScroll();
            $location.hash("");
        }
    ]);

function departureTime(time) {
    var array = time.split(':');
    if (array.length == 2) {
        var hour = parseInt(array[0]);
        var min = parseInt(array[1]);

        if (isNaN(hour) || isNaN(min)) {
            return false;
        }
        else {
            if (hour >= 0 && hour < 24 && min >= 0 && min < 60) {
                return (hour * 60 + min) * 60000;
            }
            else {
                return false;
            }

        }
    }
    else {
        return false;
    }
};
