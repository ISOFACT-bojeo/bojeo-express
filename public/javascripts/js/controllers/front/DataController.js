/**
 * Created by ernestomr87@gmail.com on 24/04/14.
 */
angular.module('bojeoApp.DataController', [])
    .controller('DataController', ['$scope', '$rootScope', '$http', '$interval', '$location',
        '$routeParams', '$anchorScroll', '$filter', '$sce', '$cookies', '$route', '$kookies','$window',
        function ($scope, $rootScope, $http, $interval, $location, $routeParams, $anchorScroll, $filter, $sce, $cookies, $route, $kookies,$window) {
            $rootScope.changePage = function (page) {
                $rootScope.PageSelected = page;
                if ($rootScope.PageSelected == 'about') {
                    $rootScope.seo_name = seo_page_name_about;
                    $rootScope.seo_description = seo_page_description_about;
                }
            }

            //console.log($route.current.params);
            //if (!$routeParams.culture) {
            //    var aux = $cookies.languageSelected || 'es';
            //    $location.path('/' + aux)
            //}
            //else {
            //    if ($cookies.languageSelected != $routeParams.culture && ($routeParams.culture == 'es' || $routeParams.culture == 'en' )) {
            //        $rootScope.changeLanguage($routeParams.culture);
            //    }
            //
            //}


            globalLoadPage = true;
            $rootScope.googleMapDep = {
                0: "#",
                1: "#",
                2: "#"
            };
            if (!$rootScope.country)$rootScope.country = null;
            $rootScope.redirect = function (url) {
                window.location = url;
            };
            $rootScope.urlServer = $location.host() + ':' + $location.port();
            $rootScope.financeUpdate = function () {
                $http(
                    {
                        method: 'GET',
                        url: 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22EURUSD%22%2C%20%22EURGBP%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback='
                    }
                ).success(function (data) {
                        $scope.dollarValue = data.query.results.rate[0].Rate;
                        $scope.libraValue = data.query.results.rate[1].Rate;
                        $scope.euroValue = 1;
                    });
            };
            $rootScope.financeUpdate();
            $rootScope.updateSession = function () {
                $http({method: 'GET', url: '/api/sessions/ping'}).
                    success(function (data) {
                        if (data == "false") {
                            $rootScope.user = null;
                        }
                        else {
                            $rootScope.user = data;
                            $rootScope.changeLanguage($rootScope.user.language);
                        }
                    });
            }
            $rootScope.updateSession();


            $scope.passwordChange = false;
            $rootScope.user = null;
            $rootScope.weather = null;
            $rootScope.getWeader = function (date, city, country) {
                if (!$rootScope.weather || (city && country)) {
                    $.simpleWeather({
                        location: city ? city : $rootScope.selectedPort.city.en + ', ' + country ? country : $rootScope.selectedPort.country.en,
                        unit: 'c',
                        success: function (weather) {
                            $rootScope.weather = weather.forecast;
                            $rootScope.$apply();
                            $rootScope.changeWeaderSelectedDay(date);
                        },
                        error: function (error) {

                        }
                    });
                }
                else {
                    $rootScope.changeWeaderSelectedDay(date);
                }
            };
            $rootScope.changeWeaderSelectedDay = function (date) {
                var is = false;
                for (var i = 0; i < $rootScope.weather.length; i++) {
                    if ($rootScope.weather[i].date == $filter('date')(date, 'd MMM y')) {
                        $rootScope.selectedDayweather = $rootScope.weather[i];
                        is = true;
                        try {
                            $rootScope.$digest();
                        } catch (e) {

                        }
                        break;
                    }
                }
                if (!is) {
                    $rootScope.selectedDayweather = null;
                }
            };

            $rootScope.getInfo = function (type, cb) {
                $http({method: 'POST', url: '/api/generalInfo/getInfo', data: {type: type}}).
                    success(function (data) {
                        if (data.res) {
                            cb(data.res);
                        }
                        else {
                            cb(false);
                        }
                    });
            };


            //show cookies polices

            $rootScope.policesAccept = $cookies.policesAccept || false;
            $rootScope.acceptPolices = function () {
                $rootScope.policesAccept = true;
                $kookies.set('policesAccept', 'true', {path: '/'});
            };
            /*Change Pass*/
            $rootScope.login_success = false;
            $rootScope.logining = false;
            $rootScope.login_error = false;
            $rootScope.sendLogin = function () {
                $rootScope.logining = true;
                try {
                    $rootScope.$apply();
                }
                catch (e) {
                }

                var bool = true;
                $rootScope.login_error = false;
                if ($scope.email == "" || $scope.email == null) {
                    $scope.errorEmail = "Empty field";
                    bool = false;
                }
                if ($scope.password == "" || $scope.password == null) {
                    $scope.errorPassword = "Empty field";
                    bool = false;
                }
                if (bool) {
                    $.post('/login', {email: $scope.email, password: $scope.password}, function (data) {
                        $rootScope.logining = false;
                        try {
                            $rootScope.$apply();
                        }
                        catch (e) {
                        }

                        if (data.error) {
                            $rootScope.login_error = true;
                            try {
                                $rootScope.$apply();
                            }
                            catch (e) {
                            }
                        }
                        else {
                            $rootScope.user = data;
                            if ($rootScope.nextStep != null) {
                                window.location = $rootScope.nextStep.path;
                            }
                            else {
                                window.location = "/index";
                            }
                        }
                    });
                }
                else {
                    $rootScope.login_error = true;
                    $rootScope.logining = false;
                    try {
                        $rootScope.$apply();
                    }
                    catch (e) {
                    }
                }
            };
            $scope.Logout = function () {
                $http({method: 'GET', url: '/logout'}).
                    success(function (data, status, headers, config) {


                    }).
                    error(function (data, status, headers, config) {

                    });
            };


            $rootScope.laguages = {
                es: {
                    min: 'es',
                    text: 'ESPAÑOL',
                    path: 'img/front/Flags/Flags_03.png'
                },
                en: {
                    min: 'en',
                    text: 'ENGLISH',
                    path: 'img/front/Flags/Flags_02.png'
                }
            }

            $rootScope.getLanByName = function (findText) {
                for (var key in $rootScope.laguages) {
                    if ($rootScope.laguages[key].text == findText)
                        return $rootScope.laguages[key];
                }
            }
            $rootScope.texts = window[$cookies.languageSelected || 'es'];
            $rootScope.languageSelected = $rootScope.laguages[$cookies.languageSelected || 'es'];


            $rootScope.myLocation = {
                es: $location.absUrl().replace("/en", "/es"),
                en:  $location.path() == "/" ?  $location.absUrl() +"en" : $location.absUrl().replace("/es", "/en")
            }


            $rootScope.updatehref = function (lang) {
                var path = $location.path();
                path = path.replace($rootScope.languageSelected.min, lang);
                $location.path(path);
            }

            $rootScope.updateLanguage = function (lang) {
                var path = $location.path();
                path = path.replace($rootScope.languageSelected.min, lang);
                $rootScope.languageSelected.min = lang;
                $http({method: 'POST', url: '/api/users/language', data: {language: lang}}).
                    success(function (data) {
                        if (data.res) {
                            $rootScope.$broadcast("changeLanguage", path);

                        }
                    }
                ).error(function (err) {
                        $rootScope.$broadcast("changeLanguage", path);
                    });
            }

            $rootScope.updatePage = function(url){
                if(url){
                    $location.path(url);
                    $window.location = $window.location.origin + url
                }
                else {
                    $window.location = $window.location.origin + $window.location.pathname;
                }
            }


            $rootScope.changeLanguage = function (lang) {
                $rootScope.languageSelected = $rootScope.laguages[lang];
                if ($rootScope.user) {
                    $http({method: 'POST', url: '/api/users/language', data: {language: lang}}).
                        success(function (data) {
                            if (data.res) {
                                bodyMethod();
                            }
                        });
                }
                else {
                    bodyMethod();
                }

                function bodyMethod() {
                    $rootScope.texts = window[$rootScope.languageSelected.min];

                    try {
                        $rootScope.calendarBojeoModelUpdate();
                    }
                    catch (err) {

                    }
                    $kookies.set('languageSelected', $rootScope.languageSelected.min, {expires: 365, path: '/'});

                    $rootScope.country = $scope.selectedCountry.name[$rootScope.languageSelected.min];
                    $rootScope.city = $scope.selectedCity[$rootScope.languageSelected.min];
                    $rootScope.nport = $rootScope.selectedPort.name[$rootScope.languageSelected.min];

                }
            };


            $rootScope.shareSelected = false;
            $rootScope.showLogin = function () {
                $location.path('/login');
                $location.path('/' + $cookies.languageSelected + '/login');
            };
            $rootScope.hideLogin = function () {
                $("#login").fadeOut(200, function () {
                    $("#iniciar").slideDown(200);
                });
            };
            $rootScope.sem = false;
            $rootScope.showIcons = function () {
                $rootScope.shareSelected = true;
                $('#share-icons').slideDown(200);
            };
            $rootScope.fadeIcons = function () {
                $rootScope.shareSelected = false;
                $('#share-icons').slideUp(200);
            };
            $rootScope.showLanguages = function () {
                $('#dropLang').slideDown(200);
            };
            $rootScope.fadeLanguage = function () {
                $('#dropLang').slideUp(200);
            };
            $rootScope.showCoins = function () {
                $('#dropCoins').slideDown(200);
            };
            $rootScope.fadeCoins = function () {
                $('#dropCoins').slideUp(200);
            };
            $rootScope.textForSearchHome = '';
            $rootScope.sendTetForSearchWithKey = function () {
                $rootScope.showtextForSearchWindow = false;
                $rootScope.sendTextForSearch($rootScope.textForSearchHome);
                $rootScope.textForSearchHome = '';
            };
            $rootScope.sendTextForSearch = function (valueToSearch) {
                $location.path('/' + $cookies.languageSelected + '/search/' + (valueToSearch || $rootScope.textForSearch));

            };
            $rootScope.textForSearch = "";
            $rootScope.showtextForSearchWindow = false;
            $rootScope.showMenuWindowForm = false;
            $rootScope.showSearchWindow = function () {
                $rootScope.showtextForSearchWindow = true;
                $rootScope.showMenuWindowForm = false;
            };
            $rootScope.showMenuWindow = function () {
                $rootScope.showtextForSearchWindow = false;
                $rootScope.showMenuWindowForm = true;
            };

            setInterval(function () {
                $http({method: 'GET', url: '/api/sessions/ping'}).
                    success(function (data) {
                        if (data == "false") {
                            $rootScope.user = null;
                        }
                    });
            }, 300000);

            /*CURRENCY*/
            $rootScope.coins = {
                USD: {
                    text: 'USD',
                    path: 'img/front/dollar.png',
                    path_Hover: 'img/front/dollar_hover.png',
                    symbol: '$'
                },
                EUR: {
                    text: 'EUR',
                    path: 'img/front/euro.png',
                    path_Hover: 'img/front/euro_hover.png',
                    symbol: '€'
                },
                GBP: {
                    text: 'GBP',
                    path: 'img/front/libra.png',
                    path_Hover: 'img/front/libra_hover.png',
                    symbol: '₤'
                }
            };
            $rootScope.coinSelected = $rootScope.coins[$cookies.coinSelected || 'EUR'];
            $rootScope.financeConverter = function () {
                var coverterCoin = 1;
                if ($rootScope.coinSelected.text == 'USD') {
                    coverterCoin = $scope.dollarValue;
                }
                if ($rootScope.coinSelected.text == 'GBP') {
                    coverterCoin = $scope.libraValue;
                }
                return coverterCoin;
            };
            $rootScope.changeCoin = function (coin) {
                $rootScope.coinSelected = $rootScope.coins[coin];
                $rootScope.fadeCoins();
                $kookies.set('coinSelected', $rootScope.coinSelected.text, {expires: 365, path: '/'});


            };

            $rootScope.ramdomImage = function () {
                var num = (generateNumber($rootScope.selectedPort.photos.length)) || 0;
                return $rootScope.selectedPort.photos[num].path;
            }
            function generateNumber(max) {
                var aux = Math.floor((Math.random() * max));
                return aux;
            };

            $scope.initialize = function () {
                if ($rootScope.countries.length) {
                    /**INITIALIZE**/
                    $rootScope.selectedCountry = angular.copy($rootScope.countries[0]);
                    $rootScope.selectedCity = angular.copy($rootScope.selectedCountry.cities[0]);
                    $rootScope.selectedPort = angular.copy($rootScope.selectedCity.ports[0]);
                    if ($rootScope.selectedPort) {
                        $rootScope.imagePort = $rootScope.ramdomImage();
                    }
                    $kookies.set('id_port', $rootScope.selectedPort._id, {expires: 365, path: '/'});

                }
                else {
                }

            }
            $scope.initialize();

            $rootScope.changeCountry = function () {
                for (var i = 0; i < $rootScope.countries.length; i++) {

                    if ($rootScope.countries[i].name[$rootScope.languageSelected.min] == $rootScope.selectedCountry.name[$rootScope.languageSelected.min]) {
                        $rootScope.selectedCountry = angular.copy($rootScope.countries[i]);
                        $rootScope.selectedCity = angular.copy($rootScope.selectedCountry.cities[0]);
                        $rootScope.selectedPort = angular.copy($rootScope.selectedCity.ports[0]);
                        $rootScope.imagePort = $rootScope.ramdomImage();
                        $kookies.set('id_port', $rootScope.selectedPort._id, {expires: 365, path: '/'});
                        break;
                    }
                }
                $rootScope.goToIndex();
            }

            $rootScope.changeCity = function () {
                for (var i = 0; i < $rootScope.selectedCountry.cities.length; i++) {
                    if ($rootScope.selectedCountry.cities[i].name[$rootScope.languageSelected.min] == $rootScope.selectedCity.name[$rootScope.languageSelected.min]) {
                        $rootScope.selectedCity = angular.copy($rootScope.selectedCountry.cities[i]);
                        $rootScope.selectedPort = angular.copy($rootScope.selectedCity.ports[0]);
                        $rootScope.imagePort = $rootScope.ramdomImage();
                        break;
                    }
                }
                $rootScope.goToIndex();
            }

            $rootScope.changePort = function () {
                for (var i = 0; i < $rootScope.selectedCity.ports.length; i++) {
                    if ($rootScope.selectedCity.ports[i].name[$rootScope.languageSelected.min] == $rootScope.selectedPort.name[$rootScope.languageSelected.min]) {
                        $rootScope.selectedPort = angular.copy($rootScope.selectedCity.ports[i]);
                        $rootScope.imagePort = $rootScope.ramdomImage();
                        $kookies.set('id_port', $rootScope.selectedPort._id, {expires: 365, path: '/'});

                        break;
                    }
                }
                $rootScope.goToIndex();
            }

            $rootScope.getGrid = function (cant) {
                var grid = [];
                for (var i = 0; i < cant; i++) {
                    if (cant - i == 1) {
                        grid.push([
                            {
                                class: "col-sm-12",
                                pos: i
                            }
                        ])
                    }
                    else if (cant - i == 3) {
                        grid.push([
                            {
                                class: "col-sm-4",
                                pos: i
                            }, {
                                class: "col-sm-4",
                                pos: i + 1
                            }, {
                                class: "col-sm-4",
                                pos: i + 2
                            }]);
                        i += 2;
                    }
                    else {
                        if (grid.length % 2) {
                            grid.push([{
                                class: "col-sm-4",
                                pos: i
                            }, {
                                class: "col-sm-8",
                                pos: i + 1
                            }]);
                        }
                        else {
                            grid.push([{
                                class: "col-sm-8",
                                pos: i
                            }, {
                                class: "col-sm-4",
                                pos: i + 1
                            }]);
                        }
                        i += 1;
                    }
                }
                return grid;
            }

            $rootScope.goToIndex = function () {
                if ($rootScope.PageSelected != 'index') {
                    $location.path('/' + $cookies.languageSelected + '/')
                }
            }


        }]);




