/**
 * Created by ernestomr87@gmail.com on 4/06/14.
 */

angular.module('bojeoApp.IndexController', [])
    .controller('IndexController', ['$scope', '$rootScope', '$location', '$http','$filter',
        function ($scope, $rootScope, $location, $http,$filter) {
            $rootScope.PageSelected = 'index';

            $scope.orderBy = $filter('orderBy');


            angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_home);
            $rootScope.seo_description = $rootScope.texts.seo_page_description_home;

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });

            $scope.goToProfile = function () {
                if ($rootScope.user == null) {
                    $location.path('/' + $cookies.languageSelected + '/login');
                }
            }

            if ($rootScope.first_Time) {
                $rootScope.first_Time = false;
            }
            else {
                $http({method: 'POST', url: '/api/vars'}).
                    success(function (data) {
                        $rootScope.countries = data.BOJEO_COUNTRIES || new Array();
                        $rootScope.noticeCookies = data.NOTICECOOKIES;
                        $rootScope.copyRights = data.BOJEO_COPYRIGHTS;
                    });
            }

        }]);

