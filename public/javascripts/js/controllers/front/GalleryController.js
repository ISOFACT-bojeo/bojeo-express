angular.module('bojeoApp.GalleryController', [])
    .controller('GalleryController', ['$scope', '$rootScope', '$routeParams', 'Ports', '$http', '$location', '$anchorScroll',
        function ($scope, $rootScope, $routeParams, Ports, $http, $location, $anchorScroll) {
            $rootScope.PageSelected = 'gallery';
            $rootScope.showMenuWindowForm = false;

            angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_gallery);
            $rootScope.seo_description = $rootScope.texts.seo_page_description_gallery;

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });



            $http({method: 'GET', url: '/api/gallery'}).
                success(function (data) {
                    if (data.res) {
                        $scope.pictures = data.res;

                        $rootScope.gallerySlides = [];
                        var cant = $scope.pictures.ports.length;
                        for (var i = 0; i < cant; i++) {
                            var image = {
                                name: $scope.pictures.ports[i].name,
                                image: $scope.pictures.ports[i].img.path
                            };
                            $scope.gallerySlides.push(image)
                        }
                        $rootScope.myInterval = 5000;

                    } else {

                    }

                });

            $scope.positionOk = false;
            $scope.setOkPosition = function () {
                $scope.positionOk = true;
            }

        }]);
