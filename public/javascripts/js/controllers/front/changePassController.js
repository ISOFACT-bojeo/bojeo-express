/**
 * Created by ernestomr87@gmail.com on 4/06/14.
 */

angular.module('bojeoApp.changePassController', [])
    .controller('changePassController', ['$scope', '$rootScope', '$http', '$routeParams',
        function ($scope, $rootScope, $http, $routeParams) {

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });

            $scope.tokenPass = $routeParams.tokenPass;

            if ($scope.tokenPass == null) {
                $scope.tokenPass = null;
            }
            else {
                $http({method: 'GET', url: '/getUserToken/' + $scope.tokenPass}).
                    success(function (data) {
                        if (data == "false") {
                            window.location = "/index";
                        }
                        else {
                            $scope.forgivePass = "";
                            $scope.forgiveNewPass = "";
                        }
                    });
            }
            ;
            $scope.changePass = function () {
                if ($scope.forgivePass == $scope.forgiveNewPass) {
                    $http({
                        method: 'POST', url: '/changePassByToken',
                        data: {token: $scope.tokenPass, password: $scope.forgivePass}
                    }).
                        success(function (data) {
                            if (data.res) {
                                $scope.passwordChange = true;
                            }
                        });
                }
            };

        }]);

''