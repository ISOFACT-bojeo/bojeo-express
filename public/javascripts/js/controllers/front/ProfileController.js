/**
 * Created by ernestomr87@gmail.com on 14/07/14.
 */

angular.module('bojeoApp.ProfileController', [])
    .controller('ProfileController', ['$scope', '$rootScope', 'Profile', '$filter', 'Abonos', '$http', '$routeParams', '$location', '$anchorScroll', '$timeout','$cookies',
        function ($scope, $rootScope, Profile, $filter, Abonos, $http, $routeParams, $location, $anchorScroll, $timeout,$cookies) {

            $rootScope.PageSelected = 'profile';

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });

            if($rootScope.user){

                $scope.passSuccessShow = false;
                $scope.showSaveError = false;
                $scope.showSaveSuccess = false;
                $scope.showActivities = false;
                $scope.letEdit = false;
                $scope.showPerfilOk = false;

                angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_profile);
                $rootScope.$on('changeLanguage', function () {
                    angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_profile);
                });
                $rootScope.seo_description = $rootScope.texts.seo_page_description_profile;


                $scope.tabSelected = 1;
                $scope.changeTab = function (tab) {
                    $scope.tabSelected = tab;
                };

                if ($routeParams.buy == "bonu") {
                    $scope.tabSelected = 1;
                    //$scope.showOk = true;
                    $location.hash('bonu');
                    $anchorScroll();
                    $location.hash('');
                }
                if ($routeParams.buy == "complete") {
                    $scope.tabSelected = 3;
                    $scope.showPerfilOk = true;
                    $anchorScroll();
                    $location.hash('');
                }


                Profile.get(function (data) {
                    if ($routeParams.buy == "buy") {
                        $scope.showOk = true;
                        $location.hash('buy-complete');
                        $anchorScroll();
                        $location.hash('');

                        if (!data.reserved) {
                            //$location.path('/occupied-booking');
                        }

                    }
                    if (data.res) {
                        $scope.timeNow = new Date(data.date);
                        $scope.yearNow = $scope.timeNow.getFullYear();
                        $scope.user = data.res;

                        //$scope.birthday.getFullYear(), $scope.birthday.getMonth(), $scope.birthday.getDate()
                        $scope.years = [];
                        for (var i = 1900; i < $scope.timeNow.getFullYear(); i++) {
                            $scope.years.push(i);
                        }
                        //end


                        if ($scope.user.reservations.length > 0 && $routeParams.buy != "bonu" && $routeParams.buy != "complete") {
                            $scope.tabSelected = 0;
                        }
                        if ($scope.user.role == 'patron' && $scope.user.activities.length > 0) {
                            $scope.showActivities = true;
                        }
                        if ($scope.user.role == 'owner') {
                            $scope.loadActivities = function () {
                                Profile.owner.query({
                                        year: $scope.yearNow
                                    }, function (ativities) {
                                        if (ativities.res) {
                                            $scope.activities = ativities.res;
                                            $scope.monthNameEs = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
                                            $scope.monthNameEn = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

                                            $scope.grandTotalService = 0;
                                            $scope.grandTotalShips = 0;
                                            for (var i = 0; i < $scope.activities.services.length; i++) {
                                                $scope.grandTotalService = $scope.grandTotalService + $scope.activities.services[i].total;
                                            }
                                            for (var i = 0; i < $scope.activities.ships.length; i++) {
                                                $scope.grandTotalShips = $scope.grandTotalShips + $scope.activities.ships[i].total;
                                            }

                                            if ($scope.activities.ships.length) {
                                                $scope.shipSelected = $scope.activities.ships[0];
                                                $scope.shipName = $scope.shipSelected.name;

                                                $scope.shipChart = function () {
                                                    $scope.shipSerieTotal = {
                                                        name: 'Total Comitions',
                                                        color: '#4572A7',
                                                        type: 'column',
                                                        yAxis: 1,
                                                        data: $scope.shipSelected.monthArrayTotal,
                                                        tooltip: {
                                                            valueSuffix: ' €'
                                                        }
                                                    }
                                                    $scope.shipSerieTour = {
                                                        name: 'Tours',
                                                        color: '#89A54E',
                                                        type: 'spline',
                                                        data: $scope.shipSelected.monthArray,
                                                        tooltip: {
                                                            valueSuffix: ' tours'
                                                        }
                                                    }
                                                    $(function () {
                                                        $('#containerShip').highcharts({
                                                            chart: {
                                                                zoomType: 'xy'
                                                            },
                                                            title: {
                                                                text: $rootScope.texts.page_account_owner_ships_activity_commissions
                                                            },
                                                            subtitle: {
                                                                text: $scope.yearNow
                                                            },
                                                            xAxis: {
                                                                categories: $rootScope.languageSelected.min == 'es' ? $scope.monthNameEs : $scope.monthNameEn
                                                            },
                                                            yAxis: [{ // Primary yAxis
                                                                labels: {
                                                                    formatter: function () {
                                                                        return this.value;
                                                                    },
                                                                    style: {
                                                                        color: '#89A54E'
                                                                    }
                                                                },
                                                                title: {
                                                                    text: 'Tours',
                                                                    style: {
                                                                        color: '#89A54E'
                                                                    }
                                                                }
                                                            }, { // Secondary yAxis
                                                                title: {
                                                                    text: 'Total Comitions',
                                                                    style: {
                                                                        color: '#4572A7'
                                                                    }
                                                                },
                                                                labels: {
                                                                    formatter: function () {
                                                                        return this.value;
                                                                    },
                                                                    style: {
                                                                        color: '#4572A7'
                                                                    }
                                                                },
                                                                opposite: true
                                                            }],
                                                            tooltip: {
                                                                shared: true
                                                            },
                                                            legend: {
                                                                layout: 'vertical',
                                                                align: 'left',
                                                                x: 120,
                                                                verticalAlign: 'top',
                                                                y: 100,
                                                                floating: true,
                                                                backgroundColor: '#FFFFFF'
                                                            },
                                                            series: [$scope.shipSerieTotal, $scope.shipSerieTour]
                                                        });
                                                    });

                                                };
                                                $scope.changeShip = function () {
                                                    for (var i = 0; i < $scope.activities.ships.length; i++) {
                                                        if ($scope.activities.ships[i].name == $scope.shipName) {
                                                            $scope.shipSelected = $scope.activities.ships[i];
                                                            $scope.shipName = $scope.shipSelected.name;
                                                            break;
                                                        }
                                                    }
                                                    $scope.shipChart();
                                                }
                                            }
                                            if ($scope.activities.services.length) {
                                                $scope.serviceSelected = $scope.activities.services[0];
                                                $scope.serviceName = $rootScope.languageSelected.min == 'es' ? $scope.serviceSelected.name.es : $scope.serviceSelected.name.en;

                                                $scope.shipService = function () {
                                                    $scope.serviceSerieTotal = {
                                                        name: 'Total Comitions',
                                                        color: '#4572A7',
                                                        type: 'column',
                                                        yAxis: 1,
                                                        data: $scope.serviceSelected.monthArrayTotal,
                                                        tooltip: {
                                                            valueSuffix: ' €'
                                                        }
                                                    }
                                                    $scope.serviceSerieTour = {
                                                        name: 'Services',
                                                        color: '#89A54E',
                                                        type: 'spline',
                                                        data: $scope.serviceSelected.monthArray,
                                                        tooltip: {
                                                            valueSuffix: ' services'
                                                        }
                                                    }
                                                    $(function () {
                                                        $('#containerService').highcharts({
                                                            chart: {
                                                                zoomType: 'xy'
                                                            },
                                                            title: {
                                                                text: $rootScope.texts.page_account_provider_services_commissions
                                                            },
                                                            subtitle: {
                                                                text: $scope.yearNow
                                                            },
                                                            xAxis: {
                                                                categories: $rootScope.languageSelected.min == 'es' ? $scope.monthNameEs : $scope.monthNameEn
                                                            },
                                                            yAxis: [{ // Primary yAxis
                                                                labels: {
                                                                    formatter: function () {
                                                                        return this.value;
                                                                    },
                                                                    style: {
                                                                        color: '#89A54E'
                                                                    }
                                                                },
                                                                title: {
                                                                    text: 'Services',
                                                                    style: {
                                                                        color: '#89A54E'
                                                                    }
                                                                }
                                                            }, { // Secondary yAxis
                                                                title: {
                                                                    text: 'Total Comitions',
                                                                    style: {
                                                                        color: '#4572A7'
                                                                    }
                                                                },
                                                                labels: {
                                                                    formatter: function () {
                                                                        return this.value;
                                                                    },
                                                                    style: {
                                                                        color: '#4572A7'
                                                                    }
                                                                },
                                                                opposite: true
                                                            }],
                                                            tooltip: {
                                                                shared: true
                                                            },
                                                            legend: {
                                                                layout: 'vertical',
                                                                align: 'left',
                                                                x: 120,
                                                                verticalAlign: 'top',
                                                                y: 100,
                                                                floating: true,
                                                                backgroundColor: '#FFFFFF'
                                                            },
                                                            series: [$scope.serviceSerieTotal, $scope.serviceSerieTour]
                                                        });
                                                    });
                                                };
                                                $scope.changeService = function () {
                                                    for (var i = 0; i < $scope.activities.services.length; i++) {
                                                        if ($scope.activities.services[i].name.es == $scope.serviceName || $scope.activities.services[i].name.en == $scope.serviceName) {
                                                            $scope.serviceSelected = $scope.activities.services[i];
                                                            break;
                                                        }
                                                    }
                                                    $scope.shipService();
                                                }

                                            }


                                        }
                                    }
                                )
                            }
                            ;
                            $scope.loadActivities();
                        }

                        $scope.birthday = new Date($scope.user.birthday);
                        $scope.birthday = new Date($scope.birthday.getFullYear(), $scope.birthday.getMonth(), $scope.birthday.getDate());

                        $scope.month = $scope.birthday.getMonth();
                        $scope.year = $scope.birthday.getFullYear();
                        $scope.changeDate();
                        $scope.day = $scope.birthday.getDate();

                        $scope.montoTotal = $scope.user.totalAmount;
                        $scope.actualDate = new Date($scope.user.today.y, $scope.user.today.m, $scope.user.today.d).getTime();
                    }
                    ;


                });
                $scope.changeDate = function () {
                    var bisiesto = ($scope.year % 4 == 0) && (($scope.year % 100 != 0) || ($scope.year % 400 == 0));
                    if ($scope.month == 0 || $scope.month == 2 || $scope.month == 4 || $scope.month == 6 || $scope.month == 7 || $scope.month == 9 || $scope.month == 11) {
                        contructDays(31);
                    }
                    if ($scope.month == 3 || $scope.month == 5 || $scope.month == 8 || $scope.month == 10) {
                        contructDays(30);
                    }
                    if ($scope.month == 1) {
                        if (bisiesto) {
                            contructDays(29);
                        } else {
                            contructDays(28);
                        }
                    }
                };

                function contructDays(cant) {
                    $scope.days = [];
                    for (var i = 1; i <= cant; i++) {
                        $scope.days.push(i);
                    }
                    if ($scope.day == null) {
                        $scope.day = 1;
                    } else {
                        if ($scope.day > cant) {
                            $scope.day = 1;
                        }
                    }
                }

                $rootScope.$on('changeLanguage', function () {
                    angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_profile);
                    $rootScope.seo_description = $rootScope.texts.seo_page_description_profile;
                    try {
                        $scope.shipChart();
                        $scope.shipService();
                    }
                    catch (err) {

                    }

                });
                $scope.formatDate = function (date) {
                    date = new Date(date);
                    return $filter('date')(date, 'dd/MM/yyyy');
                };
                $scope.compareUp = function (actual, expected) {
                    return actual.date >= expected.date;
                };
                $scope.compareDown = function (actual, expected) {
                    return actual.date < expected.date;
                };
                $scope.loadBonds = function () {
                    $http({
                        method: 'POST', url: '/api/groups/listBonds'
                    }).
                        success(function (data) {
                            if (data.res) {
                                if (data.res) {
                                    $scope.abonosList = data.res;
                                }
                                else {
                                    $scope.abonosList = new Array();
                                }
                            }

                        });
                };
                $scope.loadBonds();
                $scope.method = 'paypal';
                $scope.buyBonds = function (bond) {
                    var params = {
                        payFor: 'bonds',
                        method: $scope.method,
                        bond: bond._id || null
                    };
                    var stringParams = JSON.stringify(params);

                    $http({
                        method: 'POST', url: '/api/reservation/save',
                        data: {token: stringParams}
                    }).
                        success(function (data) {
                            if (data.res) {
                                window.location = "/api/reservation/pay/";
                            }

                        });
                };
                $scope.showEditPass = function () {
                    $scope.editPassword = true;
                    $scope.passSuccessShow = false;
                    $scope.oldPass = "";
                    $scope.newPass = "";
                    $scope.newPass1 = "";

                    $scope.validEditPass = function () {
                        var aux = ($scope.oldPass.length > 2 && $scope.newPass.length > 2 && $scope.newPass1.length > 2 && ($scope.newPass == $scope.newPass1));
                        return aux;
                    }
                };

                $scope.isOldPassNULL = function () {
                    if ($scope.oldPass == "") {
                        return true;
                    }
                    return false;
                };

                $scope.isNewPassNULL = function () {
                    if ($scope.newPass == "") {
                        return true;
                    }
                    return false;
                };

                $scope.isConfirmPassNULL = function () {
                    if ($scope.newPass1 == "") {
                        return true;
                    }
                    return false;
                };

                $scope.changePassUsersProfile = function () {
                    if ($scope.validEditPass) {
                        Profile.changePass({
                                oldPass: $scope.oldPass,
                                newPass: $scope.newPass
                            }
                            , function (data) {
                                if (data.res) {
                                    $scope.passSuccessShow = true;
                                }
                                else {
                                    $scope.passError = data.error;
                                }
                            })
                    }
                };
                $scope.changeLetEdit = function (val) {
                    $scope.letEdit = val;
                    $scope.showSaveError = false;
                    $scope.showSaveSuccess = false;
                    if (!val) {
                        $scope.passSuccessShow = false;
                        $scope.showSaveError = false;
                        $scope.showSaveSuccess = false;
                        $scope.letEdit = false;
                    }

                    $scope.validEditProfile = function () {
                        var aux = (
                        $scope.user.userName.length > 2 &&
                        $scope.user.lastName.length > 2 &&
                        $scope.emailValidator() &&
//                    $scope.phoneValidator() &&
                            //$scope.user.enterprise.length > 2 &&
                        $scope.user.address.length > 2 &&
                        $scope.user.city.length > 2 &&
                            //$scope.postaCodeValidator() &&
                        $scope.user.country.length > 2
                        );
                        return aux;
                    }

                };
                $scope.updateProfile = function () {
                    if ($scope.validEditProfile()) {
                        Profile.update({
                            userName: $scope.user.userName,
                            lastName: $scope.user.lastName,
                            sex: $scope.user.sex,
                            birthday: new Date($scope.year, $scope.month, $scope.day),
                            email: $scope.user.email,
                            phoneNumber: $scope.user.phoneNumber,
                            enterprise: $scope.user.enterprise,
                            address: $scope.user.address,
                            city: $scope.user.city,
                            postalCode: $scope.user.postalCode,
                            country: $scope.user.country,
                            subscription: $scope.user.subscription

                        }, function (data) {
                            if (data.res) {
                                $rootScope.updateSession();
                                $scope.showSaveSuccess = true;
                                $scope.letEdit = false;
                                $scope.birthday = new Date($scope.year, $scope.month, $scope.day);
                            }
                            else {
                                $scope.showSaveError = true;
                            }
                        })
                    }
                };
                $scope.showCalendar = function () {
                    $('.dropdown-calendar').slideDown(200);
                };
                $scope.fadeCalendar = function () {
                    $('.dropdown-calendar').slideUp(200);
                };
                $scope.SelectedDay = function () {
                    $scope.birthday = new Date($scope.birthday.getFullYear(), $scope.birthday.getMonth(), $scope.birthday.getDate());
                    $scope.fadeCalendar();
                };
                $scope.emailValidator = function () {
                    try {
                        if (!(/\w{1,}[@][\w\-]{1,}([.]([\w\-]{1,})){1,3}$/.test($scope.user.email))) {
                            return false;
                        }
                        else {
                            return true;
                        }
                    } catch (e) {
                        return false;
                    }

                };
                $scope.phoneValidator = function () {
                    if (!(/^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/.test($scope.user.phoneNumber))) {
                        return false;
                    }
                    else {
                        return true;
                    }
                };
                $scope.postaCodeValidator = function (postalCode) {
                    if (!(/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/.test(postalCode))) {
                        return false;
                    }
                    else {
                        return true;
                    }
                };
                $scope.changeYear = function (index) {
                    $scope.yearNow = $scope.yearNow + index;
                    $scope.loadActivities();
                }
            }
            else{
                $location.path('/' + $cookies.languageSelected + '/login');
            }


            function getTotalShip(name, month) {
                for (var i = 0; i < $scope.activities.ships; i++) {
                    if ($scope.activities.ships[i].name == name) {
                        return $scope.activities.ships[i].monthArrayTotal[month];
                    }
                }
            }

            $location.hash('page');
            $anchorScroll();
            $location.hash("");
        }])
;
