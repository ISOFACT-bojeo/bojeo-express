/**
 * Created by ernestomr87@gmail.com on 4/06/14.
 */

angular.module('bojeoApp.RouteController', [])
    .controller('RouteController', ['$scope', '$rootScope', '$location', '$routeParams', '$routeProvider',
        function ($scope, $rootScope, $location, $routeParams, $routeProvider) {
            $scope.lang = $routeParams.lang;
            $scope.page = $routeParams.page;

            if ($scope.lang == 'es') {

                $routeProvider.when('/es', {templateUrl: '/frontend/index', controller: 'IndexController'});
            }
            else {
                $routeProvider.when('/en', {templateUrl: '/frontend/index', controller: 'IndexController'});
            }


        }]);

''