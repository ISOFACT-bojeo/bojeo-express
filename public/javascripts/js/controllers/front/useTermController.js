/**
 * Created by ernestomr87@gmail.com on 19/06/14.
 */

angular.module('bojeoApp.useTermController', [])
    .controller('useTermController', ['$scope', '$rootScope',
        function ($scope, $rootScope) {
            $rootScope.PageSelected = 'terms-of-use';

            angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_payment_methods);
            $rootScope.seo_description = $rootScope.texts.seo_page_description_payment_methods;

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });


            if (!$rootScope.useTerms) {
                $rootScope.getInfo('useTerms',function(data){
                    $rootScope.useTerms = data;
                });
            }


        }]);
