/**
 * Created by ernestomr87@gmail.com on 4/06/14.
 */

angular.module('bojeoApp.EnterpriseController', [])
    .controller('EnterpriseController', ['$scope', '$rootScope', 'Enterprise',
        function ($scope, $rootScope, Enterprise) {
            $rootScope.PageSelected = 'enterprise';

            angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_partners);
            $rootScope.seo_description = $rootScope.texts.seo_page_description_partners;

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });


            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;
            $scope.loadEnterprise = function () {
                Enterprise.get({limit: $scope.maxSize, skip: $scope.skip}, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.cont;
                        $scope.enterprises = data.enterprises;
                        if(data.cont == 0){
                            $scope.empty = true;
                        }
                        if ($scope.enterprises.length > 0) {
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadEnterprise();
                            }
                        }
                    } else {
                        $scope.enterprise = new Array();
                        $scope.empty = true;
                        $rootScope.Notifications('Ha ocurrido un error al obtener las empresas.', 2);
                    }
                });
            };
            $scope.loadEnterprise();
            $scope.pageChanged = function () {
                $scope.skip = ($scope.currentPage - 1) * $scope.maxSize;
                $scope.loadEnterprise();
            };

        }]);

