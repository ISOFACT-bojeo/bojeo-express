/**
 * Created by ernestomr87@gmail.com on 4/06/14.
 */

angular.module('bojeoApp.ReturnController', [])
    .controller('ReturnController', ['$scope', '$rootScope', '$http', '$routeParams', '$anchorScroll', '$location',
        function ($scope, $rootScope, $http, $routeParams, $anchorScroll, $location) {
            $rootScope.PageSelected = 'refund';

            $scope.reservation = {};
            $scope.tour = {};
            $scope.showReturnForm = false;
            $scope.showCompleteCancel = false;
            $scope.reasonText = "";


            angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_bookings_refunds);
            $rootScope.seo_description = $rootScope.texts.seo_page_description_bookings_refunds;

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });


            if (!$rootScope.devolutions) {
                $rootScope.getInfo('devolutions',function(data){
                    $rootScope.devolutions = data;
                });
            }

            if ($routeParams.returnId) {
                if($rootScope.user){
                    $scope.showReturnForm = true;
                    $http({method: 'GET', url: '/api/reservation/' + $routeParams.returnId}).
                        success(function (data) {
                            if (data.res) {
                                $scope.reservation = data.res;
                                if ($scope.reservation.cancel) {
                                    $location.path('/' + $cookies.languageSelected + '/index');
                                }
                            }
                            $location.hash('return-form');
                            $anchorScroll();
                            $location.hash("");

                        });
                }
                else{
                    $location.path('/' + $cookies.languageSelected + '/login');
                }


            }
            else {
                $location.hash('return-page');
                $anchorScroll();
                $location.hash("");
            }

            $scope.hasError = false;
            $scope.sendDevolution = function () {
                var token = $scope.reservation.payment.token;
                var reason = $scope.reasonText;
                $scope.hasError = false;
                $scope.executeDevolution(token, reason, function (data) {
                    if (data.res == true) {
                        $scope.showReturnForm = false;
                        $scope.showCompleteCancel = true;
                    }
                    else {
                        $scope.hasError = true;
                        $scope.reasonText = "";
                    }
                })

            }
            $scope.executeDevolution = function (token, reason, cbk) {
                var data = {
                    token: token,
                    reason: reason
                }
                $http({method: 'POST', url: '/api/reservation/cancel', data: data}).success(
                    function (data) {
                        if (cbk) cbk(data);
                    });
            }

        }])
;

