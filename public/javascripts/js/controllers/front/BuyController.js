/**
 * Created by ernestomr87@gmail.com on 4/06/14.
 */

angular.module('bojeoApp.BuyController', [])
    .controller('BuyController', ['$scope', '$rootScope', '$routeParams', 'Tours', 'Ports', 'Ships', '$location', '$filter', '$http', '$window', '$filter', 'Service', '$timeout', '$cookies',
        function ($scope, $rootScope, $routeParams, Tours, Ports, Ships, $location, $filter, $http, $window, $filter, Service, $timeout, $cookies) {
            $rootScope.PageSelected = 'buy';

            $rootScope.$on('changeLanguage', function (event, data) {
                $rootScope.updatePage(data);
            });

            if ($rootScope.user == null) {
                //$location.path('/login')
                $location.path('/' + $cookies.languageSelected + '/login');
            }

            $http({
                method: 'POST', url: '/api/reservation/getBookingInfo'
            }).
                success(function (data) {
                    if (data.res) {
                        $scope.params = data.res;

                        Tours.get({id: $scope.params.slug}, function (data) {
                            if (data.res) {
                                $scope.tour = data.res;
                                $scope.photo = $scope.tour.photos[0].path;
                                $scope.ship = $scope.params.ship;
                                $scope.cantPerson = $scope.params.cantPerson;
                                $scope.monto = $scope.params.monto;
                                $scope.date = $scope.params.date;
                                $scope.days = $scope.params.days ? $scope.params.days : 1;
                                $scope.packColor = $scope.params.color || '';

                                $scope.tourMonto = $scope.params.tourMonto;
                                $rootScope.getWeader($scope.date);
                                $scope.price = $scope.params.price;
                                $scope.departure = $scope.params.departure;
                                $scope.duration = $scope.params.duration;


                                if ($scope.tour.data.payForm == 'bonds') {
                                    $scope.hour = $scope.params.hours;
                                }
                                var pack = $scope.params.pack;
                                if (pack) {
                                    Service.get({id: pack}, function (serv) {
                                        $scope.services = serv.res;
                                        $scope.pack = {
                                            services: $scope.services
                                        }
                                    })
                                }

                                $http({
                                    method: 'POST',
                                    url: '/api/reservation/tpvForm',
                                    data: {
                                        price: $scope.monto,
                                        description: $scope.tour.name[$rootScope.languageSelected.min]
                                    }
                                }).success(function (data) {
                                    if (data.res) {
                                        $scope.urlTPV = data.res.URL;
                                        delete data.res.URL;
                                        $scope.formTpv = data.res;
                                    }
                                });
                            }
                        })

                    }
                    else {
                        //$location.path('/')
                        $location.path('/' + $cookies.languageSelected);
                    }

                });

            $scope.SendConfirm = function (type) {

                $scope.params.method = type ? 'redsys' :  'paypal';
                if ($rootScope.user.complete) {
                    if ($scope.params.services) {
                        $scope.params.services = null;
                    }

                    var stringParams = JSON.stringify($scope.params);
                    $http({
                        method: 'POST', url: '/api/reservation/save',
                        data: {token: stringParams}
                    }).
                        success(function (data) {
                            if (data.res) {
                                if (type == 0) {
                                    $window.location = "/api/reservation/pay/";
                                }
                                else {
                                    angular.element('#tpvForm').submit();
                                }
                            }

                        });
                }
                else {
                    //$location.path('/profile/complete');
                    $location.path('/' + $cookies.languageSelected + '/profile/complete');
                }

            }

            $scope.changePhoto = function (path, tour) {
                angular.element('#animateImg').fadeTo(200, 'toggle');

                $timeout(function () {
                    $scope.photo = path;
                    angular.element('#animateImg').fadeIn();
                }, 200);


                $scope.photoDescription = tour || null;
            }

        }]);
