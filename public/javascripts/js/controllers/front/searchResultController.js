/**
 * Created by ernestomr87@gmail.com on 19/06/14.
 */

angular.module('bojeoApp.searchResultController', [])
    .controller('searchResultController', ['$scope', '$rootScope', '$http', '$routeParams', '$location', '$anchorScroll',
        function ($scope, $rootScope, $http, $routeParams, $location, $anchorScroll) {

            $rootScope.PageSelected = 'search';

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });
            
            $scope.error = {
                msg: null,
                visible: false
            };
            $scope.showSumary = false;
            $rootScope.search = function () {
                $http({
                    method: 'POST', url: '/search', data: {
                        text: $scope.searchText,
                        lang: $rootScope.languageSelected.min
                    }
                }).
                    success(function (data) {
                        if (data.res) {
                            $scope.result = data.res;
                            $scope.showSumary = true;
                        }
                        else {
                            $scope.result = new Array();
                        }
                    });
            };
            $scope.reduceText = function (text) {
                var lengthText = 600;
                if (text.length <= lengthText) {
                    return text;
                }
                else {
                    var aux = "", newText = "";
                    for (var i = 0; i < lengthText; i++) {
                        aux = aux + text[i];
                    }

                    var last = aux.lastIndexOf(' ');

                    for (var i = 0; i < last; i++) {
                        newText = newText + aux[i];
                    }
                    newText = newText + "...";

                    var txt = new RegExp($scope.searchText, "ig");
                    newText = newText.replace(txt, '<strong>' + $scope.searchText + '</strong>');
                    return newText;
                }
            };

            $scope.changeSearchText = function () {
                $rootScope.textForSearch = $scope.searchText;
            };

            if ($routeParams.text == null) {
                $scope.searchText = "";
            }
            else {
                $scope.searchText = $routeParams.text;
                $rootScope.search();
            }
            ;



        }]);
