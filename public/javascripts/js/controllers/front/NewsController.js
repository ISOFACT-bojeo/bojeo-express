angular.module('bojeoApp.NewsController', [])
    .controller('NewsController', ['$scope', '$rootScope', 'News', 'Statistics','$location','$anchorScroll',
        function ($scope, $rootScope, News, Statistics,$location,$anchorScroll) {
            $rootScope.PageSelected = 'news';
            $rootScope.showMenuWindowForm = false;

            angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_news);
            $rootScope.seo_description = $rootScope.texts.seo_page_description_news;

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });

            $scope.currentPage = 1;
            $scope.maxSize = 10;
            $scope.skip = 0;
            $scope.text = "";

            $scope.loadNews = function () {
                $scope.empty = true;
                News.query({limit: $scope.maxSize, skip: $scope.skip, text: $scope.text}, function (data) {
                    if (data.res) {
                        $scope.totalItems = data.cont;
                        $scope.news = data.res;
                        if (data.cont == 0) {
                            $scope.empty = true;
                        }
                        if ($scope.news.length > 0) {
                            $scope.empty = false;
                        } else {
                            if ($scope.currentPage > 1) {
                                $scope.currentPage--;
                                $scope.skip = $scope.skip - $scope.maxSize;
                                $scope.loadNews();
                            }
                        }
                    } else {
                        $scope.news = new Array();
                        $scope.empty = true;
                    }
                });
            };
            $scope.loadNews();


        }]);
