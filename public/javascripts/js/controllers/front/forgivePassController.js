/**
 * Created by ernestomr87@gmail.com on 4/06/14.
 */

angular.module('bojeoApp.forgivePassController', [])
    .controller('forgivePassController', ['$scope', '$rootScope','$http',
        function ($scope, $rootScope,$http) {
            $scope.forgiveEmail = null;

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });

            $scope.SendNewPass = function () {
                $http({method: 'POST', url: ' /forgivePass', data: {email: $scope.forgiveEmail}}).
                    success(function (data) {
                        if (data.res) {
                            $rootScope.login_success = true;
                        }
                        else {
                            $rootScope.login_error = true;
                        }
                    });
            };
        }]);
