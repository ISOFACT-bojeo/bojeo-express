/**
 * Created by ernestomr87@gmail.com on 21/10/2014.
 */

angular.module('bojeoApp.PoilerToursController', [])
    .controller('PoilerToursController', ['$scope', '$location',
        function ($scope, $location) {
            $scope.naomi = { name: 'Naomi', address: '1600 Amphitheatre' };
            $scope.igor = { name: 'Igor', address: '123 Somewhere' };

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });

            $scope.redirectToTour = function (id) {
                $location.path('/' + $cookies.languageSelected + '/tour/' + id);
            }

            $scope.redirectToGroup = function (index) {
                $location.path('/' + $cookies.languageSelected + '/groups/' + (index + 1));
            }

        }]);