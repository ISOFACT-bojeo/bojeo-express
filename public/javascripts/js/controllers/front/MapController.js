/**
 * Created by ernestomr87@gmail.com on 4/06/14.
 */

angular.module('bojeoApp.MapController', [])
    .controller('MapController', ['$scope', '$rootScope', '$http',
        function ($scope, $rootScope, $http) {
            $rootScope.PageSelected = 'map';

            angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_site_map);
            $rootScope.seo_description = $rootScope.texts.seo_page_description_site_map;

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });


        }]);

