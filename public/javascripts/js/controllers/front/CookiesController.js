/**
 * Created by jeandy.bryan@isofact.com on 11/25/2014.
 */
angular.module('bojeoApp.CookiesController', [])
    .controller('CookiesController', ['$scope', '$rootScope',
        function ($scope, $rootScope) {

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });

            if (!$rootScope.politicCookies) {
                $rootScope.getInfo('politicCookies',function(data){
                    $rootScope.politicCookies = data;
                });
            }
            else{
                eventCookies();
            }

            $scope.reduceText = function (text) {
                var removeHtmlRegExp = new RegExp("<[^<]+?>", "gi");
                text = text.replace(removeHtmlRegExp, "");
                text = NormalizeString(text);
                var lengthText = 250;
                if (text.length <= lengthText) {
                    return text;
                }
                else {
                    var aux = "", newText = "";
                    for (var i = 0; i < lengthText; i++) {
                        aux = aux + text[i];
                    }

                    var last = aux.lastIndexOf(' ');

                    for (var i = 0; i < last; i++) {
                        newText = newText + aux[i];
                    }
                    newText = newText + "...";


                    return newText;
                }
            };
            function NormalizeString(r) {
                r = r.replace(new RegExp("&aacute;"), "á");
                r = r.replace(new RegExp("&eacute;"), "é");
                r = r.replace(new RegExp("&iacute;"), "í");
                r = r.replace(new RegExp("&oacute;"), "ó");
                r = r.replace(new RegExp("&uacute;"), "ú");
                /*******************************************/
                r = r.replace(new RegExp("&aacute;"), "á");
                r = r.replace(new RegExp("&eacute;"), "é");
                r = r.replace(new RegExp("&iacute;"), "í");
                r = r.replace(new RegExp("&oacute;"), "ó");
                r = r.replace(new RegExp("&uacute;"), "ú");
                return r;
            }

            function eventCookies() {
                angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.cookies_policy_title);
                $rootScope.seo_description = $rootScope.languageSelected.min == 'es' ? $scope.reduceText($rootScope.politicCookies.es)
                    : $scope.reduceText($rootScope.politicCookies.en);

                $rootScope.$on('changeLanguage', function () {
                    angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.cookies_policy_title);
                    $rootScope.seo_description = $rootScope.languageSelected.min == 'es' ? $scope.reduceText($rootScope.politicCookies.es)
                        : $scope.reduceText($rootScope.politicCookies.en);
                });
            }


        }]);