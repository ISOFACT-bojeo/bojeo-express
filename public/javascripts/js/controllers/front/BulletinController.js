/**
 * Created by ernestomr87@gmail.com on 4/06/14.
 */

angular.module('bojeoApp.BulletinController', [])
    .controller('BulletinController', ['$scope', '$rootScope', '$http', 'User', '$location', '$anchorScroll',
        function ($scope, $rootScope, $http, User, $location, $anchorScroll) {

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });

            $rootScope.PageSelected = 'bulletin';

            angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_news_bulletin);
            $rootScope.seo_description = $rootScope.texts.seo_page_description_news_bulletin;

            $rootScope.$on('changeLanguage', function () {
                angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_news_bulletin);
                $rootScope.seo_description = $rootScope.texts.seo_page_description_news_bulletin;
            });


            if ($rootScope.user != null) {
                User.query({id: $rootScope.user._id}, function (data) {
                    $scope.suscription = data[0].subscription;
                })
            }

            $scope.changeSuscription = function () {
                $http({
                    method: 'PUT',
                    url: '/changeSuscription',
                    data: {id: $rootScope.user._id, subscription: $scope.suscription}
                }).
                    success(function (data, status, headers, config) {

                    }).
                    error(function (data, status, headers, config) {

                    });
            };

            $scope.starSession = function () {
                if ($rootScope.user == null) {
                    $rootScope.nextStep = {
                        msg: '',
                        path: '/bulletin/'
                    };
                    //$location.path('/login');
                    $location.path('/' + $cookies.languageSelected + '/login');
                }

            };
        }]);
