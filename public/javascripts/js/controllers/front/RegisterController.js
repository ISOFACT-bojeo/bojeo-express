/**
 * Created by ernestomr87@gmail.com on 4/06/14.
 */

angular.module('bojeoApp.RegisterController', [])
    .controller('RegisterController', ['$scope', '$rootScope', 'Users', '$location', '$anchorScroll',
        function ($scope, $rootScope, Users, $location, $anchorScroll) {

            $rootScope.PageSelected = 'register';

            angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_register);
            $rootScope.seo_description = $rootScope.texts.seo_page_description_register;

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });


            if ($location.path() == '/register' && $rootScope.user != null) {
                $location.path('/' + $cookies.languageSelected + '/index');
            }


            $scope.initializeUser = function () {
                $scope.msgError = false;
                var date = new Date();
                $scope.userName = "";
                $scope.lastName = "";
                $scope.sex = "male";
                $scope.birthday = date.getTime();
                $scope.email = "";
                $scope.phoneNumber = "";
                $scope.enterprise = "";
                $scope.address = "";
                $scope.city = "";
                $scope.postalCode = "";
                $scope.country = "";
                $scope.role = "user";
                $scope.subscription = "";
                $scope.password = "";
                $scope.password1 = "";

                $scope.years = [];
                for (var i = 1900; i < date.getFullYear(); i++) {
                    $scope.years.push(i);
                }
                $scope.year = date.getFullYear() - 30;
                $scope.month = 0;
                $scope.dt = new Date($scope.birthday);
                contructDays(31);
            };

            $scope.changeDate = function () {
                var bisiesto = ($scope.year % 4 == 0) && (($scope.year % 100 != 0) || ($scope.year % 400 == 0));
                if ($scope.month == 0 || $scope.month == 2 || $scope.month == 4 || $scope.month == 6 || $scope.month == 7 || $scope.month == 9 || $scope.month == 11) {
                    contructDays(31);
                }
                if ($scope.month == 3 || $scope.month == 5 || $scope.month == 8 || $scope.month == 10) {
                    contructDays(30);
                }
                if ($scope.month == 1) {
                    if (bisiesto) {
                        contructDays(29);
                    } else {
                        contructDays(28);
                    }
                }
            };

            function contructDays(cant) {
                $scope.days = [];
                for (var i = 1; i <= cant; i++) {
                    $scope.days.push(i);
                }
                if ($scope.day == null) {
                    $scope.day = 1;
                } else {
                    if ($scope.day > cant) {
                        $scope.day = 1;
                    }
                }
            }


            $scope.initializeUser();
            $scope.showCalendar = function () {
                $('.dropdown-calendar').slideDown(200);
            };
            $scope.fadeCalendar = function () {
                $('.dropdown-calendar').slideUp(200);
            };
            $rootScope.SelectedDay = function () {
                $scope.dt = new Date($scope.dt.getFullYear(), $scope.dt.getMonth(), $scope.dt.getDate());
                $scope.fadeCalendar();
            };

            $scope.emailValidator = function () {
                if (!(/\w{1,}[@][\w\-]{1,}([.]([\w\-]{1,})){1,3}$/.test($scope.email))) {
                    return false;
                }
                else {
                    return true;
                }
            };
            $scope.phoneValidator = function () {
                if (!(/^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/.test($scope.phoneNumber))) {
                    return false;
                }
                else {
                    return true;
                }
            };
            $scope.create = function () {
                if ($scope.validateCreateForm()) {
                    Users.create({
                        userName: $scope.userName,
                        lastName: $scope.lastName,
                        email: $scope.email,
                        sex: $scope.sex,
                        birthday: new Date($scope.year, $scope.month, $scope.day),
                        phoneNumber: $scope.phoneNumber,
                        enterprise: $scope.enterprise,
                        address: $scope.address,
                        city: $scope.city,
                        postalCode: $scope.postalCode,
                        country: $scope.country,
                        role: $scope.role,
                        password: $scope.password,
                        language: $rootScope.languageSelected.min
                    }, function (data) {
                        if (data.res) {
                            $scope.registerForm.$setPristine();
                            $scope.initializeUser();
                            showActionStatus(true);
                        }
                        else {
                            $scope.msgError = true;
                            showActionStatus(false, data.error);
                        }
                    })
                } else {
                    showActionStatus(false, 0);
                }

            };

            function showActionStatus(flag, codeError) {
                if (flag == true) {
                    $scope.showNoty = true;
                    $scope.notyColor = 'green';
                    $scope.notyText = $rootScope.texts.page_account_register_notification_ok + " " + $rootScope.texts.page_account_password_notification_email_sent;
                    setTimeout(function () {
                        window.location = '/#/index';
                    }, 5000);
                } else {
                    if (codeError == 0) {
                        $scope.showNoty = true;
                        $scope.notyColor = 'red';
                        $scope.notyText = $rootScope.texts.page_account_register_notification_error;
                    } else {
                        if (codeError == 1) {
                            $scope.showNoty = true;
                            $scope.notyColor = 'red';
                            $scope.notyText = $rootScope.texts.page_account_register_notification_email_in_use;
                        } else {
                            $scope.showNoty = true;
                            $scope.notyColor = 'red';
                            $scope.notyText = $rootScope.texts.page_account_register_confirmation_error;
                        }
                    }
                }
            };

            $scope.validateCreateForm = function () {
                var aux = (
                $scope.password == $scope.password1 &&
                $scope.password.length >= 6 &&
                $scope.password1.length >= 6 &&
                $scope.userName.length > 0 &&
                $scope.lastName.length > 0 &&
                $scope.emailValidator() &&
                $scope.address.length > 0 &&
                $scope.city.length > 0 &&
                $scope.country.length > 0
                );
                return aux;
            };


        }]);

