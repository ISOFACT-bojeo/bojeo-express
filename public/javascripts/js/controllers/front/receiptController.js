/**
 * Created by ernestomr87@gmail.com on 4/06/14.
 */

angular.module('bojeoApp.receiptController', [])
    .controller('receiptController', ['$scope', '$rootScope', '$location', 'ToursReserv', 'Analytics', '$cookies', '$anchorScroll',
        function ($scope, $rootScope, $location, ToursReserv, Analytics, $cookies, $anchorScroll) {
            $rootScope.PageSelected = 'receipt';

            $rootScope.$on('changeLanguage', function (event, data) {
                $rootScope.updatePage(data);
            });

            if ($rootScope.user) {
                $scope.loadReceipt = function () {
                    ToursReserv.receipt.get(function (data) {

                        if (data.res) {
                            $scope.tour = data.res;
                            $rootScope.getWeader($scope.tour.bodySale.date, $scope.tour.bodySale.tour.port.city.en, $scope.tour.bodySale.tour.port.country.en);
                            /*
                             '1234',           // transaction ID - required
                             'Acme Clothing',  // affiliation or store name
                             '11.99',          // total - required
                             '1.29',           // tax
                             '5',              // shipping
                             'San Jose',       // city
                             'California',     // state or province
                             'USA'             // country

                             */
                            Analytics.addTrans(
                                $scope.tour._id,
                                '',
                                $scope.tour.bodySale.monto,
                                '',
                                '',
                                $scope.tour.bodySale.tour.port.city.es,
                                '',
                                $scope.tour.bodySale.tour.port.country.es);

                            // - add items to transaction
                            /*
                             '1234',           // transaction ID - required
                             'DD44',           // SKU/code - required
                             'T-Shirt',        // product name
                             'Green Medium',   // category or variation
                             '11.99',          // unit price - required
                             '1'               // quantity - required
                             */
                            Analytics.addItem(
                                $scope.tour._id,
                                $scope.tour.bodySale.tour._id,
                                $scope.tour.bodySale.tour.name.es,
                                $scope.tour.bodySale.tour.group.name.es,
                                $scope.tour.bodySale.tourMonto,
                                '1');

                            if ($scope.tour.bodySale.pack) {
                                for (var i = 0; i < $scope.tour.bodySale.pack.services.length; i++) {
                                    Analytics.addItem(
                                        $scope.tour._id,
                                        $scope.tour.bodySale.pack.services[0]._id,
                                        $scope.tour.bodySale.pack.services[0].name.es,
                                        $scope.tour.bodySale.pack.services[0].type,
                                        $scope.tour.bodySale.pack.services[0].price,
                                        '1'
                                    );
                                }
                            }

                            // - complete transaction
                            Analytics.trackTrans();
                        }
                        else {
                            $location.path('/' + $cookies.languageSelected + '/');
                        }
                    })
                }
                $scope.loadReceipt();
            }
            else {
                $location.path('/' + $cookies.languageSelected + '/');
            }
            $location.hash('page');
            $anchorScroll();
            $location.hash("");

        }]);

