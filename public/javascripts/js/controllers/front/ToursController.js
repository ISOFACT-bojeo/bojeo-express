/**
 * Created by ernestomr87@gmail.com on 18/06/14.
 */

angular.module('bojeoApp.ToursController', [])
    .controller('ToursController', ['$scope', '$rootScope', '$routeParams', '$http',
        '$location', '$anchorScroll', 'Groups', '$cookies', 'ToursGroup',
        function ($scope, $rootScope, $routeParams, $http, $location, $anchorScroll, Groups, $cookies, ToursGroup) {

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage("/"+$rootScope.languageSelected.min+"/"+$scope.group.slug[$rootScope.languageSelected.min]);
            });

            $rootScope.changePage('toursEx');
            if (!$cookies.id_port) {
                $location.path('/' + $cookies.languageSelected + '/index');
            }




            $scope.loadGTours = function () {
                ToursGroup.query({group: $routeParams.group, port: $cookies.id_port}, function (data) {
                    if (data.res.length) {

                        $scope.toursGroup = data.res;

                        /*if ($scope.toursGroup.length == 1) {
                            $location.path('/' + $cookies.languageSelected + '/tour/' + $scope.toursGroup[0]._id);
                        }*/
                    }
                    else {
                        $location.path('/' + $cookies.languageSelected + '/404');
                    }
                })
            }
            $scope.loadGTours();

            Groups.get({group: $routeParams.group}, function (data) {
                if (data.res) {
                    $scope.group =sortTours(data.res);

                    var cleanUrl =  $location.absUrl().replace($location.url(),'');
                    $rootScope.myLocation = {
                        es:  cleanUrl + "/es/" + $scope.group.slug.es,
                        en:  cleanUrl + "/en/" + $scope.group.slug.en
                    }

                    if(!$scope.group.seo){
                        angular.element("#bojeo_title").text('Undefine');
                        $rootScope.seo_description = 'Undefine';

                    }
                    else{
                        angular.element("#bojeo_title").text($scope.group.seo.title[$rootScope.languageSelected.min]);
                        $rootScope.seo_description = $scope.group.seo.description[$rootScope.languageSelected.min];

                    }


                }




            });


            //update page pos


            $scope.reduceText = function (text) {
                //var myString:String = "<p><b>bold</b> <i>italic</i> <a href='#'>link</a> <br>linebreak</p>";
                //trace(myString)
                var removeHtmlRegExp = new RegExp("<[^<]+?>", "gi");
                text = text.replace(removeHtmlRegExp, "");
                text = NormalizeString(text);
                var lengthText = 250;
                if (text.length <= lengthText) {
                    return text;
                }
                else {
                    var aux = "", newText = "";
                    for (var i = 0; i < lengthText; i++) {
                        aux = aux + text[i];
                    }

                    var last = aux.lastIndexOf(' ');

                    for (var i = 0; i < last; i++) {
                        newText = newText + aux[i];
                    }
                    newText = newText + "...";


                    return newText;
                }
            };
            function NormalizeString(r) {
                r = r.replace(new RegExp("&aacute;"), "á");
                r = r.replace(new RegExp("&eacute;"), "é");
                r = r.replace(new RegExp("&iacute;"), "í");
                r = r.replace(new RegExp("&oacute;"), "ó");
                r = r.replace(new RegExp("&uacute;"), "ú");
                /*******************************************/
                r = r.replace(new RegExp("&aacute;"), "á");
                r = r.replace(new RegExp("&eacute;"), "é");
                r = r.replace(new RegExp("&iacute;"), "í");
                r = r.replace(new RegExp("&oacute;"), "ó");
                r = r.replace(new RegExp("&uacute;"), "ú");
                return r;
            }

            function sortTours(listTours) {
                for (var i = 0; i < listTours.length - 1; i++) {
                    for (var j = i + 1; j < listTours.length; j++) {
                        if (listTours[i].price.price > listTours[j].price.price) {
                            var aux = listTours[i];
                            listTours[i] = listTours[j];
                            listTours[j] = aux;
                        }
                    }
                }
                return listTours;
            };


            $location.hash('page');
            $anchorScroll();
            $location.hash("");
        }]);

