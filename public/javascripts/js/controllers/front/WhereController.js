/**
 * Created by ernestomr87@gmail.com on 4/06/14.
 */
var map;
angular.module('bojeoApp.WhereController', [])
    .controller('WhereController', ['User', '$scope', '$rootScope', '$http', '$location', '$anchorScroll','$timeout',
        function (User, $scope, $rootScope, $http, $location, $anchorScroll,$timeout) {
            $rootScope.PageSelected = 'contact';


            $scope.fullName = "";
            $scope.email = "";
            $scope.tel = "";
            $scope.company = "";
            $scope.comment = "";
            $scope.city = "";
            $scope.country = "";


            $scope.showSucces = false;
            $scope.showError = false;
            $scope.thisUser = null;


            angular.element("#bojeo_title").text('Bojeo - ' + $rootScope.texts.seo_page_name_contact);
            $rootScope.seo_description = $rootScope.texts.seo_page_description_contact;

            $rootScope.$on('changeLanguage', function (event,data) {
                $rootScope.updatePage(data);
            });


            $http({method: 'GET', url: ' /api/users/myprofile'}).
                success(function (data) {

                    if (data.res) {
                        var profile = data.res;
                        if ($rootScope.user != null) {
                            $scope.fullName = profile.userName + " " + profile.lastName;
                            $scope.email = profile.email;
                            $scope.city = profile.city;
                            $scope.country = profile.country;
                            $scope.tel = Number(profile.phoneNumber);
                            $scope.company = profile.enterprise;
                        }
                    }
                });

            $scope.sendContact = function () {
                $scope.showSucces = false;
                $scope.showError = false;
                $http({
                    method: 'POST', url: '/api/contacts',
                    data: {
                        fullName: $scope.fullName,
                        email: $scope.email,
                        city: $scope.city,
                        country: $scope.country,
                        tel: $scope.tel,
                        company: $scope.company,
                        comment: $scope.comment
                    }
                }).
                    success(function (data) {
                        if (data.res) {
                            $scope.contacForm.$setPristine();
                            $scope.comment = "";
                            $scope.showSucces = true;
                            $timeout(function () {
                                $scope.showSucces = false;
                            }, 5000);
                        }
                        else {
                            $scope.showError = true;
                            $timeout(function () {
                                $scope.showError = false;
                            }, 3000);
                        }
                    });
            };


            /*$scope.mark = {
                position: {
                    latitude: $rootScope.selectedPort.location.x,
                    longitude: $rootScope.selectedPort.location.y
                }
            }

            $scope.map = {
                draggable: "true",
                draging: "true",
                center: {
                    latitude: $rootScope.selectedPort.location.x,
                    longitude: $rootScope.selectedPort.location.y
                },
                zoom: 16,
                events: {
                    tilesloaded: function (map) {
                        $scope.$apply(function () {

                        });
                    }
                }
            };*/




        }]);

