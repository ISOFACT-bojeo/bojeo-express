var express = require('express');

var app = express();

// config app
require('./config/bojeoConf');



require('./config/mongoose').connect(function(){
    require('./config/express').configExpress(app,db.mongoose);
    require('./config/passport').configPassport(app);
    require('./config/routes').configRoutes(app);

    /// catch 404 and forwarding to error handler
    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });
});

module.exports = app;




